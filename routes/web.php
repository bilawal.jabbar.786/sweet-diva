<?php

use App\GeneralSettings;
use App\Http\Helpers\VideoStream;
use App\Mail\Facture;
use App\Order;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;

Route::get('/save-token/{token}',function ($token){
     if(Auth::user()) {
         Auth::user()->update(['device_token' => $token]);
         return redirect()->route('app.index');
     }
     else{
         return redirect()->route('app.explore');
     }
});
Route::get('/user/contact', 'User\UserController@userContact')->name('user.contact');
Route::post('/user/contact/store', 'User\UserController@userContactStore')->name('user.contact.store');
Route::get('/testnotification', 'HomeController@testnotification');

Route::get('/app/explore','App\AppController@index')->name('app.explore');

Route::get('/email', function (){
    $order = Order::find(1);
    $gs = GeneralSettings::find(1);
    dispatch(new App\Jobs\SendEmailJob($order, $gs, 'records/facture-ON-195058293.pdf'))->delay(\Carbon\Carbon::now()->addSeconds(5));
});

Route::get('/video', function (){
    $video_path = '1659846988img.mov';
    $stream = new VideoStream($video_path);
    $stream->start();
});

Route::post('/checkstock', 'Front\FrontendController@checkstock')->name('checkstock');
Route::post('/fetchtopics', 'Admin\SweetLookController@fetchtopics')->name('fetchtopics');
Route::post('/getStripeIntent', 'Front\FrontendController@getStripeIntent');

Route::get('/maintain', 'Front\FrontendController@indexold')->name('front.comming');
Route::get('/', 'Front\FrontendController@index')->name('front.index');
Route::get('/cart', 'Front\FrontendController@cart')->name('front.cart');
Route::get('/checkout', 'Front\FrontendController@checkout')->name('front.checkout')->middleware('auth');
Route::get('/about', 'Front\FrontendController@about')->name('front.about');
Route::post('/checkout/submit', 'Front\FrontendController@checkoutSubmit')->name('checkout.submit');
Route::get('/admin/login', 'Front\FrontendController@adminlogin')->name('admin.login');
Route::post('/search/product', 'Front\FrontendController@search')->name('front.search');
Route::get('/sellers/products', 'Front\FrontendController@sellersProducts')->name('sellers.products');
Route::get('/this/month', 'Front\FrontendController@thisMonth')->name('this.month');

Route::get('/cards/viewall', 'Front\FrontendController@cards')->name('cards.viewall');
Route::get('/cards/view/{id}', 'Front\FrontendController@cardview')->name('card.view');
Route::post('/cards/pay/{id}', 'Front\FrontendController@cardpay')->name('card.pay');

Route::get('/front/stripe/{price}/{id}', 'Front\FrontendController@stripe')->name('front.stripe');
Route::get('/front/success/{id}', 'Front\FrontendController@successMessage')->name('front.success');
Route::post('/stripe/submit', 'Front\FrontendController@stripesubmit')->name('stripe.submit');

Route::post('/newsletter/submit', 'Front\FrontendController@newsletter')->name('newsletter.submit');
Route::post('/price/filter', 'Front\FrontendController@pricefilter')->name('price.filter');

Route::get('/payment/success/{id}/{type}', 'Front\FrontendController@paymentsuccess')->name('payment.success');

Route::get('/front/product/{id}', 'Front\FrontendController@product')->name('front.product');
Route::get('/front/products', 'Front\FrontendController@products')->name('front.productss');
Route::get('/vendor/product/{id}', 'Front\FrontendController@vendorproduct')->name('vendor.singleproduct');
Route::get('/front/category/{id}', 'Front\FrontendController@category')->name('front.category');
Route::get('/front/maincategory/{id}', 'Front\FrontendController@maincategory')->name('front.maincategory');
Route::get('/front/subcategory/{id}', 'Front\FrontendController@subcategory')->name('front.subcategory');
Route::get('/vendor/categories/{category}', 'Front\FrontendController@vendorcategories')->name('vendor.categories');

Route::post('/addtowishlist', 'Front\FrontendController@addtowishlist')->name('addtowishlist');
Route::post('/addtocart', 'Front\FrontendController@addtocart')->name('addtocart');
Route::post('/product/cart', 'Front\FrontendController@productaddtocart')->name('product.cart');
Route::post('/removecart', 'Front\FrontendController@removecart')->name('removecart');
Route::get('/removecart/get/{id}', 'Front\FrontendController@removeCartGet')->name('removecart.get');
Route::get('/cartitems', 'Front\FrontendController@cartitems')->name('cartitems');
Route::get('/gettotal', 'Front\FrontendController@gettotal')->name('gettotal');

Route::get('/front/legal', 'Front\FrontendController@legal')->name('front.legal');
Route::get('/front/terms', 'Front\FrontendController@terms')->name('front.terms');


Route::post('/checksizestock', 'Front\FrontendController@checksizestock')->name('checksizestock');
Route::get('/redeem/points', 'User\UserController@redeempoints')->name('redeem.points');
Route::get('/redeem/sweeties', 'User\UserController@redeemsweeties')->name('redeem.sweeties');
Route::post('/checkstockproduct', 'User\UserController@checkSizeStock')->name('checkstockproduct');
Route::post('/redeem/voucher', 'User\UserController@voucher')->name('redeem.voucher');
Route::post('/smssave', 'Front\FrontendController@smssave')->name('smssave');

Auth::routes();

Route::post('/fetchsubcategory', 'Admin\CategoryController@fetchsubcategory')->name('fetchsubcategory');
Route::post('/fetchmaincategory', 'Admin\CategoryController@fetchmaincategory')->name('fetchmaincategory');


Route::group(['middleware' => ['auth', 'web', 'role']], function() {

    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('/sweet/filter/posts/{type}', 'Admin\SweetLookController@filter')->name('filter.posts');
    Route::get('/sweet/look', 'Admin\SweetLookController@index')->name('sweet.index');
    Route::post('/sweet/status/{id}', 'Admin\SweetLookController@status')->name('sweet.status');
    Route::get('/sweet/look/delete/{id}', 'Admin\SweetLookController@sweetDelete')->name('sweet.toppost');
    Route::get('/sweet/look/delete/{id}', 'Admin\SweetLookController@sweetDelete')->name('sweet.delete');
    Route::get('/sweet/toppost', 'Admin\SweetLookController@sweetTopPost')->name('sweet.toppost');
    Route::get('/sweet/topic/delete/{id}', 'Admin\SweetLookController@topicDelete')->name('sweet.topic.delete');
    Route::get('/sweet/topics', 'Admin\SweetLookController@topics')->name('sweet.topics');
    Route::post('/sweet/stopic/save', 'Admin\SweetLookController@topicSave')->name('sweet.stopic.save');
    Route::post('/sweet/post/save', 'Admin\SweetLookController@postSave')->name('sweet.post.save');

    Route::get('/main/category', 'Admin\CategoryController@maincategory')->name('maincategory.index');
    Route::post('/main/category/store', 'Admin\CategoryController@maincategorystore')->name('maincategory.store')->middleware('optimizeImages');
    Route::get('/main/category/delete/{id}', 'Admin\CategoryController@maincategorydelete')->name('maincategory.delete');
    Route::post('/main/category/sequence/', 'Admin\CategoryController@maincategorySequence')->name('maincategory.sequence');
    Route::get('/main/category/edit/{id}', 'Admin\CategoryController@maincategoryedit')->name('maincategory.edit');
    Route::post('/main/category/update/{id}', 'Admin\CategoryController@maincategoryupdate')->name('maincategory.update');


    Route::get('/main/subcategory', 'Admin\CategoryController@subcategory')->name('subcategory.index');
    Route::post('/main/subcategory/store', 'Admin\CategoryController@subcategorystore')->name('subcategory.store')->middleware('optimizeImages');
    Route::get('/main/subcategory/delete/{id}', 'Admin\CategoryController@subcategorydelete')->name('subcategory.delete');

    Route::get('/general/slider', 'Admin\GeneralSettingsController@slider')->name('general.slider');
    Route::post('/general/slider', 'Admin\GeneralSettingsController@sliderstore')->name('banners.store')->middleware('optimizeImages');

    Route::get('/general/settings', 'Admin\GeneralSettingsController@settings')->name('general.settings');
    Route::post('/general/settings/store', 'Admin\GeneralSettingsController@settingsstore')->name('general.store');

    Route::get('/products/create', 'Admin\ProductController@create')->name('products.create');
    Route::post('/products/update/{id}', 'Admin\ProductController@update')->name('product.update');
    Route::post('/products/updatecolor/{id}', 'Admin\ProductController@updatecolor')->name('product.updatecolor');
    Route::get('/products/index', 'Admin\ProductController@index')->name('products.index');
    Route::get('/products/filter/search/{id}', 'Admin\ProductController@filterProduct')->name('product.filter.search');
    Route::get('/products/draft/index', 'Admin\ProductController@indexDraft')->name('products.draft.index');
    Route::get('/products/edit/{id}', 'Admin\ProductController@edit')->name('product.edit')->middleware('optimizeImages');
    Route::get('/products/publish/{id}', 'Admin\ProductController@productPublish')->name('product.publish');
    Route::get('/products/outofstock', 'Admin\ProductController@outofstock')->name('product.outofstock');


    Route::get('/products/size', 'Admin\ProductController@productSize')->name('products.size');
    Route::get('/size/create', 'Admin\ProductController@productSizecreate')->name('size.create');
    Route::post('/getproduct', 'Admin\ProductController@getproduct')->name('getproduct');
    Route::post('/getsizes', 'Admin\ProductController@getsizes')->name('getsizes');
    Route::post('/size/store', 'Admin\ProductController@sizestore')->name('size.store');
    Route::get('/size/delete/{id}', 'Admin\ProductController@sizedelete')->name('size.delete');
    Route::get('/size/edit/{id}', 'Admin\ProductController@sizeedit')->name('size.edit');
    Route::post('/size/update/{id}', 'Admin\ProductController@sizeupdate')->name('size.update');
    Route::post('/sizecolor/update', 'Admin\ProductController@sizecolorUpdate')->name('sizecolor.update');
    Route::get('notification/admin/users', 'Admin\UserController@notiDetialsUser')->name('admin.users');
    Route::get('/admin/noti/detials/{id}', 'Admin\UserController@notiDetials')->name('admin.noti.detials');

    Route::get('clients/admin/messages', 'Admin\UserController@userMessages')->name('admin.messages');

    Route::get('clients/admin/sellers', 'Admin\UserController@sellers')->name('admin.sellers');
    Route::get('/admin/buyers', 'Admin\UserController@buyers')->name('admin.buyers');
    Route::get('clients/admin/newsletter', 'Admin\UserController@newsletter')->name('admin.newsletter');
    Route::post('/admin/shops/assign', 'Admin\UserController@shopsAssign')->name('shops.assign');
//usman
    Route::get('admin/birthday', 'Admin\UserController@birthdayUser')->name('admin.birthday');
    Route::get('admin/birthday/send', 'Admin\UserController@birthdaySend')->name('admin.birthday.send');

    //Send Group Notfication
    Route::get('notification/group/index', 'Admin\UserController@groupIndex')->name('admin.group.index');
    Route::post('admin/group/store', 'Admin\UserController@groupStore')->name('admin.group.store');
    Route::get('admin/group/delete/{id}', 'Admin\UserController@groupDelete')->name('admin.group.delete');
    Route::get('admin/group/show/{id}', 'Admin\UserController@groupShow')->name('admin.group.show');

    Route::post('admin/send/notfication', 'Admin\UserController@sendNotfication')->name('admin.send.notfication');
    Route::post('admin/confirmation/order', 'Admin\UserController@confirmOrder')->name('admin.confirmation.order');

    Route::post('admin/send/notfication/users', 'Admin\UserController@sendNotficationUsers')->name('admin.send.notfication.users');
    Route::post('admin/send/notfication/country', 'Admin\UserController@sendNotficationCountry')->name('admin.send.notfication.country');
    Route::post('admin/send/notfication/sizes', 'Admin\UserController@sendNotficationSizes')->name('admin.send.notfication.sizes');
    Route::post('admin/send/notfication/shop', 'Admin\UserController@sendNotficationShop')->name('admin.send.notfication.shop');


    Route::get('orders/all', 'Admin\OrderController@index')->name('order.index');
    Route::get('orders/view/{id}', 'Admin\OrderController@view')->name('order.view');
    Route::get('orders/download/{id}', 'Admin\OrderController@orderDownload')->name('order.download');
    Route::get('orders/print/{id}', 'Admin\OrderController@orderPrint')->name('order.print');
    Route::get('/orders/payment/success/{id}', 'Front\FrontendController@paymentsuccess2')->name('orders.payment.success');
    Route::get('orders/status/{id}', 'Admin\OrderController@status')->name('order.status');
    Route::get('orders/videdressing', 'Admin\OrderController@videdressing')->name('order.videdressing');
    Route::get('orders/complete', 'Admin\OrderController@complete')->name('order.complete');
    Route::get('orders/pad', 'Admin\OrderController@padOrder')->name('order.pad');
    Route::get('orders/complete/vide', 'Admin\OrderController@completevide')->name('order.complete-vide');

    Route::get('general/sale/product', 'Admin\GeneralSettingsController@saleproduct')->name('saleproduct');
    Route::post('/sale/product/store', 'Admin\GeneralSettingsController@saleproductstore')->name('saleproduct.store');

    Route::get('/points/index/', 'Admin\GeneralSettingsController@points')->name('points');
    Route::post('/points/store/', 'Admin\GeneralSettingsController@pointsstore')->name('points.store');

    Route::get('users/search', 'Admin\UserController@search')->name('users.search');
    Route::get('customers/delete/{id}', 'Admin\UserController@delete')->name('customer.delete');
    Route::get('customers/edit/{id}', 'Admin\UserController@edit')->name('customer.edit');
    Route::get('customers/details/{id}', 'Admin\UserController@details')->name('customer.details');
    Route::get('customers/gifts/history/{id}', 'Admin\UserController@giftHistory')->name('customer.gift.history');
    Route::post('customers/update/{id}', 'Admin\UserController@update')->name('customer.update');

    Route::get('users/filter/{country}', 'Admin\UserController@filter')->name('users.filter');
    Route::post('searchuser', 'Admin\UserController@searchuser')->name('searchuser');
    Route::get('users/filter/search/{id}', 'Admin\UserController@filterUser')->name('users.filter.search');
    Route::post('user/points', 'Admin\UserController@userPoints')->name('user.points');
    Route::post('user/points/withdraw', 'Admin\UserController@userPointsWithdraw')->name('user.points.withdraw');
    Route::get('user/create', 'Admin\UserController@create')->name('user.create');
    Route::post('user/store', 'Admin\UserController@store')->name('user.store');
    Route::post('user/export', 'Admin\UserController@export')->name('users.export');

    Route::get('/general/sms/compain', 'Admin\GeneralSettingsController@sms')->name('sms.compain');

    // Send SMS
    Route::prefix('sms')->group(function () {
      Route::get('/send', 'SmsController@send_message')->name('sms.send');
      Route::post('/send', 'SmsController@send')->name('sms.sendpost');
      Route::get('/template', 'SmsController@template')->name('sms.template');
      Route::get('/template/{id}', 'SmsController@edit_modele')->name('sms.edit');
      Route::post('/template/update/{id}', 'SmsController@update_modele')->name('sms.update');
    });
    Route::get('/general/admin/about', 'Admin\GeneralSettingsController@about')->name('admin.about');
    Route::post('about/store', 'Admin\GeneralSettingsController@aboutstore')->name('about.store');

    Route::get('general/admin/home', 'Admin\GeneralSettingsController@home')->name('admin.home');
    Route::post('home/store', 'Admin\GeneralSettingsController@homeStore')->name('home.store');

    Route::get('points/create', 'Admin\PointsController@create')->name('points.create');
    Route::post('points/store', 'Admin\PointsController@store')->name('points.store');
    Route::get('points/index', 'Admin\PointsController@index')->name('points.index');
    Route::get('points/edit/{id}', 'Admin\PointsController@edit')->name('points.edit');
    Route::post('points/update/{id}', 'Admin\PointsController@update')->name('points.update');
    Route::get('points/delete/{id}', 'Admin\PointsController@delete')->name('points.delete');
    Route::get('expire/points/details', 'Admin\PointsController@expirePointsDetails')->name('admin.expire.points');

    Route::get('points/admin/giftrequests', 'Admin\PointsController@giftrequests')->name('admin.giftrequests');
    Route::post('admin/reddem/voucher', 'Admin\PointsController@redeemvoucher')->name('admin.reddem.voucher');

    Route::get('promo/code/index', 'Admin\CardsController@promoCodeIndex')->name('promo.code.index');
    Route::post('promo/store', 'Admin\CardsController@promoCodeStore')->name('promo.store');
    Route::post('promo', 'Admin\CardsController@promoCode')->name('promo');
    Route::get('promo/edit/{id}', 'Admin\CardsController@promoCodeEdit')->name('promo.edit');
    Route::get('promo/status/{id}/{status}', 'Admin\CardsController@promoCodeStatus')->name('promo.status');
    Route::post('promo/update/{id}', 'Admin\CardsController@promoCodeUpdate')->name('promo.update');
    Route::get('promo/delete/{id}', 'Admin\CardsController@promoCodeDelete')->name('promo.delete');
    Route::get('cards/index', 'Admin\CardsController@index')->name('cards.index');
    Route::get('cards/sale', 'Admin\CardsController@sale')->name('cards.sale');
    Route::get('cards/create', 'Admin\CardsController@create')->name('cards.create');
    Route::post('cards/store', 'Admin\CardsController@store')->name('cards.store')->middleware('optimizeImages');
    Route::post('cards/store/manual', 'Admin\CardsController@cardadmin')->name('card-admin.store');
    Route::get('cards/delete/{id}', 'Admin\CardsController@delete')->name('cards.delete');
    Route::get('change/card/status/{id}', 'Admin\CardsController@status')->name('change.cardstatus');

    Route::get('report/details/{id}', 'Admin\ReportsController@detail')->name('reports.details');
    Route::get('reports/calender', 'Admin\ReportsController@calender')->name('reports.calender');
    Route::get('reports/index', 'Admin\ReportsController@index')->name('reports.index');
    Route::post('report/submit', 'Admin\ReportsController@store')->name('report.submit');
    Route::post('report/update/{id}', 'Admin\ReportsController@update')->name('report.update');

    Route::get('/update-Password', [
        'uses' => 'Admin\UserController@password_change',
        'as' => 'change.password'
    ]);
    Route::post('/update-password/store', [
        'uses' => 'Admin\UserController@password_update',
        'as' => 'update.password'
    ]);
});

Route::group(['middleware' => ['auth', 'web']], function() {

    Route::get('/vendor/product', 'User\UserController@productcreate')->name('vendor.product');
    Route::get('/vendor/products', 'User\UserController@products')->name('vendor.products');
    Route::get('/vendor/orders', 'User\UserController@orders')->name('vendor.orders');

    Route::get('/user/order/detail/{id}', 'User\UserController@orderdetails')->name('user.order-detail');
    Route::get('/user/order/status/{id}', 'User\UserController@orderstatus')->name('user.order-status');

    Route::get('/user/dashboard', 'User\UserController@dashboard')->name('user.dashboard');
    Route::get('/user/wishlist', 'User\UserController@wishlist')->name('user.wishlist');
    Route::get('/wishlist/delete/{id}', 'User\UserController@wishlistdelete')->name('wishlist.delete');


    Route::post('/user/profileupdate', 'User\UserController@profileupdate')->name('user.profileupdate');
    Route::post('/products/store', 'Admin\ProductController@store')->name('product.store')->middleware('optimizeImages');
    Route::get('/products/delete/{id}', 'Admin\ProductController@delete')->name('product.delete');
    Route::get('/products/compress', 'Admin\ProductController@compressImages')->name('product.compress');
    Route::get('/getmypoints/{id}', 'User\UserController@getmypoints')->name('getmypoints');
    Route::post('/send/message/vendor', 'User\MessageController@sendMessage')->name('message.send');
    Route::get('/vendor/messages', 'User\MessageController@messages')->name('vendor.messages');
    Route::get('/user/chat/{id}', 'User\MessageController@chat')->name('user.chat');


});

Route::get('/application/stripe/{price}/{id}/{user_id}', 'Api\PaymantController@stripe')->name('application.stripe');
Route::post('/application/stripe/submit', 'Api\PaymantController@stripesubmit')->name('application.stripe.submit');
Route::get('/application/success/{id}/{type}/{user_id}', 'Api\PaymantController@paymentsuccess')->name('application.payment.success');

Route::get('/app/about2', 'App\AppController@appAbout2')->name('app.about2');
Route::get('/app/terms', 'App\AppController@appTerms')->name('app.terms');
Route::get('/app/privicy2', 'App\AppController@appPrivicy2')->name('app.privicy2');
