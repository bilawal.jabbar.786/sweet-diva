<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('login', 'Api\UserController@login');
Route::post('register', 'Api\UserController@register');
Route::post('send/otp', 'Api\ApiController@sendOtpEmail');
Route::post('verify/otp', 'Api\ApiController@otpEmailVerify');
Route::post('new/password', 'Api\ApiController@appPassword');


Route::group(['middleware' => 'auth:api'], function(){
    Route::post('/details', 'Api\UserController@details');
    Route::post('/delete/account', 'Api\UserController@deleteAccount');


    Route::post('/addtowishlist', 'Api\ApiController@addtowishlist');
    Route::get('/wishlist', 'Api\ApiController@wishlist');
    Route::get('/orders', 'Api\ApiController@orders');

    Route::post('/checkout', 'Api\ApiController@checkout');
    Route::post('/promo/code', 'Api\PaymantController@promoCode');
    Route::get('/notifications', 'Api\ApiController@notifications');
    Route::get('/save-token/{token}', 'Api\ApiController@token');
    Route::post('/profile/update', 'Api\ApiController@profileUpdate');
    Route::post('/image/update', 'Api\ApiController@imageUpdate');

    Route::get('/referral/users', 'Api\ApiController@referral');
    Route::get('/getmypoints/{id}', 'Api\ApiController@getmypoints');

    Route::get('/points', 'Api\ApiController@points');
    Route::get('/points/history', 'Api\ApiController@pointsHistory');
    Route::get('/gifts/history', 'Api\ApiController@giftsHistory');
    Route::post('/change/password', 'Api\ApiController@changePassword');

    Route::get('/astuce/persons/posts/{id}/{name}', 'Api\SweetLookController@astucePersons');
    Route::get('/sweetlook/access', 'Api\SweetLookController@access');
    Route::get('/sweetlook/grant/access', 'Api\SweetLookController@grantAccess');
    Route::post('/add/post', 'Api\SweetLookController@addPost');
    Route::get('/my/posts', 'Api\SweetLookController@myPost');
    Route::get('/like/post/{id}', 'Api\SweetLookController@likePost');
    Route::get('/all/posts', 'Api\SweetLookController@allPosts');
    Route::get('/astuce', 'Api\SweetLookController@astuce');
    Route::get('/top/10', 'Api\SweetLookController@top10');
    Route::get('/singlePost/{id}', 'Api\SweetLookController@singlePost');
    Route::post('/comment/post', 'Api\SweetLookController@commentPost');
    Route::get('/comments/{id}', 'Api\SweetLookController@comments');

    Route::get('/followers', 'Api\SweetLookController@followers');
    Route::get('/profile/{id}', 'Api\SweetLookController@profile');

    Route::get('/follow/{id}', 'Api\SweetLookController@follow');
    Route::get('/topics/posts/{id}', 'Api\SweetLookController@topicPosts');

});

Route::get('/settings', 'Api\ApiController@settings');
Route::get('/thismonth', 'Api\ApiController@thismonth');
Route::get('/sold', 'Api\ApiController@sold');
Route::post('search', 'Api\ApiController@search');

Route::get('/cards', 'Api\ApiController@cards');
Route::get('/sizes/{size}', 'Api\ApiController@sizes');

Route::get('/banners', 'Api\ApiController@banners');
Route::get('/categories', 'Api\ApiController@categories');
Route::get('/categories/products/{id}', 'Api\ApiController@categoriesProduct');
Route::get('/products', 'Api\ApiController@products');
Route::get('/single/product/{id}', 'Api\ApiController@singleProduct');
Route::get('/fetch/sizes/{id}/{color}', 'Api\ApiController@fetchSize');
Route::post('/fetch/fetchStock', 'Api\ApiController@fetchStock');

Route::post('/addtocart', 'Api\ApiController@addtocart');
Route::get('/cartitems/{device_id}', 'Api\ApiController@cartitems');

Route::get('/removecart/{id}', 'Api\ApiController@removecart');
Route::post('/contact/save', 'Api\ApiController@userContactStore');

Route::get('/sweet/topics', 'Api\SweetLookController@topics');
Route::get('/sweet/topics/{name}', 'Api\SweetLookController@topicsPersons');
Route::get('/top/sweet', 'Api\SweetLookController@topSweet');


Route::get('/cartitems1/{device_id}', 'Api\ApiController@cartitems1');
Route::post('/addtocart1', 'Api\ApiController@addtocart1');

Route::prefix('v1')->group(function () {
    Route::get('/cartitems/{device_id}', 'Api\V1\ApiController@cartitems');

    Route::get('/categories/products/{id}', 'Api\V1\ApiController@categoriesProduct');
    Route::get('/thismonth', 'Api\V1\ApiController@thismonth');
    Route::get('/sold', 'Api\V1\ApiController@sold');
    Route::post('/search', 'Api\V1\ApiController@search');
    Route::get('/sizes/{size}', 'Api\V1\ApiController@sizes');
    Route::get('/top/sweet', 'Api\V1\ApiController@topSweet');

    Route::group(['middleware' => 'auth:api'], function(){
        Route::get('/notifications', 'Api\V1\ApiController@notifications');
        Route::get('/wishlist', 'Api\V1\ApiController@wishlist');
        Route::get('/all/posts', 'Api\V1\ApiController@allPosts');
    });
});
Route::prefix('v2')->group(function () {
    Route::get('/categories/products/{id}', 'Api\V1\ProductController@categoriesProduct');
    Route::get('/thismonth', 'Api\V1\ProductController@thismonth');
    Route::get('/sold', 'Api\V1\ProductController@sold');
    Route::post('/search', 'Api\V1\ProductController@search');
    Route::get('/sizes/{size}', 'Api\V1\ProductController@sizes');
    Route::get('/top/sweet', 'Api\V1\ProductController@topSweet');
    Route::group(['middleware' => 'auth:api'], function(){
        Route::get('/wishlist', 'Api\V1\ProductController@wishlist');
    });
});
