@extends('layouts.front')
@section('content')
    <div class="slideshow-container">

        <div class="mySlides fade1">
            <div class="numbertext">1 / 3</div>
            <img src="{{asset($banners->vide1)}}" style="width:100%">
        </div>
        <div class="mySlides fade1">
            <div class="numbertext">1 / 3</div>
            <img src="{{asset($banners->vide2)}}" style="width:100%">
        </div>
        <div class="mySlides fade1">
            <div class="numbertext">1 / 3</div>
            <img src="{{asset($banners->vide3)}}" style="width:100%">
        </div>


        <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
        <a class="next" onclick="plusSlides(1)">&#10095;</a>

    </div>
    <br>
    <div id="dots" style="text-align:center">
        <span class="dot" onclick="currentSlide(1)"></span>
        <span class="dot" onclick="currentSlide(2)"></span>
        <span class="dot" onclick="currentSlide(3)"></span>
    </div>
    <!-- Begin Kenne's Content Wrapper Area -->
    <div class="kenne-content_wrapper" style="padding: 50px 0 95px;">
        <div class="container">
            <div class="row">
                @include('include.videdressing')
                <div class="col-xl-9 col-lg-8 order-1 order-lg-2">
                    <div class="shop-toolbar">
                        <div class="product-view-mode">
                            <a class="active grid-3" data-target="gridview-3" data-toggle="tooltip" data-placement="top" title="Vue grille"><i class="fa fa-th"></i></a>
                            <a class="list" data-target="listview" data-toggle="tooltip" data-placement="top" title="Affichage en liste"><i class="fa fa-th-list"></i></a>
                        </div>
                        @auth()
                        @if($orders)
                        <div class="product-item-selection_area">
                            <div class="product-short">
                                <label class="select-label">Facture # : {{$orders->order_number}}</label>
                                <a href="{{route('user.order-status', ['id' => $orders->id])}}">
                                    <button style="padding: 10px" class="mybutton">Valider la réception de votre commande</button>
                                </a>
                            </div>
                        </div>
                        @endif
                        @endauth
                    </div>
                    <div class="shop-product-wrap grid gridview-3 row" style="background-color: pink">
                        @foreach($sellersProducts as $product)
                            <div class="col-lg-4 col-md-4 col-sm-6" >
                                <div class="product-item" >
                                    <div class="single-product" style="padding: 0px !important; ">
                                        <div class="row" style="padding-top: 10px; background-color: white">
                                            <div class="col-4" style="padding: 0px">
                                                <img style="height: 30px; border-radius: 50px" src="{{empty($product->user->photo)? asset('front/assets/images/avatar.png') : asset($product->user->photo)}}" alt="">
                                            </div>
                                            <div class="col-4" style="padding-left: 0px; margin-left: -20px">
                                                <p style="padding-top: 5px;">{{$product->user->name}}</p>
                                            </div>
                                            <div class="col-4">
                                            </div>
                                        </div>
                                        <div class="product-img">
                                            <a href="{{route('vendor.singleproduct', ['id' => $product->id])}}">
                                                <img height="250px" loading="lazy" class="primary-img" src="{{asset($product->photo1)}}" alt="Kenne's Product Image">
                                                <img height="250px" loading="lazy" class="secondary-img" src="{{asset($product->photo2)}}" alt="Kenne's Product Image">
                                            </a>
                                            <div class="add-actions">
                                                <ul>
                                                    <li><a onclick="addtowishlist(this)" id="{{$product->id}}" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i
                                                                class="ion-ios-heart-outline"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="product-content" style="background-color: white">
                                            <div class="product-desc_info">
                                                <h3 class="product-name"><a href="{{route('vendor.product', ['id' => $product->id])}}">{{$product->title}}</a></h3>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="price-box">
                                                            <span class="new-price">{{$product->price}} €</span>
                                                            @if($product->oldprice)
                                                                <span class="old-price">{{$product->oldprice}} €</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        Taille:   {{$product->sizesimple}}
                                                    </div>
                                                </div>

                                                {{$product->user->country}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="list-product_item">
                                    <div class="single-product">
                                        <div class="product-img">
                                            <a href="{{route('vendor.singleproduct', ['id' => $product->id])}}">
                                                <img src="{{asset($product->photo1)}}" alt="Kenne's Product Image">
                                            </a>
                                        </div>
                                        <div class="product-content">
                                            <div class="product-desc_info">
                                                <div class="price-box">
                                                    <span class="new-price">{{$product->price}} €</span>
                                                    @if($product->oldprice)
                                                        <span class="old-price">{{$product->oldprice}} €</span>
                                                    @endif
                                                </div>
                                                <h6 class="product-name"><a href="{{route('vendor.product', ['id' => $product->id])}}">
                                                        {{$product->title}}
                                                    </a></h6>
                                                <div class="product-short_desc">
                                                    <p>
                                                        {{$product->description}}
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="add-actions">
                                                <ul>
{{--                                                    <li><a onclick="addtowishlist(this)" id="{{$product->id}}" data-toggle="tooltip" data-placement="top" title="Ajouter à la liste de souhaits"><i--}}
{{--                                                                class="ion-ios-heart-outline"></i></a>--}}
{{--                                                    </li>--}}

                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Kenne's Content Wrapper Area End Here -->
@endsection
