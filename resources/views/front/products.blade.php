@extends('layouts.front')
@section('content')
<div class="container">
    <div class="row">
        <h3 style="padding: 40px 0px 30px">Tous les produits</h3>

    </div>
    <div class="row">
        @foreach($products as $row)
            <div class="col-md-3">
                <div class="product-item">
                    <div class="single-product">
                        <div class="product-img">
                            <a href="{{route('front.product', ['id' => $row->id])}}">
                                <img class="primary-img lazy" src="{{asset($row->photo1)}}" alt="Kenne's Product Image">
                                {{--                                        <img class="secondary-img lazy" src="{{asset($row->photo2)}}" alt="Kenne's Product Image">--}}
                            </a>
                            <span class="sticker-2">{{$row->categorytype->name}}</span>
                            <div class="add-actions">
                                <ul>
                                    @auth
                                        <li><a  onclick="addtowishlist(this)" id="{{$row->id}}" data-toggle="tooltip" data-placement="right" title="Ajouter à la liste de souhaits"><i class="ion-ios-heart-outline"></i></a>
                                        </li>
                                    @endauth
                                </ul>
                            </div>
                        </div>
                        <div class="product-content">
                            <div class="product-desc_info">
                                <h3 class="product-name"><a href="{{route('front.product', ['id' => $row->id])}}">{{$row->title}}</a></h3>
                                <div class="price-box">
                                    <span class="new-price">{{$row->price}} €</span>
                                    @if($row->oldprice)
                                        <span class="old-price">{{$row->oldprice}} €</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="kenne-paginatoin-area">
                <div class="row">
                    <div class="col-lg-12">
                        {{$products->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
