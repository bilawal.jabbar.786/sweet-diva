@extends('layouts.front')
@section('content')
    <style>
        #card-element {
            border-radius: 4px 4px 0 0;
            border: 1px solid rgba(50, 50, 93, 0.1);
            width: 100%;
            background: white;
            padding-left: 12px;
            padding-right: 12px;
        }

        #payment-request-button {
            margin-bottom: 32px;
        }
         #test{
            background: #242424;
            border: medium none;
            color: #ffffff;
            font-size: 17px;
            height: 50px;
            margin: 20px 0 0;
            padding: 12px;
            text-transform: uppercase;
            width: 100%;
            border: 1px solid transparent;
            cursor: pointer;
        }
    </style>
    <!-- Begin Kenne's Checkout Area -->
    <div class="checkout-area">
        <div class="container">

                <div class="row">
                    <div class="col-lg-6 col-12">
                        <div class="your-order">
                            <h3>Comment ça marche</h3>
                            <div class="your-order-table table-responsive">
                                <?php $gs = \App\GeneralSettings::find(1);?>
                             <p>{!! $gs->pad !!}</p>
                            </div>


                        </div>
                    </div>
                    <div class="col-lg-6 col-12">
                        <div class="your-order">
                            <h3>Votre commande</h3>
                            <div class="your-order-table table-responsive">
                                <table class="table">
                                    <tbody>

                                    @foreach($cartitems as $items)
                                        <tr class="cart_item">
                                            <td class="cart-product-name"> {{$items->name}}<strong
                                                    class="product-quantity">× {{$items->quantity}}</strong></td>
                                            <td class="cart-product-total"><span class="amount">{{$items->price * $items->quantity}} €</span>
                                            </td>
                                        </tr>
                                    @endforeach
                                    <tr class="cart_item">
                                        <td class="cart-product-name"><strong class="product-quantity">Frais de
                                                livraison </strong></td>
                                        <td class="cart-product-total"><span class="amount deliverycharges">{{$total- $del}}</span>
                                        </td>
                                    </tr>
                                    </tbody>
                                    <tfoot>
                                    <tr class="order-total">
                                        <th>Total de la commande</th>
                                        <td><strong>
                                                <span class="finalamount">{{$total}} €</span>
                                            </strong></td>

                                    </tr>
                                    <tr>
                                        <td>


                                        </td>
                                    </tr>
                                    </tfoot>
                                </table>
                                <a href="{{route('front.success',['id'=>$id])}}" class="btn btn-success test" id="test">Payer</a>
                            </div>



                        </div>
                    </div>
                </div>

        </div>
    </div>


@endsection

