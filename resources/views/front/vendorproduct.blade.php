@extends('layouts.front')
@section('content')
    <!-- Begin Kenne's Single Product Variable Area -->
    <div class="sp-area">
        <div class="container">
            <div class="sp-nav">
                <div class="row">
                    <div class="col-md-12">
                        <a href="{{route('sellers.products')}}"><button class="mybutton">Vide Dressing</button></a>
                    </div>
                </div>
                <br><br>
                <div class="row">
                    <div class="col-lg-5">
                        <div class="sp-img_area">
                            <div class="sp-img_slider slick-img-slider kenne-element-carousel" data-slick-options='{
                                "slidesToShow": 1,
                                "arrows": false,
                                "fade": true,
                                "draggable": false,
                                "swipe": false,
                                "asNavFor": ".sp-img_slider-nav"
                                }'>
                                <div class="single-slide red zoom">
                                    <img src="{{asset($product->photo1)}}" alt="Kenne's Product Image">
                                </div>
                                @if(!empty ( $product->gallery ))
                                    @foreach(json_decode($product->gallery, true) as $images)
                                        <div class="single-slide red zoom">
                                            <img src="{{asset('gallery/'.$images)}}" alt="Kenne's Product Image">
                                        </div>
                                    @endforeach
                                @endif

                            </div>
                            <div class="sp-img_slider-nav slick-slider-nav kenne-element-carousel arrow-style-2 arrow-style-3" data-slick-options='{
                                "slidesToShow": 3,
                                "asNavFor": ".sp-img_slider",
                                "focusOnSelect": true,
                                "arrows" : true,
                                "spaceBetween": 30
                                }' data-slick-responsive='[
                                        {"breakpoint":1501, "settings": {"slidesToShow": 3}},
                                        {"breakpoint":1200, "settings": {"slidesToShow": 2}},
                                        {"breakpoint":992, "settings": {"slidesToShow": 4}},
                                        {"breakpoint":768, "settings": {"slidesToShow": 3}},
                                        {"breakpoint":575, "settings": {"slidesToShow": 2}}
                                    ]'>
                                <div class="single-slide red">
                                    <img src="{{asset($product->photo1)}}" alt="Kenne's Product Thumnail">
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <div class="sp-content">
                            <form method="POST" action="{{route('product.cart')}}">
                                @csrf
                                <div class="sp-heading">
                                    <h5><a href="#">{{$product->title}}</a></h5>
                                </div>
                                <div class="sp-essential_stuff">
                                    <ul>
                                        <li>Code produit: <a href="javascript:void(0)">{{$product->sku}}</a></li>
                                        <li>Etat: <a href="javascript:void(0)">{{$product->state}}</a></li>
                                        <li>Taille: <a href="javascript:void(0)">{{$product->sizesimple}}</a></li>
                                        <hr>
                                        <li>{!! $product->description !!}</li>
                                        <hr>
                                    </ul>
                                </div>
                                <br>
                                <div class="sp-heading">
                                    <h5>{{$product->price}} €
                                        @if($product->oldprice)
                                            <del> {{$product->oldprice}} € </del>
                                        @endif
                                    </h5>
                                </div>
                                <div class="quantity" style="display: none">
                                    <label>Quantity</label>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <input type="number" readonly name="quantity" class="form-control quantity" max="1" min="1" value="1" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="id" value="{{$product->id}}">
                                <div class="row">
                                    <div class="col-md-4">
                                            <ul>
                                                <li><button class="mybutton" type="submit" href="#">Ajouter au panier</button></li>
                                            </ul>
                                    </div>
                                    <div class="col-md-5">
                                        @auth
                                        <a class="mybutton" style="color: white"  data-toggle="modal" data-target="#myModal">Contacter le vendeur</a>
                                        @endauth
                                    </div>
                                </div>


                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Kenne's Single Product Variable Area End Here -->


    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                    <form action="{{route('message.send')}}" method="POST">
                        @csrf
                        <textarea required name="message" placeholder="Votre message" class="form-control" id="" cols="30" rows="10"></textarea>
                        <br>
                        <input type="hidden" name="r_id" value="{{$product->user->id}}">
                        <div class="row">
                            <div class="col-md-6">
                                <button class="mybutton">Envoyer</button>
                            </div>
                        </div>
                    </form>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                </div>
            </div>

        </div>
    </div>

@endsection
@section('script')
    <script>
        function checkstock(elem){
            let id = $(elem).attr("id");
            let size = elem.value;
            let _token   = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url: "{{route('checkstock')}}",
                type:"POST",
                data:{
                    id:id,
                    _token: _token,
                    size: size,
                },
                success:function(response){
                    if(response.success == '0') {
                        $('.quantity').val(0);
                        $('.ps-success').html("EN RUPTURE DE STOCK");
                        $(':input[type="submit"]').prop('disabled', true);
                    }else {
                        console.log(response.quantity);
                        $('.quantity').attr('max', response.quantity);
                        $('.ps-success').html("EN STOCK (" + response.quantity + ")");
                        $(':input[type="submit"]').prop('disabled', false);
                    }
                },
            });
        }
    </script>
@endsection
