@extends('layouts.front')
@section('content')
    <div class="grid-view_area">
        <div class="container">
            <div class="row">
                <form action="{{route('card.pay', ['id' => $card->id])}}" method="POST">
                    @csrf
                <div class="col-lg-12 order-lg-2 order-1">
                    <div class="row blog-item_wrap">
                      <div class="col-md-6">
                          <img src="{{asset($card->photo)}}" style="width: 100%" alt="">
                      </div>
                      <div class="col-md-6">
                          <h3>{{$card->title}}</h3>
                          <p>Code du bon cadeau : {{$card->sku}}</p>
                          <p>Sélectionnez un montant : <select name="price" onchange="amount(this)" id="">
                                  <option value="50">50€</option>
                                  <option value="100">100€</option>
                                  <option value="150">150€</option>
                                  <option value="200">200€</option>
                                  <option value="250">250€</option>
                                  <option value="300">300€</option>
                              </select></p>
                          <h3>Prix : <b class="priceenter">50</b> €</h3>
                          <hr>
                          <div class="row">
                              <div class="col-md-12">
                                  <div class="checkout-form-list">
                                      <label>Nom du destinataire <span class="required">*</span></label>
                                      <input class="form-control" name="name" type="text" required>
                                  </div>
                              </div>
                              <div class="col-md-12">
                                  <div class="checkout-form-list">
                                      <label>Destinataire E-mail <span class="required">*</span></label>
                                      <input class="form-control" name="email" type="email" required>
                                  </div>
                              </div>
                              <div class="col-md-12">
                                  <div class="checkout-form-list">
                                      <label>Votre nom <span class="required">*</span></label>
                                      <input class="form-control" name="sendername" type="text" required>
                                  </div>
                              </div>
                              <div class="col-md-12">
                                  <div class="checkout-form-list">
                                      <label>Votre Telephone <span class="required">*</span></label>
                                      <input class="form-control" name="senderphone" type="text" required>
                                  </div>
                              </div>
                              <div class="col-md-12">
                                  <div class="checkout-form-list">
                                      <label>Message <span class="required"></span></label>
                                      <textarea name="message" class="form-control" id="" cols="30" rows="10"></textarea>
                                  </div>
                              </div>
                          </div>
                          <br>
                          <div class="row">
                              <div class="col-md-6">
                                  <div class="checkout-form-list">
                                      <button class="mybutton">Ajouter au panier</button>
                                  </div>
                              </div>
                          </div>
                      </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        function amount(elem) {
            $('.priceenter').html(elem.value);
        }
    </script>
@endsection
