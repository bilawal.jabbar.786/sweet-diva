@extends('layouts.front')
@section('content')
    
    <div class="kenne-cart-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <form action="javascript:void(0)">
                        <div class="table-content table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th class="kenne-product-remove"></th>
                                    <th class="cart-product-name">Article</th>
                                    <th class="kenne-product-price">Prix</th>
                                    <th class="kenne-product-quantity">Quantité</th>
                                    <th class="kenne-product-subtotal">Total</th>
                                </tr>
                                </thead>
                                <tbody class="items">

                                </tbody>
                            </table>
                        </div>

                        <div class="row">
                            <div class="col-md-5 ml-auto">
                                <div class="cart-page-total">
                                    <h2>Total panier</h2>
                                    <ul>
                                        <li>Total <span class="total">0</span></li>
                                    </ul>
                                    <a href="/front/category/1">Continuer ma commande</a><a href="{{route('front.checkout')}}">Valider la commande</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        mycartitems();
    </script>
@endsection
