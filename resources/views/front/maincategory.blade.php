@extends('layouts.front')
@section('content')
    <img src="{{asset($photo->banner)}}" style="width: 100%" alt="">
    <!-- Begin Kenne's Content Wrapper Area -->
<!--    <div class="container">
        <div class="row">
            <div class="col-md-2" style="padding-left: 0px; padding-right: 0px">
                <button class="mybutton">Abcdefghi</button>
            </div>
        </div>
    </div>-->

    <div class="kenne-content_wrapper">
        <div class="container">
            <div class="row">
                @include('include.filters')
                <div class="col-xl-9 col-lg-8 order-1 order-lg-2">
                    <div class="shop-toolbar">
                        <div class="product-view-mode">
                            <a class="active grid-3" data-target="gridview-3" data-toggle="tooltip" data-placement="top" title="Vue grille"><i class="fa fa-th"></i></a>
                            <a class="list" data-target="listview" data-toggle="tooltip" data-placement="top" title="Affichage en liste"><i class="fa fa-th-list"></i></a>
                        </div>
                    </div>
                    <div class="shop-product-wrap grid gridview-3 row">
                        @foreach($products as $product)
                            <div class="col-lg-4 col-md-4 col-sm-6">
                                <div class="product-item">
                                    <div class="single-product">
                                        <div class="product-img">
                                            <a href="{{route('front.product', ['id' => $product->id])}}">
                                                <img  class="primary-img" src="{{asset($product->photo1)}}" alt="Kenne's Product Image">
                                                <img  class="secondary-img" src="{{asset($product->photo2)}}" alt="Kenne's Product Image">
                                            </a>
{{--                                            <span class="sticker">{{$product->maincategory->name}}</span>--}}
                                            <div class="add-actions">
                                                <ul>
                                                    @auth
                                                    <li><a onclick="addtowishlist(this)" id="{{$product->id}}" data-toggle="tooltip" data-placement="right" title="Ajouter à la liste de souhaits"><i
                                                                class="ion-ios-heart-outline"></i></a>
                                                    </li>
                                                    @endauth
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="product-content">
                                            <div class="product-desc_info">
                                                <h3 class="product-name"><a href="{{route('front.product', ['id' => $product->id])}}">{{$product->title}}</a></h3>
                                                <div class="price-box">
                                                    <span class="new-price">{{$product->price}} €</span>
                                                    @if($product->oldprice)
                                                        <span class="old-price">{{$product->oldprice}} €</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="list-product_item">
                                    <div class="single-product">
                                        <div class="product-img">
                                            <a href="{{route('front.product', ['id' => $product->id])}}">
                                                <img  src="{{asset($product->photo1)}}" alt="Kenne's Product Image">
                                            </a>
                                        </div>
                                        <div class="product-content">
                                            <div class="product-desc_info">
                                                <div class="price-box">
                                                    <span class="new-price">{{$product->price}} €</span>
                                                    @if($product->oldprice)
                                                        <span class="old-price">{{$product->oldprice}} €</span>
                                                    @endif
                                                </div>
                                                <h6 class="product-name"><a href="{{route('front.product', ['id' => $product->id])}}">
                                                        {{$product->title}}
                                                    </a></h6>
                                                <div class="product-short_desc">
                                                    <p>
                                                        {{$product->description}}
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="add-actions">
                                                <ul>
                                                    @auth
                                                    <li><a onclick="addtowishlist(this)" id="{{$product->id}}" data-toggle="tooltip" data-placement="top" title="Ajouter à la liste de souhaits"><i
                                                                class="ion-ios-heart-outline"></i></a>
                                                    </li>
                                                    @endauth
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="kenne-paginatoin-area">
                                <div class="row">
                                    <div class="col-lg-12">
                                        {{$products->links()}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Kenne's Content Wrapper Area End Here -->
@endsection
