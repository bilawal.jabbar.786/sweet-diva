@extends('layouts.front')
@section('content')
    <div class="kenne-wishlist_area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <form action="javascript:void(0)">
                        <div class="table-content table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th class="kenne-product_remove">supprimer</th>
                                    <th class="kenne-product-thumbnail">images</th>
                                    <th class="cart-product-name">Produit</th>
                                    <th class="kenne-product-price">Prix ​​unitaire</th>
{{--                                    <th class="kenne-cart_btn">Ajouter au panier</th>--}}
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($products as $row)
                                <tr>
                                    <td class="kenne-product_remove"><a href="{{route('wishlist.delete', ['id' => $row->id])}}"><i class="fa fa-trash"
                                                                                                     title="Remove"></i></a></td>
                                    <td class="kenne-product-thumbnail"><a href="javascript:void(0)">
                                            <img loading="lazy" style="height: 40px" src="{{asset($row->photo1)}}" alt="Kenne's Wishlist Thumbnail"></a>
                                    </td>
                                    <td class="kenne-product-name"><a href="{{route('front.product', ['id' => $row->id])}}">{{$row->title}}</a></td>
                                    <td class="kenne-product-price"><span class="amount">{{$row->price}} €</span></td>
{{--                                    <td class="kenne-cart_btn"><a onclick="addtocart(this)" id="{{$row->id}}">Ajouter au panier</a></td>--}}
                                </tr>
                                </tbody>
                                @endforeach

                            </table>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
