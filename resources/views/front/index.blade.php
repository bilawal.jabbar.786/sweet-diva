@extends('layouts.front')
@section('content')
    @auth
    <div style="background-color: black; color: pink; border-top: 0px solid white">
        <marquee>
            <p style="font-family: Impact; font-size: 18pt; margin: 0px !important; font-weight: 900;">
                Bienvenue {{Auth::user()->name}}, vous avez {{Auth::user()->points}}
                points et
                @if(Auth::user()->role == 1)

                @else
                    {{Auth::user()->wallet}}
                    Sweeties
                @endif
                sur votre compte
            </p>
        </marquee>
    </div>
    @endauth
    <div class="container">
        <div class="row">
            <div class="col-md-4">

            </div>
            <div class="col-md-4" style="padding: 10px">
                <img style="width: 100%" src="{{asset('front/assets/04.jpeg')}}"  alt="">
            </div>
            <div class="col-md-4">

            </div>
        </div>
    </div>
    <div class="banner-area-3" style="background-color: #ffe1e7">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div style="background-image:url('{{asset($banners->cat1)}}'); background-repeat: no-repeat; background-size: 100% 100%; text-align: center; ">
                        <div style="padding-top: 200px; padding-bottom: 20px">
                            <a href="{{route('front.category', ['id' => '1'])}}"><button class="mybutton1">SWEET DIVA</button></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div style="background-image:url('{{asset($banners->cat2)}}'); background-repeat: no-repeat; background-size: 100% 100%; text-align: center; ">
                        <div style="padding-top: 200px; padding-bottom: 20px">
                            @if($gs->working == 1)
                            <a href="{{route('front.category', ['id' => '2'])}}"><button class="mybutton1">WORKING GIRL</button></a>
                            @else
                                <a href="#"><button class="mybutton1">BIENTÔT DISPONIBLE</button></a>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div style="background-image:url('{{asset($banners->cat3)}}'); background-repeat: no-repeat; background-size: 100% 100%; text-align: center; ">
                        <div style="padding-top: 200px; padding-bottom: 20px">
                            @if($gs->fashioncod == 1)
                            <a href="{{route('front.category', ['id' => '3'])}}"><button  class="mybutton1">FASHION COD</button></a>
                            @else
                                <a href="#"><button class="mybutton1">BIENTÔT DISPONIBLE</button></a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <br>
    <div class="slideshow-container">

        <div class="mySlides fade1">
            <div class="numbertext">1 / 3</div>
            <img src="{{asset($banners->image1)}}" style="width:100%">
        </div>

        <div class="mySlides fade1">
            <div class="numbertext">2 / 3</div>
            <img src="{{asset($banners->image2)}}" style="width:100%">
        </div>
        <div class="mySlides fade1">
            <div class="numbertext">3 / 3</div>
            <img src="{{asset($banners->image3)}}" style="width:100%">
        </div>
        <div class="mySlides fade1">
            <div class="numbertext">3 / 3</div>
            <img src="{{asset($banners->image5)}}" style="width:100%">
        </div>
        <div class="mySlides fade1">
            <div class="numbertext">3 / 3</div>
            <img src="{{asset($banners->image6)}}" style="width:100%">
        </div>
        <div class="mySlides fade1">
            <div class="numbertext">3 / 3</div>
            <img src="{{asset($banners->image7)}}" style="width:100%">
        </div>
        <div class="mySlides fade1">
            <div class="numbertext">3 / 3</div>
            <img src="{{asset($banners->image8)}}" style="width:100%">
        </div>
        <div class="mySlides fade1">
            <div class="numbertext">3 / 3</div>
            <img src="{{asset($banners->image9)}}" style="width:100%">
        </div>

        <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
        <a class="next" onclick="plusSlides(1)">&#10095;</a>

    </div>
    <br>
    <div id="dots" style="text-align:center">
        <span class="dot" onclick="currentSlide(1)"></span>
        <span class="dot" onclick="currentSlide(2)"></span>
        <span class="dot" onclick="currentSlide(3)"></span>
        <span class="dot" onclick="currentSlide(4)"></span>
        <span class="dot" onclick="currentSlide(5)"></span>
        <span class="dot" onclick="currentSlide(6)"></span>
        <span class="dot" onclick="currentSlide(7)"></span>
        <span class="dot" onclick="currentSlide(8)"></span>
    </div>
    <br>
    <br>

    <div class="container">
        <a href="{{route('this.month')}}"><img style="width: 100%" src="{{asset('front/assets/23.png')}}" alt=""></a>
    </div>

    <!-- Begin Product Area -->
    <div class="product-area" id="allproducts">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <h3>PRODUITS</h3>
                        <div class="product-arrow"></div>
                        <a href="{{route('front.productss')}}">Voir Tout</a>
                    </div>
                </div>
                <div class="row">
                    @foreach($adminproducts as $row)
                        <div class="col-lg-3">
                                <div class="product-item">
                                    <div class="single-product">
                                        <div class="product-img">
                                            <a href="{{route('front.product', ['id' => $row->id])}}">
                                                <img class="primary-img lazy" src="{{asset($row->photo1)}}" alt="Kenne's Product Image">
                                                {{--                                        <img class="secondary-img lazy" src="{{asset($row->photo2)}}" alt="Kenne's Product Image">--}}
                                            </a>
                                            <span class="sticker-2">{{$row->categorytype->name}}</span>
                                            <div class="add-actions">
                                                <ul>
                                                    @auth
                                                        <li><a  onclick="addtowishlist(this)" id="{{$row->id}}" data-toggle="tooltip" data-placement="right" title="Ajouter à la liste de souhaits"><i class="ion-ios-heart-outline"></i></a>
                                                        </li>
                                                    @endauth
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="product-content">
                                            <div class="product-desc_info">
                                                <h3 class="product-name"><a href="{{route('front.product', ['id' => $row->id])}}">{{$row->title}}</a></h3>
                                                <div class="price-box">
                                                    <span class="new-price">{{$row->price}} €</span>
                                                    @if($row->oldprice)
                                                        <span class="old-price">{{$row->oldprice}} €</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    @endforeach
                </div>

            </div>
        </div>
    </div>

    <br>
    <br>
    <!-- Begin Banner Area Three -->
    <div class="banner-area-3" style="background-color: #ffe1e7">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-6 custom-xxs-col">
                    <div class="banner-item img-hover_effect">
                        <div class="banner-img">
                            <a href="{{$banners->link1}}">
                                <img class="img-full lazy" src="{{asset($banners->banner1)}}" alt="Banner">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-6 custom-xxs-col">
                    <div class="banner-item img-hover_effect">
                        <div class="banner-img">
                            <a href="{{$banners->link2}}">
                                <img class="img-full lazy" src="{{asset($banners->banner2)}}" alt="Banner">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-6 custom-xxs-col">
                    <div class="banner-item img-hover_effect">
                        <div class="banner-img">
                            <a href="{{$banners->link3}}">
                                <img class="img-full lazy" src="{{asset($banners->banner3)}}" alt="Banner">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-6 custom-xxs-col">
                    <div class="banner-item img-hover_effect">
                        <div class="banner-img">
                            <a href="{{$banners->link4}}">
                                <img class="img-full lazy" src="{{asset($banners->banner4)}}" alt="Banner">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Banner Area Three End Here -->
    <br>
    <br>

    <!-- Product Area End Here -->
    <div class="grid-view_area">
        <div class="container">
            <div class="row">
                <div class="col-6">
                    <h3>Cartes cadeaux</h3>
                </div>
                <div class="col-6">
                    <a href="{{route('cards.viewall')}}"> <p style="text-align: right">Voir Tout</p></a>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-lg-12 order-lg-2 order-1">
                    <div class="row blog-item_wrap">
                        @foreach($cards as $card)
                            @include('include.card')
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>


    <br>
    <br>

    <br>
    <br>
    @if($saleproduct)
    <?php
    $product = \App\Product::where('id', $saleproduct->product_id)->first();
    ?>
    @if($product)
        @if(!$saleproduct->date->ispast())
        <div class="banner-area-3" style="background-color: #ffe1e7">
            <div class="container">
                <div class="row">
                    <div class="col-md-3" style="display: none" id="saleproduct1">
                        <img style="height: 300px; width: 100%" src="{{asset('front/assets/07.png')}}" alt="">
                    </div>
                    <div class="col-md-5">
                        <img class="img-full lazy responsive" style="width: 400px; float: right" src="{{asset($product->photo1)}}" alt="">
                    </div>
                    <div class="col-md-4" style="padding-top: 30px;">
                        <h1 style="padding-bottom: 10px">{{$product->title}}</h1>
                        <br>
                        <p style="font-size: 40px">{{$product->price}} €
                            @if($product->oldprice)
                                <del style="font-size: 30px"> {{$product->oldprice}} €</del></p>
                        @endif
                        <br>
                        <div class="countdown-wrap">
                            <div class="countdown item-4" data-countdown="{{$saleproduct->date->format('c')}}" data-format="short">
                                <div class="countdown__item">
                                    <span class="countdown__time daysLeft"></span>
                                    <span style="font-size: 10px" class="countdown__text daysText"></span>
                                </div>
                                <div class="countdown__item">
                                    <span class="countdown__time hoursLeft"></span>
                                    <span style="font-size: 10px" class="countdown__text hoursText"></span>
                                </div>
                                <div class="countdown__item">
                                    <span class="countdown__time minsLeft"></span>
                                    <span style="font-size: 10px" class="countdown__text minsText"></span>
                                </div>
                                <div class="countdown__item">
                                    <span class="countdown__time secsLeft"></span>
                                    <span style="font-size: 10px" class="countdown__text secsText"></span>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-8">
                                <a href="{{route('front.product', ['id' => $product->id])}}"><button class="mybutton">Acheter maintenant</button></a>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-3" id="saleproduct">
                        <img style="height: 300px; width: 100%" src="{{asset('front/assets/07.png')}}" alt="">
                    </div>
                </div>

            </div>
        </div>
        @endif
    @endif
    @endif
    <br>
    <br>


@if($gs->diva == 1)
    <div class="container">
    <div class="product-tab_area" style="padding: 0px">
        <a href="{{route('sellers.products')}}">
            <img class="lazy mdsdh" style="width: 100%" src="{{asset('front/assets/18.jpg')}}" alt="">
            <img class="lazy mdn" style="width: 100%;" src="{{asset('front/assets/08.png')}}" alt="">
        </a>
        <div class="row" id="questions">
            <div class="col-md-4">

            </div>
            <div class="col-md-4">
                <button class="mybutton">Conditions générales d'utilisation</button>
            </div>
        </div>
    </div>
    </div>
@endif

    <br>
    <br>
    <br>
    <br>

    <div style="background-image:url('{{asset($banners->videobanner)}}'); background-repeat: no-repeat; background-size: 100% 100%; text-align: center; ">
        <div id="pt35" style="padding-top: 200px; padding-bottom: 20px">
            <button id="myBtn" class="mybutton1">Appuyez sur PLAY</button>
        </div>
    </div>
    <!-- The Modal -->
    <div id="myModal" class="modal1">

        <!-- Modal content -->
        <div class="modal-content1">
            <span class="close1">&times;</span>

            <iframe id="video" width="520" height="345" src="https://www.youtube.com/embed/{{$banners->videoid}}">
            </iframe>

        </div>

    </div>


    <!-- Begin List Product Area -->
    <div class="list-product_area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <h3>Magasins SWEET DIVA</h3>
                        <div class="list-product_arrow"></div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="kenne-element-carousel list-product_slider slider-nav" data-slick-options='{
                        "slidesToShow": 3,
                        "slidesToScroll": 1,
                        "infinite": false,
                        "arrows": true,
                        "dots": false,
                        "spaceBetween": 30,
                        "appendArrows": ".list-product_arrow"
                        }' data-slick-responsive='[
                        {"breakpoint":1200, "settings": {
                        "slidesToShow": 2
                        }},
                        {"breakpoint":768, "settings": {
                        "slidesToShow": 1
                        }},
                        {"breakpoint":575, "settings": {
                        "slidesToShow": 1
                        }}
                    ]'>

                        @foreach($shops as $shop)
                        <div class="product-item">
                            <div class="single-product">
                                <div class="product-img">
                                    <a href="">
                                        <img class="primary-img lazy" src="{{empty($shop->photo) ? asset('front/assets/images/avatar.png') : asset($shop->photo)}}" alt="Kenne's Product Image">
                                    </a>
                                </div>
                                <br>
                                <div class="product-content">
                                    <div class="product-desc_info">
                                        <h2 class="product-name"><a href="">{{$shop->name}}</a>
                                        </h2>
                                        <p> <i class="fa fa-phone"></i>  {{$shop->phone}}<br>
                                            <i class="fa fa-map-pin"></i>  <a href="https://maps.google.com/?q={{$shop->address}}"> {{$shop->address}}</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div style="background-image:url('{{asset('front/assets/121.jpg')}}'); background-repeat: no-repeat; background-size: 100% 100%; text-align: center; " class="mdn">
        <div class="row" style="padding-top: 50px;">
            <div class="col-md-2">
            </div>
            <div class="col-md-2">
                <a href="https://www.facebook.com/contact.sweetdiva/"> <button id="myBtn" class="mybutton1">Sweet Diva</button></a>
            </div>
            <div class="col-md-2" style="margin-left: -65px">
                <a href="https://www.facebook.com/FashionCod"><button id="myBtn" class="mybutton1">Fashion Cod</button></a>
            </div>
             <div class="col-md-2" style="margin-top: 150px; margin-bottom: 20px">
                 <a style="color: transparent; padding-top: 40px; padding-bottom: 230px" href="https://www.tiktok.com/@sweet_diva.fwi?lang=fr">sdhsdddddddjskjdsjd</a>
{{--                 <a href="https://www.tiktok.com/@sweet_diva.fwi?lang=fr"><button id="myBtn" class="mybutton1">Sweet Diva</button></a>--}}
             </div>
            <div class="col-md-2" style="margin-left: 125px">
                <a href="https://www.instagram.com/sweet_diva_fwi/"><button id="myBtn" class="mybutton1">Sweet Diva</button></a>
            </div>
            <div class="col-md-2" style="margin-left: -70px">
                <button id="myBtn" class="mybutton1">Fashion Cod</button>
            </div>
        </div>
    </div>
    <div class="mdsdh">
        <div class="container">
            <a href="https://www.tiktok.com/@sweet_diva.fwi?lang=fr"><img src="{{asset('front/assets/16.jpg')}}" style="width: 100%" alt=""></a>
            <div class="row">
                <div class="col-6">
                    <a href="https://www.instagram.com/sweet_diva_fwi/"><button class="mybutton">Sweet Diva</button></a>
                </div>
                <div class="col-6">
                    <a href="https://www.facebook.com/contact.sweetdiva/"><button class="mybutton">Sweet Diva</button></a>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-6">
                    <button class="mybutton">Fashion Cod</button>
                </div>
                <div class="col-6">
                    <a href="https://www.facebook.com/FashionCod"><button class="mybutton">Fashion Cod</button></a>
                </div>
            </div>
        </div>
    </div>
    <!-- List Product Area End Here -->
@endsection
@section('script')
    @guest
        <script type="text/javascript">
            setInterval(click, 10000);

            function click()
            {
                $('.minicart-btn1').trigger('click');
            }
            click();
        </script>
    @endguest
@endsection
