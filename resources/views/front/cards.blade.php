@extends('layouts.front')
@section('content')
    <div class="grid-view_area">
        <div class="container">
            <div class="row">
                <div class="col-6">
                    <h3>Cartes cadeaux</h3>
                </div>
                <div class="col-6">

                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-lg-12 order-lg-2 order-1">
                    <div class="row blog-item_wrap">
                        @foreach($cards as $card)
                            @include('include.card')
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
