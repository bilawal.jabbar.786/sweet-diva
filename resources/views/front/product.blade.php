@extends('layouts.front')
@section('content')
    <style>
        .div-inline-list{
            text-align: center;
        }
        .div-inline-list > .product-container{
            display: inline-block;
        }
        .product-container{
            position: relative;
            width: 100px;
            height: 100px;
            padding: 0px;
        }

        .product{
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);

        }
        .clickable{
            position: absolute;
            width: 100%;
            height: 100%;
            top: 0px;
            left: 0px;
            border: 1px solid green;
            border-radius: 4px;
            transition: all ease .5s;
            z-index: 10;
        }
        .checked-box{
            position: absolute;
            top: 0px;
            right: 0px;
            width: 22px;
            height: 22px;
            background-color: #0055ff;
            color: #fff;
            text-align: center;
            border-top-right-radius: 4px;
            display: none;
        }

        input[name="myradio"]:checked + .clickable{
            border-color: #0055ff;
            box-shadow: 0px 0px 6px 2px #0055ff;
        }

        input[name="myradio"]:checked + .clickable .checked-box{
            display: block;
        }
    </style>
    <!-- Begin Kenne's Single Product Variable Area -->
    <div class="sp-area">
        <div class="container">
            <div class="sp-nav">
                <div class="row">
                    <div class="col-lg-5">
                        <div class="sp-img_area">
                            <div class="sp-img_slider slick-img-slider kenne-element-carousel" data-slick-options='{
                                "slidesToShow": 1,
                                "arrows": false,
                                "fade": true,
                                "draggable": false,
                                "swipe": false,
                                "asNavFor": ".sp-img_slider-nav"
                                }'>
                                @if(!empty ( $product->gallery ))
                                    @foreach(json_decode($product->gallery, true) as $images)
                                        <div class="single-slide zoom">
                                            <img  src="{{asset('gallery/'.$images)}}" alt="Kenne's Product Image">
                                        </div>
                                    @endforeach
                                @endif
                                @if(!empty ( $product->colorimage ))
                                    @foreach(json_decode($product->colorimage, true) as $key => $images)
                                        <div class="single-slide {{json_decode($product->color, true)[$key]}} zoom">
                                            <img  src="{{asset('colorimage/'.$images)}}" alt="Kenne's Product Image">
                                        </div>
                                    @endforeach
                                @endif

                            </div>
                        </div>
                        <hr>
                        <p>{!! $product->description !!}</p>
                        <hr>
                    </div>
                    <div class="col-lg-7">
                        <div class="sp-content">
                            <div class="sp-heading">
                                <h5><a href="#">{{$product->title}}</a></h5>
                            </div>
                                <p>{{$product->categorytype->name }}</p>
                            <span class="reference">{{$product->maincategory->name }}</span>
                            <div class="sp-essential_stuff">
                                <ul>
                                    <li>Code produit: <a href="javascript:void(0)">{{$product->sku}}</a></li>
                                    @if($product->material)
                                    <li>Matière du produit: <a href="javascript:void(0)">{{$product->material}}</a></li>
                                    @endif
                                </ul>
                            </div>
                            <br>
                            <div class="sp-heading">
                                <h5>{{$product->price}} €
                                    @if($product->oldprice)
                                    <del> {{$product->oldprice}} € </del>
                                        @endif
                                </h5>
                            </div>
                                <div class="sp-img_slider-nav slick-slider-nav kenne-element-carousel arrow-style-2 arrow-style-3" data-slick-options='{
                                "slidesToShow": 3,
                                "asNavFor": ".sp-img_slider",
                                "focusOnSelect": true,
                                "arrows" : true,
                                "spaceBetween": 30
                                }' data-slick-responsive='[
                                        {"breakpoint":1501, "settings": {"slidesToShow": 3}},
                                        {"breakpoint":1200, "settings": {"slidesToShow": 2}},
                                        {"breakpoint":992, "settings": {"slidesToShow": 4}},
                                        {"breakpoint":768, "settings": {"slidesToShow": 3}},
                                        {"breakpoint":575, "settings": {"slidesToShow": 2}}
                                    ]'>
                                    @if(!empty ( $product->colorimage ))
                                        @foreach(json_decode($product->colorimage, true) as $key => $images)
                                            <div class="single-slide {{json_decode($product->color, true)[$key]}}">
                                                <img onclick="checkstock(this)" id="{{$product->id}}" color="{{json_decode($product->color, true)[$key]}}" src="{{asset('colorimage/'.$images)}}" alt="Kenne's Product Thumnail">
                                            </div>
                                        @endforeach
                                    @endif
                                    @if(!empty ( $product->gallery ))
                                        @foreach(json_decode($product->gallery, true) as $images)
                                            <div class="single-slide red">
                                                <img  src="{{asset('gallery/'.$images)}}" alt="Kenne's Product Thumnail">
                                            </div>
                                        @endforeach
                                    @endif


                                </div>
                            <form method="POST" action="{{route('product.cart')}}" class="formsubmit">
                                @csrf
                               {{-- @if($product->color)
                                    <div class="color-list_area" style="margin-top: 0px">
                                        <span class="sub-title">Couleurs</span>
                                        <div class="color-list">

                                            @foreach(json_decode($product->color, true) as $color)
                                                <a onclick="checkstock(this)" id="{{$product->id}}" color="{{$color}}" class="single-color" style="background-color: {{$color}}" data-swatch-color="{{$color}}">
                                                    <span style="background-color: {{$color}}"></span>
                                                </a>
                                            @endforeach

                                        </div>
                                    </div>
                                @endif--}}
                                <input type="hidden" class="givecolor" name="color" value="">
                            <div class="row">
                                <div class="col-md-12">
<!--                                    <span>Taille</span>
                                    <select required onchange="checksizestock(this)" id="{{$product->id}}" name="size" class="form-control sizeget">
                                        <option value="">Choisissez la couleur</option>
                                    </select>-->
    <br>
    <br>
    <p style="font-size: 20px">Disponibilité: <a href="javascript:void(0)" class="ps-success">Sélectionnez la taille</a></p>
                                    <div class="product-size_box">
                                        <div class="div-inline-list">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="product-size_box">
                                        <span style="font-size: 20px">Quantité</span>
                                        <input type="number" name="quantity" class="form-control quantity" max="0" min="0" value="0" >
                                    </div>
                                </div>
                            </div>
                                <div class="row" style="margin-top: 20px">
                                    <div class="col-md-12">
                                        @auth
                                            <a class="mybutton smsbutton" style="display: none; color: white; padding: 15px 0px;" data-toggle="modal" data-target="#myModal"  href="#">Me contacter lorsque ma taille est disponible à nouveau</a>
                                        @endauth
                                    </div>
                                </div>

                                <input type="hidden" name="id" value="{{$product->id}}">
                            <div class="qty-btn_area">
                                <ul>
                                    <li><input class="mybutton" value="Ajouter au panier" type="submit"></li>
                                 </ul>
                            </div>
                            </form><br>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Kenne's Single Product Variable Area End Here -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                    <form action="{{route('smssave')}}" method="POST">
                        @csrf
                        <h3>ENVOYEZ-MOI UN SMS QUAND
                            DISPONIBLE</h3>
                        @if(!empty ( $product->size ))
                            <select required name="size" id="{{$product->id}}" class="form-control">
                                <option>Sélectionnez la taille</option>
                                @foreach(json_decode($product->size, true) as $size)
                                    <option value="{{$size}}">{{$size}}</option>
                                @endforeach
                            </select>
                        @endif
                        <input type="hidden" value="{{$product->id}}" name="product_id">
                        <br>
                        <button class="mybutton">Me contacter lorsque ma taille est disponible à nouveau</button>
                        <br>
                        <br>
                    </form>
                    <p>Nous vous informerons lorsque ce produit sera en stock Nous ne partageons votre numéro de téléphone avec personne d'autre.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                </div>
            </div>

        </div>
    </div>


@endsection
@section('script')
    <script>
        jQuery(function(){
            jQuery('#{{$product->id}}').click();
        });
        $(document).ready(function(){
            //on radio button "myradio" state change log to the console to view
            $('input[name="myradio"]').change(function(){
                console.log("#" + $('input[name="myradio"]:checked').attr('id'));
            });
        });
    </script>
    <script>
        function smsbox(){
          return   alert("asa");
        }
        function checkstock(elem){
            $('.quantity').val(0);
            $('.quantity').attr('max', 0);
            $('.quantity').attr('min', 0);
            $(':input[type="submit"]').prop('disabled', true);
            let id = $(elem).attr("id");
            let color = $(elem).attr("color");
            $('.givecolor').val(color);
            $('.div-inline-list').empty();
            $('.ps-success').html("Sélectionnez la taille");
            let _token   = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url: "{{route('checkstock')}}",
                type:"POST",
                data:{
                    id:id,
                    _token: _token,
                    color: color,
                },
                success:function(response){
                    if(response.success == '0') {
                        return false;
                    }else {
                        if (response.size == 0){
                            $('.div-inline-list').append('<p>Taille non disponible</p>')
                        }else {
                            // $('.sizeget').append('<option>Sélectionnez la taille</option>')
                            $.each(response.size, function(i, item) {
                                // $('.sizeget').append('<option value="'+item+'">'+item+'</option>')
                                $('.div-inline-list').append(' <div class="product-container">\n' +
                                    '                                                <input onchange="checksizestock(this)" id="'+item+'" value="'+item+'" type="radio" name="size">\n' +
                                    '                                                <label for="'+item+'" class="clickable"><span class="checked-box">&#10004;</span></label>\n' +
                                    '                                                <p class="product">'+item+'</p>\n' +
                                    '                                            </div>')
                            });
                        }

                    }
                },
            });
        }

        function checksizestock(elem){
            $('.quantity').val(0);
            $('.quantity').attr('max', 0);
            $('.quantity').attr('min', 0);
            $(':input[type="submit"]').prop('disabled', true);
            let id = {{$product->id}};
            let size = $(elem).attr("id");
            let color = $('.givecolor').val();
            // let size = $(elem).find(":selected").val()
            let _token   = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url: "{{route('checksizestock')}}",
                type:"POST",
                data:{
                    id:id,
                    _token: _token,
                    color: color,
                    size: size
                },
                success:function(response){
                    if(response.success == '0') {
                        $('.quantity').val(0);
                        $('.ps-success').html("EN RUPTURE DE STOCK");
                        $('.smsbutton').show();
                        $(':input[type="submit"]').prop('disabled', true);
                    }else {
                        $('.quantity').attr('max', response.quantity);
                        $('.quantity').attr('min', 1);
                        $('.ps-success').html("EN STOCK (" + response.quantity + ")");
                        $('.smsbutton').hide();
                        $(':input[type="submit"]').prop('disabled', false);
                        $.each(response.size, function(i, item) {
                                $('.sizeget').append('<option value="'+item+'">'+item+'</option>');
                        });
                    }
                },
            });
        }
    </script>
@endsection
