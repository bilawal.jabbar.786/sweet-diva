@extends('layouts.front')
<style>

</style>
@section('content')
    <div class="kenne-wishlist_area">
        <div class="container">
            <div class="row" >
                <div class="col-sm-12 col-md-6 col-lg-6 col-xs-12" style="margin-left: 290px;">
                    <form action="{{route('user.contact.store')}}" method="POST">
                        @csrf
                        <div class="login-form">
                            <h4 class="login-title">Client Soutien</h4>
                            <div class="row">
                                <div class="col-md-12 col-12">
                                    <label>Nom</label>
                                    <input class="form-control" type="text" name="name" required placeholder="Nom">
                                </div>

                                <div class="col-md-12">
                                    <label> E-mail*</label>
                                    <input class="form-control" name="email" type="email" required placeholder="Adresse e-mail">
                                </div>

                                <div class="col-md-12">
                                    <label>Subject*</label>
                                    <input class="form-control" type="text" required name="subject" placeholder="Subject">
                                </div>
                                <div class="col-md-12">
                                    <label>Message*</label>
                                    <textarea class="form-control"  required name="message" placeholder="Message"></textarea>

                                </div>

                                <div class="col-12">
                                    <button class="kenne-register_btn">Soumettre</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
