@extends('layouts.front')
@section('content')
    <div class="kenne-instagram_area" style="background-color: #ffe1e7">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="kenne-section_area">
                        <h3>ACHETER PAR CATEGORIES</h3>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="kenne-element-carousel instagram-slider arrow-style arrow-style-3" data-slick-options='{
                    "slidesToShow": 5,
                    "slidesToScroll": 1,
                    "infinite": false,
                    "arrows": true,
                    "dots": false,
                    "spaceBetween": 30
                    }' data-slick-responsive='[
                    {"breakpoint":1200, "settings": {
                    "slidesToShow": 5
                    }},
                    {"breakpoint":992, "settings": {
                    "slidesToShow": 4
                    }},
                    {"breakpoint":768, "settings": {
                    "slidesToShow": 3
                    }},
                    {"breakpoint":576, "settings": {
                    "slidesToShow": 2
                    }},
                    {"breakpoint":480, "settings": {
                    "slidesToShow": 1
                    }}
                ]'>

                        @foreach($maincategory as $cat)
                            <div class="single-item img-hover_effect">
                                <div class="instagram-img">
                                    <a href="{{route('front.maincategory', ['id' => $cat->id])}}">
                                        <img  src="{{asset($cat->photo)}}">
                                    </a>
                                    <button class="mybutton">{{$cat->name}}</button>
                                </div>
                            </div>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </div>

<!-- Begin Kenne's Content Wrapper Area -->
<div class="kenne-content_wrapper">
    <div class="container">
        <div class="row">
            @include('include.filters')
            <div class="col-xl-9 col-lg-8 order-1 order-lg-2">
                <div class="shop-toolbar">
                    <div class="product-view-mode">
                        <a class="active grid-3" data-target="gridview-3" data-toggle="tooltip" data-placement="top" title="Vue grille"><i class="fa fa-th"></i></a>
                        <a class="list" data-target="listview" data-toggle="tooltip" data-placement="top" title="Affichage en liste"><i class="fa fa-th-list"></i></a>
                    </div>
                </div>
                <div class="shop-product-wrap grid gridview-3 row">
                    @foreach($products as $product)
                    <div class="col-lg-4 col-md-4 col-sm-6">
                        <div class="product-item">
                            <div class="single-product">
                                <div class="product-img">
                                    <a href="{{route('front.product', ['id' => $product->id])}}">
                                        <img class="primary-img" src="{{asset($product->photo1)}}" alt="Kenne's Product Image">
                                        <img class="secondary-img" src="{{asset($product->photo2)}}" alt="Kenne's Product Image">
                                    </a>
{{--                                    <span class="sticker">{{$product->maincategory->name}}</span>--}}
                                    <div class="add-actions">
                                        <ul>
                                            @auth
                                            <li><a onclick="addtowishlist(this)" id="{{$product->id}}" data-toggle="tooltip" data-placement="right" title="Add To Wishlist"><i
                                                        class="ion-ios-heart-outline"></i></a>
                                            </li>
                                            @endauth
                                        </ul>
                                    </div>
                                </div>
                                <div class="product-content">
                                    <div class="product-desc_info">
                                        <h3 class="product-name"><a href="{{route('front.product', ['id' => $product->id])}}">{{$product->title}}</a></h3>
                                        <div class="price-box">
                                            <span class="new-price">{{$product->price}} €</span>
                                            @if($product->oldprice)
                                            <span class="old-price">{{$product->oldprice}} €</span>
                                                @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="list-product_item">
                            <div class="single-product">
                                <div class="product-img">
                                    <a href="{{route('front.product', ['id' => $product->id])}}">
                                        <img src="{{asset($product->photo1)}}" alt="Kenne's Product Image">
                                    </a>
                                </div>
                                <div class="product-content">
                                    <div class="product-desc_info">
                                        <div class="price-box">
                                            <span class="new-price">{{$product->price}} €</span>
                                            @if($product->oldprice)
                                                <span class="old-price">{{$product->oldprice}} €</span>
                                            @endif
                                        </div>
                                        <h6 class="product-name"><a href="{{route('front.product', ['id' => $product->id])}}">
                                                {{$product->title}}
                                            </a></h6>
                                        <div class="product-short_desc">
                                            <p>
                                                {{$product->description}}
                                            </p>
                                        </div>
                                    </div>
                                    <div class="add-actions">
                                        <ul>
                                            @auth
                                            <li><a onclick="addtowishlist(this)" id="{{$product->id}}" data-toggle="tooltip" data-placement="top" title="Ajouter à la liste de souhaits"><i
                                                        class="ion-ios-heart-outline"></i></a>
                                            </li>
                                        @endauth
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                        @endforeach
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="kenne-paginatoin-area">
                            <div class="row">
                                <div class="col-lg-12">
                                    {{$products->links()}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Kenne's Content Wrapper Area End Here -->
@endsection
