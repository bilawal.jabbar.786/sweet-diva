@extends('layouts.front')
@section('content')
    <style>
        #card-element {
            border-radius: 4px 4px 0 0;
            border: 1px solid rgba(50, 50, 93, 0.1);
            width: 100%;
            background: white;
            padding-left: 12px;
            padding-right: 12px;
        }

        #payment-request-button {
            margin-bottom: 32px;
        }
    </style>
    <!-- Begin Kenne's Checkout Area -->
    <div class="checkout-area">
        <div class="container">
            <form action="{{route('checkout.submit')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-lg-6 col-12">
                        @if(Session::has('success'))
                            <div class="alert">
                                <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
                                <strong>{{ Session::get('success') }}!</strong>
                            </div>
                        @endif
                        <div class="checkbox-form">
                            <h3>Détails de la facturation</h3>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="country-select clearfix">
                                        <label>Pays <span class="required">*</span></label>
                                        <select name="country" class="form-control" id="country">
                                            @foreach($countries as $count)
                                                <option value="{{$count->name}}"
                                                        @if(auth()->user()->country == $count->name) selected @endif>{{$count->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="checkout-form-list">
                                        <label>Nom complet<span class="required">*</span></label>
                                        <input class="form-control" name="name"
                                               value="{{auth()->user()->name.' '.auth()->user()->surname}}" type="text"
                                               required>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="checkout-form-list">
                                        <label>Détails d'expédition<span class="required">*</span></label>
                                        <input class="form-control" placeholder="Maison" name="house" type="text" required><br>
                                        <input class="form-control" placeholder="Rue" name="street" type="text" required><br>
                                        <input class="form-control" placeholder="Adress" name="address" type="text" required><br>
                                        <input class="form-control" placeholder="Ville" name="city" type="text" required><br>


                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="checkout-form-list">
                                        <label>E-mail <span class="required">*</span></label>
                                        <input class="form-control" name="email" type="email"
                                               value="{{auth()->user()->email}}" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="checkout-form-list">
                                        <label>Telephone <span class="required">*</span></label>
                                        <input class="form-control" type="text" name="phone"
                                               value="{{auth()->user()->phone}}" required>
                                    </div>
                                </div>
                            </div>
                            <div class="different-address">
                                <div class="order-notes">
                                    <div class="checkout-form-list checkout-form-list-2">
                                        <label>Notes de commande</label>
                                        <textarea class="form-control" name="notes" id="checkout-mess" cols="30"
                                                  rows="10"
                                                  placeholder="Notes sur votre commande, par ex. notes spéciales pour la livraison."></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-12">
                        <div class="your-order">
                            <h3>Votre commande</h3>
                            <div class="your-order-table table-responsive">
                                <table class="table">
                                    <tbody>
                                    @foreach($cartitems as $items)
                                        <tr class="cart_item">
                                            <td class="cart-product-name"> {{$items->name}}<strong
                                                    class="product-quantity">× {{$items->quantity}}</strong></td>
                                            <td class="cart-product-total"><span class="amount">{{$items->price * $items->quantity}} €</span>
                                            </td>
                                        </tr>
                                    @endforeach
                                    <tr class="cart_item">
                                        <td class="cart-product-name"><strong class="product-quantity">Frais de
                                                livraison </strong></td>
                                        <td class="cart-product-total"><span class="amount deliverycharges">5 €</span>
                                        </td>
                                    </tr>
                                    </tbody>
                                    <tfoot>
                                    <tr class="order-total">
                                        <th>Total de la commande</th>
                                        <td><strong>
                                                <span class="finalamount">{{$total}} €</span>
                                            </strong></td>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <input type="hidden" value="5" class="delivery" name="shipping">
                            <input type="hidden" value="{{$total}}" class="totalamount" name="totalamount">
                            <input type="hidden" value="{{$total}}" class="finalamount1" name="finalamount">
                            @auth
                                <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                            @endauth

                            <div class="payment-method">
                                <div class="payment-accordion">
                                    <div class="order-button-payment">
                                        <p>Avez-vous un code promo ou une carte-cadeau ? <a href="javascript:void(0)"
                                                                                            data-toggle="modal"
                                                                                            data-target="#myModal">
                                                Cliquez ici</a></p>
                                        <div class="clearfix">
                                            <label>Moyen de paiement <span class="required">*</span></label>
                                            <select name="pay" class="form-control">
                                                @if($total > 150)
                                                    <option value="installments">Payer en plusieurs fois avec ALMA
                                                    </option>
                                                @endif
                                                <option value="complete">Payer en 1 seule fois par carte bancaire</option>
                                                    @if($gs->padstatus==1)
                                                    <option value="pad">PAD( Paiement à Distance)</option>
                                                    @endif
                                            </select>
                                        </div>
                                        {{--                                    <div class="form-group stripe-payment-method-div">--}}
                                        {{--                                      <label>{{ __('Carte bancaire') }}</label> <span class="text-danger">*</span>--}}
                                        {{--                                      <div id="card-element"></div>--}}
                                        {{--                                      <div id="card-errors" class="text-danger" role="alert"></div>--}}
                                        {{--                                    </div>--}}
                                        {{--                                    <input id="card-button" value="Payer" type="submit" data-secret="{{ $intent }}">--}}
                                        <input value="Payer" type="submit">
{{--                                        <button type="submit"> Paye</button>--}}
                                    </div>
                                </div>
                            </div>
                            <br>
                            @auth()
                                <?php
                                $user = Auth::user();
                                $points = \App\Points::find(1);
                                ?>
                                @if($user->role == '2')
                                    <p>Votre solde Sweeties <b style="color: red">{{$user->wallet}}</b> (1 Sweeties égal
                                        à 1 €)</p>
                                    <a href="{{route('redeem.sweeties')}}" style="color: white" class="mybutton">Utiliser
                                        Sweeties</a>
                                @endif
                            @endauth
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- Kenne's Checkout Area End Here -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                    <form action="{{route('redeem.voucher')}}" method="POST">
                        @csrf
                        <input required name="code" placeholder="Entrer le code du bon" class="form-control">
                        <br>
                        <input type="hidden" name="r_id" value="">
                        <div class="row">
                            <div class="col-md-6">
                                <button class="mybutton">Envoyer</button>
                            </div>
                        </div>
                    </form>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('script')
    <script>
        var total = $('.totalamount').val();

        function cal() {
            var total = $('.totalamount').val();
            var delivery = $('.delivery').val();

            var finalamount = (+total) + (+delivery);
            $('.finalamount').html(finalamount + " €");
            $('.finalamount1').val(finalamount);
        }

        $(document).ready(function () {
            $('#country').on('change', function () {
                console.log(elemeen);
                if (this.value == 'Guadeloupe' || this.value == 'Martinique' || this.value == 'Saint Martin (French part)') {
                    $('.delivery').val(5);
                    $('.deliverycharges').html(5 + " €");
                    cal();
                }
                else if (this.value == 'France') {
                    $('.delivery').val(20);
                    $('.deliverycharges').html(20 + " €");
                    cal();
                }
                else {
                    $('.delivery').val(10);
                    $('.deliverycharges').html(10 + " €");
                    cal();
                }
            });
            var elemeen = jQuery('#country').val();
            console.log(elemeen);
            if (elemeen == 'Guadeloupe' || elemeen == 'Martinique'||  elemeen == 'Saint Martin (French part)') {
                $('.delivery').val(5);
                $('.deliverycharges').html(5 + " €");
                cal();
            } else if (elemeen == 'France') {
                $('.delivery').val(20);
                $('.deliverycharges').html(20 + " €");
                cal();
            } else {
                $('.delivery').val(10);
                $('.deliverycharges').html(10 + " €");
                cal();
            }
        });

        cal();
    </script>
@endsection
