<!DOCTYPE html>
<html lang="zxx">
<head>
    <meta charset="utf-8" />
    <title>LPDF</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="keywords" />
    <meta content="" name="author" />

    <!--[if lt IE 9]>
    <script src="{{asset('comming/js/html5shiv.js')}}"></script>
    <![endif]-->

    <!-- CSS Files
        ================================================== -->
    <link href="{{asset('comming/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('comming/css/jpreloader.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('comming/css/animate.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('comming/css/owl.theme.css')}}css/owl.theme.css" rel="stylesheet" type="text/css" />
    <link href="{{asset('comming/css/owl.theme.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('comming/css/owl.transitions.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('comming/css/magnific-popup.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('comming/css/jquery.countdown.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('comming/css/style.css')}}" rel="stylesheet" type="text/css" />

    <!-- color scheme -->
    <link id="colors" href="{{asset('comming/css/colors/scheme-01.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('comming/css/coloring.css')}}" rel="stylesheet" type="text/css" />
    <style>
        @media only screen and (max-width: 600px) {
            #heightlogo{
                height: 75px !important;
            }
        }
        #heightlogo{
            height: 150px
        }
        a, a:hover, .id-color, #mainmenu li:hover > ul, .date-box .day, .slider_text h1, h1.id-color, h2.id-color, h3.id-color, h4.id-color, .pricing-box li h1, .title span, i.large:hover, .feature-box-small-icon-2 i, .pricing-dark .pricing-box li.price-row, .ratings i, header.smaller #mainmenu a.active, .pricing-dark .pricing-box li.price-row, .dark .feature-box-small-icon i, a.btn-slider::after, a.btn-line::after, .team-list .social a, .de_contact_info i, .dark .btn-line:hover::after, .dark a.btn-line:hover::after, .dark a.btn-line.hover::after, a.btn-text::after, .separator span i, address span strong, .de_tab.tab_steps .de_nav li span:hover, .de_testi_by, .widget_tags li a, .dark .btn-line::after, .dark a.btn-line::after, .crumb a, .btn-right::after, .btn-left::before, #mainmenu li a::after, header .info .social i:hover, #back-to-top:hover::before, #services-list li.active, #services-list li.active a::after, .testimonial-list::before, span.deco-big, h2.hs1 span, .wm, .wm2, .blog-list .date-box .day, .social-icons-sm i, .de_tab.tab_style_4 .de_nav li span, .schedule-item .sc-name, .de_testi.opt-2 blockquote::before, .pricing-s1 .bottom i, .profile_pic .subtitle, .countdown-s3 .countdown-period, .countdown-s4 .countdown-period, .social-icons i:hover, a.btn-link, blockquote.s1::before, .accordion a::before, .expand-custom .toggle::before, .sitemap.s1 li::before, .list li::before, .post-meta span::before, footer .widget a:hover, .mask .cover .date::before, .feature-box-type-2 i, .pricing-s2 .bottom i, .post-text h3 a:hover, .pricing-s1 .top h2, .demo-icon-wrap i, .demo-icon-wrap-s2 span, a.btn-border.light:hover, blockquote.testimonial-big::before, #menuside li a.active, h3.d_timeline-title, .h3-s1, ul.info-list li .d_title, .d_timeline-text .d_title span, #mainmenu a:hover, .date-box, .countdown-section {
            color: pink;
        }
    </style>
</head>

<body>
<div id="wrapper">


    <div class="transition"></div>

    <!-- header begin -->
    <header class="transparent">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="d-flex justify-content-between">
                        <div class="align-self-center header-col-left">
                            <!-- logo begin -->
                            <div id="logo">
                                <a>
                                    <img id="heightlogo" src="{{asset($gs->logo)}}" alt="">
                                </a>
                            </div>
                            <!-- logo close -->
                        </div>

                        <div class="align-self-center ml-auto header-col-right">
                            <div class="social-icons sc-plain">
                                <a href="{{$gs->facebook}}"><i class="fa fa-facebook fa-lg"></i></a>

                                <a href="{{$gs->instagram}}"><i class="fa fa-instagram fa-lg"></i></a>
                            </div>

                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- header close -->
    <!-- content begin -->
    <div class="no-bottom no-top" id="content">
        <div id="top"></div>

        <section id="section-main" class="vertical-center text-light" data-bgimage="url({{asset('comming/images/background/6.jpg')}}) top">
            <div id="particles-js"></div>
            <div id="v-wrap">
                <div class="vw-inner">
                    <div class="vw-col"><i class="fa fa-mobile id-color"></i> {{$gs->phone}}</div>
                </div>
            </div>
            <div class="container">
                <div class="row wow fadeIn" data-wow-delay=".3s">
                    <div class="col-md-8 offset-md-2">
                        <div id="defaultCountdown"></div>
                        <div class="spacer-single"></div>
                    </div>
                    <div class="col-lg-6 offset-lg-3 text-center">
                        <h1>Avant de lancer notre <span class="id-color">Nouveau site Web</span></h1>
                    </div>
                    <div class="col-lg-6 offset-lg-3 text-center">
                        <p>Bienvenue sur le www.lepalaisdesfemmes.com
                            Le site internet qui séduira toutes les femmes…
                            Vous y retrouvez entre autres votre boutique SWEET DIVA pour vos achats en ligne sans bouger de chez vous!</p>
                        <div class="spacer-10"></div>

                    </div>
                </div>
            </div>
        </section>



    </div>
    <!-- content close -->

</div>



<!-- Javascript Files
    ================================================== -->
<script src="{{asset('comming/js/jquery.min.js')}}"></script>
<script src="{{asset('comming/js/jpreLoader.min.js')}}"></script>
<script src="{{asset('comming/js/bootstrap.min.js')}}"></script>
<script src="{{asset('comming/js/wow.min.js')}}"></script>
<script src="{{asset('comming/js/jquery.isotope.min.js')}}"></script>
<script src="{{asset('comming/js/easing.js')}}"></script>
<script src="{{asset('comming/js/owl.carousel.js')}}"></script>
<script src="{{asset('comming/js/validation.js')}}"></script>
<script src="{{asset('comming/js/jquery.magnific-popup.min.js')}}"></script>
<script src="{{asset('comming/js/enquire.min.js')}}"></script>
<script src="{{asset('comming/js/jquery.stellar.min.js')}}"></script>
<script src="{{asset('comming/js/jquery.plugin.js')}}"></script>
<script src="{{asset('comming/js/jquery.countdown.js')}}"></script>
<script src="{{asset('comming/js/typed.js')}}"></script>
<script src="{{asset('comming/js/typed-custom.js')}}"></script>
<script src="{{asset('comming/js/particles.js')}}"></script>
<script src="{{asset('comming/js/app.js')}}"></script>
<script src="{{asset('comming/js/designesia.js')}}"></script>

<!-- Countdown
    ================================================== -->
<script>
    jQuery(document).ready(function() {
        $(function () {
            $('#defaultCountdown').countdown({until: new Date(2021, 13-1, 6, 17)}); // year, month (value-1), date, hour
        });
    });
</script>
</body>
</html>
