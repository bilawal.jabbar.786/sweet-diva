<?php
$gs = \App\GeneralSettings::find(1);

    if($gs->working == 1 && $gs->fashioncod == 1) {
        $maincategories = \App\MainCategory::all();
    }else if($gs->working == 1){
        $maincategories = \App\MainCategory::where('category_id', '2')->orwhere('category_id', '1')->get();
    }else if($gs->fashioncod == 1) {
        $maincategories = \App\MainCategory::where('category_id', '3')->orwhere('category_id', '1')->get();
    }else{
     $maincategories = \App\MainCategory::where('category_id', '1')->get();
    }

?>

<div class="col-xl-3 col-lg-4 order-2 order-lg-1">
    <div class="kenne-sidebar-catagories_area">
        <div class="kenne-sidebar_categories">
            <div class="kenne-categories_title first-child">
                <h5>Filtrer par prix</h5>
            </div>
            <div class="price-filter">
                <form action="{{route('price.filter')}}" method="POST">
                    @csrf
                <div id="slider-range"></div>
                <div class="price-slider-amount">
                    <div class="label-input">
                        <label>prix : </label>
                        <input type="text" id="amount" name="price" placeholder="Add Your Price" />
                        <button class="filter-btn">Valider</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
        <div class="kenne-sidebar_categories category-module">
            <div class="kenne-categories_title">
                <h5>Catégories de produits</h5>
            </div>
            <div class="sidebar-categories_menu">
                <ul>
                    @foreach($maincategories as $cat)
{{--                        class="has-sub"--}}
                    <li><a href="{{route('front.maincategory', ['id' => $cat->id])}}">{{$cat->name}}<i class="ion-ios-plus-empty"></i></a>
                        <ul style="display: block">
                            @foreach($cat->subcategories as $subcat)
                            <li><a href="{{route('front.subcategory', ['id' => $subcat->id])}}">{{$subcat->name}}</a></li>
                            @endforeach
                        </ul>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>
