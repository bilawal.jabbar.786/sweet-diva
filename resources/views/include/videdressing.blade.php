<?php
use App\Messages;use App\Order;$maincategories = \App\MainCategory::all();
?>
<div class="col-xl-3 col-lg-4 order-2 order-lg-1" style="background-color: black">
    <div class="kenne-sidebar-catagories_area" style="background-color: black">
        <div class="kenne-sidebar_categories category-module" style="background-color: black">

            <div class="kenne-categories_title" style="padding-top: 20px">
                <a href="{{route('sellers.products')}}"><h5 style="color: white; background-color: red; text-align: center; padding-bottom: 10px; padding-top: 10px">Vide Dressing</h5></a>
                <br>
                <h5 style="color: white">Catégories de produits</h5>
            </div>
            <div class="sidebar-categories_menu" style="background-color: black">
                <ul>
                        <li>
                            <a style="color: white" href="{{route('vendor.categories', ['category' => 'ROBE'])}}">- ROBE</a>
                        </li>
                        <li>
                            <a style="color: white" href="{{route('vendor.categories', ['category' => 'COMBI'])}}">- COMBI</a>
                        </li>
                        <li>
                            <a style="color: white" href="{{route('vendor.categories', ['category' => 'PANTALON'])}}">- PANTALON</a>
                        </li>
                        <li>
                            <a style="color: white" href="{{route('vendor.categories', ['category' => 'JUPE'])}}">- JUPE</a>
                        </li>
                        <li>
                            <a style="color: white" href="{{route('vendor.categories', ['category' => 'SHORT'])}}">- SHORT</a>
                        </li>
                        <li>
                            <a style="color: white" href="{{route('vendor.categories', ['category' => 'TOP'])}}">- TOP</a>
                        </li>
                        <li>
                            <a style="color: white" href="{{route('vendor.categories', ['category' => 'JEAN'])}}">- JEAN</a>
                        </li>
                        <li>
                            <a style="color: white" href="{{route('vendor.categories', ['category' => 'VESTE'])}}">- VESTE</a>
                        </li>
                        <li>
                            <a style="color: white" href="{{route('vendor.categories', ['category' => 'AUTRE'])}}">- AUTRE</a>
                        </li>
                </ul>
            </div>
        </div>
        <br>
    </div>
    @php

        if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on')
        {
          $link = "https";
        }
        else
        {
          $link = "http";

          // Here append the common URL characters.
          $link .= "://";

          // Append the host(domain name, ip) to the URL.
          $link .= $_SERVER['HTTP_HOST'];

          // Append the requested resource location to the URL
          $link .= $_SERVER['REQUEST_URI'];
        }

    @endphp
    @auth
    @if(Auth::user()->role == '2')
        <div class="kenne-sidebar-catagories_area" style="background-color: black">
            <div class="kenne-sidebar_categories category-module" style="background-color: black">
                <div class="kenne-categories_title">
                    <h5 style="color: white">Espace Vente</h5>
                </div>
                <div class="sidebar-categories_menu" style="background-color: black">
                    <?php
                    $messagescount = Messages::where('r_id', Auth::user()->id)->where('is_seen',1)->get()->count();
                    $orderscount = Order::where('user_id', Auth::user()->id)->where('paymentstatus', '1')->where('status', '0')->count();
                    ?>
                    <ul style="background-color: #584545">
                        <li style="padding: 10px; {{ $link == route('sellers.products') ? 'background-color: #e52828':'' }}">
                            <a style="color: white; padding-bottom: 0px" href="{{route('sellers.products')}}">Vide Dressing</a>
                        </li>
                        <li style="padding: 10px; {{ $link == route('vendor.product') ? 'background-color: #e52828':'' }}">
                            <a style="color: white; padding-bottom: 0px" href="{{route('vendor.product')}}">Ajouter un produit</a>
                        </li>
                        <li style="padding: 10px; {{ $link == route('vendor.products') ? 'background-color: #e52828':'' }}">
                            <a style="color: white; padding-bottom: 0px" href="{{route('vendor.products')}}">Tous les produits</a>
                        </li>
                        <li style="padding: 10px; {{ $link == route('vendor.orders') ? 'background-color: #e52828':'' }}">
                            <a style="color: white; padding-bottom: 0px" href="{{route('vendor.orders')}}">Nouvelles commandes <span class="badge badge-warning">{{$orderscount}}</span></a>
                        </li>

                        <li style="padding: 10px; {{ $link == route('vendor.messages') ? 'background-color: #e52828':'' }}">
                            <a style="color: white; padding-bottom: 0px" href="{{route('vendor.messages')}}">Messages  <span class="badge badge-warning">{{$messagescount}}</span></a>
                        </li>
                        <li style="padding: 10px;">
                            <a style="color: white; padding: 0px" href="{{route('logout')}}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"
                               class="nav-link">
                                <p>
                                    Déconnecter
                                </p>
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
            <br>
        </div>
    @endif
    @endauth
</div>
