<div class="col-lg-4">
    <div class="blog-item">
        <div class="blog-img">
            <a href="{{route('card.view', ['id' => $card->id])}}">
                <img src="{{asset($card->photo)}}" alt="Card Image">
            </a>
        </div>
        <div class="blog-content">
            <h3 class="heading">
                <a href="{{route('card.view', ['id' => $card->id])}}">{{$card->title}}</a>
            </h3>
            <p class="short-desc">
                Code du bon cadeau : {{$card->sku}}
            </p>
        </div>
    </div>
</div>
