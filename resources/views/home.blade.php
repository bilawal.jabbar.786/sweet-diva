@extends('layouts.admin')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-8">
                        <h1 class="m-0">Bienvenue dans le tableau de bord d'administration</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-4">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Domicile</a></li>
                            <li class="breadcrumb-item active">Tableau de bord</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-primary">
                            <div class="inner">
                                <h3>{{$app_users}}</h3>

                                <p>Application Mobile Client Total</p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-folder-plus"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-secondary">
                            <div class="inner">
                                <h3>{{$website_users}}</h3>

                                <p>Site Internet Client Total</p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-folder-plus"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-success">
                            <div class="inner">
                                <h3>{{$adminproducts}}</h3>

                                <p>Produits d'administration</p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-folder-plus"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-warning">
                            <div class="inner">
                                <h3>{{$videdressingproducts}}</h3>

                                <p>Vide Dressing Produits</p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-folder-plus"></i>
                            </div>
                        </div>
                    </div>
                    @if(Auth::user()->role == 0)
                        <div class="col-lg-3 col-6">
                            <!-- small box -->
                            <div class="small-box bg-success">
                                <div class="inner">
                                    <h3>{{$adminordersnew}}</h3>

                                    <p>Admin Nouvelles commandes</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-folder-plus"></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-6">
                            <!-- small box -->
                            <div class="small-box bg-warning">
                                <div class="inner">
                                    <h3>{{$videdressingordersnew}}</h3>

                                    <p>Vide Dressing Nouvelles commandes</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-folder-plus"></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-6">
                            <!-- small box -->
                            <div class="small-box bg-danger">
                                <div class="inner">
                                    <h3>{{$gitcard}}</h3>

                                    <p>Vente de cartes-cadeaux</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-folder-plus"></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-6">
                            <!-- small box -->
                            <div class="small-box bg-dark">
                                <div class="inner">
                                    <h3>{{$sum}}€</h3>

                                    <p>Revenu total</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-folder-plus"></i>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>

            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
