@extends('layouts.admin')

@section('content')
    <style>.ms-auto {
            margin-left: auto !important;
        }</style>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <!-- <h1>DataTables</h1> -->
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Accueil</a></li>
                            <li class="breadcrumb-item active">Détail de la commande</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">

                        <div class="card">
                            <div class="card-header py-3">
                                <div class="row g-3 align-items-center">
                                    <div class="col-md-4 me-auto">
                                        <h5 class="mb-1">{{$order->updated_at}}</h5>
                                        <p class="mb-0">Commande {{$order->order_number}}</p>
                                    </div>

                                    <div class="col-md-4">
                                        <a href="{{route('order.status', ['id' => $order->id])}}">
                                            <button class="btn alert-success btn-sm radius-30 px-4">Marquer la commande
                                                comme terminée
                                            </button>
                                        </a>
                                        @if($order->payment_method=='pad' && $order->paymentstatus!=2 )
                                            <a href="{{route('orders.payment.success', ['id' => $order->id])}}"
                                               style="margin-left: 20px;">
                                                <button class="btn alert-warning radius-30 px-4">Annuler la commande
                                                </button>
                                            </a>
                                        @endif
                                        <button class="btn btn-primary btn-sm  radius-30 px-4"
                                                data-toggle="modal" data-target="#exampleModal"> Livrer la commande
                                        </button>
                                    </div>


                                    <!--<div class="col-12 col-lg-3 col-6 col-md-3">
                                      <select class="form-select">
                                        <option>Change Status (à venir)</option>
                                        <option>Awaiting Payment</option>
                                        <option>Confirmed</option>
                                        <option>Shipped</option>
                                        <option>Delivered</option>
                                      </select>
                                    </div>
                                    <div class="col-12 col-lg-3 col-6 col-md-3">
                                       <button type="button" class="btn btn-primary">Save</button>
                                       <button type="button" class="btn btn-secondary"><i class="bi bi-printer-fill"></i> Print</button>
                                    </div>-->
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="card border shadow-none radius-10">
                                            <div class="card-body">
                                                <div class="d-flex align-items-center gap-3">
                                                    <div class="icon-box bg-light-primary border-0">
                                                        <i class="bi bi-person text-primary"></i>
                                                    </div>
                                                    <div class="info">
                                                        <h6 class="mb-2">Client</h6>
                                                        <p class="mb-1">{{$order->name}}</p>
                                                        <p class="mb-1">{{$order->email}}</p>
                                                        <p class="mb-1">{{$order->phone}}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="card border shadow-none radius-10">
                                            <div class="card-body">
                                                <div class="d-flex align-items-center gap-3">
                                                    <div class="icon-box bg-light-success border-0">
                                                        <i class="bi bi-truck text-success"></i>
                                                    </div>
                                                    <div class="info">
                                                        <h6 class="mb-2">Info commande</h6>
                                                        <p class="mb-1"><strong>Livraison</strong> : Colissimo</p>
                                                        <p class="mb-1"><strong>Pay Method</strong>
                                                            : {{$order->payment_method}}</p>
                                                        <p class="mb-1"><strong>Status</strong>
                                                            : @if($order->status == '0')
                                                                Nouvelle commande
                                                            @else
                                                                Compléter
                                                            @endif</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="card border shadow-none radius-10">
                                            <div class="card-body">
                                                <div class="d-flex align-items-center gap-3">
                                                    <div class="icon-box bg-light-danger border-0">
                                                        <i class="bi bi-geo-alt text-danger"></i>
                                                    </div>
                                                    <div class="info">
                                                        <h6 class="mb-2">Livraison</h6>
                                                        <p class="mb-1"><strong>Note</strong> : {{$order->notes}}</p>
                                                        <p class="mb-1"><strong>Source</strong> : {{$order->resource}}
                                                        </p>
                                                        <p class="mb-1"><strong>Adresse</strong>
                                                            : {{$order->address}} {{$order->country}}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div><!--end row-->

                                <div class="row">
                                    <div class="col-12 col-lg-8">
                                        <div class="card border shadow-none radius-10">
                                            <div class="card-body">
                                                <div class="table-responsive">
                                                    <table class="table align-middle mb-0">
                                                        <thead class="table-light">
                                                        <tr>
                                                            <th>Produit</th>
                                                            <th>Taille</th>
                                                            <th>Couleurs</th>
                                                            <th>Quantité</th>
                                                            <th>Prix</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach(json_decode($order->products) as $item)
                                                            <tr>
                                                                <td>
                                                                    <div class="orderlist">
                                                                        <a class="d-flex align-items-center gap-2"
                                                                           href="javascript:;">
                                                                            <div class="row">
                                                                                <div class="col-md-8">
                                                                                    <p class="mb-0 product-title"><a
                                                                                            target="_blank"
                                                                                            href="{{route('front.product', ['id' => $item->id])}}"> {{\App\Product::find($item->id)->title??$item->name??'Produit Supreme'}}</a>
                                                                                    </p>
                                                                                </div>
                                                                                <div class="col-md-4">
                                                                                    <img src="{{asset(\App\Product::find($item->id)->photo1??'no-image.png')}}" style="height: 100px; border-radius: 5px" alt="">
                                                                                </div>
                                                                            </div>
                                                                        </a>
                                                                    </div>
                                                                </td>
                                                                <td>{{$item->attributes->size}}</td>
                                                                <td>
                                                                    <div
                                                                        style="background-color:{{$item->attributes->color}}; height: 40px; border-radius: 50px"></div>
                                                                </td>
                                                                <td>{{$item->quantity}}</td>
                                                                <td>{{$item->price.'€'}}</td>
                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-4">
                                        <div class="card border shadow-none bg-light radius-10">
                                            <div class="card-body">
                                                <div class="d-flex align-items-center mb-4">
                                                    <div>
                                                        <h5 class="mb-0">Résumé de la commande</h5>
                                                    </div>
                                                    <div class="ms-auto">
                                                        <!--<button type="button" class="btn alert-success radius-30 px-4">Confirmed</button>-->
                                                    </div>
                                                </div>
                                                <div class="d-flex align-items-center mb-3">
                                                    <div>
                                                        <p class="mb-0">Frais de livraison</p>
                                                    </div>
                                                    <div class="ms-auto">
                                                        <h5 class="mb-0">{{$order->shipping.'€' ?? '0'}}</h5>
                                                    </div>
                                                </div>
                                                <div class="d-flex align-items-center mb-3">
                                                    <div>
                                                        <p class="mb-0">TVA (8.5%)</p>
                                                    </div>
                                                    <div class="ms-auto">
                                                        <h5 class="mb-0"> à venir </h5>
                                                    </div>
                                                </div>
                                                <div class="d-flex align-items-center mb-3">
                                                    <div>
                                                        <p class="mb-0">Sous total</p>
                                                    </div>
                                                    <div class="ms-auto">
                                                        <h5 class="mb-0">{{$order->total.'€'}}</h5>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-12">
                                        <a href="{{route('order.download',['id'=>$order->id])}}" style="float:right;"
                                           class="btn btn-warning"><i class="fas fa-print"></i> Télécharger la
                                            facture</a>
                                        <a href="{{route('order.print',['id'=>$order->id])}}"
                                           style="float:right;margin-right: 20px;" class="btn btn-success"><i
                                                class="fas fa-print"></i> Print</a>
                                    </div>
                                </div>

                                <!--end row-->
                            </div>
                        </div>
                        <!-- /.card -->

                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Livrer la commande</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <form action="{{route('admin.confirmation.order')}}" method="post" enctype="multipart/form-data"
                          data-parsley-validate>
                        @csrf
                        <input type="hidden" name="order_no" value="{{$order->order_number}}">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="title"><b>Message </b><span class="text-danger">*</span></label>
                                <textarea class="form-control" rows="8" name="message" placeholder="Message">
                                Bonjour.

Merci pour votre commande !

Votre colis a été posté ce jour.
Le numéro de suivi est le ……

Vous pourrez suivre l’évolution de celui ci sur le site de La Poste
www.la poste.fr (rubrique suivre un envoi).

Nous restons à votre disposition si besoin.

Bien cordialement.

Sweet Diva
                            </textarea>
                            </div>
                        </div>
                        <div class="col-md-12 pull-right">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-block">Envoyer</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>


        </div>

    </div>

@endsection
