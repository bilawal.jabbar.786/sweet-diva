@extends('layouts.admin')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <!-- <h1>DataTables</h1> -->
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Accueil</a></li>
                            <li class="breadcrumb-item active">Toutes les commandes</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <!-- /.card -->

                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Toutes les commandes</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body table-responsive">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>N°</th>
                                        <th>Nom</th>
                                        <th>E-mail</th>
                                        <th>Téléphone</th>
                                        <th>Total</th>
                                        <th>Ressource</th>
                                        <th>Statut</th>
                                        <th>Date</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($orders as $row)
                                        <tr>
                                            <td>{{$row->order->order_number}}</td>
                                            <td>{{$row->order->name}}</td>
                                            <td>{{$row->order->email}}</td>
                                            <td>{{$row->order->phone}}</td>
                                            <td>
                                                @if($row->order->total == '0')
                                                    Points
                                                @else
                                                {{$row->order->total}} €</td>
                                                @endif

                                            <td>{{$row->order->resource}}</td>
                                            <td>
                                                @if($row->order->status == '0')
                                                    Nouvelle commande
                                                @elseif($row->order->status == '2')
                                                    Annuler
                                                @else
                                                    Compléter
                                                @endif
                                            </td>
                                            <td>{{$row->created_at->format('d-m-Y')}}</td>
                                            <td>
                                                <a href="{{route('order.view' , ['id'=>$row->order->id])}}" class="btn btn-sm btn-primary" data-toggle="tooltip" title="View">
                                                    <i class="fa fa-eye"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>

@endsection
