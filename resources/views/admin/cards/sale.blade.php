@extends('layouts.admin')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <!-- <h1>DataTables</h1> -->
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Domicile</a></li>
                            <li class="breadcrumb-item active">Vente de cartes-cadeaux</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <!-- /.card -->

                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Vente de cartes-cadeaux</h3>
                                <button class="btn btn-primary btn-sm float-right" data-toggle="modal" data-target="#exampleModal">Ajouter un nouveau</button>
                                <button class="btn btn-success btn-sm float-right mr-3" data-toggle="modal" data-target="#exampleModal1">Échanger un bon de réduction</button>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Code de la carte</th>
                                        <th>Nom de l'acheteur</th>
                                        <th>Téléphone de l'acheteur</th>
                                        <th>Nom du destinataire</th>
                                        <th>Courriel du destinataire</th>
                                        <th>Prix</th>
                                        <th>Message</th>
                                        <th>Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($cards as $row)
                                        <tr>
                                            <td>{{$row->card_number}}</td>
                                            <td>{{$row->sendername}}</td>
                                            <td>{{$row->senderphone}}</td>
                                            <td>{{$row->r_name}}</td>
                                            <td>{{$row->r_email}}</td>
                                            <td>{{$row->price}} €</td>
                                            <td>{{$row->message}}</td>
                                            <td>
                                                @if($row->valid == '1')
                                                    Pas valide
                                                @else
                                                    <a href="{{route('change.cardstatus', ['id' => $row->id])}}"> <button class="btn btn-primary btn-sm">Valide</button></a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ajouter une nouvelle</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <form action="{{route('card-admin.store')}}" method="post" enctype="multipart/form-data" data-parsley-validate>
                        @csrf
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="title"><b>Select Card</b><span class="text-danger">*</span></label>
                                <select name="card_id" class="form-control" id="">
                                    @foreach($allcards as $crad)
                                    <option value="{{$crad->sku}}">{{$crad->title}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <input type="hidden" class="form-control" value="00000000" name="senderphone" required>
                        <input type="hidden" class="form-control" value="Admin" name="sendername" required>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="title"><b>Nom du destinataire</b><span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="r_name" required>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="title"><b>Courriel du destinataire</b><span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="r_email" required>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="title"><b>Message</b><span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="message" required>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="title"><b>Prix</b><span class="text-danger">*</span></label>
                                <input type="number" class="form-control" name="price" required>
                            </div>
                        </div>

                        <div class="col-md-12 pull-right">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-block">Sauvegarder</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>



        </div>

    </div></div>
 <!-- Modal -->
    <div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Échanger le bon</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <form action="{{route('admin.reddem.voucher')}}" method="post" enctype="multipart/form-data" data-parsley-validate>
                        @csrf
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="title"><b>Entrer le code du bon</b><span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="code" placeholder="Entrer le code du bon">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="title"><b>Courriel du client</b><span class="text-danger">*</span></label>
                                <input type="email" class="form-control" name="email" placeholder="Courriel du client">
                            </div>
                        </div>
                        <div class="col-md-12 pull-right">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-block">Sauvegarder</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>



        </div>

    </div></div>

@endsection
