@extends('layouts.admin')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <!-- <h1>DataTables</h1> -->
                    </div>
                    <div class="col-sm-6">

                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Domicile</a></li>
                            <li class="breadcrumb-item active">Sweet Looks</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <!-- /.card -->
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-md-2">
                                        <h3 class="card-title">Sweet Look</h3>
                                    </div>
                                    <div class="col-md-3">
                                        <select onchange="location = this.value;" class="form-control" name="" id="">
                                            <option value="#">Filtrer les messages</option>
                                            <option value="{{route('filter.posts', ['type' => 'astuce'])}}">Astuce
                                            </option>
                                            <option value="{{route('filter.posts', ['type' => 'between'])}}">
                                                Entre-nous
                                            </option>
                                        </select>
                                    </div>
                                    <div class="col-md-7">
                                        <button data-toggle="modal" data-target="#modalpost"
                                                class="btn btn-warning btn-sm float-right ml-1">Ajouter un post
                                        </button>
                                        <button data-toggle="modal" data-target="#modalastuce"
                                                class="btn btn-success btn-sm float-right ml-1">Ajouter un nouveau
                                            Astuce
                                        </button>
                                        <button data-toggle="modal" data-target="#modal"
                                                class="btn btn-primary btn-sm float-right">Ajouter un nouveau Entre-nous
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body table-responsive">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Nom d'utilisateur</th>
                                        <th>Date</th>
                                        <th>Poste</th>
                                        <th>Aime</th>
                                        <th>Commentaires</th>
                                        <th>Astuce</th>
                                        <th>Entre nous</th>
                                        <th>Description</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($posts as $post)
                                        <tr>
                                            <td>{{$post->user->name. ' ' . $post->user->surname}}</td>
                                            <td>{{$post->created_at->format('d-m-y')}}</td>
                                            <td><a target="_blank" href="{{asset($post->img)}}">View Post</a></td>
                                            <td>{{$post->likes->count()}}</td>
                                            <td>{{$post->comments->count()}}</td>
                                            <td>
                                                @if($post->astuce == 1)
                                                    Oui
                                                @else
                                                    Non
                                                @endif
                                            </td>
                                            <td>
                                                @if($post->topic_id != null && $post->name == null)
                                                    Oui
                                                @else
                                                    Non
                                                @endif
                                            </td>
                                            <td>
                                               <a class="btn btn-sm btn-primary" data-toggle="collapse" data-target="#demo{{$post->id}}">Vue</a>
                                                <div id="demo{{$post->id}}" class="collapse">
                                                    {{$post->description}}
                                                </div>
                                            </td>

                                            <td>
                                                {{--                                                <a data-toggle="modal" data-target="#modaledit{{$post->id}}"--}}
                                                {{--                                                   class="btn btn-sm btn-primary"--}}
                                                {{--                                                ><i class="fa fa-pen"></i>--}}
                                                {{--                                                </a>--}}
                                                <a href="{{route('sweet.delete',['id'=>$post->id])}}"
                                                   class="btn btn-sm btn-danger"
                                                ><i class="fa fa-trash"></i>
                                                </a>
                                            </td>
                                            {{--                                            <div class="modal fade" id="modaledit{{$post->id}}">--}}
                                            {{--                                                <div class="modal-dialog">--}}
                                            {{--                                                    <div class="modal-content">--}}
                                            {{--                                                        <form method="post"--}}
                                            {{--                                                              action="{{route('sweet.status', ['id' => $post->id])}}"--}}
                                            {{--                                                              enctype="multipart/form-data">--}}
                                            {{--                                                            @csrf--}}
                                            {{--                                                            <div class="modal-body">--}}
                                            {{--                                                                <div class="form-group">--}}
                                            {{--                                                                    <label for="exampleInputFile">Est-ce que ce haut est--}}
                                            {{--                                                                        doux ?</label>--}}
                                            {{--                                                                    <br>--}}
                                            {{--                                                                    <input--}}
                                            {{--                                                                        {{$post->top_sweet == 1 ? "checked" : ""}} type="checkbox"--}}
                                            {{--                                                                        value="1"--}}
                                            {{--                                                                        name="top_sweet">--}}
                                            {{--                                                                </div>--}}
                                            {{--                                                                <hr>--}}
                                            {{--                                                                <div class="form-group">--}}
                                            {{--                                                                    <label for="exampleInputFile">Est-ce le top 10--}}
                                            {{--                                                                        ?</label>--}}
                                            {{--                                                                    <br>--}}
                                            {{--                                                                    <input--}}
                                            {{--                                                                        {{$post->top_10 == 1 ? "checked" : ""}} type="checkbox"--}}
                                            {{--                                                                        value="1"--}}
                                            {{--                                                                        name="top_10">--}}
                                            {{--                                                                </div>--}}
                                            {{--                                                            </div>--}}
                                            {{--                                                            <div class="modal-footer justify-content-between">--}}
                                            {{--                                                                <button type="button" class="btn btn-default"--}}
                                            {{--                                                                        data-dismiss="modal">Fermer--}}
                                            {{--                                                                </button>--}}
                                            {{--                                                                <button type="submit" class="btn btn-primary">--}}
                                            {{--                                                                    Soumettre--}}
                                            {{--                                                                </button>--}}

                                            {{--                                                            </div>--}}
                                            {{--                                                        </form>--}}
                                            {{--                                                    </div>--}}
                                            {{--                                                    <!-- /.modal-content -->--}}
                                            {{--                                                </div>--}}
                                            {{--                                                <!-- /.modal-dialog -->--}}
                                            {{--                                            </div>--}}
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <div class="modal fade" id="modalpost">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="{{route('sweet.post.save')}}" method="post" accept-charset="UTF-8"
                      enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group" id="input-upload-file">
                            <label for="exampleInputFile"> Image / Video</label>
                            <input id="fileUpnew" required type="file" class="form-control" name="image">
                        </div>
                        <pre id="infosnew"></pre>
                        <div class="form-group">
                            <label> Description</label>
                            <textarea name="description" class="form-control" id="" cols="30" rows="10"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button id="disablenew" type="submit" class="btn btn-primary">Soumettre</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="{{route('sweet.post.save')}}" method="post" accept-charset="UTF-8"
                      enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="exampleInputFile"> Nom du sujet</label>
                            <?php
                            $topics = \App\Topic::where('person', null)->get();
                            ?>
                            <select required name="topic_id" class="form-control" id="">
                                @foreach($topics as $topic)
                                    <option value="{{$topic->id}}">{{$topic->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group" id="input-upload-file">
                            <label for="exampleInputFile"> Image / Video</label>
                            <input id="fileUp" required type="file" class="form-control" name="image">
                        </div>
                        <pre id="infos"></pre>
                        <div class="form-group">
                            <label> Description</label>
                            <textarea name="description" class="form-control" id="" cols="30" rows="10"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button id="disable" type="submit" class="btn btn-primary">Soumettre</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="modalastuce">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="{{route('sweet.post.save')}}" method="post" accept-charset="UTF-8"
                      enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="exampleInputFile"> La personne</label>
                            <select onchange="personselect(this)" required name="name" class="form-control" id="">
                                <option value="">Select personne</option>
                                <option value="Marie">Marie</option>
                                <option value="Anaelle">Anaelle</option>
                                <option value="Audrey">Audrey</option>
                                <option value="Laurine">Laurine</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputFile"> Titre</label>
                            <select required name="topic_id" class="form-control topics" id="">

                            </select>
                        </div>
                        <div class="form-group">
                            <label> Image / Video</label>
                            <input id="fileUp1" required type="file" class="form-control" name="image">
                        </div>
                        <pre id="infos1"></pre>
                        <input type="hidden" name="astuce" value="1">
                        <div class="form-group">
                            <label for="exampleInputFile"> Description</label>
                            <textarea name="description" class="form-control" id="" cols="30" rows="10"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button id="disable1" type="submit" class="btn btn-primary">Soumettre</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection
@section('script')
    <script>
        function personselect(elem) {
            $('.topics').html('<option value="">Sélectionnez une Titre</option>');
            event.preventDefault();
            let name = elem.value;
            let _token = $('meta[name="csrf-token"]').attr('content');

            $.ajax({
                url: "{{route('fetchtopics')}}",
                type: "POST",
                data: {
                    name: name,
                    _token: _token
                },
                success: function (response) {
                    $.each(response, function (i, item) {
                        $('.topics').append('<option value="' + item.id + '">' + item.name + '</option>');
                    });
                },
            });
        }
    </script>
    <script>
        var myVideos = [];
        window.URL = window.URL || window.webkitURL;
        document.getElementById('fileUp').onchange = setFileInfo;

        function setFileInfo() {
            var files = this.files;
            myVideos.push(files[0]);
            var video = document.createElement('video');
            video.preload = 'metadata';
            video.onloadedmetadata = function () {
                window.URL.revokeObjectURL(video.src);
                var duration = video.duration;
                myVideos[myVideos.length - 1].duration = duration;
                updateInfos();
            }
            video.src = URL.createObjectURL(files[0]);
        }

        function updateInfos() {
            var infos = document.getElementById('infos');
            infos.textContent = "";
            for (var i = 0; i < myVideos.length; i++) {
                if (myVideos[i].duration > 30) {
                    infos.textContent += "Votre vidéo Pas moins de 30 secondes Durée: " + myVideos[i].duration + '\n';
                    $('#disable').prop('disabled', true);
                } else {
                    $('#infos').hide();
                    $('#disable').prop('disabled', false);
                }
            }
        }
    </script>
    <script>
        var myVideos = [];
        window.URL = window.URL || window.webkitURL;
        document.getElementById('fileUp1').onchange = setFileInfo1;

        function setFileInfo1() {
            var files = this.files;
            myVideos.push(files[0]);
            var video = document.createElement('video');
            video.preload = 'metadata';
            video.onloadedmetadata = function () {
                window.URL.revokeObjectURL(video.src);
                var duration = video.duration;
                myVideos[myVideos.length - 1].duration = duration;
                updateInfos1();
            }
            video.src = URL.createObjectURL(files[0]);
        }

        function updateInfos1() {
            var infos1 = document.getElementById('infos1');
            infos1.textContent = "";
            for (var i = 0; i < myVideos.length; i++) {
                if (myVideos[i].duration > 30) {
                    infos1.textContent += "Votre vidéo Pas moins de 30 secondes Durée: " + myVideos[i].duration + '\n';
                    $('#disable1').prop('disabled', true);
                } else {
                    $('#infos1').hide();
                    $('#disable1').prop('disabled', false);
                }
            }
        }
    </script>
    <script>
        var myVideos = [];
        window.URL = window.URL || window.webkitURL;
        document.getElementById('fileUpnew').onchange = setFileInfo2;

        function setFileInfo2() {
            var files = this.files;
            myVideos.push(files[0]);
            var video = document.createElement('video');
            video.preload = 'metadata';
            video.onloadedmetadata = function () {
                window.URL.revokeObjectURL(video.src);
                var duration = video.duration;
                myVideos[myVideos.length - 1].duration = duration;
                updateInfos2();
            }
            video.src = URL.createObjectURL(files[0]);
        }

        function updateInfos2() {
            var infosnew = document.getElementById('infosnew');
            infosnew.textContent = "";
            for (var i = 0; i < myVideos.length; i++) {
                if (myVideos[i].duration > 30) {
                    infosnew.textContent += "Votre vidéo Pas moins de 30 secondes Durée: " + myVideos[i].duration + '\n';
                    $('#disablenew').prop('disabled', true);
                } else {
                    $('#infosnew').hide();
                    $('#disablenew').prop('disabled', false);
                }
            }
        }
    </script>
@endsection
