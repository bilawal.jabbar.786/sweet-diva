@extends('layouts.admin')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <!-- <h1>DataTables</h1> -->
                    </div>
                    <div class="col-sm-6">

                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Domicile</a></li>
                            <li class="breadcrumb-item active">Sweet Looks Les sujets</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <!-- /.card -->
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Les sujets</h3>
                                <button data-toggle="modal" data-target="#modal1"
                                        class="btn btn-warning ml-2 btn-sm float-right">Ajouter un sujet pour Astuce
                                </button>
                                <button data-toggle="modal" data-target="#modal"
                                        class="btn btn-primary btn-sm float-right">Ajouter un sujet pour entre nous
                                </button>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body table-responsive">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Nom</th>
                                        <th>La personne</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($topics as $topic)
                                        <tr>
                                            <td>{{$topic->id}}</td>
                                            <td>{{$topic->name}}</td>
                                            <td>{{$topic->person??"Entre nous"}}</td>
                                            <td>
                                                <a href="{{route('sweet.topic.delete', ['id' => $topic->id])}}"
                                                   class="btn btn-sm btn-danger"
                                                >
                                                    <i class="fa fa-times"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <div class="modal fade" id="modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="{{route('sweet.stopic.save')}}" method="post">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="exampleInputFile">Nom du sujet</label>
                            <input type="text" class="form-control" name="name">
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="submit" class="btn btn-primary">Soumettre</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="modal1">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="{{route('sweet.stopic.save')}}" method="post">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="exampleInputFile"> La personne</label>
                            <select required name="person" class="form-control" id="">
                                <option value="Marie">Marie</option>
                                <option value="Anaelle">Anaelle</option>
                                <option value="Audrey">Audrey</option>
                                <option value="Laurine">Laurine</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputFile">Nom du sujet</label>
                            <input type="text" class="form-control" name="name">
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="submit" class="btn btn-primary">Soumettre</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection
