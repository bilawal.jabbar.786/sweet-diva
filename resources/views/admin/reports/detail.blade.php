@extends('layouts.admin')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <!-- <h1>DataTables</h1> -->
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Accueil</a></li>
                            <li class="breadcrumb-item active">Rapports</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <!-- /.card -->

                        <div class="card">
                            <!-- /.card-header -->
                            <div class="card-body">
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label for="exampleInputFile">Espèces</label>
                                        <input type="text" class="form-control" value="{{$report->p3}} €" readonly
                                               placeholder="€" name="p3">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputFile">Carte bleu</label>
                                        <input type="text" class="form-control" value="{{$report->p4}} €" readonly
                                               placeholder="€" name="p4">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputFile">Chèque / Top 3</label>
                                        <input type="text" class="form-control" value="{{$report->p2}} €" readonly
                                               placeholder="€" name="p2">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputFile">Alma </label>
                                        <input type="text" class="form-control" value="{{$report->p1}} €" readonly
                                               placeholder="€" name="p1">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputFile">VAD </label>
                                        <input type="text" class="form-control" value="{{$report->p5}} €" readonly
                                               placeholder="€" name="p1">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputFile">Amex </label>
                                        <input type="text" class="form-control" value="{{$report->p6}} €" readonly
                                               placeholder="€" name="p1">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputFile">Montant total</label>
                                        <input type="text" class="form-control" value="{{$report->total}} €" readonly
                                               placeholder="€" name="total">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputFile">Commentaires</label>
                                        <textarea name="comments" readonly placeholder="Commentaires" id=""
                                                  class="form-control" cols="30"
                                                  rows="5">{{$report->comments}}</textarea>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection
