@extends('layouts.admin')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <!-- <h1>DataTables</h1> -->
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Accueil</a></li>
                            <li class="breadcrumb-item active">Sweet caisse</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <!-- /.card -->

                        <div class="card">
                            <div class="card-header">
                                <button type="button" id="sendNewSms" class="btn btn-sm btn-info"
                                        data-toggle="modal" data-target="#modal-share"><i
                                        class="fa fa-plus"> </i> Ajouter des Sweet caisse aujourd'hui
                                </button>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Total</th>
                                        <th>Boutique</th>
                                        <th>Date</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($reports as $report)
                                        <tr>
                                            <td>{{$report->id}}</td>
                                            <td>{{$report->total}}€</td>
                                            <td>{{$report->user->name}}</td>
                                            <td>{{$report->created_at->format('d-m-y')}}</td>
                                            <td>
                                                <a data-toggle="modal" data-target="#modal{{$report->id}}"
                                                   class="btn btn-sm btn-success"
                                                >
                                                    <i class="fa fa-eye"></i>
                                                </a>
                                                <a data-toggle="modal" data-target="#modaledit{{$report->id}}"
                                                   class="btn btn-sm btn-primary"
                                                >
                                                    <i class="fa fa-pen"></i>
                                                </a>
                                                <div class="modal fade" id="modal{{$report->id}}">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-body">
                                                                <div class="form-group">
                                                                    <label for="exampleInputFile">Espèces</label>
                                                                    <input type="text" class="form-control"
                                                                           value="{{$report->p3}} €" readonly
                                                                           placeholder="€" name="p3">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="exampleInputFile">Carte bleu</label>
                                                                    <input type="text" class="form-control"
                                                                           value="{{$report->p4}} €" readonly
                                                                           placeholder="€" name="p4">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="exampleInputFile">Chèque / Top 3</label>
                                                                    <input type="text" class="form-control"
                                                                           value="{{$report->p2}} €" readonly
                                                                           placeholder="€" name="p2">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="exampleInputFile">Alma </label>
                                                                    <input type="text" class="form-control"
                                                                           value="{{$report->p1}} €" readonly
                                                                           placeholder="€" name="p1">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="exampleInputFile">VAD </label>
                                                                    <input type="text" class="form-control"
                                                                           value="{{$report->p4}} €" readonly
                                                                           placeholder="€" name="p1">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="exampleInputFile">Amex </label>
                                                                    <input type="text" class="form-control"
                                                                           value="{{$report->p5}} €" readonly
                                                                           placeholder="€" name="p1">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="exampleInputFile">Montant total</label>
                                                                    <input type="text" class="form-control"
                                                                           value="{{$report->total}} €" readonly
                                                                           placeholder="€" name="total">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="exampleInputFile">Commentaires</label>
                                                                    <textarea name="comments" readonly
                                                                              placeholder="Commentaires" id=""
                                                                              class="form-control" cols="30"
                                                                              rows="5">{{$report->comments}}</textarea>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer justify-content-between">
                                                                <button type="button" class="btn btn-default"
                                                                        data-dismiss="modal">Fermer
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <!-- /.modal-content -->
                                                    </div>
                                                    <!-- /.modal-dialog -->
                                                </div>
                                                <div class="modal fade" id="modaledit{{$report->id}}">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <form method="post"
                                                                  action="{{route('report.update', ['id' => $report->id])}}"
                                                                  enctype="multipart/form-data">
                                                                @csrf
                                                                <div class="modal-body">
                                                                    <div class="form-group">
                                                                        <label for="exampleInputFile">Espèces</label>
                                                                        <input type="text" class="form-control"
                                                                               value="{{$report->p3}}" placeholder="€"
                                                                               name="p3">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label for="exampleInputFile">Carte bleu</label>
                                                                        <input type="text" class="form-control"
                                                                               value="{{$report->p4}}" placeholder="€"
                                                                               name="p4">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label for="exampleInputFile">Chèque / Top
                                                                            3</label>
                                                                        <input type="text" class="form-control"
                                                                               value="{{$report->p2}}" placeholder="€"
                                                                               name="p2">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label for="exampleInputFile">Alma </label>
                                                                        <input type="text" class="form-control"
                                                                               value="{{$report->p1}}" placeholder="€"
                                                                               name="p1">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label for="exampleInputFile">VAD </label>
                                                                        <input type="text" class="form-control"
                                                                               value="{{$report->p5}}" placeholder="€"
                                                                               name="p5">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label for="exampleInputFile">Amex </label>
                                                                        <input type="text" class="form-control"
                                                                               value="{{$report->p6}}" placeholder="€"
                                                                               name="p6">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label for="exampleInputFile">Montant
                                                                            total</label>
                                                                        <input type="text" class="form-control"
                                                                               value="{{$report->total}}"
                                                                               placeholder="€" name="total">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label
                                                                            for="exampleInputFile">Commentaires</label>
                                                                        <textarea name="comments"
                                                                                  placeholder="Commentaires" id=""
                                                                                  class="form-control" cols="30"
                                                                                  rows="5">{{$report->comments}}</textarea>
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer justify-content-between">
                                                                    <button type="button" class="btn btn-default"
                                                                            data-dismiss="modal">Fermer
                                                                    </button>
                                                                    <button type="submit" class="btn btn-primary">
                                                                        Soumettre
                                                                    </button>

                                                                </div>
                                                            </form>
                                                        </div>
                                                        <!-- /.modal-content -->
                                                    </div>
                                                    <!-- /.modal-dialog -->
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <div class="modal fade" id="modal-share">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Ajouter des Sweet caisse aujourd'hui</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post" action="{{route('report.submit')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="exampleInputFile">Espèces</label>
                            <input type="text" class="form-control" placeholder="€" name="p3">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputFile">Carte bleu</label>
                            <input type="text" class="form-control" placeholder="€" name="p4">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputFile">Chèque / Top 3</label>
                            <input type="text" class="form-control" placeholder="€" name="p2">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputFile">Alma </label>
                            <input type="text" class="form-control" placeholder="€" name="p1">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputFile">VAD </label>
                            <input type="text" class="form-control" placeholder="€" name="p5">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputFile">Amex </label>
                            <input type="text" class="form-control" placeholder="€" name="p6">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputFile">Montant total</label>
                            <input type="text" class="form-control" required placeholder="€" name="total">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputFile">Commentaires</label>
                            <textarea name="comments" placeholder="Commentaires" id="" class="form-control" cols="30"
                                      rows="5"></textarea>
                        </div>

                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                        <button type="submit" class="btn btn-primary">Soumettre</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

@endsection
