@extends('layouts.admin')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <!-- <h1>DataTables</h1> -->
                    </div>
                    <div class="col-sm-6">

                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Domicile</a></li>
                            <li class="breadcrumb-item active">Tous les produits</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <!-- /.card -->
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Tous les produits</h3>
                                <a href="{{route('products.draft.index')}}" style="margin-left: 30px;"
                                   class="btn btn-primary btn-sm float-right">Projet de produit</a>
                                <div class="row">
                                    <div class="col-4">

                                    </div>
                                    <div class="col-2">

                                    </div>
                                    <div class="col-6">
                                        <select name="user_id" class="form-control float-right select2"
                                                onchange="location = this.value;">
                                            <option value="Select customers">Rechercher un produit
                                            </option>
                                            @foreach($allProducts as $row)
                                                <option value="{{route('product.filter.search', ['id' => $row->id])}}">
                                                    {{$row->title}}
                                                </option>
                                            @endforeach

                                        </select>
                                    </div>

                                </div>
                                {{--                                <a  href="{{route('products.draft.index')}}" style="margin-left: 30px;" class="btn btn-primary btn-sm float-right" >Produits brouillons</a>--}}
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body table-responsive">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Sku</th>
                                        <th>Titre</th>
                                        <th>Prix</th>
                                        <th>Photo</th>
                                        <th>Catégorie</th>
                                        <th>Pricipal Catégorie</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($products as $row)
                                        <tr>
                                            <td>{{$row->sku}}</td>
                                            <td>
                                                <a target="_blank"
                                                   href="{{route('front.product', ['id' => $row->id])}}"> {{$row->title}}</a>
                                            </td>
                                            <td>{{$row->price}} €</td>
                                            <td><img src="{{asset($row->photo1)}}" height="50px" alt=""></td>

                                            <td>{{$row->categorytype->name}}</td>
                                            <td>{{$row->maincategory->name}}</td>
                                            <td>
                                                <a href="{{route('product.edit' , ['id'=>$row->id])}}"
                                                   class="btn btn-sm btn-success" data-toggle="tooltip"
                                                   title="edit">
                                                    <i class="fa fa-pen"></i>
                                                </a>
                                                <a href="{{route('product.delete' , ['id'=>$row->id])}}"
                                                   id="delete" class="btn btn-sm btn-danger"
                                                   data-toggle="tooltip" title="edit">
                                                    <i class="fa fa-times"></i>
                                                </a>
                                                @if($row->draft==2)
                                                    <a href="{{route('product.publish' , ['id'=>$row->id])}}"
                                                       class="btn btn-sm btn-primary" data-toggle="tooltip"
                                                       title=publish">
                                                        <i class="fa fa-users" aria-hidden="true"></i>
                                                    </a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <br><br>
                                {{ $products->links() }}
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>

@endsection
