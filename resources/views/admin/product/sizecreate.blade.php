@extends('layouts.admin')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Domicile</a></li>
                            <li class="breadcrumb-item active">Tailles des produits</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- SELECT2 EXAMPLE -->
                <div class="card card-default">
                    <div class="card-header">
                        <h3 class="card-title">Tailles des produits</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove">
                                <i class="fas fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <form action="{{route('size.store')}}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
                    @csrf
                    <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Sélectionner un produit</label>
                                        <select onchange="getproduct(this)" class="form-control select2" name="product_id" style="width: 100%;" required>
                                            <option value="">Sélectionner un produit</option>
                                            @foreach($products as $row)
                                                <option value="{{$row->id}}">{{$row->title}} ({{$row->sku}})</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Sélectionnez une couleur</label>
                                        <div class="colorsget">

                                        </div>
<!--
                                        <select class="form-control colorsget" name="color" id="" required>
                                       </select>-->
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <label for="">Entrez la taille et la quantité</label>
                                        <div class="sizequantity">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <input type="text" name="size[]" placeholder="Taille" required  class="form-control">
                                                </div>
                                                <div class="col-md-6">
                                                    <input  type="number" name="quantity[]" placeholder="Quantité" required  class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="">Action</label><br>
                                        <a  onclick="addmorerow()" class="btn btn-primary btn-sm" >Ajouter</a>
                                        <a  onclick="removerow()" class="btn btn-danger btn-sm" >Supprimer</a>
                                    </div>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-primary">Sauvegarder</button>
                        </div>
                    </form>
                    <!-- /.card -->

                    <!-- /.container-fluid -->
        </section>
    </div>
@endsection
@section('script')
    <script>
        function getproduct(elem){
           let id = $(elem).find(":selected").val();
            let _token   = $('meta[name="csrf-token"]').attr('content');
            $('.colorsget').empty();
            $.ajax({
                url: "{{route('getproduct')}}",
                type:"POST",
                data:{
                    id:id,
                    _token: _token,
                },
                success:function(response){
                    $.each(response, function(i, item) {
                        $('.colorsget').append('  <div class="row">\n' +
                            '                                                <div class="col-md-6" style="background-color: '+item+'">\n' +
                            '                                                    '+item+'\n' +
                            '                                                </div>\n' +
                            '                                                <div class="col-md-6">\n' +
                            '                                                    <input type="radio" name="color" value="'+item+'" class="form-control">\n' +
                            '                                                </div>\n' +
                            '                                            </div>');
                        // $('.colorsget').append('<option style="background-color: '+item+' !important;" value="'+item+'">'+item+'</option>');
                    });
                },
            });
        }

        function addmorerow(){
            $('.sizequantity').append('<div class="row">\n' +
                '                                                <div class="col-md-6">\n' +
                '                                                    <input type="text" name="size[]" required placeholder="Taille" class="form-control">\n' +
                '                                                </div>\n' +
                '                                                <div class="col-md-6">\n' +
                '                                                    <input  type="number" name="quantity[]" placeholder="Quantité" required  class="form-control">\n' +
                '                                                </div>\n' +
                '                                            </div>');
        }
        function removerow(){
            if($('.sizequantity .row').length>1) {//remove all except one
                $('.sizequantity .row:last').remove();
            }
        }
    </script>
@endsection
