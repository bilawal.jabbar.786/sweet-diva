@extends('layouts.admin')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Domicile</a></li>
                            <li class="breadcrumb-item active">Tailles des produits</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- SELECT2 EXAMPLE -->
                <div class="card card-default">
                    <div class="card-header">
                        <h3 class="card-title">Tailles des produits</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove">
                                <i class="fas fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <form action="{{route('size.update', ['id' => $size->id])}}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
                    @csrf
                    <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Produit</label>
                                        <select readonly class="form-control">
                                            <option value="">{{$size->product->title}}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Couleur</label>
                                        <select style="background-color: {{$size->color}}" readonly class="form-control">
                                            <option value="">{{$size->color}}</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <label for="">Entrez la taille et la quantité</label>
                                        <?php
                                        $q = json_decode($size->quantity);
                                        ?>
                                        <div class="sizequantity">
                                            @foreach(json_decode($size->newsize, true) as $key => $s)
                                                <div class="row">
                                                    <div class="col-md-5">
                                                        <input type="text" name="size[]" value="{{$s}}" placeholder="Taille" required  class="form-control">
                                                    </div>
                                                    <div class="col-md-5">
                                                        <input  type="number" name="quantity[]" value="{{$q[$key]}}" placeholder="Quantité" required  class="form-control">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <a onclick="removecolor(this)" class="btn btn-sm btn-danger" data-toggle="tooltip" title="delete">
                                                            <i class="fa fa-times"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="">Action</label><br>
                                        <a  onclick="addmorerow()" class="btn btn-primary btn-sm" >Ajouter</a>
                                    </div>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-primary">Sauvegarder</button>
                        </div>
                    </form>
                    <!-- /.card -->

                    <!-- /.container-fluid -->
        </section>
    </div>
@endsection
@section('script')
    <script>

        function addmorerow(){
            $('.sizequantity').append('<div class="row">\n' +
                '                                                    <div class="col-md-5">\n' +
                '                                                        <input type="text" name="size[]" placeholder="Taille" required  class="form-control">\n' +
                '                                                    </div>\n' +
                '                                                    <div class="col-md-5">\n' +
                '                                                        <input  type="number" name="quantity[]" placeholder="Quantité" required  class="form-control">\n' +
                '                                                    </div>\n' +
                '                                                    <div class="col-md-2">\n' +
                '                                                        <a onclick="removecolor(this)" class="btn btn-sm btn-danger" data-toggle="tooltip" title="delete">\n' +
                '                                                            <i class="fa fa-times"></i>\n' +
                '                                                        </a>\n' +
                '                                                    </div>\n' +
                '                                                </div>');
        }
        function removecolor(elem){
            $(elem).parent('div').parent('div').remove();
        }
    </script>
@endsection
