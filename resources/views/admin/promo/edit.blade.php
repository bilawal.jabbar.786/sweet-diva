@extends('layouts.admin')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Domicile</a></li>
                            <li class="breadcrumb-item active">Ajouter un nouveau</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- SELECT2 EXAMPLE -->
                <div class="card card-default">
                    <div class="card-header">
                        <h3 class="card-title">Modifier le code promotionnel</h3>

                        <div class="card-tools">

                        </div>
                    </div>
                    <form action="{{route('promo.update', ['id' => $promo->id])}}" method="post" accept-charset="UTF-8"
                          enctype="multipart/form-data">
                        @csrf
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Code promo</label>
                                        <input type="text" name="code" required placeholder="Code promo"
                                               value="{{$promo->code}}" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="">Prix ​​hors</label>
                                        <input type="number" name="off" required placeholder="Prix ​​hors"
                                               value="{{$promo->off}}" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Produits sur lesquels le code promo n'est pas applicable</label>
                                        <select class="select2" multiple="" name="excluded_products[]"
                                                data-placeholder="Sélectionnez des produits" style="width: 100%;">
                                            @foreach($products as $row)
                                                <option {{in_array($row->id, json_decode($promo->excluded_products)) ?'selected': "" }}
                                                    value="{{$row->id}}">{{$row->title}}</option>
                                            @endforeach
                                        </select></div>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-primary">Sauvegarder</button>
                        </div>
                    </form>
                    <!-- /.card -->

                    <!-- /.container-fluid -->

                </div>
            </div>
        </section>
    </div>
@endsection
