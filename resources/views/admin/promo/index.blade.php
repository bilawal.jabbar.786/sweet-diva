@extends('layouts.admin')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <!-- <h1>DataTables</h1> -->
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Domicile</a></li>
                            <li class="breadcrumb-item active">catégories de produits</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <!-- /.card -->
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Code promo</h3>
                                <button style="margin-left: 30px;" class="btn btn-primary btn-sm float-right"
                                        data-toggle="modal" data-target="#exampleModal">Ajouter un nouveau
                                </button>
                                {{--                                <button class="btn btn-secondary btn-sm float-right" data-toggle="modal" data-target="#exampleModal1">Séquence de catégorie</button>--}}
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Code promo</th>
                                        <th>Désactivé</th>
                                        <th>Statut</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($promo as $row)
                                        <tr>
                                            <td>{{$row->id}}</td>
                                            <td>{{$row->code}}</td>
                                            <td>{{$row->off}}%</td>
                                            @if($row->status==1)
                                                <th style="color: #28a745">Activer</th>
                                            @else
                                                <th style="color: #ffc107">Désactiver</th>
                                            @endif
                                            <td>
                                                <a href="{{route('promo.edit' , ['id'=>$row->id])}}" id="edit"
                                                   class="btn btn-sm btn-primary" title="edit">
                                                    <i class="fa fa-pen"></i>
                                                </a>
                                                <a href="{{route('promo.delete' , ['id'=>$row->id])}}" id="delete"
                                                   class="btn btn-sm btn-danger" data-toggle="tooltip" title="edit">
                                                    <i class="fa fa-times"></i>
                                                </a>
                                                @if($row->status==1)
                                                    <a href="{{route('promo.status' , ['id'=>$row->id,'status'=>0])}}"
                                                       id="désactiver" class="btn btn-sm btn-warning"
                                                       title="Désactiver">
                                                        <i class="fa fa-ban"></i>
                                                    </a>
                                                @else
                                                    <a href="{{route('promo.status' , ['id'=>$row->id,'status'=>1])}}"
                                                       id="activer" class="btn btn-sm btn-success" title="Activer">
                                                        <i class="fa fa-check-square" aria-hidden="true"></i>
                                                    </a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ajouter Code promo</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('promo.store')}}" method="post" enctype="multipart/form-data"
                          data-parsley-validate>
                        @csrf
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="code"><b>Code promo</b><span class="text-danger">*</span></label>
                                <input type="text" name="code" required placeholder="Code promo" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="off"><b>Rabais %</b><span class="text-danger">*</span></label>
                                <input type="number" name="off" required placeholder="Prix ​​hors" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Produits sur lesquels le code promo n'est pas applicable</label>
                                <select class="select2" multiple="" name="excluded_products[]"
                                        data-placeholder="Sélectionnez des produits" style="width: 100%;">
                                    @foreach($products as $row)
                                        <option
                                            value="{{$row->id}}">{{$row->title}}</option>
                                    @endforeach
                                </select></div>
                        </div>
                        <div class="col-md-12 pull-right">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-block">Sauvegarder</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div></div>
@endsection
