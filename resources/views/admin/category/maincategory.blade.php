@extends('layouts.admin')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <!-- <h1>DataTables</h1> -->
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Domicile</a></li>
                            <li class="breadcrumb-item active">catégories de produits</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <!-- /.card -->

                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Catégories de produits</h3>
                                <button style="margin-left: 30px;" class="btn btn-primary btn-sm float-right" data-toggle="modal" data-target="#exampleModal">Ajouter un nouveau</button>
{{--                                <button class="btn btn-secondary btn-sm float-right" data-toggle="modal" data-target="#exampleModal1">Séquence de catégorie</button>--}}
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Séquence</th>
                                        <th>Nom</th>

                                        <th>Category</th>
                                        <th>Photo</th>

                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($maincategory as $row)
                                        <tr>
                                            <td>{{$row->id}}</td>
                                            @if($row->no)
                                            <td>{{$row->no}}</td>
                                            @else
                                                <td>Aucune séquence assignée</td>
                                            @endif
                                            <td>{{$row->name}}</td>
                                            <td>{{$row->categorytype->name}}</td>
                                            <td>
                                                <img height="30px" src="{{asset($row->photo)}}" alt="">
                                            </td>

                                            <td>
{{--                                                <a href="{{route('maincategory.delete' , ['id'=>$row->id])}}" id="delete" class="btn btn-sm btn-danger" data-toggle="tooltip" title="edit">--}}
{{--                                                    <i class="fa fa-times"></i>--}}
{{--                                                </a>--}}
                                                <a href="{{route('maincategory.edit' , ['id'=>$row->id])}}" id="edit" class="btn btn-sm btn-primary"  title="edit">
                                                    <i class="fa fa-pen"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ajouter Séquence Catégorie</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <form action="{{route('maincategory.sequence')}}" method="post" enctype="multipart/form-data" data-parsley-validate>
                        @csrf
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="title"><b>Catégorie</b><span class="text-danger">*</span></label>
                                <select name="category_id" class="form-control select2" id="">
                                    @foreach($maincategory as $cat)
                                        <option value="{{$cat->id}}">{{$cat->name}}-{{$cat->no}} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="title"><b>Séquence Catégorie</b><span class="text-danger">*</span></label>
                                <input type="number"  name="no" required placeholder="Séquence" class="form-control">
                            </div>
                        </div>




                        <div class="col-md-12 pull-right">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-block">Sauvegarder</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>



        </div>

    </div></div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ajouter une nouvelle catégorie</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <form action="{{route('maincategory.store')}}" method="post" enctype="multipart/form-data" data-parsley-validate>
                        @csrf
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="title"><b>Catégorie</b><span class="text-danger">*</span></label>
                                <select name="category_id" class="form-control" id="">
                                    @foreach($category as $cat)
                                    <option value="{{$cat->id}}">{{$cat->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="title"><b>Nom de catégorie</b><span class="text-danger">*</span></label>
                                <input type="text"  name="name" required placeholder="Nom de catégorie" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="title"><b>Image1 (420 x 420)</b><span class="text-danger">*</span></label>
                                <input type="file"  name="photo" required class="form-control">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="title"><b>Image2 (420 x 420)</b><span class="text-danger">*</span></label>
                                <input type="file"  name="photo2" required class="form-control">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="title"><b>Image3 (420 x 420)</b><span class="text-danger">*</span></label>
                                <input type="file"  name="photo3" required class="form-control">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="title"><b>Bannière (1920 x 800)</b><span class="text-danger">*</span></label>
                                <input type="file"  name="banner" required class="form-control">
                            </div>
                        </div>



                        <div class="col-md-12 pull-right">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-block">Sauvegarder</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>

    </div></div>

@endsection
