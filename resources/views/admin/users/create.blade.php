@extends('layouts.admin')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Accueil</a></li>
                            <li class="breadcrumb-item active">Ajouter un nouveau client</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- SELECT2 EXAMPLE -->
                <div class="card card-default">
                    <div class="card-header">
                        <h3 class="card-title">Ajouter un nouvel utilisateur</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove">
                                <i class="fas fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <form action="{{route('user.store')}}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
                    @csrf
                    <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6 col-12 mb--20">
                                    <label>Nom</label>
                                    <div class="form-group">
                                    <input class="form-control" type="text" name="name" required placeholder="Nom">
                                    <input type="hidden" name="role" value="2">
                                  </div>
                                </div>
                                <div class="col-md-6 col-12 mb--20">
                                  <div class="form-group">
                                    <label>Prénom</label>
                                    <input class="form-control" type="text" name="surname" required placeholder="Prénom">
                                  </div>
                                </div>
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label>Numéro de téléphone</label>
                                    <input class="form-control" name="phone" type="number" required placeholder="0000000000">
                                  </div>
                                </div>
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label>Adresse e-mail*</label>
                                    <input class="form-control" name="email" type="email" required placeholder="Adresse e-mail">
                                  </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Pays*</label>
                                        <select name="country" class="form-control float-right select2" >
                                            <option value="Select customers as country">Sélectionner les clients du pays</option>
                                            <option value="Martinique">Martinique</option>
                                            <option value="Guadeloupe">Guadeloupe</option>
                                            <option value="Faroe">Faroe </option>
                                            <option value="French Guiana">French Guiana </option>
                                        </select>

                                    </div>
                                </div>
                                @if(Auth::user()->role==0 )
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputFile">Choisissez la boutique</label>
                                        <select class="form-control select2bs4" name="shop" id="methods" required=""
                                                style="width: 100% !important;">
                                            <option value="">--SÉLECTIONNER LA BOUTIQUE--</option>

                                            <option value="SWEET DIVA MARTINIQUE">SWEET DIVA MARTINIQUE</option>
                                            <option value="SWEET DIVA BESSON">SWEET DIVA BESSON</option>
                                            <option value="SWEET DIVA BASSE TERRE">SWEET DIVA BASSE TERRE</option>
                                        </select>
                                    </div>
                                </div>
                                @endif
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Taille*</label>
                                        <select name="size" class="form-control float-right select2" >

                                            <option value="TU">TU</option>
                                            <option value="XL(44/46)">XL (44/46)</option>
                                            <option value="1XL(46/48)">1XL (46/48)</option>
                                            <option value="2XL(48/50)">2XL (48/50)</option>
                                            <option value="3XL(50/52)">3XL (50/52)</option>
                                            <option value="4XL(52/54)">4XL (52/54)</option>
                                            <option value="5XL(54/56)">5XL (54/56)</option>


                                        </select>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label>Adresse</label>
                                    <input class="form-control" name="address" type="text" placeholder="12 rue du puits">
                                  </div>
                                </div>
                                <div class="col-md-3">
                                  <div class="form-group">
                                    <label>Code Postal</label>
                                    <input class="form-control" name="cp" type="text" placeholder="97122">
                                  </div>
                                </div>
                                <div class="col-md-3">
                                  <div class="form-group">
                                    <label>Ville</label>
                                    <input class="form-control" name="city" type="text" placeholder="Baie-mahault">
                                  </div>
                                </div>
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label>Mot de passe</label>
                                    <input class="form-control" type="text" required name="password" placeholder="Mot de passe">
                                  </div>
                                </div>
                                <div class="col-md-3">
                                    <label>Points</label>
                                    <div class="form-group">
                                    <input class="form-control" type="number" required name="points" placeholder="Nombre de point">
                                  </div>
                                </div>
                                <div class="col-md-3">
                                  <div class="form-group">
                                    <label>Sweeties</label>
                                    <input class="form-control" type="number" required name="wallet" placeholder="Nombre de sweeties">
                                  </div>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-primary btn-block">Sauvegarder</button>
                        </div>
                    </form>
                    <!-- /.card -->

                    <!-- /.container-fluid -->
        </section>
    </div>
@endsection
