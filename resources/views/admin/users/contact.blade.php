@extends('layouts.admin')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <!-- <h1>DataTables</h1> -->
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Domicile</a></li>
                            <li class="breadcrumb-item active">Messages Clients </li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <!-- /.card -->

                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Messages Clients</h3>
{{--                                <button style="margin-left: 30px;" class="btn btn-primary btn-sm float-right" data-toggle="modal" data-target="#exampleModal">Ajouter un nouveau</button>--}}
                                {{--                                <button class="btn btn-secondary btn-sm float-right" data-toggle="modal" data-target="#exampleModal1">Séquence de catégorie</button>--}}
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Nom</th>
                                        <th>email</th>
                                        <th>subject</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($contact as $key => $row)
                                        <tr>
                                            <td>{{$key++}}</td>
                                            <td>{{$row->name}}</td>
                                            <td>{{$row->email}}</td>
                                            <td>{{$row->subject}}</td>


                                            <td>
                                                {{--                                                <a href="{{route('maincategory.delete' , ['id'=>$row->id])}}" id="delete" class="btn btn-sm btn-danger" data-toggle="tooltip" title="edit">--}}
                                                {{--                                                    <i class="fa fa-times"></i>--}}
                                                {{--                                                </a>--}}
                                                <a  id="edit" data-toggle="modal" data-target="#exampleModal{{$row->id}}" class="btn btn-sm btn-primary"  title="edit">
                                                    <i class="fa fa-eye"></i>
                                                </a>

                                            </td>
                                        </tr>
                                        <!-- Modal -->
                                        <div class="modal fade" id="exampleModal{{$row->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Messages Clients</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>

                                                    <div class="modal-body">
                                                        <form action="{{route('maincategory.store')}}" method="post" enctype="multipart/form-data" data-parsley-validate>
                                                            @csrf

                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label for="title"><b>{{$row->name}} Message</b><span class="text-danger">*</span></label>
                                                                    <textarea cols="50" rows="10" readonly>{{$row->message}}</textarea>
                                                                </div>
                                                            </div>

                                                        </form>
                                                    </div>
                                                </div>



                                            </div>

                                        </div>

                            @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>


@endsection
