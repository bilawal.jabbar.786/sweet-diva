@extends('layouts.admin')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Accueil</a></li>
                            <li class="breadcrumb-item active">Modifier le client</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->

        <section class="content">
            <div class="container-fluid">
                <!-- SELECT2 EXAMPLE -->
                <div class="card card-default">
                    <div class="card-header">
                        <h3 class="card-title">Détails du client,, <small>Membre depuis {{$customer->created_at->format('d, m-Y')}}</small></h3>

                        <div class="card-tools">
                            <button class="btn btn-primary btn-sm float-right"
                                    data-toggle="modal" data-target="#exampleModal4"><i class="fa fa-plus"></i> Ajouter
                                des points
                            </button>
                            <button style="margin-right: 10px;" class="btn btn-success btn-sm float-right"
                                    data-toggle="modal" data-target="#exampleModal3"><i class="fa fa-minus"></i> Retrait
                                de points
                            </button>
                            <a href="{{route('customer.details' , ['id'=>$customer->id])}}">
                                <button style="margin-right: 10px;" class="btn btn-warning btn-sm float-right"><i
                                        class="fa fa-hand-point-down"></i> Points d'historique
                                </button>
                            </a>
                            <a href="{{route('customer.gift.history' , ['id'=>$customer->id])}}">
                                <button style="margin-right: 10px;" class="btn  btn-dark btn-sm float-right"><i
                                        class="fa fa-history"></i> Historique des cadeaux
                                </button>
                            </a>

                        </div>
                    </div>
                    <form action="{{route('customer.update', ['id' => $customer->id])}}" method="post"
                          accept-charset="UTF-8" enctype="multipart/form-data">
                        @csrf
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Nom</label>
                                        <input class="form-control" type="text" name="name" value="{{$customer->name}}"
                                               required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Prénom</label>
                                        <input class="form-control" type="text" name="surname"
                                               value="{{$customer->surname}}" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">E-mail</label>
                                        <input class="form-control" type="text" name="email"
                                               value="{{$customer->email}}" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Taille*</label>
                                        <select name="size" class="form-control float-right select2">

                                            <option value="TU" {{ $customer->size == 'TU' ? 'selected' : '' }}>TU
                                            </option>
                                            <option
                                                value="XL(44/46)" {{ $customer->size == 'XL(44/46)' ? 'selected' : '' }}>
                                                XL (44/46)
                                            </option>
                                            <option
                                                value="1XL(46/48)" {{ $customer->size == '1XL(46/48)' ? 'selected' : '' }}>
                                                1XL (46/48)
                                            </option>
                                            <option
                                                value="2XL(48/50)" {{ $customer->size == '2XL(48/50)' ? 'selected' : '' }}>
                                                2XL (48/50)
                                            </option>
                                            <option
                                                value="3XL(50/52)" {{ $customer->size == '3XL(50/52)' ? 'selected' : '' }}>
                                                3XL (50/52)
                                            </option>
                                            <option
                                                value="4XL(52/54)" {{ $customer->size == '4XL(52/54)' ? 'selected' : '' }}>
                                                4XL (52/54)
                                            </option>
                                            <option
                                                value="5XL(54/56)" {{ $customer->size == '5XL(54/56)' ? 'selected' : '' }}>
                                                5XL (54/56)
                                            </option>


                                        </select>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Téléphone</label>
                                        <input class="form-control" type="text" name="phone"
                                               value="{{$customer->phone}}" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Adresse</label>
                                        <input class="form-control" name="address" type="text"
                                               value="{{$customer->address}}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Code Postal</label>
                                        <input class="form-control" name="cp" type="text"
                                               value="{{$customer->cp}}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Ville</label>
                                        <input class="form-control" name="city" type="text"
                                               value="{{$customer->city}}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="points">Points</label>
                                        <input class="form-control" type="text" name="points"
                                               value="{{$customer->points}}" readonly>
                                        <input class="form-control" type="hidden" name="points2"
                                               value="{{$customer->points}}" required>
                                    </div>
                                </div>
                                @if($customer->role == '2')
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Sweeties</label>
                                            <input class="form-control" type="text" name="wallet"
                                                   value="{{$customer->wallet}}" required>
                                        </div>
                                    </div>
                                @endif
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Mot de passe</label>
                                        <input class="form-control" type="text" name="password"
                                        >
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary btn-block">Sauvegarder</button>
                        </div>
                    </form>
                    <!-- /.card -->
                </div>
                @if(Auth::user()->role == '0')
                <div class="card card-default">
                    <div class="card-header">
                        <h3 class="card-title">Historique des commandes</h3>
                    </div>
                    <div class="card-body">
                        <div class="card-body table-responsive">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>N°</th>
                                    <th>Nom</th>
                                    <th>E-mail</th>
                                    <th>Téléphone</th>
                                    <th>Total</th>
                                    <th>Ressource</th>
                                    <th>Statut</th>
                                    <th>Date</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($orders as $row)
                                    <tr>
                                        <td>{{$row->order->order_number}}</td>
                                        <td>{{$row->order->name}}</td>
                                        <td>{{$row->order->email}}</td>
                                        <td>{{$row->order->phone}}</td>
                                        <td>
                                            @if($row->order->total == '0')
                                                Points
                                            @else
                                                {{$row->order->total}} €</td>
                                        @endif

                                        <td>{{$row->order->resource}}</td>
                                        <td>
                                            @if($row->order->status == '0')
                                                Nouvelle commande
                                            @elseif($row->order->status == '2')
                                                Annuler
                                            @else
                                                Compléter
                                            @endif
                                        </td>
                                        <td>{{$row->created_at->format('d-m-Y')}}</td>
                                        <td>
                                            <a href="{{route('order.view' , ['id'=>$row->order->id])}}" class="btn btn-sm btn-primary" data-toggle="tooltip" title="View">
                                                <i class="fa fa-eye"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                @endif
            </div>
            <!-- /.container-fluid -->
        </section>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Points Cliente</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <form action="{{route('user.points.withdraw')}}" method="post"
                          enctype="multipart/form-data" data-parsley-validate>
                        @csrf
                        <input type="hidden" name="user_id" value="{{$customer->id}}">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="title"><b>Points </b><span class="text-danger">*</span></label>
                                <input type="number" class="form-control" placeholder="Retrait de points" name="points">
                            </div>
                        </div>


                        <div class="col-md-12 pull-right">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-block">Retrait de points</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>


        </div>

    </div>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Points Cliente</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <form action="{{route('user.points')}}" method="post"
                          enctype="multipart/form-data" data-parsley-validate>
                        @csrf
                        <input type="hidden" name="user_id" value="{{$customer->id}}">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="title"><b>Points </b><span class="text-danger">*</span></label>
                                <input type="number" class="form-control" placeholder="Points" name="points">
                            </div>
                        </div>


                        <div class="col-md-12 pull-right">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-block">Ajouter des points</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>


        </div>

    </div>

@endsection
