@extends('layouts.admin')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <!-- <h1>DataTables</h1> -->
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Accueil</a></li>
                            <li class="breadcrumb-item active">Vendeurs</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <!-- /.card -->

                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-md-4">
                                        <a href="{{route('user.create')}}">
                                            <button class="btn btn-primary btn-sm">Ajouter un nouveau</button>
                                        </a>
                                        @if(Auth::user()->role==0 )
                                            <a data-toggle="modal" data-target="#exampleModal">
                                                <button class="btn btn-success btn-sm">Exporter des utilisateurs
                                                </button>
                                            </a>
                                            {{--                                    <button type="button" id="sendNewSms" class="btn btn-sm btn-info"--}}
                                            {{--                                            data-toggle="modal" data-target="#modal-share"><i--}}
                                            {{--                                            class="fa fa-share-alt"></i> Attribuer à la boutique--}}
                                            {{--                                    </button>--}}

                                        @endif
                                    </div>
                                    <div class="col-md-4"></div>
                                    <div class="col-md-4">
{{--                                        <select name="country" class="form-control float-right select2"--}}
{{--                                                onchange="location = this.value;">--}}
{{--                                            <option value="Select customers as country">Sélectionner les clients du--}}
{{--                                                pays--}}
{{--                                            </option>--}}
{{--                                            <option value="{{route('users.filter', ['country' => 'Martinique'])}}">--}}
{{--                                                Martinique--}}
{{--                                            </option>--}}
{{--                                            <option value="{{route('users.filter', ['country' => 'Guadeloupe'])}}">--}}
{{--                                                Guadeloupe--}}
{{--                                            </option>--}}
{{--                                            <option value="{{route('users.filter', ['country' => 'France'])}}">--}}
{{--                                                France--}}
{{--                                            </option>--}}
{{--                                            <option value="{{route('users.filter', ['country' => 'French Guiana'])}}">--}}
{{--                                                French Guiana--}}
{{--                                            </option>--}}
{{--                                            <option value="{{route('users.filter', ['country' => 'Réunion'])}}">--}}
{{--                                                Réunion--}}
{{--                                            </option>--}}
{{--                                        </select>--}}
                                    </div>
{{--                                    <div class="col-md-4">--}}
{{--                                        <form method="get" action="{{route('users.search')}}">--}}
{{--                                            @csrf--}}
{{--                                            <div class="input-group mb-3">--}}
{{--                                                <input name="search" type="text" placeholder="Nom, e-mail et téléphone"--}}
{{--                                                       class="form-control">--}}
{{--                                                <span class="input-group-append">--}}
{{--                                                <button type="submit" class="btn btn-info btn-flat">Recherche</button>--}}
{{--                                              </span>--}}
{{--                                            </div>--}}
{{--                                        </form>--}}
{{--                                    </div>--}}
{{--                                    <div class="col-md-4">--}}
{{--                                        <form method="get" action="{{route('users.search')}}">--}}
{{--                                            @csrf--}}
{{--                                            <input type="text" class="form-control" placeholder="Cherche ici" onkeyup="searchFunction(this)">--}}
{{--                                            <ul class="nav flex-column searchresult">--}}

{{--                                            </ul>--}}
{{--                                        </form>--}}
{{--                                    </div>--}}
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table class=" table-bordered table-striped">
                                    <div class="card-body table-responsive">
                                        <table  class="data-table table table-bordered table-stripe">
                                            <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Nom </th>
                                                <th>Prénom</th>
                                                <th>E-mail</th>
                                                <th>Telephone</th>
                                                <th>Adresse</th>
                                                <th>Pays</th>
                                                <th>Points</th>
{{--                                                <th>Sweeties</th>--}}
{{--                                                <th>Ressource</th>--}}
{{--                                                <th>Taille</th>--}}
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </table>
                                    <!-- /.card-body -->
                            </div>
                            <!-- /.card -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
        </section>
        <!-- /.content -->
    </div>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Sélectionnez le pays pour les clients
                        exportation</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <form action="{{route('users.export')}}" method="post" enctype="multipart/form-data"
                          data-parsley-validate>
                        @csrf
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="title"><b>Pays</b><span class="text-danger">*</span></label>
                                <select name="country" class="form-control" id="">
                                    <option value="Martinique">Martinique</option>
                                    <option value="Guadeloupe">Guadeloupe</option>
                                    <option value="All">Tous les clients</option>
                                </select>
                            </div>
                        </div>


                        <div class="col-md-12 pull-right">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-block">Importer</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>


        </div>

    </div>
    <div class="modal fade" id="modal-share">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Attribuer à un magasin spécifique</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post" action="{{route('shops.assign')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <input type="hidden" name="users" id="myField" value="" required=""/>

                        <div class="form-group">
                            <label for="exampleInputFile">Choisissez la boutique</label>
                            <select class="form-control select2bs4" name="shop" id="methods" required=""
                                    style="width: 100% !important;">
                                <option value="">--SÉLECTIONNER LA BOUTIQUE--</option>

                                <option value="SWEET DIVA MARTINIQUE">SWEET DIVA MARTINIQUE</option>
                                <option value="SWEET DIVA BESSON">SWEET DIVA BESSON</option>
                                <option value="SWEET DIVA BASSE TERRE">SWEET DIVA BASSE TERRE</option>
                            </select>
                        </div>

                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                        <button type="submit" class="btn btn-primary">Soumettre</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

@endsection
@section('script')
    <script>

        function searchFunction(elem){
            console.log(elem.value);
            $('.searchresult').html(' <li class="nav-item"><a href="#" class="nav-link"> </a> </li>');
            event.preventDefault();
            let search = elem.value;
            let _token   = $('meta[name="csrf-token"]').attr('content');

            $.ajax({
                url: "{{route('searchuser')}}",
                type:"POST",
                data:{
                    search:search,
                    _token: _token
                },
                success:function(response){
                    console.log(response);
                    $.each(response, function(i, item) {
                        $('.searchresult').append('<li class="nav-item"><a href="/customers/edit/'+item.id+'" class="nav-link">'+item.name+' ('+item.email+')  '+item.phone+'</a> </li>');
                    });
                },
            });
        }
    </script>
    <script type="text/javascript">
        $(function () {

            var table = $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('admin.sellers') }}",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'surname', name: 'surname'},
                    {data: 'email', name: 'email'},
                    {data: 'phone', name: 'phone'},
                    {data: 'address', name: 'address'},
                    {data: 'country', name: 'country'},
                    {data: 'points', name: 'points'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });

        });
    </script>
@endsection
