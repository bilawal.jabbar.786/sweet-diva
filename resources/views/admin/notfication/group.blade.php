@extends('layouts.admin')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <!-- <h1>DataTables</h1> -->
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Domicile</a></li>
                            <li class="breadcrumb-item active">Groupes</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <!-- /.card -->
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Groupes</h3>
                                <button class="btn btn-primary btn-sm float-right" data-toggle="modal"
                                        data-target="#exampleModal"><i class="fa fa-plus" aria-hidden="true"></i>
                                    Ajouter un nouveau
                                </button>
                                <button class="btn btn-success btn-sm float-right" style="margin-right: 10px;"
                                        data-toggle="modal" data-target="#exampleModal2">Envoyer une notification
                                </button>
                                <button class="btn btn-danger btn-sm float-right" style="margin-right: 10px;"
                                        data-toggle="modal" data-target="#exampleModal3">Clients
                                </button>
                                @if(Auth::user()->role == 0)
                                    <button class="btn btn-warning btn-sm float-right" style="margin-right: 10px;"
                                            data-toggle="modal" data-target="#exampleModal4">Pays
                                    </button>
                                    <button class="btn btn-secondary btn-sm float-right" style="margin-right: 10px;"
                                            data-toggle="modal" data-target="#exampleModal5">Tailles
                                    </button>
                                    <button type="button" id="sendNewSms" style="margin-right: 10px;"
                                            class="btn btn-sm btn-info btn-sm float-right"
                                            data-toggle="modal" data-target="#modal-share"><i
                                            class="fa fa-share-alt"></i> Boutique
                                    </button>
                                @endif
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Nom</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($groups as $row)
                                        <tr>
                                            <td>{{$row->id}}</td>
                                            <td>{{$row->name}}</td>
                                            <td>
                                                <a href="{{route('admin.group.show' , ['id'=>$row->id])}}" id="edit"
                                                   class="btn btn-sm btn-success" title="Éditer">
                                                    <i class="fa fa-eye"></i>
                                                </a>
                                                <a href="{{route('admin.group.delete' , ['id'=>$row->id])}}" id="delete"
                                                   class="btn btn-sm btn-danger" data-toggle="tooltip"
                                                   title="Supprimer">
                                                    <i class="fa fa-times"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal5" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Envoyer des notifications via les tailles</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('admin.send.notfication.sizes')}}" method="post"
                          enctype="multipart/form-data" data-parsley-validate>
                        @csrf
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Sélectionnez une taille</label>
                                <select class="select2" name="size" style="width: 100%;">
                                    <option value="TU">TU</option>
                                    <option value="XL(44/46)">XL (44/46)</option>
                                    <option value="1XL(46/48)">1XL (46/48)</option>
                                    <option value="2XL(48/50)">2XL (48/50)</option>
                                    <option value="3XL(50/52)">3XL (50/52)</option>
                                    <option value="4XL(52/54)">4XL (52/54)</option>
                                    <option value="5XL(54/56)">5XL (54/56)</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="title"><b>URL </b><span class="text-danger">*</span></label>
                                <input type="text" class="form-control" placeholder="URL" name="generate_id">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="title"><b>Message </b><span class="text-danger">*</span></label>
                                <textarea class="form-control" rows="4" name="message" placeholder="Message"></textarea>
                            </div>
                        </div>
                        <div class="col-md-12 pull-right">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-block">Envoyer</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Pays Cliente</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('admin.send.notfication.country')}}" method="post"
                          enctype="multipart/form-data" data-parsley-validate>
                        @csrf
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Choisissez le Utilisatrice</label>
                                <select class="select2" name="country" style="width: 100%;">
                                    <option value="Martinique">Martinique</option>
                                    <option value="Guadeloupe">Guadeloupe</option>
                                    <option value="France">France</option>
                                    <option value="Guyane">Guyane</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="title"><b>URL </b><span class="text-danger">*</span></label>
                                <input type="text" class="form-control" placeholder="URL" name="generate_id">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="title"><b>Message </b><span class="text-danger">*</span></label>
                                <textarea class="form-control" rows="4" name="message" placeholder="Message"></textarea>
                            </div>
                        </div>
                        <div class="col-md-12 pull-right">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-block">Envoyer</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Clients à sélection multiple</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('admin.send.notfication.users')}}" method="post" enctype="multipart/form-data"
                          data-parsley-validate>
                        @csrf
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Clients</label>
                                <select class="select2" multiple="" name="user_id[]"
                                        data-placeholder="Select a State" style="width: 100%;">
                                    <option value="send_to_all">Envoyer tout</option>
                                    @foreach($users as $row)
                                        <option
                                            value="{{$row->id}}">{{$row->name . ' ' . $row->surname}}</option>
                                    @endforeach
                                </select></div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="title"><b>URL </b><span class="text-danger">*</span></label>
                                <input type="text" class="form-control" placeholder="URL" name="generate_id">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="title"><b>Message </b><span class="text-danger">*</span></label>
                                <textarea class="form-control" rows="4" name="message" placeholder="Message"></textarea>
                            </div>
                        </div>
                        <div class="col-md-12 pull-right">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-block">Envoyer</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Envoyer une notification</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('admin.send.notfication')}}" method="post" enctype="multipart/form-data"
                          data-parsley-validate>
                        @csrf
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="title"><b>Groupes Clients</b><span class="text-danger">*</span></label>
                                <select name="user_id" class="form-control" id="">
                                    <option value="">Sélectionnez une option</option>
                                    <option value="send_to_all">Envoyer tout</option>
                                    @foreach($groups as $row)
                                        <option value="{{$row->id}}">{{$row->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="title"><b>URL </b><span class="text-danger">*</span></label>
                                <input type="text" class="form-control" placeholder="URL" name="generate_id">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="title"><b>Message </b><span class="text-danger">*</span></label>
                                <textarea class="form-control" rows="4" name="message" placeholder="Message"></textarea>
                            </div>
                        </div>
                        <div class="col-md-12 pull-right">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-block">Envoyer</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="modal-share">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Notification de la boutique</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post" action="{{route('admin.send.notfication.shop')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <input type="hidden" name="users" id="myField" value="" required=""/>
                        <div class="form-group">
                            <label for="exampleInputFile">Choisissez la boutique</label>
                            <select class="form-control select2bs4" name="shop" id="methods" required=""
                                    style="width: 100% !important;">
                                <option value="">--SÉLECTIONNER LA BOUTIQUE--</option>
                                <option value="SWEET DIVA MARTINIQUE">SWEET DIVA MARTINIQUE</option>
                                <option value="SWEET DIVA BESSON">SWEET DIVA BESSON</option>
                                <option value="SWEET DIVA BASSE TERRE">SWEET DIVA BASSE TERRE</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="title"><b>URL </b><span class="text-danger">*</span></label>
                            <input type="text" class="form-control" placeholder="URL" name="generate_id">
                        </div>
                        <div class="form-group">
                            <label for="title"><b>Message </b><span class="text-danger">*</span></label>
                            <textarea class="form-control" rows="4" name="message" placeholder="Message"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                        <button type="submit" class="btn btn-primary">Soumettre</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ajouter de nouveaux groupes</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('admin.group.store')}}" method="post" enctype="multipart/form-data"
                          data-parsley-validate>
                        @csrf
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Clients</label>
                                <select class="select2" multiple="" name="user_id[]"
                                        data-placeholder="Select a State" style="width: 100%;">
                                    @foreach($users as $row)
                                        <option data-select2-id="{{$row->id}}"
                                                value="{{$row->id}}">{{$row->name . ' ' . $row->surname}}</option>
                                    @endforeach
                                </select></div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="title"><b>Nom </b><span class="text-danger">*</span></label>
                                <input type="text" name="name" required placeholder="Nom du groupe"
                                       class="form-control">
                            </div>
                        </div>
                        {{--                        <div class="col-md-12">--}}
                        {{--                            <div class="form-group">--}}
                        {{--                                <label for="title"><b>URL </b><span class="text-danger">*</span></label>--}}
                        {{--                                <input type="text" class="form-control" placeholder="URL" name="generate_id" >--}}
                        {{--                            </div>--}}
                        {{--                        </div>--}}
                        <div class="col-md-12 pull-right">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-block">Valider</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div></div>
@endsection
