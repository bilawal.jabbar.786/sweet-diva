@extends('layouts.admin')
<style>
    .timeline {
         margin: 0 0 0px !important;
        padding: 0;
        /* position: relative; */
    }
</style>
@section('content')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <!-- <h1>DataTables</h1> -->
                    </div>

                </div>
            </div><!-- /.container-fluid -->
        </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">

                <!-- /.col -->
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header p-2">
{{--                            <ul class="nav nav-pills">--}}
{{--                                <li class="nav-item "><a class="nav-link active" href="#timeline" data-toggle="tab">Timeline</a></li>--}}
{{--                            </ul>--}}
                        </div><!-- /.card-header -->
                        <div class="card-body">
                            <div class="tab-content">
                                <div class="tab-pane active" id="timeline">
                                    <!-- The timeline -->
                                   @if($notfication->isempty())
                                       <h4 style="text-align: center;">Pas de données disponibles</h4>
                                    @else
                                    @foreach($notfication as $row)

                                    <div class="timeline timeline-inverse">
                                        <!-- timeline time label -->
                                        <div class="time-label">

                                        </div>
                                        <!-- /.timeline-label -->
                                        <!-- timeline item -->
                                        <div>
                                            @if($row->activity == "Bienvenue")
                                            <i class="fas fa-envelope bg-primary"></i>

                                            <div class="timeline-item">
                                                <span class="time"><i class="far fa-clock"></i>
                                                    <?php
                                                    \Carbon\Carbon::setLocale('fr');
                                                    $date = \Carbon\Carbon::parse($row->created_at);
                                                    ?>
                                                 <b>{{$date->diffForHumans()}}</b>
                                                </span>

                                                <h3 class="timeline-header"><a href="#">{{$row->activity}}</a> </h3>

                                                <div class="timeline-body">
                                                    {{$row->message}}
                                                </div>

                                            </div>

                                        </div>
                                        <!-- END timeline item -->
                                        <!-- timeline item -->
                                        <div>
                                            @else
                                            <i class="fas fa-user bg-info"></i>

                                            <div class="timeline-item">
                                               <span class="time"><i class="far fa-clock"></i>
                                                    <?php
                                                   \Carbon\Carbon::setLocale('fr');
                                                   $date = \Carbon\Carbon::parse($row->created_at);
                                                   ?>
                                                   {{$date->diffForHumans()}}
                                                </span>

                                                <h3 class="timeline-header border-0"><a href="#">{{$row->activity}}</a> {{$row->message}}
                                                </h3>
                                            </div>
                                        </div>
                                        <!-- END timeline item -->
                                        <!-- timeline item -->
                                        <div>

                                        </div>
                                        <!-- END timeline item -->
                                        <!-- timeline time label -->
                                        <div class="time-label">

                                        </div>
                                        <!-- /.timeline-label -->
                                        <!-- timeline item -->
                                        <div>

                                        </div>
                                        <!-- END timeline item -->
                                        <div>

                                        </div>
                                    </div>
                                        @endif
                                    @endforeach
                                    @endif

                                </div>
                                <!-- /.tab-pane -->

                            </div>
                            <!-- /.tab-content -->
                        </div><!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    </div>
@endsection
