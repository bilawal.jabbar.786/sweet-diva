@extends('app.include.layouts')
@section('content')
    <style>


        .cart-table table a {
            color: #020310;
            font-weight: 700;
            font-size: 18px;
        }

        element.style {
        }
        body, html {
            font-family: 'Poppins', sans-serif;
            overflow-x: hidden;
            color: #fee1e5 !important;
            background-color: #fee1e5 !important;
        }
    </style>

    <div class="page-content-wrapper" style="background-color: #fee1e5" >
        <div class="container" style="background-color: #fee1e5"  >

            <!-- Cart Wrapper-->
            <div class="cart-wrapper-area py-3"  >

                <div class="cart-table card mb-3 cartt" style="background-color: #fee1e5 !important; border:#fee1e5!important; "  >
                    @if(Auth::user()->refferal!=null)
                    <div><h5 id="copycontent"  style="  margin-top: 30px !important; margin-left: 34px!important; margin: 0;display: inline-block; text-align: center!important; " id="myInput"> {{Auth::user()->refferal}} </h5><button class="btn btn-primary" style="margin-left: 34px;" onclick="mycopyFunction()" ><i class="lni lni-empty-file" ></i>  Copie</button></div><hr>
                    @else
                        <a class="btn btn-warning"  href="{{route('app.generate.code')}}" style="width: 156px;margin-left: 103px; margin-top:20px; background-color: pink">Générer un code </a><hr>

                        @endif
                        <div class="container" style="background-color: #fee1e5"   >
                            <!-- Notifications Area-->
                            <div class="notification-area ">

                               @if($refferal->isempty())
                                    <h6 style="text-align: center">Parrainez vos proches, et gagnez des points dès leur premier achat.Copies ton code, et partages le en 1 clic</h6>
                                @else
                                    <div class="list-group">
                                    @foreach($refferal as $row)
                                            <div class="row" style="background-color: #fee1e5"  >
                                                <div class="col-12">
                                                    <a style="background-color: #fee1e5"  class="list-group-item d-flex align-items-center" href="#"><span class="noti-icon"><i class="lni lni-gift"></i></span>

                                                        <div class="noti-info" >
                                                            <h6 class="mb-0">{{$row->user->name.' '.$row->user->surname}}</h6>
                                                        </div></a>
                                                </div>
                                            </div>


                                        @endforeach

                                    </div>

                                @endif
                            </div>
                        </div>


                </div>


                <!-- Coupon Area-->


            </div>
        </div>
    </div>



@endsection
<script>
    function mycopyFunction() {
        var copyText = document.getElementById("copycontent").innerText;
        var elem = document.createElement("textarea");
        document.body.appendChild(elem);
        elem.value = copyText;
        elem.select();
        document.execCommand("copy");
        document.body.removeChild(elem);
    }
</script>
