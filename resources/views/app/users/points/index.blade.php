@extends('app.include.layouts')
@section('content')
    <style>


        .cart-table table a {
            color: #020310;
            font-weight: 700;
            font-size: 18px;
        }
        .cart-table img {
            max-width: 37.5rem;
            border: 1px solid #ebebeb;
        }

    </style>

    <div class="page-content-wrapper">
        <div class="container">

            <!-- Cart Wrapper-->
            <div class="cart-wrapper-area py-3" >

                <div class="cart-table card  cartt" style="background-color: #fee1e5" >
                    <h3 style="margin-left: 10px; margin-top: 10px; text-align: center;">Vos points {{Auth::user()->points}}</h3><hr>
                    <!-- Page Content Wrapper-->
                    <img src="{{asset('sweetdiva.jpeg')}}" >
                        <div class="container" style="background-color: #fee1e5" >

                            <!-- Notifications Area-->
                            <div class="notification-area " style="background-color: #fee1e5" >

                                <div class="list-group" style="background-color: #fee1e5; " >
                                @foreach($points as $point)
                                    <!-- Single Notification-->

                                    <div class="row" style="margin-top: 10px;">
                                        <div class="col-7">
                                            <a style="background-color: #fee1e5" class="list-group-item d-flex align-items-center" href="#"><span class="noti-icon"><i class="lni lni-gift"></i></span>

                                                <div class="noti-info" >
                                                    <h6 class="mb-0">{{$point->name}}</h6>
                                                </div></a>
                                        </div>
                                        <div class="col-2">
                                         <b style="color: black; ">{{$point->points}}</b>
                                        </div>
                                        <div class="col-2">
                                            <a href="{{route('getmypoints', ['id' => $point->id])}}"><b style="margin-left: 40px;">🎁</b></a>
                                        </div>
                                    </div>



                                    @endforeach
<hr>
                                    <div class="row" style="margin-top: 10px;">
                                        <div class="col-12">
                                            <a style="background-color: #fee1e5" class="list-group-item d-flex align-items-center" href="#">
                            <?php $gs = \App\GeneralSettings::first();?>
                                                <div class="noti-info" >
                                                    <p>{{$gs->points}}</p>
                                                </div></a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>




                <!-- Coupon Area-->


            </div>
        </div>
    </div>



@endsection
