@extends('app.include.layouts')
@section('content')
    <div class="page-content-wrapper">
        <div class="container">
            <div class="my-order-wrapper py-3">
                <!-- Single Order Status-->
                <div class="single-order-status">
                    <div class="card bg-warning mb-3">
                        <div class="card-body d-flex align-items-center">
                            <div class="order-icon"><i class="lni lni-checkmark-circle"></i></div>
                            <div class="order-status">Mes Commandes</div>
                        </div>
                    </div>
                    <div class="row g-3">
                        <!-- Single Ordered Product Card-->
                        @foreach($orders as $row)
                        <div class="col-12 col-md-12 col-lg-12">
                            <div class="card top-product-card">
                                <div class="card-body"><span class="badge badge-success">#{{$row->order_number}}</span><a class="wishlist-btn" href="#" style="color: #ffaf00"><i class="fa fa-heart"></i></a><a class="product-thumbnail d-block" href="{{route('app.single.order', ['id' => $row->id])}}"><img class="mb-2"  loading="lazy" src="img/product/3.jpg" alt=""></a><a class="product-title d-block" href="{{route('app.single.order', ['id' => $row->id])}}">
                                        @if($row->status == '0')
                                            Nouvelle commande
                                        @else
                                            Compléter
                                        @endif
                                    </a>
                                    <p class="sale-price">{{$row->total}}€</p>
                                    <div class="product-rating"><h6>{{$row->created_at->format('d-m-y')}}</h6></div><a class="btn btn-success btn-sm add2cart-notify" href="{{route('app.single.order', ['id' => $row->id])}}"><i class="lni lni-eye"></i></a>
                                </div>
                            </div>
                        </div>
                        @endforeach

                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
