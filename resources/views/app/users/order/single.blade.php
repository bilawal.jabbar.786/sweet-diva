@extends('app.include.layouts')
<style>
    .single-user-review .user-thumbnail {
        margin-top: 0.5rem;
        -webkit-box-flex: 0;
        -ms-flex: 0 0 40px;
        flex: 0 0 40px;
        width: 74px !important;
        max-width: 108px !important;
        margin-right: 0.5rem;
    }
</style>
@section('content')

    <div class="page-content-wrapper">
        <div class="container">
            <div class="shipping-method-choose mb-3 py-3">
                <div class="card shipping-method-choose-title-card bg-primary">
                    <div class="card-body">
                        <h6 class="text-center mb-0 text-white">Facture # {{$order->order_number}}</h6>
                    </div>
                </div>
                <div class="card shipping-method-choose-card">
                    <div class="card-body">
                        <div class="shipping-method-choose">
                            <div class="rating-and-review-wrapper bg-white py-3 mb-3">
                                <div class="container">

                                    <div class="rating-review-content">
                                        <table style="width:100%;">
                                            <thead >
                                            <tr >
                                                <th>Des produits</th>
                                                <th>Quantité</th>
                                                <th>Size</th>
                                                <th>Color</th>
{{--                                                <th>Prix</th>--}}
                                            </tr>

                                            </thead>
                                            <tbody>
                                           <tr>
                                               @foreach(json_decode($order->products) as $item)

                                               <td style="text-align: center;"><b>{{$item->name}}</b></td>
                                               <td style="text-align: center;"><b>{{$item->quantity}}</b></td>
                                               <td><b>{{$item->attributes->size}}</b></td>
                                               <td><input type="color" value="{{$item->attributes->color}}" disabled ></td>
{{--                                                   <td style="text-align: center;"><b>{{$item->price}}</b></td>--}}
                                               @endforeach
                                           </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Checkout Wrapper-->
            <div class="checkout-wrapper-area ">
                <!-- Billing Address-->
                <div class="billing-information-card mb-3">
                    <div class="card billing-information-title-card bg-primary">
                        <div class="card-body">
                            <h6 class="text-center mb-0 text-white">Détails de la commande</h6>
                        </div>
                    </div>
                    <div class="card user-data-card">
                        <div class="card-body">
                            <div class="single-profile-data d-flex align-items-center justify-content-between">
                                <div class="title d-flex align-items-center"><i class="lni lni-user"></i><span>Nom</span></div>
                                <div class="data-content">{{$order->name}}</div>
                            </div>
                            <div class="single-profile-data d-flex align-items-center justify-content-between">
                                <div class="title d-flex align-items-center"><i class="lni lni-envelope"></i><span>E-mail </span></div>
                                <div class="data-content">{{$order->email}}</div>
                            </div>
                            <div class="single-profile-data d-flex align-items-center justify-content-between">
                                <div class="title d-flex align-items-center"><i class="lni lni-phone"></i><span>Telephone</span></div>
                                <div class="data-content">{{$order->phone}}</div>
                            </div>
                            <div class="single-profile-data d-flex align-items-center justify-content-between">
                                <div class="title d-flex align-items-center"><i class="lni lni-map-marker"></i><span> Adresse</span></div>
                                <div class="data-content">{{$order->address}}</div>
                            </div>
                            <div class="single-profile-data d-flex align-items-center justify-content-between">
                                <div class="title d-flex align-items-center"><i class="lni lni-map-marker"></i><span> Pays</span></div>
                                <div class="data-content">{{$order->country}}</div>
                            </div>
                            <div class="single-profile-data d-flex align-items-center justify-content-between">
                                <div class="title d-flex align-items-center"><i class="lni lni-envelope"></i><span>Remarques </span></div>
                                <div class="data-content" > {{$order->notes}}</div>
                            </div>
                            <div class="single-profile-data d-flex align-items-center justify-content-between">
                                <div class="title d-flex align-items-center"><i class="lni lni-user"></i><span>Statut</span></div>
                                <div class="data-content">
                                    @if($order->status == '0')
                                        Nouvelle commande
                                    @else
                                        Compléter
                                    @endif
                                </div>
                            </div>
                        @if(Auth::user()->role == 1)
                            @if($order->status == '0')
                            <!-- Edit Address--><a class="btn btn-primary w-100" href="{{route('user.order-status', ['id' => $order->id])}}">Valider la réception de votre commande</a>
                                @endif
                            @endif

                        </div>
                    </div>
                </div>
                <!-- Shipping Method Choose-->


            </div>
        </div>
    </div>
@endsection
