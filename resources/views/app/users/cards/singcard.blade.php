@extends('app.include.layouts')
<style>


    .owl-carousel .owl-item img {
        display: block;
        width: 100%;
        height: 100%;
    }
    .mb-3 {
         margin-bottom: -2rem!important;
    }
</style>
@section('content')
    <div class="page-content-wrapper">
        <!-- Product Slides-->
        <div class="product-slides owl-carousel">
            <!-- Single Hero Slide catagory-single-img-->
            <div class="" ><img style="height: 50%;" src="{{asset($card->photo)}}"  loading="lazy"></div>

        </div>
        <div class="product-description pb-3">
            <!-- Product Title & Meta Data-->
            <div class="product-title-meta-data bg-white mb-3 py-3">
                <div class="container d-flex justify-content-between">
                    <div class="p-title-price">
                        <h6 class="mb-1">{{$card->title}}</h6>
                        <p class="mb-1">Code du bon cadeau : <b>{{$card->sku}}</b></p>
                    </div>

                </div>
                <!-- Ratings-->
                <div class="product-ratings">
                    <div class="container d-flex align-items-center justify-content-between">

                <div ><h3>Prix : <b class="priceenter">50</b> €</h3></div>
                    </div>
                </div>
            </div>
            <!-- Flash Sale Panel-->
            <div class="flash-sale-panel bg-white mb-3 py-3">
                <div class="container">
                    <!-- Sales Offer Content-->

                        <!-- Sales End-->
                    <form action="{{route('app.card.pay', ['id' => $card->id])}}" method="POST">
                        @csrf
                        <div class="sales-end">

                            <p>Sélectionnez un montant :
                                <select name="price"  style="margin-left: 33px;width: 125px;" onchange="amount(this)" id="">
                                    <option value="50">50€</option>
                                    <option value="100">100€</option>
                                    <option value="150">150€</option>
                                    <option value="200">200€</option>
                                    <option value="250">250€</option>
                                    <option value="300">300€</option>
                                </select></p>
                        </div>
                        <!-- Sales Volume-->

                    </div>
                </div>
            </div>
            <!-- Selection Panel-->
            <div class="selection-panel bg-white mb-3 py-3">
                <div class="container d-flex align-items-center justify-content-between">
                    <!-- Choose Color-->
                    <div class="choose-color-wrapper">

                            <div class="card-body p-4 d-flex align-items-center">
                                <?php $user = Auth::user();?>
                                     </div>

                                        </div>
                            </div>
                            <!-- User Meta Data-->
                            <div class="card user-data-card">
                                <div class="card-body">

                                    <div class="mb-3">
                                        <div class="title mb-2"><i class="lni lni-user"></i><span>Nom du destinataire</span></div>
                                        <input class="form-control" type="text" name="name" required>
                                    </div>
                                    <div class="mb-3">
                                        <div class="title mb-2"><i class="lni lni-envelope"></i><span>Destinataire E-mail </span></div>
                                        <input class="form-control" type="email" name="email"required >
                                    </div>
                                    <div class="mb-3">
                                        <div class="title mb-2"><i class="lni lni-user"></i><span>Votre nom </span></div>
                                        <input class="form-control" type="text" name="sendername" required>
                                    </div>

                                    <div class="mb-3">
                                        <div class="title mb-2"><i class="lni lni-phone"></i><span>Votre Telephone</span></div>
                                        <input class="form-control" name="senderphone" type="text" >
                                    </div>


                                    <div class="mb-3">
                                        <div class="title mb-2"><i class="lni lni-user"></i><span>Message</span></div>
                                        <textarea class="form-control mb-3" id="message" name="message"cols="8" rows="10" placeholder="Écris quelque chose..."></textarea>
                                    </div>
                                    <button class="btn btn-primary w-100" type="submit">Ajouter au panier
                                        </button>
                        </form>
                    </div>

                </div>
            </div>

        </div>
    </div>

@endsection
<script>
    function amount(elem) {

        $('.priceenter').html(elem.value);
    }
</script>

