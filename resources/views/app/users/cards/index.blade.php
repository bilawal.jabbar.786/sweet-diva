@extends('app.include.layouts')
@section('content')
    <div class="page-content-wrapper">
        <!-- Top Products-->
        <div class="top-products-area py-3">
            <div class="container">
                <div class="section-heading d-flex align-items-center justify-content-between">

                </div>
                <div class="single-order-status">
                    <div class="card bg-warning mb-3">
                        <div class="card-body d-flex align-items-center">
                            <div class="order-icon"><i class="lni lni-checkmark-circle"></i></div>
                            <div class="order-status">{{$title}}</div>
                        </div>
                    </div>
                <div class="row g-3">
                    <!-- Featured Product Card-->
                    @foreach($cards as $card)
                    <div class="col-12 col-md-12 col-lg-12">
                        <div class="card featured-product-card">
                            <div class="card-body"><span class="badge badge-warning custom-badge"><i class="lni lni-star"></i></span>
                                <div class="product-thumbnail-side"><a class="product-thumbnail d-block" href="{{route('app.single.card',['id'=>$card->id])}}"><img src="{{asset($card->photo)}}"  loading="lazy" alt=""></a></div>
                                <div class="product-description"><a class="product-title d-block" style="font-size: 19px;" href="{{route('app.single.card',['id'=>$card->id])}}">{{$card->title}}</a>
                                    <p class="sale-price" style="font-size: 12px"> Code du bon cadeau : <b >{{$card->sku}}</b></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach

                </div>
            </div>
        </div>
    </div>
@endsection
