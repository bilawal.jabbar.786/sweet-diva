@extends('app.include.layouts')
<style>
    body, html {
        font-family: 'Poppins', sans-serif;
        color: #848484;
        overflow-x: hidden;
        /* background-color: #c97ea6; */
        background: linear-gradient(to left, #ffe1e7, #ffe1e7);
    }

    .card1 .card-body1 {
        padding: 5rem !important;
    }

    .top-product-card .wishlist-btn {
        position: revert !important;
        float: right;
        top: 1rem;
        right: 1rem;
        z-index: 10;
        color: #ea4c62;
        font-size: 1.25rem;
        line-height: 1;
    }
    .single-hero-slide {
        position: relative;
        z-index: 1;
        width: 100%;
        background-position: center center;
        background-size: cover;
    }

    .blog-card .post-img::after {
        position: absolute;
        width: 75% !important;
        height: 54% !important;
        top: 0;
        left: 0;
        content: "";
        background-color: e !important;
        opacity: 0.3;
        z-index: 1;
        border-radius: 0.75rem;
    }

</style>
<?php $gs = \App\GeneralSettings::first();?>
@section('content')

    <div class="page-content-wrapper">
        <!-- Hero Slides-->
        <div class="hero-slides owl-carousel">

            <div class="single-hero-slide">
                <img src="{{asset($banners->image1)}}" loading="lazy" style="width: 100%; height: 100%;">
            </div>
            <!-- Single Hero Slide-->
            <div class="single-hero-slide">
                <img src="{{asset($banners->image2)}}" loading="lazy" style="width: 100%; height: 100%;">
            </div>
            <!-- Single Hero Slide-->
            <div class="single-hero-slide">
                <img src="{{asset($banners->image3)}}" loading="lazy" style="width: 100%; height: 100%;">
            </div>



        </div>

        @auth
            <div style="background-color: black;  border-top: 0px solid white">
                <marquee>
                    <p style="font-family: Impact; font-size: 15pt; color: #ffe1e7!important; margin: 0px !important; font-weight: 700;">
                        {{$gs->home}}
                    </p>
                </marquee>
            </div>
    @endauth
    <!-- Product Categories-->
        <!-- Our Trending Products-->
        <div class="top-products-area clearfix " style="background-color: #ffe1e7;">
            <div class="container" >
                <div class="row">
                    {{--                        <div class="col-12">--}}
                    {{--                            <input class="form-control" style="max-width: 300px;!important; height: 39px!important; margin-bottom: 10px!important;margin-left: 30px;" name="keywords" type="search" placeholder="Entrez votre mot-clé" required>--}}
                    {{--                            <button type="submit" style="padding-top: 20px; padding-left: 310px;"><i class="fa fa-search"></i></button>--}}
                    {{--                        </div>--}}
                    <?php $size = \App\Size::whereNotNull('newsize')->pluck('newsize')->unique();?>
                    <div class="col-12 p-3">
                        <select class="form-control select2" name="size"  onchange="location = this.options[this.selectedIndex].value;">
                            <option value="" >Choisissez votre taille</option>
                            <option value="{{route('app.search.product.size',['size'=>encrypt('TU')] ) }}">TU</option>
                            <option value="{{route('app.search.product.size',['size'=>encrypt('XL')] ) }}">XL (44/46)</option>
                            <option value="{{route('app.search.product.size',['size'=>encrypt('1XL')] ) }}">1XL (46/48)</option>
                            <option value="{{route('app.search.product.size',['size'=>encrypt('2XL')] ) }}">2XL (48/50)</option>
                            <option value="{{route('app.search.product.size',['size'=>encrypt('3XL')] ) }}">3XL (50/52)</option>
                            <option value="{{route('app.search.product.size',['size'=>encrypt('4XL')] ) }}">4XL (52/54)</option>
                            <option value="{{route('app.search.product.size',['size'=>encrypt('5XL')] ) }}">5XL (54/56)</option>



                        </select>
                    </div>

                </div>
                <div class="row" style="background-color: white !important;">
                    @foreach($maincategories as $key => $row)
                        @if($key % 2 == 0)
                            <div class="col-6" style="padding: 0px !important;">
                                <div class="hero-slides owl-carousel">
                                    <div class="single-hero-slide">
                                        @if($row->photo)
                                            <img loading="lazy" style="width: 100%;" src="{{asset($row->photo)}}"
                                                 alt=""></div>
                                    @endif
                                    @if($row->photo2)
                                        <div class="single-hero-slide">
                                            <img loading="lazy" style="width: 100%;" src="{{asset($row->photo2)}}"
                                                 alt=""></div>
                                    @endif
                                    @if($row->photo3)
                                        <div class="single-hero-slide">
                                            <img loading="lazy" style="width: 100%;" src="{{asset($row->photo3)}}"
                                                 alt=""></div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-6"
                                 style="padding: 20px; text-align: center; margin-top: 20px; padding-left: 0px !important;padding-right: 0px !important; ">
                                <h3>{{$row->name}}</h3>
                                <a href="{{route('app.product',['id'=>$row->id])}}">
                                    <button
                                        style="color: black; margin-top:10px;  background-color: white; text-align: center; padding: 10px; width: 90%; border: 2px solid black">
                                        <b style="margin: 15px; font-size: 14px;">Sélectionner</b>
                                    </button>
                                </a>
                            </div>
                        @else
                            <div class="col-6"
                                 style="padding: 20px; text-align: center; margin-top: 20px; padding-left: 0px !important;padding-right: 0px !important;">
                                <h3>{{$row->name}}</h3>
                                <a href="{{route('app.product',['id'=>$row->id])}}">
                                    <button
                                        style="color: black; margin-top:10px;  background-color: white; text-align: center; padding: 10px; width: 90%; border: 2px solid black">
                                        <b style="margin: 15px; font-size: 14px;">Sélectionner</b>
                                    </button>
                                </a>
                            </div>
                            <div class="col-6 " style="padding: 0px !important;">
                                <div class="hero-slides owl-carousel">
                                <div class="single-hero-slide">
                                    @if($row->photo)
                                    <img loading="lazy" style="width: 100%;" src="{{asset($row->photo)}}"
                                     alt=""></div>
                                    @endif
                                    @if($row->photo2)
                                    <div class="single-hero-slide">
                                        <img loading="lazy" style="width: 100%;" src="{{asset($row->photo2)}}"
                                                                        alt=""></div>
                                    @endif
                                    @if($row->photo3)
                                    <div class="single-hero-slide">
                                        <img loading="lazy" style="width: 100%;" src="{{asset($row->photo3)}}"
                                             alt=""></div>
                                    @endif
                                </div>
                            </div>
                        @endif
                        {{--                        <div class="col-6 col-md-6 col-lg-6"--}}
                        {{--                             style="padding: 0px !important;">--}}
                        {{--                            <div class="card catagory-card"--}}
                        {{--                                 style="background-image:url('{{asset($row->photo)}}'); background-repeat: no-repeat; background-size: 100% 100%; position: relative; width: 100%;  text-align: center; ">--}}
                        {{--                                <div class="card-body" style="height: 200px;">--}}
                        {{--                                    <a href="{{route('app.subcategory',['id'=>$row->id])}}">--}}
                        {{--                                        <button--}}
                        {{--                                            style="color: black; margin-top:110px;  bottom: 22px; background-color: white; text-align: center; ">--}}
                        {{--                                            <b style="margin: 15px; font-size: 14px;">{{$row->name}}</b></button>--}}
                        {{--                                    </a>--}}
                        {{--                                </div>--}}
                        {{--                            </div>--}}
                        {{--                        </div>--}}

                    @endforeach


                </div>
            </div>
        </div>
        <div class="cta-area">
            <div class="container">
                <a href="{{route('this.month.product')}}">
                    <img src="{{asset('app/23-min.png')}}" loading="lazy"
                                                                style="border-radius: 43px;">
                </a>
            </div>
        </div>
        <br>
        <div class="top-products-area clearfix ">
            <div class="container">
                <a href="{{route('this.sold.product')}}">
                <div class="row">
                    <div class="col-8" style="background-color: white">
                        <h3 style="padding: 30px">Nos soldes !</h3>
                    </div>
                    <div class="col-4" style="text-align: center;  background-color: black">
                        <h3 style="margin-top: 25px; color: white; font-size: 30px;">SOLDES</h3>
                    </div>
                </div>
                </a>
            </div>
        </div>
        <br>
        @if($saleproduct)
            <?php
            $product = \App\Product::where('id', $saleproduct->product_id)->first();
            ?>
            @if($product)
                @if(!$saleproduct->date->ispast())
                        <div class="top-products-area clearfix "style="background-color: white">
                            <div class="container">
                                <a href="{{route('all.product')}}">
                                    <div class="row">

                                        <div class="col-5" style="margin: auto">
                                            <a href="{{route('single.product', ['id' => $product->id])}}"><img class="img-full lazy responsive" style=" " src="{{asset($product->photo1)}}" alt=""></a>
                                        </div>
                                        <div class="col-7 p-2" style="text-align: left;">
                                            <h4 style="">{{$product->title}}</h4>
                                            <p style="font-size: 30px; margin: 0px !important;"><b>{{$product->price}} €</b>
                                                @if($product->oldprice)
                                                    <del style="font-size: 20px"> {{$product->oldprice}} €</del></p>
                                            @endif
                                            <h2  class="heading" style="color: black; font-size: 20px"  id="demotime"></h2>
                                            <a href="{{route('single.product', ['id' => $product->id])}}"><button class="btn btn-primary mt-1">Acheter maintenant</button></a>
                                        </div>


                                    </div>
                                </a>
                            </div>
                        </div>
                        <br>

                @endif
            @endif
        @endif
{{--        <div class="top-products-area clearfix ">--}}
{{--            <div class="container">--}}
{{--                <div class="row">--}}
{{--                    <div class="col-7">--}}
{{--                        <a href="{{route('sweet.look')}}"><img src="{{asset('front/img.png')}}" alt=""></a>--}}
{{--                    </div>--}}
{{--                    <div class="col-5" style="padding-top: 40px">--}}
{{--                        <h3 style="font-size: 25px">Partage ton look et gagne des points !</h3>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}

        <div class="top-products-area clearfix ">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <a href="{{route('app.allcards')}}"><img src="{{asset('app/giftt-min.png')}}" alt=""></a>
                    </div>

                </div>
            </div>
        </div>

<!--

        <div style="background-image:url('{{asset($banners->videobanner)}}'); margin-top: 15px!important; background-repeat: no-repeat; background-size: 100% 100%; text-align: center; border-radius:20px;  " class="pik">
            <div id="pt35" style="padding-top: 160px; padding-bottom: 20px">
                <button id="myBtn" class="btn btn-success" onclick="addVideo()" >Appuyez sur PLAY</button>
            </div>
        </div>
        &lt;!&ndash; The Modal &ndash;&gt;
        <div id="myModal" class="modal1 myModal" style="display: none">

            &lt;!&ndash; Modal content &ndash;&gt;
            <div class="modal-content1">
                <span class="close1 " onclick="removeVideo()">&times;</span>

                <iframe id="video" width="100%" height="245" src="https://www.youtube.com/embed/{{$banners->videoid}}">
                </iframe>

            </div>

        </div>
-->

    <!--        <div class="top-products-area clearfix ">
            <div class="container">
                <div class="my-order-wrapper py-0">
                    &lt;!&ndash; Single Order Status&ndash;&gt;
                    <div class="single-order-status">
                        <div class="card bg-warning mb-4 ">
                            <div class="card-body d-flex align-items-center"
                                 style="background-color: white !important;">
                                <div class="order-icon"><img src="{{asset($banners->image9)}}" style="width: 80px; ">
                                </div>
                                <a href="{{route('all.product')}}">
                                    <div class="order-status" style="color: black !important; margin-left: 30px;">Nos
                                        produits tendance
                                    </div>
                                </a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>-->
{{--        <div class="top-products-area clearfix ">--}}
{{--            <div class="container">--}}
{{--                <div class="row">--}}
{{--                    <div class="col-12">--}}
{{--                        <a href="{{route('app.allcards')}}">--}}
{{--                            <img src="{{asset('app/assets/gift.jpeg')}}" loading="lazy"--}}
{{--                                 style="width: 100%; border-radius: 30px;">--}}
{{--                        </a>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="cta-area">--}}
{{--                <div class="container">--}}
{{--                    <a href="https://www.tiktok.com/@sweet_diva.fwi?lang=fr"> <img--}}
{{--                            src="{{asset('app/assets/socialone.png')}}" loading="lazy" style="height: 50%; width: 100%"></a>--}}

{{--                    <a class="" href="https://www.instagram.com/sweet_diva_fwi/" style="">--}}
{{--                        <img src="{{asset('app/assets/socialtwo.PNG')}}" loading="lazy"--}}
{{--                             style=" height: 19%; width: 49%">--}}
{{--                    </a>--}}

{{--                    <a class="" href="https://www.facebook.com/contact.sweetdiva/" style="">--}}
{{--                        <img src="{{asset('app/assets/socialthree.PNG')}}" loading="lazy"--}}
{{--                             style=" height: 19%; width: 49%">--}}
{{--                    </a>--}}
{{--                </div>--}}
{{--            </div>--}}
            <br><br>

            <div class="row g-3">
                <!-- Single Trending Product Card-->
                {{--                    @foreach($adminproducts as $row)--}}

                {{--                    <div class="col-6 col-md-4 col-lg-3">--}}
                {{--                        <div class="card top-product-card">--}}

                {{--                            <div class="card-body">--}}
                {{--                                <span class="badge badge-warning">Nouvelle</span>--}}
                {{--                                <?php $like = \App\Wishlist::where('user_id','=',Auth::user()->id)->where('product_id','=',$row->id)->first();?>--}}

                {{--                                <a class="product-thumbnail d-block" href="{{route('single.product',['id'=>$row->id])}}"><img class="mb-2"  loading="lazy" src="{{asset($row->photo1)}}" alt=""></a>--}}
                {{--                                <a class="product-title d-block " style="color: #ffaf00;font-size: 13px;" href="{{route('single.product',['id'=>$row->id])}}">{{$row->categorytype->name}}</a>--}}
                {{--                                <a class="product-title d-block" href="{{route('single.product',['id'=>$row->id])}}">{{$row->title}}</a>--}}

                {{--                                @if($row->oldprice)--}}
                {{--                                    <p class="sale-price ">{{$row->price}}€<span>{{$row->oldprice}}€</span></p>--}}
                {{--                                @else--}}
                {{--                                    <p class="sale-price">{{$row->price}}€</p>--}}
                {{--                                @endif--}}
                {{--                                @if($like)--}}
                {{--                                    <a class="wishlist-btn" href="{{route('wishlist.delete', ['id' => $row->id])}}" style="color: red;" ><i class="fa fa-heart"></i></a>--}}
                {{--                                @else--}}
                {{--                                    <a class="wishlist-btn fav{{$row->id}}" onclick="addtowishlist(this)" id="{{$row->id}}" href="#"><i class="lni lni-heart okk{{$row->id}}"></i></a>--}}
                {{--                                @endif--}}


                {{--                            </div>--}}
                {{--                        </div>--}}
                {{--                    </div>--}}
                {{--                    @endforeach--}}

            </div>
        <div class="row">
{{--            7063534421822934277--}}
            <blockquote class="tiktok-embed" cite="https://www.tiktok.com/@sweet_diva.fwi/video/{{$gs->tiktok}}" data-video-id="{{$gs->tiktok}}" style="max-width: 605px;min-width: 325px;" > <section> <a target="_blank" title="@sweet_diva.fwi" href="https://www.tiktok.com/@sweet_diva.fwi">@sweet_diva.fwi</a> Quelle Valentine es tu ? <a title="plussizeedition" target="_blank" href="https://www.tiktok.com/tag/plussizeedition">#plussizeedition</a>  <a title="valentinesday" target="_blank" href="https://www.tiktok.com/tag/valentinesday">#valentinesday</a> <a title="selflovebestlove" target="_blank" href="https://www.tiktok.com/tag/selflovebestlove">#selflovebestlove</a> <a title="guadeloupe" target="_blank" href="https://www.tiktok.com/tag/guadeloupe">#Guadeloupe</a> <a title="martinique" target="_blank" href="https://www.tiktok.com/tag/martinique">#Martinique</a> <a target="_blank" title="♬ Bòkò - Enposib" href="https://www.tiktok.com/music/Bòkò-6846823077251123202">♬ Bòkò - Enposib</a> </section> </blockquote> <script async src="https://www.tiktok.com/embed.js"></script>
        </div>
        </div>
    </div>
    <!-- Cool Facts Area-->


@endsection
<script>
    function addVideo() {
        $('.myModal').show();
        $('.pik').hide();

    }
</script>
<script>
    function removeVideo() {
        $('.myModal').hide();
        $('.pik').show();

    }
</script>
<script>
    // Get the modal
    var modal = document.getElementById("myModal");

    // Get the button that opens the modal
    var btn = document.getElementById("myBtn");

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close1")[0];

    // When the user clicks the button, open the modal
    btn.onclick = function () {
        modal.style.display = "block";
    }

    // When the user clicks on <span> (x), close the modal
    span.onclick = function () {
        modal.style.display = "none";
    }

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function (event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
</script>
<script>
    // Set the date we're counting down to
    // ->format('M d, Y H:m')
    var countDownDate = new Date("{{$saleproduct->date->format('c') ?? ''}}").getTime();

    // Update the count down every 1 second
    var x = setInterval(function() {

        // Get today's date and time
        var now = new Date().getTime();

        // Find the distance between now and the count down date
        var distance = countDownDate - now;

        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        // Output the result in an element with id="demo"
        document.getElementById("demotime").innerHTML = days + "jrs  " + hours + "h  "
            + minutes + "m  " + seconds + "s  ";

        // If the count down is over, write some text
        if (distance < 0) {
            clearInterval(x);
            document.getElementById("demotime").innerHTML = "EXPIRED";
        }
    }, 1000);
</script>
