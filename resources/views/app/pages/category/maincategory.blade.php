@extends('app.include.layouts')
@section('content')
    <div class="page-content-wrapper">


        <!-- Top Products-->
        <div class="top-products-area pb-3">
            <div class="container">
                <div class="section-heading d-flex align-items-center justify-content-between">

                </div>
                <div class="row g-3">
                    <!-- Single Top Product Card-->
                    @foreach($maincategories as $row)
                    <div class="col-6 col-md-4 col-lg-3">
                        <div class="card top-product-card">
                            <div class="card-body"><a class="product-thumbnail d-block" href="{{route('app.subcategory',['id'=>$row->id])}}"><img class="mb-2" style="width: 91%;" src="{{asset($row->photo)}}"  loading="lazy" alt=""></a><a class="product-title d-block" href="{{route('app.subcategory',['id'=>$row->id])}}" style="font-size: 17px; text-align: center;">{{$row->name}}</a>

                            </div>
                        </div>
                    </div>
                    @endforeach

                </div>
            </div>
        </div>
    </div>


@endsection
