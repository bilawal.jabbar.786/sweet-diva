@extends('app.include.layouts')
@section('content')
    <div class="page-content-wrapper py-3">
        <div class="container">

            <ul class="page-nav pl-0">
                @foreach($subcategories as $row )
                <li><a href="{{route('app.product',['id'=>$row->id])}}">{{$row->name}}<i class="lni lni-chevron-right"></i></a></li>
                @endforeach

            </ul>
        </div>
    </div>


@endsection
