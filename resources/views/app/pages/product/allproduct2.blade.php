@extends('app.include.layouts')
<style>
    .slidecontainer {
        width: 100%;
    }

    .slider {
        -webkit-appearance: none;
        width: 100%;
        height: 10px;
        border-radius: 5px;
        background: #d3d3d3;
        outline: none;
        opacity: 0.7;
        -webkit-transition: .2s;
        transition: opacity .2s;
    }

    .slider:hover {
        opacity: 1;
    }

    .slider::-webkit-slider-thumb {
        -webkit-appearance: none;
        appearance: none;
        width: 23px;
        height: 24px;
        border: 0;
        background: url('{{asset('app/assets/img/coment.jpeg')}}');
        background-repeat: no-repeat;
        cursor: pointer;
    }

    .slider::-moz-range-thumb {
        width: 23px;
        height: 24px;
        border: 0;
        background-repeat: no-repeat;
        background: url('{{asset('app/assets/img/coment.jpeg')}}');
        cursor: pointer;
    }
    .form-range {
        width: 63% !important;
        height: 0.4rem !important;
        padding: 0;
        background-color: transparent;
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
    }
</style>
@section('content')
    <div class="page-content-wrapper">
        <!-- Top Products-->
        <div class="top-products-area py-3">
            <div class="container">
                <div class="section-heading d-flex align-items-center justify-content-between">
                    <button class="btn btn-primary" onclick="document.getElementById('id01').style.display='block'" >Filtre</button>
                </div>
                <div class="row g-3">
                    <!-- Single Weekly Product Card-->
                    @foreach($product as $row)
                        <div class="col-12 col-md-6">
                            <div class="card weekly-product-card">
                                <div class="card-body d-flex align-items-center" style="padding: 0rem;">


                                    <div class="product-thumbnail-side">
                                        <?php $like = \App\Wishlist::where('user_id','=',Auth::user()->id)->where('product_id','=',$row->id)->first();?>
                                        @if($like)
                                            <a class="wishlist-btn" href="{{route('wishlist.delete', ['id' => $row->id])}}" style="color: red;" ><i class="fa fa-heart"></i></a>
                                        @else
                                            <a class="wishlist-btn fav{{$row->id}}" onclick="addtowishlist(this)" id="{{$row->id}}" href="#"><i class="lni lni-heart okk{{$row->id}}"></i></a>
                                        @endif
                                        <a class="product-thumbnail d-block" href="{{route('single.product',['id'=>$row->id])}}"><img src="{{asset($row->photo1)}}"  loading="lazy" alt=""></a></div>
                                    <div class="product-description">
                                        <a class="product-title d-block" href="{{route('single.product',['id'=>$row->id])}}">{{$row->title}}</a>
                                        <a class="product-title d-block " style="color: #ffaf00;font-size: 13px;" href="{{route('single.product',['id'=>$row->id])}}">{{$row->categorytype->name}}</a>

                                        @if($row->oldprice)
                                            <p class="sale-price ">{{$row->price}}€<span>{{$row->oldprice}}€</span></p>
                                        @else
                                            <p class="sale-price">{{$row->price}}€</p>
                                        @endif
                                        <?php
                                        //rateing
                                        $reviews = \App\Reviews::where('order_id','=',$row->id)->get();
                                        if (!$reviews->isempty()){
                                            $totalReview = \App\Reviews::where('order_id','=',$row->id)->sum('star');
                                            $total = $reviews->count();
                                            $totalReviews = round($totalReview / $total,1);
                                        }
                                        else{
                                            $reviews =null;
                                            $totalReviews=0;
                                            $total=0;

                                        }
                                        //endrating
                                        ?>
                                        <div class="product-rating"><i class="lni lni-star-filled"></i>{{$totalReviews}} ({{$total}})</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>
    </div><br><br>
    <!-- Modal content -->

    <div id="id01" class="w3-modal">
        <div class="w3-modal-content w3-card-4 w3-animate-zoom" style="max-width:100%;">


            <div class="page-content-wrapper">
                <div class="container" >
                    <!-- Profile Wrapper-->
                    <div class="profile-wrapper-area py-3">
                        <!-- User Information-->
                        <div class="card user-info-card">
                            <div class="w3-center"><br>
                                <span onclick="document.getElementById('id01').style.display='none'" class="w3-button w3-xlarge w3-hover-red w3-display-topright" title="Close Modal">&times;</span>

                            </div>
                            <div class="card-body p-4 d-flex align-items-center">

                                <div class="user-info">
                                    <h5 class="mb-0">Rechercher un Produit</h5>
                                </div>
                            </div>
                        </div>
                        <!-- User Meta Data-->
                        <div class="card user-data-card">
                            <div class="card-body">
                                <form action="{{route('app.filter.product')}}" method="post">
                                    @csrf
                                    <div class="mb-3">
                                        <div class="title mb-2"><span>Échelle des prix <b style="color: red; font-size: 18px;">*</b></span> </div>
                                        <input type="range" name="price" class="form-range" id="formControlRange" min="1" max="1000" onInput="$('#rangeval').html($(this).val())">  &nbsp; &nbsp; &nbsp;  &nbsp;  &nbsp; &nbsp; &nbsp;  &nbsp; <b id="rangeval">50 €</b>
                                    </div>
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="title mb-2"><span>Couleurs<b style="color: red; font-size: 18px;">*</b></span></div>

                                            </div>

                                            <div class="col-8">

                                                @foreach($color as $row)


                                                    <input class="form-check-input " type="radio" style="background-color: {{$row}} " value="{{$row}}" name="color" >


                                                @endforeach

                                            </div>
                                        </div>
                                        <div class="row" style="margin-top: 20px;">
                                            <div class="col-4">
                                                <div class="title mb-2"><span>Taille<b style="color: red; font-size: 18px;">*</b></span></div>

                                            </div>

                                            <div class="col-6">
                                                <select class="form-control select2" name="size" style="margin-bottom: 20px;">
                                                    <option value="">Sélectionnez la Taille</option>
                                                    @foreach(json_decode($size, true) as $row)
                                                        @foreach(json_decode($row, true) as $sizes)
                                                            <option value="{{$sizes}}">{{$sizes}}</option>
                                                        @endforeach

                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>


                                    <button class="btn btn-primary w-100" type="submit">Chercher</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<script>
    var slider = document.getElementById("myRange");
    var output = document.getElementById("demo");
    output.innerHTML = slider.value;
    ;

    slider.oninput = function() {
        output.innerHTML = this.value;
    }
</script>

