@extends('app.include.layouts')
@section('content')
    <style>


        .cart-table table a {
            color: #020310;
            font-weight: 700;
            font-size: 18px;
        }
    </style>

    <div class="page-content-wrapper">
        <div class="container">

            <!-- Cart Wrapper-->
            <div class="cart-wrapper-area py-3">

                <div class="cart-table card mb-3 cartt"  >
                    <h4 style="margin-left: 10px;">Panie</h4>
                    <h6 style="margin-left: 10px;"> Articles du panier</h6><hr>
                    <div class="table-responsive card-body">
                        <table class="table mb-0">
                            <tbody >

                            @foreach($cartitems as $row)

                            <tr class="items{{$row->id}}">


                                <td><a href="#">{{$row->name}}<span>{{$row->price}}× {{$row->quantity}}</span></a></td>
                                <td>
                                    <div class="quantity">
                                        <input class="qty-text" type="text" value="{{$row->quantity}}" readonly>
                                    </div>
                                </td>
                                <th scope="row"><a class="remove-product"   href="{{route('removecart.get',['id'=>$row->id])}}"><i class="lni lni-close"></i></a></th>
                            </tr>

                            @endforeach

                           @if($total2!=0)
                            <tr><td><a href="#">Remise d'anniversaire<span>10% de réduction</span></a></td>
                                <td>
                                    <div class="quantity">
                                        <input class="qty-text" type="text" value="{{-$total2}}€" readonly>
                                    </div>
                                </td>
                            </tr>
                           @endif

                            </tbody>
                        </table>
                    </div>
                </div>


                <!-- Coupon Area-->

                <!-- Cart Amount Area-->
                <div class="card cart-amount-area">
                    <div class="card-body d-flex align-items-center justify-content-between">

                        <h5 class="total-price mb-0 price">{{$total1}} €</h5>
                        @if($total1=='0')
                        <a class="btn btn-warning checkout1" href="#" >Passer à la caisse</a>
                        @else
                        <a class="btn btn-warning checkout" href="{{route('app.checkout')}}" style="background-color: pink!important;">Passer à la caisse</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
<script>
    function removecartt(elem){

        let id = $(elem).attr("id");


        let _token   = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            url: "{{route('removecart')}}",
            type:"POST",
            data:{
                id:id,
                _token: _token
            },
            success:function(response){
                console.log(response);
                if(response) {
                    $(".items"+id).hide();
                    var price  = response.price;

                    // var cartprice  = $(".price").html();
                    // var finalprice = parseInt(cartprice) - price;
                    //
                    // $(".price").html(finalprice+'€');

                }
            },
        });
    }


</script>


@endsection
