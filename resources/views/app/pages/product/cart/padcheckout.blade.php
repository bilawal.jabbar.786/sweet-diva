@extends('app.include.layouts')
@section('content')
    <style>

    </style>
    <div class="page-content-wrapper">
        <div class="container">

                    <!-- Checkout Wrapper-->
                <div class="checkout-wrapper-area py-3">
                    <!-- Billing Address-->
                    <div class="billing-information-card mb-3">
                        <div class="card billing-information-title-card bg-primary" style="background: linear-gradient(to left, #ffe1e7, #a55580)!important;">
                            <div class="card-body">
                                <h6 class="text-center mb-0 text-white">Comment ça marche</h6>
                            </div>
                        </div>
                        <div class="card user-data-card">
                            <div class="card-body">
                                <?php $gs = \App\GeneralSettings::find(1);?>
                                <p>{!! $gs->pad !!}</p>

                            </div>
                        </div>
                        <!-- Shipping Method Choose-->
                        <div class="billing-information-card mb-3">
                            <div class="card billing-information-title-card bg-primary" style="background: linear-gradient(to left, #ffe1e7, #a55580)!important;">
                                <div class="card-body">
                                    <h6 class="text-center mb-0 text-white">Votre commande</h6>
                                </div>
                            </div>
                            <div class="card user-data-card">
                                @foreach($cartitems as $items)
                                    <div class="card-body">
                                        <div class="single-profile-data d-flex align-items-center justify-content-between">
                                            <div class="title d-flex align-items-center"><span><b>{{$items->name}} × {{$items->quantity}}</b></span></div>
                                            <div class="data-content"><b style="color: black">{{$items->price * $items->quantity}} €</b></div>

                                        </div>
                                    </div>
                                @endforeach
                                    <div class="card-body">
                                        <div class="single-profile-data d-flex align-items-center justify-content-between">
                                            <div class="title d-flex align-items-center"><span><b>Frais de livraison </b></span></div>
                                            <div class="data-content deliverycharges"><b style="color: black">{{$total - ($items->price * $items->quantity) ?? '5'}}€</b></div>
                                        </div>
                                    </div>
                                    <div class="card-body d-flex align-items-center justify-content-between">
                                        <h5 class="total-price mb-0 ">{{$total}} €</h5>
                                        {{--                        <button type="submit" class="btn btn-warning" id="card-button" data-secret="{{ $intent }}" style="background: linear-gradient(to left, #ffe1e7, #a55580)!important;">Payer</button>--}}

                                            <a href="{{route('app.pad.success',['id'=>$id])}}" class="btn btn-warning" style="background: linear-gradient(to left, #ffe1e7, #a55580)!important;">Soumettre</a>
                                    </div>
                                </div>




                            </div>
                        </div>
                    </div>

                </div>
        </div>
        y
    </div>

@endsection

@section('script')
    {{--        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>--}}
    <script>
        function copon() {
            $(".copon").show();
        }
    </script>
    <script>
        function promo() {
            let code = $(".promo").val();
            let finalamount1 = $(".finalamount1").val();
            let finalamount = $(".finalamount").html();
            let promodiscount = $(".promodiscount").html();

            let _token   = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url: "{{route('promo')}}",
                type:"POST",
                data:{
                    code:code,
                    _token: _token
                },
                success:function(response){
                    // console.log(response);

                    if(response.code===code) {
                        // console.log(response.code)
                        $(".copon").hide();
                        $(".promotext").hide();
                        $(".promodetials").show();
                        $(".promodetials1").show();
                        let perc =  ( parseInt(finalamount)  * response.off )/100;
                        let final = parseInt(finalamount) - perc;
                        $('.amount').html("-" +perc+ " €");
                        $('.discount').html(response.off+ "%");
                        $('.finalamount').html(parseFloat(final).toFixed(2)+ " €");
                        $('.discountt').val(response.off);
                        $('.finalamounttt').val(parseFloat(final).toFixed(2));
                        $('.promo_id').val(response.id);

                    }
                    else{
                        $(".invalid").show();
                    }
                },
            });
        }
    </script>
    <script>
        var total = $('.totalamount').val();
        function cal(){
            var total = $('.totalamount').val();
            var delivery = $('.delivery').val();

            var finalamount = (+total) + (+delivery);
            $('.finalamount').html(finalamount+ " €");
            $('.finalamounttt').val(finalamount);
        }
        $(document).ready(function() {
            $('#country').on('change', function() {
                // alert(total);
                if (this.value == 'Guadeloupe' || this.value == 'Martinique' || this.value == 'Saint Martin (French part)'){
                    $('.delivery').val(5);
                    $('.deliverycharges').html(5 + " €");
                    cal();
                }else {
                    $('.delivery').val(10);
                    $('.deliverycharges').html(10 + " €");
                    cal();
                }
            });
            var elemeen = jQuery('#country').val();
            if (elemeen == 'Guadeloupe' || elemeen == 'Martinique' || elemeen == 'Saint Martin (French part)'){
                $('.delivery').val(5);
                $('.deliverycharges').html(5 + " €");
                cal();
            }else {
                $('.delivery').val(10);
                $('.deliverycharges').html(10 + " €");
                cal();
            }
        });

        cal();
    </script>
    <script>
        $(".loginformsubmit").submit(function(){
            $(this).find(':input[type=submit]').prop('disabled', true);
            $(this).find(':input[type=submit]').html("Chargement..");
        });
    </script>
@endsection
