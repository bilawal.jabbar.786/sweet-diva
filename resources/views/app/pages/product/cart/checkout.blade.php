@extends('app.include.layouts')
@section('content')
    <style>

    </style>
    <div class="page-content-wrapper">
        <div class="container">

            <form action="{{route('app.checkout.submit')}}" class="loginformsubmit" method="POST" enctype="multipart/form-data">
            @csrf            <!-- Checkout Wrapper-->
            <div class="checkout-wrapper-area py-3">
                <!-- Billing Address-->
                <div class="billing-information-card mb-3">
                    <div class="card billing-information-title-card bg-primary" style="background: linear-gradient(to left, #ffe1e7, #a55580)!important;">
                        <div class="card-body">
                            <h6 class="text-center mb-0 text-white">{{$title}}</h6>
                        </div>
                    </div>
                    <div class="card user-data-card">
                        <div class="card-body">
                            <?php $user = auth()->user();?>
                                <div class="mb-3">
                                    <div class="title mb-2"><i class="lni lni-user"></i><span>Nom complet</span></div>
                                    <input class="form-control" type="text" name="name" value="{{auth()->user()->name.' '.auth()->user()->surname}}">
                                </div>
                                <div class="col-md-12 mb--20">
                                    <div class="title mb-2"><i class="lni lni-map-marker"></i><span> Pays</span></div>
                                    <select name="country" class="mb-3 form-select" id="country" style="margin-bottom: 10px;">
                                        @foreach($countries as $country)
                                            <option value="{{$country->name}}">{{$country->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="mb-3">
                                    <div class="title mb-2"><i class="lni lni-map-marker"></i><span> Adresse complète</span></div>
                                    <input class="form-control" type="text" name="address" value="" required>
                                </div>
                                <div class="mb-3">
                                    <div class="title mb-2"><i class="lni lni-envelope"></i><span>E-mail</span></div>
                                    <input class="form-control" type="text" name="email" value="{{$user->email}}">
                                </div>
                                <div class="mb-3">
                                    <div class="title mb-2"><i class="lni lni-phone"></i><span>Téléphoner</span></div>
                                    <input class="form-control" type="text" name="phone" value="{{$user->phone}}">
                                </div>
                                <div class="mb-3">
                                    <div class="title mb-2"><i class="lni lni-user"></i><span>Notes de commande</span></div>
                                    <textarea class="form-control mb-3" id="message" name="notes"cols="8" rows="10" placeholder="Écris quelque chose..."></textarea>
                                </div>

                        </div>
                </div>
                <!-- Shipping Method Choose-->
                    <div class="billing-information-card mb-3">
                        <div class="card billing-information-title-card bg-primary" style="background: linear-gradient(to left, #ffe1e7, #a55580)!important;">
                            <div class="card-body">
                                <h6 class="text-center mb-0 text-white">Votre commande</h6>
                            </div>
                        </div>
                        <div class="card user-data-card">
                            @foreach($cartitems as $items)
                            <div class="card-body">
                                <div class="single-profile-data d-flex align-items-center justify-content-between">
                                    <div class="title d-flex align-items-center"><span><b>{{$items->name}} × {{$items->quantity}}</b></span></div>
                                    <div class="data-content"><b style="color: black">{{$items->price * $items->quantity}} €</b></div>

                                </div>
                            </div>
                            @endforeach
                                <div class="card-body">
                                    <div class="single-profile-data d-flex align-items-center justify-content-between">
                                        <div class="title d-flex align-items-center"><span><b>Frais de livraison </b></span></div>
                                        <div class="data-content deliverycharges"><b style="color: black">5€</b></div>
                                    </div>
                                <div class="single-profile-data d-flex align-items-center justify-content-between " >
                                    <span><b style="display: none" class="promodetials1">Remise </b></span>
                                    <b style="color: black;display: none" class="promodetials1 discount" > - 5€</b>
                                </div>
                                <div class="single-profile-data d-flex align-items-center justify-content-between " >
                                    <span><b style="display: none" class="promodetials">Montant de la remise </b></span>
                                    <b style="color: black;display: none" class="promodetials amount" > - 5€</b>
                                </div>
                                <div class="single-profile-data d-flex align-items-center justify-content-between ">
                                    <p class="promotext">Avez-vous un code promo ou une carte-cadeau ? <a href="javascript:void(0)" onclick="copon()"  > Cliquez ici</a></p>
                                </div>

                                <div class="single-profile-data d-flex align-items-center justify-content-between">

                                    <select name="pay" class="mb-3 form-select" id="pay">
                                        @if($total > 150)
                                            <option value="installments">Payer en plusieurs fois avec ALMA</option>
                                        @endif
                                        <option value="complete">Payer en 1 seule fois par carte bancaire</option>
                                            <option value="pad">PAD( Paiement à Distance)</option>
                                    </select>
                                </div><br>
{{--                                <div class="">--}}


{{--                                    <div class="form-group stripe-payment-method-div">--}}
{{--                                        <label>{{ __('Carte bancaire') }}</label> <span class="text-danger">*</span>--}}
{{--                                        <div id="card-element"></div>--}}
{{--                                        <div id="card-errors" class="text-danger" role="alert"></div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}

                                    </div>

                            </div>
                        </div>
                    </div>

                <!-- Cart Amount Area-->
                <div class="card cart-amount-area">
                    <input type="hidden" value="5" class="delivery" name="shipping">
                    <input type="hidden" value="{{$total}}" class="totalamount" name="totalamount">
                    <input type="hidden" value="{{$total}}" class="finalamounttt" name="finalamount">
                    <input type="hidden" value="" class="promo_id" name="promo_id">
                    <input type="hidden" value="0" class="discountt" name="discount">
                    @auth
                        <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                    @endauth
                    <div class="card coupon-card mb-3 copon" style="display: none" >
                        <div class="card-body">
                            <div class="apply-coupon">
                                <h6 class="mb-0">Avez-vous un coupon?</h6><br>

                                <div class="coupon-form">
                                    {{--                        action="{{route('redeem.voucher')}}"--}}
                                    <form  method="POST">
                                        @csrf
                                        <input class="form-control promo" name="code" type="text" placeholder="Revumo35">
                                        <input type="hidden" name="r_id" value="">
                                        <button class="btn btn-primary" type="button" onclick="promo()">Envoyer</button>
                                        <p style="color: red; display: none" class="invalid">Code promotionnel invalide</p>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div><br>


                    <div class="card-body d-flex align-items-center justify-content-between">
                        <h5 class="total-price mb-0 finalamount">{{$total}} €</h5>
{{--                        <button type="submit" class="btn btn-warning" id="card-button" data-secret="{{ $intent }}" style="background: linear-gradient(to left, #ffe1e7, #a55580)!important;">Payer</button>--}}
                        <button type="submit" class="btn btn-warning" style="background: linear-gradient(to left, #ffe1e7, #a55580)!important;">Suivante</button>
                    </div>
                </div>
            </div>
        </div>
            </form>
    </div>

@endsection
        @section('script')
{{--        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>--}}
        <script>
            function copon() {
                $(".copon").show();
            }
        </script>
            <script>
                function promo() {
                    let code = $(".promo").val();
                    let finalamount1 = $(".finalamount1").val();
                    let finalamount = $(".finalamount").html();
                    let promodiscount = $(".promodiscount").html();

                    let _token   = $('meta[name="csrf-token"]').attr('content');
                    $.ajax({
                        url: "{{route('promo')}}",
                        type:"POST",
                        data:{
                            code:code,
                            _token: _token
                        },
                        success:function(response){
                             // console.log(response);

                            if(response.code===code) {
                                // console.log(response.code)
                                $(".copon").hide();
                                $(".promotext").hide();
                                $(".promodetials").show();
                                $(".promodetials1").show();
                                let perc =  ( parseInt(finalamount)  * response.off )/100;
                                let final = parseInt(finalamount) - perc;
                                $('.amount').html("-" +perc+ " €");
                                $('.discount').html(response.off+ "%");
                                $('.finalamount').html(parseFloat(final).toFixed(2)+ " €");
                                $('.discountt').val(response.off);
                                $('.finalamounttt').val(parseFloat(final).toFixed(2));
                                $('.promo_id').val(response.id);

                            }
                            else{
                                $(".invalid").show();
                            }
                        },
                    });
                }
            </script>
        <script>
            var total = $('.totalamount').val();
            function cal(){
                var total = $('.totalamount').val();
                var delivery = $('.delivery').val();

                var finalamount = (+total) + (+delivery);
                $('.finalamount').html(finalamount+ " €");
                $('.finalamounttt').val(finalamount);
            }
            $(document).ready(function() {
                $('#country').on('change', function() {
                    // alert(total);
                    if (this.value == 'Guadeloupe' || this.value == 'Martinique' || this.value == 'Saint Martin (French part)'){
                        $('.delivery').val(5);
                        $('.deliverycharges').html(5 + " €");
                        cal();
                    }else {
                        $('.delivery').val(10);
                        $('.deliverycharges').html(10 + " €");
                        cal();
                    }
                });
                var elemeen = jQuery('#country').val();
                if (elemeen == 'Guadeloupe' || elemeen == 'Martinique' || elemeen == 'Saint Martin (French part)'){
                    $('.delivery').val(5);
                    $('.deliverycharges').html(5 + " €");
                    cal();
                }else {
                    $('.delivery').val(10);
                    $('.deliverycharges').html(10 + " €");
                    cal();
                }
            });

            cal();
        </script>
            <script>
                $(".loginformsubmit").submit(function(){
                    $(this).find(':input[type=submit]').prop('disabled', true);
                    $(this).find(':input[type=submit]').html("Chargement..");
                });
            </script>
@endsection
