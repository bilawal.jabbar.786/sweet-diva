@extends('app.include.layouts')

@section('content')
    <div class="page-content-wrapper">
        <!-- Product Slides-->
        <div class="product-slides owl-carousel">
            <!-- Single Hero Slide-->
            {{--            single-product-slide--}}
            @if(!empty ( $product->gallery ))
                @foreach(json_decode($product->gallery, true) as $images)
                    <div class="  "><img style="height: 100% !important" src="{{asset('gallery/'.$images)}}"
                                         loading="lazy"></div>
                @endforeach
            @endif
            @if(!empty ( $product->colorimage ))
                @foreach(json_decode($product->colorimage, true) as $key => $images)
                    <div class=""><img style="height: 100% !important" src="{{asset('colorimage/'.$images)}}"
                                       loading="lazy"></div>

                @endforeach
            @endif


        </div>
        <div class="product-description pb-3">
            <!-- Product Title & Meta Data-->
            <!-- Product Title & Meta Data-->
            <div class="product-title-meta-data bg-white mb-3 py-3">
                <div class="container d-flex justify-content-between">
                    <div class="p-title-price">
                        <h6 class="mb-1">{{$product->title}}</h6>

                        <p style=" font-size:12px;">Matière du produit: <b>{{$product->material}}</b></p>


                    </div>
                    <div class="p-wishlist-share">
                        @if($product->oldprice)
                            <p class="sale-price ">{{$product->price}}€<span>{{$product->oldprice}}€</span></p>
                        @else
                            <p class="sale-price">{{$product->price}}€</p>
                        @endif
                    </div>
                </div>
                <!-- Ratings-->
                <div class="product-ratings">
                    <div class="container d-flex align-items-center justify-content-between">
                        <div class="ratings"><span class="pl-1"
                                                   style="color:#ffaf00; font-size: 15px;">{{$product->categorytype->name }}</span>
                        </div>
                        <div><span style="color: green">Code produit:<b>{{$product->sku}} </b></span></div>
                    </div>
                </div>
            </div>

            <!-- Flash Sale Panel-->
            <form method="POST" class="paymentformsubmit" action="{{route('product.cart')}}">
                @csrf
                <input type="hidden" name="id" value="{{$id}}">
                <input type="hidden" name="type" value="1">
                <!-- Selection Panel-->
                <div class="selection-panel bg-white mb-3 py-3">
                    <div class="container d-flex align-items-center justify-content-between">
                        <!-- Choose Color-->
                        <div class="choose-color-wrapper">
                            <p class="mb-1 font-weight-bold">Color</p>
                            <div class="choose-color-radio d-flex align-items-center">
                                <!-- Single Radio Input-->

                                @if(!empty ($product->color ))
                                    @foreach(json_decode($product->color, true) as $key => $color)

                                        <div class="form-check mb-0 ">
                                            <input class="form-check-input givecolor " id="firstcolor{{$product->id}}"
                                                   onclick="getImage(this)" value="{{$color}}" data-color="{{$color}}"
                                                   data-image="{{json_decode($product->colorimage, true)[$key]}}"
                                                   style="background-color: {{$color}}" id="colorRadio1" type="radio"
                                                   name="color">
                                            <label class="form-check-label" for="colorRadio1"></label>
                                        </div>

                                    @endforeach
                                @endif

                            </div>
                        </div>
                        <!-- Choose Size-->

                    </div>
                </div>
                <!-- Add To Cart-->
                <div class="cart-form-wrapper bg-white mb-3 py-3">
                    <div class="container">
                        <div class="choose-size-wrapper">
                            <p class="mb-1 font-weight-bold" style="display: none">Sélectionnez la taille</p>
                            <div class="choose-size-radio d-flex align-items-center" style="text-align: left">
                                <div class="row div-inline-list">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="flash-sale-panel bg-white mb-3 py-3 main" style="display: none">
                    <div class="container">
                        <!-- Sales Offer Content-->
                        <div class="sales-offer-content d-flex align-items-end justify-content-between">
                            <!-- Sales End-->
                            <div class="sales-end">
                                <p class="mb-1 font-weight-bold quan"></p>

                            </div>

                        </div>
                    </div>
                </div>
                <div class="cart-form-wrapper bg-white mb-3 py-3">
                    <div class="container">
                        <div class="cart-form">
                            <div class="order-plus-minus d-flex align-items-center ">
                                <div class="quantity-button-handler">-</div>
                                <input class="form-control cart-quantity-input " type="number" step="1" name="quantity"
                                       max="0" min="1" value="0">
                                <div class="quantity-button-handler">+</div>
                            </div>
                            <input class="btn btn-primary ml-3" value="Ajouter au panier" type="submit">
                        </div>
                    </div>
                </div>
            </form>
            <!-- Product Specification-->
            <div class="p-specification bg-white mb-3 py-3">
                <div class="container">
                    <h6>La description</h6>

                    <p class="mb-0">{!! $product->description !!}</p>
                </div>
            </div>

            <!-- Rating & Review Wrapper-->
            <div class="rating-and-review-wrapper bg-white py-3 mb-3">
                <div class="container">
                    <h6>Évaluations Avis </h6>
                    <div class="rating-review-content">
                        <ul class="pl-0">
                            @foreach($reviews as $row)
                                <li class="single-user-review d-flex">
                                    @if($row->user->photo)
                                        <div class="user-thumbnail"><img src="{{asset($row->user->photo)}}" alt="">
                                        </div>
                                    @else
                                        <div class="user-thumbnail"><img src="{{asset('app/assets/img/bg-img/7.jpg')}}"
                                                                         alt=""></div>
                                    @endif
                                    <div class="rating-comment">
                                        <?php  \Carbon\Carbon::setLocale('fr');
                                        $date = \Carbon\Carbon::parse($row->created_at);
                                        ?>

                                        <h6>{{$row->user->name .' '.$row->user->surname}}</h6>
                                        <p class="comment mb-0">{{$row->message}}</p><span
                                            class="name-date">{{$date->diffForHumans()}}</span>
                                        <div class="rating">
                                            @if($row->star=='1')
                                                <i class="lni lni-star-filled"></i>
                                            @elseif($row->star=='2')
                                                <i class="lni lni-star-filled"></i><i class="lni lni-star-filled"></i>
                                            @elseif($row->star=='3')
                                                <i class="lni lni-star-filled"></i><i class="lni lni-star-filled"></i><i
                                                    class="lni lni-star-filled"></i>
                                            @elseif($row->star=='4')
                                                <i class="lni lni-star-filled"></i><i class="lni lni-star-filled"></i><i
                                                    class="lni lni-star-filled"></i><i class="lni lni-star-filled"></i>
                                            @elseif($row->star=='5')
                                                <i class="lni lni-star-filled"></i><i class="lni lni-star-filled"></i><i
                                                    class="lni lni-star-filled"></i><i class="lni lni-star-filled"></i>
                                                <i class="lni lni-star-filled"></i>
                                            @endif

                                        </div>
                                    </div>
                                </li>
                            @endforeach

                        </ul>
                    </div>
                </div>
            </div>
            <!-- Ratings Submit Form-->
            <div class="ratings-submit-form bg-white py-3">
                <div class="container">
                    <h6>Soumettre un avis</h6>
                    <form action="{{route('app.product.review', ['id' => $product->id])}}" method="POST">
                        @csrf
                        <div class="stars mb-3">
                            <input class="star-1" type="radio" name="star" value="1" id="star1">
                            <label class="star-1" for="star1"></label>
                            <input class="star-2" type="radio" name="star" value="2" id="star2">
                            <label class="star-2" for="star2"></label>
                            <input class="star-3" type="radio" name="star" value="3" id="star3">
                            <label class="star-3" for="star3"></label>
                            <input class="star-4" type="radio" name="star" value="4" id="star4">
                            <label class="star-4" for="star4"></label>
                            <input class="star-5" type="radio" name="star" value="5" id="star5">
                            <label class="star-5" for="star5"></label><span></span>
                        </div>
                        <textarea class="form-control mb-3" id="comments" name="message" cols="30" rows="10"
                                  data-max-length="200" placeholder="Donnez votre avis..." required></textarea>
                        <button class="btn btn-sm btn-primary" type="submit">Enregistrer l'avis</button>
                    </form>
                </div>
            </div>

        </div>
    </div>

    <script>
        window.onload = function () {
            document.getElementById("firstcolor{{$product->id}}").click();
        };
    </script>
    <script>
        function smsbox() {
            return alert("asa");
        }
        function getImage(elem){
            let image = $(elem).data('image');
            let color = $(elem).data('color');
            $(".main").hide();
            let id = {{$id}}
             // alert(id);
            $('.single-product-slide').html(' <img style="height: 100% !important"  loading="lazy"  src="{{asset('colorimage')}}/'+image+'">');

            $('.givecolor').val(color);
            $('.div-inline-list').empty();

            let _token   = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url: "{{route('checkstock')}}",
                type:"POST",
                data:{
                    id:id,
                    _token: _token,
                    color: color,
                },
                success:function(response){
                    if(response.success == '0') {
                        return false;
                    }else {
                        if (response.size == 0){
                            $('.div-inline-list').append('<p>Taille non disponible</p>')
                        }else {
                            // $('.sizeget').append('<option>Sélectionnez la taille</option>')
                            $.each(response.size, function(i, item) {
                                // $('.sizeget').append('<option value="'+item+'">'+item+'</option>')
                                $(".font-weight-bold").show();
                                $('.div-inline-list').append(' <div class="product-container">\n' +
                                    '                                                <input onchange="checksizestock(this)" id="'+item+'" value="'+item+'" type="radio" class="form-check-input" name="size">\n' +
                                    '                                                <label for="'+item+'" class="clickable"><span class="checked-box"></span></label>\n' +
                                    '                                                <p class="product">'+item+'</p>\n' +
                                    '                                            </div>')
                            });
                        }

                    }
                },
            });

        }

        function checksizestock(elem) {
            let id = {{$id}};
            let size = $(elem).attr("id");
            let color = $('.givecolor').val();

            // let size = $(elem).find(":selected").val()
            let _token = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url: "{{route('checksizestock')}}",
                type: "POST",
                data: {
                    id: id,
                    _token: _token,
                    color: color,
                    size: size
                },
                success: function (response) {
                    if (response.success == '0') {
                        $(".main").show();
                        $('.cart-quantity-input').val(0);
                        $('.quan').html("EN RUPTURE DE STOCK");
                        $('.smsbutton').show();
                        $(':input[type="submit"]').prop('disabled', true);
                    } else {
                        $(".main").show();
                        $('.cart-quantity-input').attr('max', response.quantity);
                        $('.quan ').html("EN STOCK (" + response.quantity + ")");
                        $('.smsbutton').hide();
                        $(':input[type="submit"]').prop('disabled', false);
                        $.each(response.size, function (i, item) {

                            $('.sizeget').append('<option value="' + item + '">' + item + '</option>');
                        });
                    }
                },
            });
        }

    </script>



@endsection
