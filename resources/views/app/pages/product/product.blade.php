@extends('app.include.layouts')
@section('content')
    <div class="top-products-area py-3">
        <div class="container">
            <div class="section-heading d-flex align-items-center justify-content-between">
            <br><br>
            </div>
            <div class="row g-3">
                <!-- Featured Product Card-->
                @foreach($product as $row)
                    <div class="col-12 col-md-6">
                        <div class="card weekly-product-card">
                            <div class="card-body d-flex align-items-center" style="padding: 0rem;">


                                <div class="product-thumbnail-side">
                                    @auth
                                    <?php $like = \App\Wishlist::where('user_id','=',Auth::user()->id)->where('product_id','=',$row->id)->first();?>
                                    @if($like)
                                        <a class="wishlist-btn" href="{{route('wishlist.delete', ['id' => $row->id])}}" style="color: red;" ><i class="fa fa-heart"></i></a>
                                    @else
                                        <a class="wishlist-btn fav{{$row->id}}" onclick="addtowishlist(this)" id="{{$row->id}}" href="#"><i class="lni lni-heart okk{{$row->id}}"></i></a>
                                    @endif
                                    @else
                                        <a class="wishlist-btn fav{{$row->id}}" id="{{$row->id}}" href="#"><i class="lni lni-heart okk{{$row->id}}"></i></a>

                                    @endauth
                                    <a class="product-thumbnail d-block" href="{{route('single.product',['id'=>$row->id])}}"><img src="{{asset($row->photo1)}}"  loading="lazy" alt=""></a></div>
                                <div class="product-description">
                                    <a class="product-title d-block" href="{{route('single.product',['id'=>$row->id])}}">{{$row->title}}</a>
                                    <a class="product-title d-block " style="color: #ffaf00;font-size: 13px;" href="{{route('single.product',['id'=>$row->id])}}">{{$row->categorytype->name}}</a>

                                    @if($row->oldprice)
                                        <p class="sale-price ">{{$row->price}}€<span>{{$row->oldprice}}€</span></p>
                                    @else
                                        <p class="sale-price">{{$row->price}}€</p>
                                    @endif
                                    <?php
                                    //rateing
                                    $reviews = \App\Reviews::where('order_id','=',$row->id)->get();
                                    if (!$reviews->isempty()){
                                        $totalReview = \App\Reviews::where('order_id','=',$row->id)->sum('star');
                                        $total = $reviews->count();
                                        $totalReviews = round($totalReview / $total,1);
                                    }
                                    else{
                                        $reviews =null;
                                        $totalReviews=0;
                                        $total=0;

                                    }
                                    //endrating
                                    ?>
                                    <div class="product-rating"><i class="lni lni-star-filled"></i>{{$totalReviews}} ({{$total}})</div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                {{ $product->links() }}

            </div>
        </div>
    </div><br><br>

@endsection
