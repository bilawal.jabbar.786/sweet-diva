@extends('app.include.layouts')
<style>
    .slidecontainer {
        width: 100%;
    }

    .slider {
        -webkit-appearance: none;
        width: 100%;
        height: 10px;
        border-radius: 5px;
        background: #d3d3d3;
        outline: none;
        opacity: 0.7;
        -webkit-transition: .2s;
        transition: opacity .2s;
    }

    .slider:hover {
        opacity: 1;
    }

    .slider::-webkit-slider-thumb {
        -webkit-appearance: none;
        appearance: none;
        width: 23px;
        height: 24px;
        border: 0;
        background: url('{{asset('app/assets/img/coment.jpeg')}}');
        background-repeat: no-repeat;
        cursor: pointer;
    }

    .slider::-moz-range-thumb {
        width: 23px;
        height: 24px;
        border: 0;
        background-repeat: no-repeat;
        background: url('{{asset('app/assets/img/coment.jpeg')}}');
        cursor: pointer;
    }
    .form-range {
        width: 63% !important;
        height: 0.4rem !important;
        padding: 0;
        background-color: transparent;
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
    }
</style>
@section('content')
    <div class="page-content-wrapper">
        <!-- Top Products-->
        <div class="top-products-area py-3">
            <div class="container">

                <div class="row g-3">
                    <!-- Single Weekly Product Card-->
                    @foreach($product as $row)
                        <div class="col-12 col-md-6">
                            <div class="card weekly-product-card">
                                <div class="card-body d-flex align-items-center" style="padding: 0rem;">


                                    <div class="product-thumbnail-side">
                                        @auth
                                        <?php $like = \App\Wishlist::where('user_id','=',Auth::user()->id)->where('product_id','=',$row->id)->first();?>
                                        @if($like)
                                            <a class="wishlist-btn" href="{{route('wishlist.delete', ['id' => $row->id])}}" style="color: red;" ><i class="fa fa-heart"></i></a>
                                        @else
                                            <a class="wishlist-btn fav{{$row->id}}" onclick="addtowishlist(this)" id="{{$row->id}}" href="#"><i class="lni lni-heart okk{{$row->id}}"></i></a>
                                        @endif
                                        @else
                                            <a class="wishlist-btn fav{{$row->id}}" id="{{$row->id}}" href="#"><i class="lni lni-heart okk{{$row->id}}"></i></a>
                                        @endauth
                                        <a class="product-thumbnail d-block" href="{{route('single.product',['id'=>$row->id])}}"><img src="{{asset($row->photo1)}}"  loading="lazy" alt=""></a></div>
                                    <div class="product-description">
                                        <a class="product-title d-block" href="{{route('single.product',['id'=>$row->id])}}">{{$row->title}}</a>
                                        <a class="product-title d-block " style="color: #ffaf00;font-size: 13px;" href="{{route('single.product',['id'=>$row->id])}}">{{$row->categorytype->name}}</a>

                                        @if($row->oldprice)
                                            <p class="sale-price ">{{$row->price}}€<span>{{$row->oldprice}}€</span></p>
                                        @else
                                            <p class="sale-price">{{$row->price}}€</p>
                                        @endif
                                        <?php
                                        //rateing
                                        $reviews = \App\Reviews::where('order_id','=',$row->id)->get();
                                        if (!$reviews->isempty()){
                                            $totalReview = \App\Reviews::where('order_id','=',$row->id)->sum('star');
                                            $total = $reviews->count();
                                            $totalReviews = round($totalReview / $total,1);
                                        }
                                        else{
                                            $reviews =null;
                                            $totalReviews=0;
                                            $total=0;

                                        }
                                        //endrating
                                        ?>
                                        <div class="product-rating"><i class="lni lni-star-filled"></i>{{$totalReviews}} ({{$total}})</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach

                    {{ $product->links() }}


                </div>
            </div>
        </div>
    </div><br><br>

@endsection
<script>
    var slider = document.getElementById("myRange");
    var output = document.getElementById("demo");
    output.innerHTML = slider.value;
    ;

    slider.oninput = function() {
        output.innerHTML = this.value;
    }
</script>

