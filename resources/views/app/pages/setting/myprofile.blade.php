@extends('app.include.layouts')
@section('content')
    <div class="page-content-wrapper">
        <div class="container">
            <!-- Profile Wrapper-->
            <div class="profile-wrapper-area py-3">
                <!-- User Information-->
                <div class="card user-info-card">
                    <div class="card-body p-4 d-flex align-items-center">
                        @if($user->photo)
                        <div class="user-profile mr-3"><img src="{{asset($user->photo)}}"  loading="lazy" alt=""></div>
                        @else
                            <div class="user-profile mr-3"><img src="{{asset('app/assets/img/bg-img/9.jpg')}}"  loading="lazy" alt=""></div>
                        @endif
                        <div class="user-info">

                            <h5 class="mb-0">{{$user->name .' ' . $user->surname}}</h5>
                        </div>
                    </div>
                </div>
                <!-- User Meta Data-->
                <div class="card user-data-card">
                    <div class="card-body">
                        <div class="single-profile-data d-flex align-items-center justify-content-between">
                            <div class="title d-flex align-items-center"><i class="lni lni-user"></i><span>Nom</span></div>
                            <div class="data-content">{{$user->name .' ' . $user->surname}}</div>
                        </div>
                        <div class="single-profile-data d-flex align-items-center justify-content-between">
                            <div class="title d-flex align-items-center"><i class="lni lni-envelope"></i><span>E-mail </span></div>
                            <div class="data-content">{{$user->email}}                               </div>
                        </div>

                        <div class="single-profile-data d-flex align-items-center justify-content-between">
                            <div class="title d-flex align-items-center"><i class="lni lni-phone"></i><span>Téléphoner</span></div>
                            <div class="data-content">{{$user->phone}}  </div>
                        </div>
                        <div class="single-profile-data d-flex align-items-center justify-content-between">
                            <div class="title d-flex align-items-center"><i class="lni lni-user"></i><span>Adresse</span></div>
                            <div class="data-content">{{$user->address}}</div>
                        </div>

                        <div class="single-profile-data d-flex align-items-center justify-content-between">
                            <div class="title d-flex align-items-center"><i class="lni lni-map-marker"></i><span>Code Postal</span></div>
                            <div class="data-content">{{$user->cp}}</div>
                        </div>
                        <div class="single-profile-data d-flex align-items-center justify-content-between">
                            <div class="title d-flex align-items-center"><i class="lni lni-map-marker"></i><span>Ville</span></div>
                            <div class="data-content">{{$user->city}}</div>
                        </div>
                        <div class="single-profile-data d-flex align-items-center justify-content-between">
                            <div class="title d-flex align-items-center"><i class="lni lni-map-marker"></i><span>Pays</span></div>
                            <div class="data-content">{{$user->country}}</div>
                        </div>
                        <div class="single-profile-data d-flex align-items-center justify-content-between">
                            <div class="title d-flex align-items-center"><i class="lni lni-star"></i><span>Mes  commandes</span></div>
                            <div class="data-content"><a class="btn btn-danger btn-sm" href="{{route('app.myorder')}}">Voir</a></div>
                        </div>
                    </div>
                </div>
                <!-- Edit Profile-->
                <div class="edit-profile-btn mt-3"><a class="btn btn-primary w-100" href="{{route('edit.profile')}}"><i class="lni lni-pencil mr-2"></i>Editer le profil</a></div>
            </div>
        </div>
    </div>
@endsection
