@extends('app.include.layouts')
@section('content')
    <div class="page-content-wrapper">
        <div class="container">
            <!-- Settings Wrapper-->
            <div class="settings-wrapper py-3">

                <!-- Single Setting Card-->
                @auth
                <div class="card settings-card">
                    <div class="card-body">
                        <!-- Single Settings-->
                        <div class="single-settings d-flex align-items-center justify-content-between">
                            <div class="title"><i class="lni lni-user"></i><span>Editer le profil</span></div>
                            <div class="data-content"><a href="{{route('edit.profile')}}">Changer<i class="lni lni-chevron-right"></i></a></div>
                        </div>
                    </div>
                </div>
            @endauth
                <!-- Single Setting Card-->
                <div class="card settings-card">
                    <div class="card-body">
                        <!-- Single Settings-->
                        <div class="single-settings d-flex align-items-center justify-content-between">
                            <div class="title"><i class="lni lni-lock"></i><span>Politique de confidentialité</span></div>
                            <div class="data-content"><a href="{{route('app.privicy')}}"><i class="lni lni-chevron-right"></i></a></div>
                        </div>
                    </div>
                </div>
                <!-- Single Setting Card-->
                <div class="card settings-card">
                    <div class="card-body">
                        <!-- Single Settings-->
                        <div class="single-settings d-flex align-items-center justify-content-between">
                            <div class="title"><i class="lni lni-night"></i><span>Mode nuit</span></div>

                            <div class="data-content">
                                <div class="toggle-button-cover">
                                    <div class="button r">
                                        <input class="checkbox" id="darkSwitch" type="checkbox">
                                        <div class="knobs"></div>
                                        <div class="layer"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Single Setting Card-->
                <div class="card settings-card">
                    <div class="card-body">
                        <!-- Single Settings-->
                        <div class="single-settings d-flex align-items-center justify-content-between">
                            <div class="title"><i class="lni lni-question-circle"></i><span>Termes et conditions</span></div>
                            <div class="data-content"><a class="pl-4" href="{{route('app.terms')}}"><i class="lni lni-chevron-right"></i></a></div>
                        </div>
                    </div>
                </div>
                <!-- Single Setting Card-->
                <div class="card settings-card">
                    <div class="card-body">
                        <!-- Single Settings-->
                        <div class="single-settings d-flex align-items-center justify-content-between">
                            <div class="title"><i class="lni lni-protection"></i><span>A PROPOS DE NOUS</span></div>
                            <div class="data-content"><a class="pl-4" href="{{route('app.about')}}"><i class="lni lni-chevron-right"></i></a></div>
                        </div>
                    </div>
                </div>
                @auth
                <div class="card settings-card">
                    <div class="card-body">
                        <!-- Single Settings-->
                        <div class="single-settings d-flex align-items-center justify-content-between">
                            <div class="title"><i class="lni lni-protection"></i><span>Historique des points de fidélité</span></div>
                            <div class="data-content"><a class="pl-4" href="{{route('app.points.history')}}"><i class="lni lni-chevron-right"></i></a></div>
                        </div>
                    </div>
                </div>
                @endauth

            </div>
        </div>
    </div>
@endsection
