@extends('app.include.layouts')
@section('content')
    <div class="page-content-wrapper">
        <div class="container">
            <div class="about-content-wrap py-3"><img class="mb-3" style="width: 50%;" src="{{asset('logo png sweet diva.png')}}"  loading="lazy" alt="">
                <h4>A propos de Sweet Diva:</h4>
                <p>{!! $gs->about1 !!}</p>

            </div>
        </div>


                <!-- Single Order Status-->
                <div class="single-order-status">
                    <div class="card bg-warning mb-3">
                        <div class="card-body d-flex align-items-center">
                            <div class="order-icon"><i class="lni lni-checkmark-circle"></i></div>
                            <div class="order-status">Magasins SWEET DIVA</div>
                        </div>
                    </div>



            </div>

            <div class="row g-3">
                <!-- Featured Product Card-->
                @foreach($shops as $row )
                <div class="col-6 col-md-4 col-lg-3">
                    <div class="card featured-product-card">
                        <div class="card-body"><span class="badge badge-warning custom-badge"><i class="lni lni-star"></i></span>
                            <div class="product-thumbnail-side"><a class="wishlist-btn" href="#"><i class="lni lni-heart"></i></a><a class="product-thumbnail d-block" href="{{route('app.index')}}"><img src="{{asset($row->photo)}}"  loading="lazy" alt=""></a></div>
                            <div class="product-description">
                                <a class="product-title d-block" href="#" style="font-size: 19px; color: #ffaf00">{{$row->name}}</a>
                                <a class="product-title d-block" href="#">{{$row->phone}}</a>
                                <p ><span></span>{{$row->address}}</p>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach


                <div class="copyright">
                    <span>Copyright &copy; {{date('Y')}} <a href="https://ikaedigital.com/">Ikae Digital.</a> Tous droits réservés.</span>
                </div><br><br>


            </div>
        </div>



@endsection
