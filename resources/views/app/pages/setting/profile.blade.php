@extends('app.include.layouts')
@section('content')
    <div class="page-content-wrapper">
        <div class="container">
            <!-- Profile Wrapper-->
            <div class="profile-wrapper-area py-3">
                <!-- User Information-->
                <div class="card user-info-card">
                    <form action="{{route('user.profileupdate')}}" method="POST" class="kenne-form"
                          accept-charset="UTF-8" enctype="multipart/form-data">
                        @csrf
                        <div class="card-body p-4 d-flex align-items-center">
                            @auth
                                @if($user->photo)
                                    <div class="user-profile mr-3"><img src="{{asset($user->photo)}}" loading="lazy"
                                                                        alt="">
                                        @else
                                            <div class="user-profile mr-3"><img
                                                    src="{{asset('app/assets/img/bg-img/9.jpg')}}" loading="lazy"
                                                    alt="">
                                                @endif
                                                @endauth

                                                <div class="change-user-thumb">

                                                    <input class="form-control-file" name="photo" type="file"
                                                           accept="image/*" id="imgInp">
                                                    <button><i class="lni lni-pencil"></i></button>

                                                </div>
                                            </div>
                                            <div class="user-info">
                                                <p class="mb-0 text-white">{{$user->email}}</p>
                                                <h5 class="mb-0">{{$user->name .' ' . $user->surname}}</h5>
                                            </div>
                                    </div>
                        </div>
                        <!-- User Meta Data-->
                        <div class="card user-data-card">
                            <div class="card-body">

                                <div class="mb-3">
                                    <div class="title mb-2"><i class="lni lni-user"></i><span>Nom</span></div>
                                    <input class="form-control" type="text" name="name" value="{{$user->name}}">
                                </div>

                                <div class="mb-3">
                                    <div class="title mb-2"><i class="lni lni-phone"></i><span>Téléphoner</span></div>
                                    <input class="form-control" type="text" name="phone" value="{{$user->phone}}">
                                </div>

                                <div class="mb-3">
                                    <div class="title mb-2"><i class="lni lni-map-marker"></i><span> Adresse</span>
                                    </div>
                                    <input class="form-control" type="text" name="address" value="{{$user->address}}">
                                </div>

                                <div class="mb-3">
                                    <div class="title mb-2"><i class="lni lni-map-marker"></i><span> Pays</span></div>
                                    <input class="form-control" type="text" name="country" value="{{$user->country}}">
                                </div>
                                <div class="mb-3">
                                    <div class="title mb-2"><i
                                            class="lni lni-user"></i><span> Choisissez votre taille</span></div>
                                    <select class="mb-3 form-select" id="topic" name="size" required>
                                        <option value="TU" {{ $user->size == 'TU' ? 'selected' : '' }}>TU</option>
                                        <option value="XL(44/46)" {{ $user->size == 'XL(44/46)' ? 'selected' : '' }}>XL
                                            (44/46)
                                        </option>
                                        <option value="1XL(46/48)" {{ $user->size == '1XL(46/48)' ? 'selected' : '' }}>
                                            1XL (46/48)
                                        </option>
                                        <option value="2XL(48/50)" {{ $user->size == '2XL(48/50)' ? 'selected' : '' }}>
                                            2XL (48/50)
                                        </option>
                                        <option value="3XL(50/52)" {{ $user->size == '3XL(50/52)' ? 'selected' : '' }}>
                                            3XL (50/52)
                                        </option>
                                        <option value="4XL(52/54)" {{ $user->size == '4XL(52/54)' ? 'selected' : '' }}>
                                            4XL (52/54)
                                        </option>
                                        <option value="5XL(54/56)" {{ $user->size == '5XL(54/56)' ? 'selected' : '' }}>
                                            5XL (54/56)
                                        </option>
                                    </select>
                                </div>
                                <div class="mb-3">
                                    <div class="title mb-2"><i
                                            class="lni lni-user"></i><span>Boutique associée</span></div>
                                    <select class="mb-3 form-select" name="shop" required>
                                        @if(!$user->shop)
                                            <option value="">Sélectionnez votre boutique pertinente</option>
                                        @endif
                                        <option
                                            {{ $user->shop == 'SWEET DIVA MARTINIQUE' ? 'selected' : '' }} value="SWEET DIVA MARTINIQUE">
                                            SWEET DIVA MARTINIQUE
                                        </option>
                                        <option
                                            {{ $user->shop == 'SWEET DIVA BESSON' ? 'selected' : '' }} value="SWEET DIVA BESSON">
                                            SWEET DIVA BESSON
                                        </option>
                                        <option
                                            {{ $user->shop == 'SWEET DIVA BASSE TERRE' ? 'selected' : '' }} value="SWEET DIVA BASSE TERRE">
                                            SWEET DIVA BASSE TERRE
                                        </option>

                                    </select>
                                </div>
                                <div class="mb-3">
                                    <div class="title mb-2"><i class="lni lni-key"></i><span>Ancien mot de passe</span>
                                    </div>
                                    <input class="form-control" type="password" name="oldpassword">
                                </div>
                                <div class="mb-3">
                                    <div class="title mb-2"><i class="lni lni-key"></i><span>Nouveau mot de passe</span>
                                    </div>
                                    <input class="form-control" type="password" name="newpassword">
                                </div>
                                <button class="btn btn-primary w-100" type="submit">SAUVEGARDER
                                    CHANGEMENTS
                                </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
