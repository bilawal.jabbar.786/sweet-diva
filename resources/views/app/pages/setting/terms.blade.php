@extends('app.include.layouts')
@section('content')
    <div class="page-content-wrapper">
        <div class="container">
            <!-- Privacy Policy-->
            <div class="privacy-policy-wrapper py-3">
                <p>
                    {!! $gs->condition !!}
                </p>
            </div>
        </div>
    </div>
@endsection
