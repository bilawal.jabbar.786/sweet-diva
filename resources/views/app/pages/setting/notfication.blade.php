@extends('app.include.layouts')
<style>
    .notification1 .badge1 {
        position: absolute!important;
        top: -4px!important;
        right: 1px!important;
        width: 8px;
        height: 8px;
         padding: 0px 0px!important;
        border-radius: 50%!important;
        background-color: #ffc107!important;
        color: white!important;
    }

     .notification-area .list-group-item .noti-icon::before {
         position: absolute;
         width: 10px;
         height: 10px;
         background-color: #ffaf0000!important;
         content: "";
         top: -2px;
         right: -2px;
         border-radius: 50%;
         z-index: 1;
     }

</style>
@section('content')
    <div class="page-content-wrapper animate-bottom">
        <div class="container">
            <!-- Notifications Area-->
            <div class="notification-area pt-3 pb-2">
                <h6 class="pl-1">Notifications</h6>
                <div class="list-group">
                @foreach($notfications as $row)
                    @if($row->activity == "Bienvenue")
                    <!-- Single Notification-->
                    @if($row->status==0)
                        <a class="list-group-item d-flex align-items-center notification1" href="#" >
                            <span class="noti-icon"  ><span class="badge1"></span><i class="lni lni-alarm"  ></i></span>
                            @else
                                <a class="list-group-item d-flex align-items-center " href="#" >
                                    <span class="noti-icon"  ><i class="lni lni-alarm"  ></i></span>
                            @endif
                            <div class="noti-info">
                            <h6 class="mb-0">{{$row->activity}}</h6>
                            <p class="mb-0">{{$row->message}}</p>
                            <?php
                            \Carbon\Carbon::setLocale('fr');
                            $date = \Carbon\Carbon::parse($row->created_at);
                            ?>
                            <span>{{$date->diffForHumans()}}</span>
                        </div></a>

                    @elseif($row->activity == "Confirmation de commande")
                                    @if($row->status=='0')
                                        <a class="list-group-item  d-flex align-items-center notification1" href="{{route('app.single.order', ['id' => $row->generate_id])}}">
                                            <span class="noti-icon"><span class="badge1"></span><i class="lni lni-ship"></i></span>
                                         @else
                                                <a class="list-group-item  d-flex align-items-center" href="{{route('app.single.order', ['id' => $row->generate_id])}}">
                                                    <span class="noti-icon"><i class="lni lni-ship"></i></span>
                                                    @endif


                            <div class="noti-info">
                                <h6 class="mb-0">{{$row->activity}}</h6>
                                <p class="mb-0">{{$row->message}}</p>
                                <?php
                                \Carbon\Carbon::setLocale('fr');
                                $date = \Carbon\Carbon::parse($row->created_at);
                                ?>
                                <span>{{$date->diffForHumans()}}</span>
                            </div></a>

                    @elseif($row->activity == "Parrainage")
                                                    @if($row->status==0)
                                                    <a class="list-group-item d-flex align-items-center notification1" href="#" >
                                                        <span class="noti-icon"  ><span class="badge1"></span><i class="lni lni-ship"></i></span>
                                                        @else
                                                            <a class="list-group-item d-flex align-items-center " href="#" >
                                                                <span class="noti-icon"  ><i class="lni lni-ship"></i></span>
                                                                @endif

                                <div class="noti-info" >
                                    <h6 class="mb-0">{{$row->activity}}</h6>
                                    <p class="mb-0">{{$row->message}}</p>
                                    <?php
                                    \Carbon\Carbon::setLocale('fr');
                                    $date = \Carbon\Carbon::parse($row->created_at);
                                    ?>
                                    <span>{{$date->diffForHumans()}}</span>
                                </div></a>

                    @elseif($row->activity == "Livrer la commande")
                        @if($row->status==0)
                        <a class="list-group-item d-flex align-items-center notification1" href="{{route('app.single.order', ['id' => $row->generate_id])}}" >
                            <span class="noti-icon"  ><span class="badge1"></span><i class="lni lni-ship"></i></span>
                            @else
                                <a class="list-group-item d-flex align-items-center " href="{{route('app.single.order', ['id' => $row->generate_id])}}" >
                                    <span class="noti-icon"  ><i class="lni lni-ship"></i></span>
                                    @endif


                                <div class="noti-info" >
                                    <h6 class="mb-0">{{$row->activity}}</h6>
                                    <p class="mb-0">{{$row->message}}</p>
                                    <?php
                                    \Carbon\Carbon::setLocale('fr');
                                    $date = \Carbon\Carbon::parse($row->created_at);
                                    ?>
                                    <span>{{$date->diffForHumans()}}</span>
                                </div></a>

                    @elseif($row->activity == "Expédié")
                                    @if($row->status==0)
                                        <a class="list-group-item d-flex align-items-center notification1" href="{{route('app.single.order', ['id' => $row->generate_id])}}" >
                                            <span class="noti-icon"  ><span class="badge1"></span><i class="lni lni-ship"></i></span>
                                            @else
                                                <a class="list-group-item d-flex align-items-center " href="{{route('app.single.order', ['id' => $row->generate_id])}}" >
                                                    <span class="noti-icon"  ><i class="lni lni-ship"></i></span>
                                                    @endif

                                <div class="noti-info">
                                    <h6 class="mb-0">{{$row->activity}}</h6>
                                    <p class="mb-0">{{$row->message}}</p>
                                    <?php
                                    \Carbon\Carbon::setLocale('fr');
                                    $date = \Carbon\Carbon::parse($row->created_at);
                                    ?>
                                    <span>{{$date->diffForHumans()}}</span>
                                </div></a>

                     @elseif($row->activity == "Souhait d'anniversaire")
                                                    @if($row->status==0)
                                                        <a class="list-group-item d-flex align-items-center notification1" href="#" >
                                                            <span class="noti-icon"  ><span class="badge1"></span><i class="lni lni-gift"></i></span>
                                                            @else
                                                                <a class="list-group-item d-flex align-items-center " href="#" >
                                                                    <span class="noti-icon"  ><i class="lni lni-gift"></i></span>
                                                                    @endif

                                <div class="noti-info">
                                    <h6 class="mb-0">{{$row->activity}}</h6>
                                    <p class="mb-0">{{$row->message}}</p>
                                    <?php
                                    \Carbon\Carbon::setLocale('fr');
                                    $date = \Carbon\Carbon::parse($row->created_at);
                                    ?>
                                    <span>{{$date->diffForHumans()}}</span>
                                </div></a>

                     @elseif($row->activity == "Points")
                    @if($row->status==0)
                        <a class="list-group-item d-flex align-items-center notification1" href="{{route('app.points')}}" >
                            <span class="noti-icon"  ><span class="badge1"></span><i class="lni lni-reply"></i></span>
                            @else
                                <a class="list-group-item d-flex align-items-center " href="{{route('app.points')}}" >
                                    <span class="noti-icon"  ><i class="lni lni-reply"></i></span>
                                    @endif

                                <div class="noti-info">
                                    <h6 class="mb-0">{{$row->activity}}</h6>
                                    <p class="mb-0">{{$row->message}}</p>
                                    <?php
                                    \Carbon\Carbon::setLocale('fr');
                                    $date = \Carbon\Carbon::parse($row->created_at);
                                    ?>
                                    <span>{{$date->diffForHumans()}}</span>

                                </div></a>

                     @elseif($row->activity == "Message de Sweet Diva")
                                    @if($row->status==0)
                                        <a class="list-group-item d-flex align-items-center notification1" href="{{$row->generate_id}}" >
                                            <span class="noti-icon"  ><span class="badge1"></span><i class="lni lni-offer"></i></span>
                                            @else
                                                <a class="list-group-item d-flex align-items-center " href="{{$row->generate_id}}" >
                                                    <span class="noti-icon"  ><i class="lni lni-offer"></i></span>
                                                    @endif

                                <style>
                                    .notification-area .list-group-item .noti-icon::before {
                                        position: absolute;
                                        width: 10px;
                                        height: 10px;
                                        @if($row->status==0)
                                        background-color: #ffc107 ;
                                        @else
                                        background-color: #ffaf0000;
                                        @endif
content: "";
                                        top: -2px;
                                        right: -2px;
                                        border-radius: 50%;
                                        z-index: 1;
                                    }
                                </style>
                                <div class="noti-info">
                                    <h6 class="mb-0">{{$row->activity}}</h6>
                                    <p class="mb-0">{{$row->message}}</p>
                                    <?php
                                    \Carbon\Carbon::setLocale('fr');
                                    $date = \Carbon\Carbon::parse($row->created_at);
                                    ?>
                                    <span>{{$date->diffForHumans()}}</span>
                                </div></a>
                        @endif
                    @endforeach
                    {{ $notfications->links() }}

                </div>
            </div>
        </div>
    </div>
    <script>
        $("#loader111").addClass("d-none");
    </script>
@endsection
