@extends('app.include.layouts')
<style>
    .card .card-body {
        padding: 0rem !important;
    }

    .card {
        border: #7c7c7c solid 1px !important;
        background-color: #ffe1e7 !important;
        color:  black !important;
        border-radius: 10px !important;
        margin-bottom: 5px !important;
        margin-top: 5px !important;
    }
</style>
@section('content')
    <div class="page-content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-12 p-0">
                    @foreach($posts as $row)
                    <div class="card">
                        <div class="card-header">

                            <div class="row">
                                <div class="col-2">
                                    @if($row->user->photo)
                       <img style="height: 40px; width: 40px; border-radius: 50%;" src="{{asset($row->user->photo)}}" alt="">
                                    @else
                                        <img style="height: 40px"
                                             src="https://cdn.pixabay.com/photo/2021/01/04/10/41/icon-5887126_1280.png"
                                             alt="">
                                    @endif
                                </div>
                                <div class="col-9">
                                    <h4>{{$row->user->name .' ' . $row->user->surname}}</h4>
                                    <div style="margin-top: -10px">
                                        <?php
                                        \Carbon\Carbon::setLocale('fr');
                                        $date = \Carbon\Carbon::parse($row->created_at);
                                        ?>

                                        <i style="font-size: 10px !important;">{{$date->diffForHumans()}}</i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <img style="width: 100%"
                                 class="{{$row->filter}}" src="{{asset($row->img)}}"
                                 alt="">

                            <div class="row p-3">
                                <div class="col-2 fav{{$row->id}}">
                                    <img style="height: 40px; background-color: white; "  src="{{asset('app/assets/img/msg.png')}}" onclick="addtoLike(this)" id="{{$row->id}}" alt="">
                                </div>
                                <?php $like = \App\Like::where('user_id','=',Auth::user()->id)->where('post_id','=',$row->id)->first();
                                      $totallike = \App\Like::where('post_id','=',$row->id)->get();
                                ?>
                                @if($like)
                                        <div class="col-2 ">
                                            <img style="height: 30px" src="{{asset('app/assets/img/dislike.png')}}"  alt="">
                                        </div>
                                @else
                                        <div class="col-2 fav{{$row->id}}">
                                            <img style="height: 30px" src="{{asset('app/assets/img/like.png')}}" onclick="addtoLike(this)" id="{{$row->id}}" alt="">
                                        </div>
                                @endif

                                <div class="col-6 pl-0 mt-1 add{{$row->id}}" >
                                    <b class="hidee{{$row->id}}">{{$totallike->count() ?? 0 }} Like</b>
                                    <input class="likes{{$row->id}}" type="hidden" value="{{$totallike->count()}}">
                                </div>

                            </div>
                            <div class="row pl-3 pr-3 pb-3">
                                <div class="col-12">
                                    <p style="color: black">{{$row->description}}

                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach

                </div>
            </div>
        </div>
    </div>
@endsection

<script>

    function addtoLike(elem) {
        event.preventDefault();
        let id = $(elem).attr("id");
        $(".fav"+id).html(' <img style="height: 30px" src="{{asset('app/assets/img/dislike.png')}}"  alt="">');
        let check = $(".likes"+id).val();
        let sum = check+1;
        if(sum=='01'){
            sum2=1;
        }
        else if(sum=='02'){
            sum2=2;
        }
        else if(sum=='03'){
            sum2=3;
        }
        else if(sum=='04'){
            sum2=4;
        }
        else if(sum=='05'){
            sum2=5;
        }
        else if(sum=='06'){
            sum2=6;
        }
        else if(sum=='07'){
            sum2=7;
        }
        else if(sum=='08'){
            sum2=8;
        }
        else if(sum=='09'){
            sum2=9;
        }
        else{
            sum2=sum;
        }
        $(".hidee"+id).hide();

        $(".add"+id).html('<b>'+sum2+' Aime</b>');

        $.ajax({
            method:"GET",
            url: "{{url('/fetch/like')}}/"+id,
            async: false,
            success : function(response) {
                if(response){
                   console.log(response);

                }

            },
            error: function() {

            }
        });
    }
</script>
