@extends('app.include.layouts')

@section('content')
    <div class="page-content-wrapper">
        <!-- Product Slides-->
        <div class="product-slides owl-carousel mainimg" >
            <!-- Single Hero Slide-->
            <div class="" >
                <img src="{{asset('app/assets/img/bg-img/8.jpg')}}" class="output_image"  id="output_image" style=" width: 100% !important;"   loading="lazy"  >
            </div>

        </div>

        <div class="product-description pb-3 " >
            <!-- Flash Sale Slide-->
            <div class="flash-sale-wrapper slider" style="background-color: white">
                <div class="container">

                    <!-- Flash Sale Slide-->
                    <div class="flash-sale-slide owl-carousel">

                        <!-- Single Flash Sale Card-->
                        <div class="card flash-sale-card">
                            <div class="card-body" ><a href="#"><img src="{{asset('app/assets/img/product/1.jpg')}}" onclick="addFilter(this)" data-filter="none"  class="none" id="output_image1" alt="">
                                </a></div>
                        </div>
                        <!-- Single Flash Sale Card-->
                        <div class="card flash-sale-card">
                            <div class="card-body"><a href="#"><img src="{{asset('app/assets/img/product/1.jpg')}}" class="brightness"  onclick="addFilter(this)" data-filter="brightness"  id="output_image2" alt="">
                                </a></div>
                        </div>

                        <!-- Single Flash Sale Card-->
                        <div class="card flash-sale-card">
                            <div class="card-body"><a href="#"><img src="{{asset('app/assets/img/product/1.jpg')}}" onclick="addFilter(this)" data-filter="contrast"  class="contrast" id="output_image3" alt="">
                                </a></div>
                        </div>
                        <!-- Single Flash Sale Card-->
                        <div class="card flash-sale-card">
                            <div class="card-body"><a href="#"><img src="{{asset('app/assets/img/product/1.jpg')}}" onclick="addFilter(this)" data-filter="shadow"  class="shadow" id="output_image4" alt="">
                                   </a></div>
                        </div>
                        <!-- Single Flash Sale Card-->
                        <div class="card flash-sale-card">
                            <div class="card-body" ><a href="#"><img src="{{asset('app/assets/img/product/1.jpg')}}" onclick="addFilter(this)" data-filter="grayscale"  class="grayscale" id="output_image5" alt="">
                                </a></div>
                        </div>
                        <!-- Single Flash Sale Card-->
                        <div class="card flash-sale-card">
                            <div class="card-body"><a href="#"><img src="{{asset('app/assets/img/product/1.jpg')}}" class="rotate"  onclick="addFilter(this)" data-filter="rotate" id="output_image6" alt="">
                                </a></div>
                        </div>

                        <!-- Single Flash Sale Card-->
                        <div class="card flash-sale-card">
                            <div class="card-body"><a href="#"><img src="{{asset('app/assets/img/product/1.jpg')}}" onclick="addFilter(this)" data-filter="invert"   class="invert" id="output_image7" alt="">
                                </a></div>
                        </div>
                        <!-- Single Flash Sale Card-->
                        <div class="card flash-sale-card">
                            <div class="card-body"><a href="#"><img src="{{asset('app/assets/img/product/1.jpg')}}" onclick="addFilter(this)" data-filter="blur"  class="blur" id="output_image8" alt="">
                                </a></div>
                        </div>
                        <!-- Single Flash Sale Card-->
                        <div class="card flash-sale-card">
                            <div class="card-body" ><a href="#"><img src="{{asset('app/assets/img/product/1.jpg')}}" onclick="addFilter(this)" data-filter="opacity"  class="opacity" id="output_image9" alt="">
                                </a></div>
                        </div>
                        <!-- Single Flash Sale Card-->
                        <div class="card flash-sale-card">
                            <div class="card-body"><a href="#"><img src="{{asset('app/assets/img/product/1.jpg')}}" onclick="addFilter(this)" data-filter="saturate"  class="saturate"  id="output_image10" alt="">
                                </a></div>
                        </div>

                        <!-- Single Flash Sale Card-->
                        <div class="card flash-sale-card">
                            <div class="card-body"><a href="#"><img src="{{asset('app/assets/img/product/1.jpg')}}"class="sepia" onclick="addFilter(this)" data-filter="sepia"  id="output_image11" alt="">
                                </a></div>
                        </div>
                        <!-- Single Flash Sale Card-->
                        <div class="card flash-sale-card">
                            <div class="card-body"><a href="#"><img src="{{asset('app/assets/img/product/1.jpg')}}" class="mix" onclick="addFilter(this)" data-filter="mix"  id="output_image12" alt="">
                                </a></div>
                        </div>
                        <div class="">

                        </div>



                    </div>
                </div>
            </div>
            <!-- Ratings Submit Form-->
            <div class="ratings-submit-form bg-white py-3">
                <div class="container">
                    <form method="POST" action="{{ route('sweet.look.store') }}" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" class="check" name="filter" value="none">
                        <label class="form-control @error('img') is-invalid @enderror img" >
                            <center><i class="fa fa-image"></i> <!-- Add main screen --> <strong style="font-size: 14px;">Ajouter une image</strong></center><input type="file" style="display: none;"name="img" value="{{ old('img') }}"  accept="image/png, image/gif, image/jpeg" id="imgInp" onchange="preview_image(event)"><br>
                        </label><br>
                        <textarea name="description" style="height: 158px; display: none" cols="25" rows="8" class="form-control notes" placeholder="La description..."></textarea><br>
                        <button class="btn btn-sm btn-primary"  style="margin-left: 137px; display: none;" type="submit" >Poster</button>
                    </form>
                    <button class="btn btn-sm btn-primary next" onclick="removeDiv()"  style="margin-left: 137px;" href="#">Suivante</button>
                </div>
            </div>
        </div>
    </div>
@endsection
<script>
    function addFilter(elem) {
        let color = $(elem).data('filter');
        $(".output_image").addClass(color);
        $(".check").val(color);

    }
    function removeDiv() {
        $(".mainimg").hide();
        $(".slider").hide();
        $(".img").hide();
        $(".notes").show();
        $(".btn").show();
        $(".next").hide();

    }

    function preview_image(event)
    {
        var reader = new FileReader();
        reader.onload = function()
        {
            var output = document.getElementById('output_image');
            var output1 = document.getElementById('output_image1');
            var output2 = document.getElementById('output_image2');
            var output3 = document.getElementById('output_image3');
            var output4 = document.getElementById('output_image4');
            var output5 = document.getElementById('output_image5');
            var output6 = document.getElementById('output_image6');
            var output7 = document.getElementById('output_image7');
            var output8 = document.getElementById('output_image8');
            var output9 = document.getElementById('output_image9');
            var output10 = document.getElementById('output_image10');
            var output11 = document.getElementById('output_image11');
            var output12 = document.getElementById('output_image12');

            output.src = reader.result;
            output1.src = reader.result;
            output2.src = reader.result;
            output3.src = reader.result;
            output4.src = reader.result;
            output5.src = reader.result;
            output6.src = reader.result;
            output7.src = reader.result;
            output8.src = reader.result;
            output9.src = reader.result;
            output10.src = reader.result;
            output11.src = reader.result;
            output12.src = reader.result;
        }
        reader.readAsDataURL(event.target.files[0]);
    }

</script>

