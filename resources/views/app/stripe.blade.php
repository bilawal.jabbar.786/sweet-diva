<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>
    <style>
        @media only screen and (max-width: 600px) {
            body {
                background-color: #fad1d5;
            }
            #bg{
                background-image: none !important;
            }
            #p-0{
                padding-top: 20px !important;
                margin-top: 20px !important;
            }
            #mds{
                display: block !important;
            }
        }
        .form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
            background-color: #f70000;
            opacity: 1;
        }
    </style>
</head>

<body id="bg" style="background-image:url('{{asset('front/assets/19.jpg')}}'); background-repeat: no-repeat; background-size: 100% 100%;">
<div class="row" style="display: none" id="mds">
    <div class="col-md-4" style="text-align: center">
        <img src="{{asset('https://lepalaisdesfemmes.com/logo%20png%20sweet%20diva.png')}}" style="height: auto; width: 60%" alt="">
    </div>
</div>
<div class="container">
    <div class='row'>
        <div class='col-md-4'></div>
        <div class='col-md-4' id="p-0" style="margin-top: 100px; padding: 20px; background-color: white">
            <script src='https://js.stripe.com/v2/' type='text/javascript'></script>
            <form accept-charset="UTF-8" action="{{route('app.stripe.submit')}}" class="require-validation"
                  data-cc-on-file="false"
                  data-stripe-publishable-key="{{env('STRIPE_PUBLISHABLE_KEY')}}"
                  id="payment-form" method="post">
                {{ csrf_field() }}
                @if ((Session::has('success-message')))
                    <div class="alert alert-success col-md-12">{{
					Session::get('success-message') }}</div>
                @endif @if ((Session::has('fail-message')))
                    <div class="alert alert-danger col-md-12">{{
					Session::get('fail-message') }}</div>
                @endif
                <div class='form-row'>
                    <div class='col-12 error form-group d-none'>
                        <div class='alert-danger alert'>Veuillez corriger les erreurs et essayer de nouveau.</div>
                    </div>
                </div>
                <div class='form-row'>
                    <div class='col-12 form-group required'>
                        <label class='control-label'>Nom sur la carte</label> <input class='form-control' size='4' type='text'>
                    </div>
                </div>
                <div class='form-row'>
                    <div class='col-12 form-group card required'>
                        <label class='control-label'>Numéro de carte</label> <input
                            autocomplete='off' class='form-control card-number' size='20'
                            type='text'>
                    </div>
                </div>
                <div class='form-row'>
                    <div class='col-4 form-group cvc required'>
                        <label class='control-label'>CVC</label> <input
                            autocomplete='off' class='form-control card-cvc'
                            placeholder='ex. 311' size='4' type='text'>
                    </div>
                    <div class='col-4 form-group expiration required'>
                        <label class='control-label'>Expiration</label> <input
                            class='form-control card-expiry-month' placeholder='MM' size='2'
                            type='text'>
                    </div>
                    <div class='col-4 form-group expiration required'>
                        <label class='control-label'>Année</label> <input
                            class='form-control card-expiry-year' placeholder='YYYY'
                            size='4' type='text'>
                    </div>
                </div>
                <div class='form-row'>
                    <div class='col-12'>
                        <div class='total'>
                           <b> Total: <span class='amount'>{{$price}} €</span></b>
                            <input type="hidden" name="price" value="{{$price}}">
                            <input type="hidden" name="id" value="{{$id}}">
                        </div>
                    </div>
                </div>
                <hr>
                <div class='form-row'>
                    <div class='col-12 form-group'>
                        <button class='loader form-control btn btn-primary submit-button'
                                type='submit' style="margin-top: 10px;">Payer »</button>
                    </div>
                </div>
                <div class='form-row'>
                    <div class='col-12 form-group'>
                        <a href="{{route('app.index')}}" class='form-control btn btn-success submit-button'>Retour à l'application</a>
                    </div>
                </div>
            </form>
        </div>
        <div class='col-md-4'></div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-1.12.3.min.js"
        integrity="sha256-aaODHAgvwQW1bFOGXMeX+pC4PZIPsvn2h1sArYOhgXQ="
        crossorigin="anonymous"></script>
<script
    src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"
    integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS"
    crossorigin="anonymous"></script>
<script>
    $(function() {
        $('form.require-validation').bind('submit', function(e) {
            var $form         = $(e.target).closest('form'),
                inputSelector = ['input[type=email]', 'input[type=password]',
                    'input[type=text]', 'input[type=file]',
                    'textarea'].join(', '),
                $inputs       = $form.find('.required').find(inputSelector),
                $errorMessage = $form.find('div.error'),
                valid         = true;

            $errorMessage.addClass('d-none');
            $('.is-invalid').removeClass('is-invalid');
            $inputs.each(function(i, el) {
                var $input = $(el);
                if ($input.val() === '') {
                    $input.addClass('is-invalid');
                    $errorMessage.removeClass('d-none');
                    e.preventDefault(); // cancel on first error
                }
            });
        });
    });

    $(function() {
        var $form = $("#payment-form");

        $form.on('submit', function(e) {
            if (!$form.data('cc-on-file')) {
                e.preventDefault();
                Stripe.setPublishableKey($form.data('stripe-publishable-key'));
                Stripe.createToken({
                    number: $('.card-number').val(),
                    cvc: $('.card-cvc').val(),
                    exp_month: $('.card-expiry-month').val(),
                    exp_year: $('.card-expiry-year').val()
                }, stripeResponseHandler);
            }
        });
        var errorMessages = {
            incorrect_number: "Le numéro de carte est incorrect.",
            invalid_number: "Le numéro de carte n'est pas un numéro de carte de crédit valide.",
            invalid_expiry_month: "Le mois d'expiration de la carte n'est pas valide.",
            invalid_expiry_year: "L'année d'expiration de la carte est invalide.",
            invalid_cvc: "Le code de sécurité de la carte est invalide.",
            expired_card: "La carte a expiré.",
            incorrect_cvc: "Le code de sécurité de la carte est incorrect.",
            incorrect_zip: "La validation du code postal de la carte a échoué.",
            card_declined: "La carte a été refusée.",
            missing: "Il n'y a pas de carte sur un client qui est facturé.",
            processing_error: "Une erreur s'est produite lors du traitement de la carte.",
            rate_limit:  "Une erreur s'est produite en raison de demandes atteignant l'API trop rapidement. S'il vous plaît laissez-nous savoir si vous rencontrez régulièrement cette erreur."
        };
        function stripeResponseHandler(status, response) {
            if (response.error) {
                $('.error')
                    .removeClass('d-none')
                    .find('.alert')
                    .text(errorMessages[ response.error.code ]);
            } else {
                $(".loader").html("Veuillez patienter paiement en cours...");
                // token contains id, last4, and card type
                var token = response['id'];
                // insert the token into the form so it gets submitted to the server
                $form.find('input[type=text]').empty();
                $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
                $form.get(0).submit();
            }
        }
    })
</script>
</body>
</html>
