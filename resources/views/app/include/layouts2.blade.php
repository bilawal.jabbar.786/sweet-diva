<!DOCTYPE html>
<html lang="en">
<head>
    <?php
    $gs = \App\GeneralSettings::find(1);
    ?>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, viewport-fit=cover, shrink-to-fit=no">
    <meta name="description" content="Revumo - Multipurpose Ecommerce Mobile HTML Template">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="theme-color" content="#100DD1">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <!-- The above tags *must* come first in the head, any other head content must come *after* these tags-->
    <!-- Title-->
    <title>{{$gs->sitename}}</title>
    <!-- Favicon-->
    <link rel="icon" href="{{asset($gs->logo)}}">
    <!-- Apple Touch Icon-->
    <link rel="apple-touch-icon" href="{{asset($gs->logo)}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset($gs->logo)}}">
    <link rel="apple-touch-icon" sizes="167x167" href="{{asset($gs->logo)}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset($gs->logo)}}">
    <!-- Stylesheet-->
    <link rel="stylesheet" href="{{asset('app/assets/style.css')}}">

    <style>
        .sidenav-profile .user-profile img {
            border-radius: 50%;
            height: 100%;
        }
        .top-search-form form .form-control {
            max-width: 218px;
            background-color: #ffffff;
            height: 32px;
            font-size: 14px!important;
            padding: 0px 0px;
            padding-left: 8px!important;
            border: 1px solid #ebebeb;
            border-radius: 1rem;
            color: #747794;
        }
        .notification .badge {
            position: absolute;
            top: -10px;
            right: 29px;
            padding: 5px 7px;
            border-radius: 50%;
            background-color: #dc9cbe;
            color: white;
        }

        .featured-product-card .custom-badge::before {
            border-color: #ad628a transparent #b2698f #f4d0da;
            border-style: solid;
            border-width: 11px 8px;
            content: "";
            height: 22px;
            position: absolute;
            right: -17px;
            top: 0;
            width: 17px;
            z-index: 1;
        }
        .user-info-card .user-profile img {
            border-radius: 50%;
            height: 100%;
        }

        element.style {
        }
        .page-item.active .page-link {
            z-index: 3;
            color: #fff;
            background-color: #ffe1e7;
            border-color: #ffe1e7;
        }


        .revumo-sidenav-wrapper {
            -webkit-transition-duration: 500ms;
            transition-duration: 500ms;
            position: fixed;
            width: 100%;
            z-index: 999999;
            top: 0;
            left: -250px;
            height: 100%;
            background: #1f1f1f;
            background: -webkit-gradient(linear, right top, left top, from(#1f1f1f), to(#313131));
            background: linear-gradient(to left, #ffe1e7, #a55580);
            overflow-y: auto;
            width: 240px;
        }
        .top-search-form form .form-control {
            max-width: 190px;
            background-color: #ffffff;
            height: 32px;
            font-size: 12px;
            padding: 0px 0px;
            padding-left: 35px;
            border: 1px solid #ebebeb;
            border-radius: 1rem;
            color: #747794;
        }
        .bg-warning, .badge-warning {
            background: linear-gradient(to left, #ffe1e7, #a55580); !important;
            color: #020310;
        }
        .single-order-status .order-status {
            color: white !important;
        }
        .single-order-status .order-icon {
            color: white;
            margin-right: 0.5rem;
        }
        .user-info-card {
            position: relative;
            z-index: 1;
            background: linear-gradient(to left, #ffe1e7, #a55580); !important;
            border-color: #ffe1e7 !important;
        }
    </style>
    <style>
        .brightness {filter: brightness(0.30);}


        .choose-size-radio {
            position: relative;
            z-index: 1;
            text-align: left !important;
        }
        .choose-size-radio .form-check-label {
            font-size: 17px !important;
            font-weight: 700;
        }
        .none{
            filter: none;
        }
        .brightness{
            filter: brightness(200%);
        }
        .blur{
            filter: blur(5px);
        }
        .contrast{
            filter: contrast(200%);
        }
        .shadow{
            filter: drop-shadow(8px 8px 10px gray);
        }

        .grayscale{
            filter: grayscale(100%);
        }
        .rotate{
            filter: hue-rotate(90deg);
        }
        .invert{
            filter: invert(100%);
        }
        .opacity{
            filter: opacity(30%);
        }
        .saturate{
            filter: saturate(8);
        }
        .sepia{
            filter: sepia(100%);
        }
        .mix{
            filter: contrast(200%) brightness(150%);
        }
        /* Add animation to "page content" */
        .page-content-wrapper {
            position: relative;
            -webkit-animation-name: animatebottom;
            -webkit-animation-duration: 1s;
            animation-name: animatebottom;
            animation-duration: 1s
        }

        @-webkit-keyframes animatebottom {
            from { bottom:-100px; opacity:0 }
            to { bottom:0px; opacity:1 }
        }

        @keyframes animatebottom {
            from{ bottom:-100px; opacity:0 }
            to{ bottom:0; opacity:1 }
        }

        .d-none{
            display: none !important;
        }

    </style>


</head>
<body>

@yield('content')



<!-- All JavaScript Files-->


<script src="{{asset('app/assets/js/jquery.min.js')}}"></script>
<script src="{{asset('app/assets/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('app/assets/js/waypoints.min.js')}}"></script>
<script src="{{asset('app/assets/js/jquery.easing.min.js')}}"></script>
<script src="{{asset('app/assets/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('app/assets/js/jquery.counterup.min.js')}}"></script>
<script src="{{asset('app/assets/js/jquery.countdown.min.js')}}"></script>
<script src="{{asset('app/assets/js/default/jquery.passwordstrength.js')}}"></script>
<script src="{{asset('app/assets/js/wow.min.js')}}"></script>
<script src="{{asset('app/assets/js/jarallax.min.js')}}"></script>
<script src="{{asset('app/assets/js/jarallax-video.min.js')}}"></script>
<script src="{{asset('app/assets/js/default/dark-mode-switch.js')}}"></script>
<script src="{{asset('app/assets/js/default/no-internet.js')}}"></script>
<script src="{{asset('app/assets/js/default/active.js')}}"></script>


@yield('script')
</body>
</html>
