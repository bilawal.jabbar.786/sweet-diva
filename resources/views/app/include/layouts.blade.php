<!DOCTYPE html>
<html lang="en">
<head>
    <?php
    $gs = \App\GeneralSettings::find(1);
    ?>
        <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, viewport-fit=cover, shrink-to-fit=no">
    <meta name="description" content="Revumo - Multipurpose Ecommerce Mobile HTML Template">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="theme-color" content="#100DD1">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <!-- The above tags *must* come first in the head, any other head content must come *after* these tags-->
    <!-- Title-->
    <title>{{$gs->sitename}}</title>
    <!-- Favicon-->
    <link rel="icon" href="{{asset($gs->logo)}}">
    <!-- Apple Touch Icon-->
    <link rel="apple-touch-icon" href="{{asset($gs->logo)}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset($gs->logo)}}">
    <link rel="apple-touch-icon" sizes="167x167" href="{{asset($gs->logo)}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset($gs->logo)}}">
    <!-- Stylesheet-->
    <link rel="stylesheet" href="{{asset('app/assets/style.css')}}">
    <!-- Web App Manifest-->
{{--    <link rel="manifest" href="{{asset('app/assets/manifest.json')}}">--}}

        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">

        <style>
        .sidenav-profile .user-profile img {
            border-radius: 50%;
            height: 100%;
        }
        .top-search-form form .form-control {
            max-width: 218px;
            background-color: #ffffff;
            height: 32px;
            font-size: 14px!important;
            padding: 0px 0px;
            padding-left: 8px!important;
            border: 1px solid #ebebeb;
            border-radius: 1rem;
            color: #747794;
        }
        .notification .badge {
            position: absolute;
            top: -10px;
            right: 29px;
            padding: 5px 7px;
            border-radius: 50%;
            background-color: #dc9cbe;
            color: white;
        }

        .featured-product-card .custom-badge::before {
            border-color: #ad628a transparent #b2698f #f4d0da;
            border-style: solid;
            border-width: 11px 8px;
            content: "";
            height: 22px;
            position: absolute;
            right: -17px;
            top: 0;
            width: 17px;
            z-index: 1;
        }
        .user-info-card .user-profile img {
            border-radius: 50%;
            height: 100%;
        }

        element.style {
        }
        .page-item.active .page-link {
            z-index: 3;
            color: #fff;
            background-color: #ffe1e7;
            border-color: #ffe1e7;
        }


        .revumo-sidenav-wrapper {
            -webkit-transition-duration: 500ms;
            transition-duration: 500ms;
            position: fixed;
            width: 100%;
            z-index: 999999;
            top: 0;
            left: -250px;
            height: 100%;
            background: #1f1f1f;
            background: -webkit-gradient(linear, right top, left top, from(#1f1f1f), to(#313131));
            background: linear-gradient(to left, #ffe1e7, #a55580);
            overflow-y: auto;
            width: 240px;
        }
        .top-search-form form .form-control {
            max-width: 190px;
            background-color: #ffffff;
            height: 32px;
            font-size: 12px;
             padding: 0px 0px;
            padding-left: 35px;
            border: 1px solid #ebebeb;
            border-radius: 1rem;
            color: #747794;
        }
        .bg-warning, .badge-warning {
            background: linear-gradient(to left, #ffe1e7, #a55580); !important;
            color: #020310;
        }
        .single-order-status .order-status {
            color: white !important;
        }
        .single-order-status .order-icon {
            color: white;
            margin-right: 0.5rem;
        }
        .user-info-card {
            position: relative;
            z-index: 1;
            background: linear-gradient(to left, #ffe1e7, #a55580); !important;
            border-color: #ffe1e7 !important;
        }
    </style>
        <style>
            .brightness {filter: brightness(0.30);}


            .choose-size-radio {
                position: relative;
                z-index: 1;
                text-align: left !important;
            }
            .choose-size-radio .form-check-label {
                font-size: 17px !important;
                font-weight: 700;
            }
            .none{
                filter: none;
            }
            .brightness{
                filter: brightness(200%);
            }
            .blur{
                filter: blur(5px);
            }
            .contrast{
                filter: contrast(200%);
            }
            .shadow{
                filter: drop-shadow(8px 8px 10px gray);
            }

            .grayscale{
                filter: grayscale(100%);
            }
            .rotate{
                filter: hue-rotate(90deg);
            }
            .invert{
                filter: invert(100%);
            }
            .opacity{
                filter: opacity(30%);
            }
            .saturate{
                filter: saturate(8);
            }
            .sepia{
                filter: sepia(100%);
            }
            .mix{
                filter: contrast(200%) brightness(150%);
            }
            /* Add animation to "page content" */
            .page-content-wrapper {
                position: relative;
                -webkit-animation-name: animatebottom;
                -webkit-animation-duration: 1s;
                animation-name: animatebottom;
                animation-duration: 1s
            }

            @-webkit-keyframes animatebottom {
                from { bottom:-100px; opacity:0 }
                to { bottom:0px; opacity:1 }
            }

            @keyframes animatebottom {
                from{ bottom:-100px; opacity:0 }
                to{ bottom:0; opacity:1 }
            }

            .d-none{
                display: none !important;
            }

        </style>


</head>
<body>
<!-- Preloader-->
{{--<div class="preloader" id="preloader">--}}
{{--    <div class="spinner-grow text-secondary" role="status">--}}
{{--        <div class="sr-only">Loading...</div>--}}
{{--    </div>--}}
{{--</div>--}}
<!-- Header Area-->
{{--<div id="loader111" class="d-none"></div>--}}
<div class="header-area" id="headerArea">
    <div class="container h-100 d-flex align-items-center justify-content-between">
        <!-- Logo Wrapper-->
        @if( request()->is('app/index') || request()->is('app/explore'))

        <div class="logo-wrapper"><a href="{{route('app.index')}}"><img style="width: 45px;"  loading="lazy" src="{{asset('logo png sweet diva.png')}}" alt=""></a></div>
        <!-- Search Form-->

        <div class="top-search-form">
            <form action="{{route('app.search.product')}}" method="post">
                @csrf
                <div class="container" style="padding-top: 20px;">
                    <div class="row" style="padding-left: 0px!important;">
{{--                        <div class="col-7">--}}
{{--                            <input class="form-control" name="keywords" type="search" placeholder="Entrez votre mot-clé">--}}
{{--                            <button type="submit" style="padding-top: 28px;padding-left: 20px;"><i class="fa fa-search"></i></button>--}}
{{--                        </div>--}}
{{--                        <?php $size = \App\Size::whereNotNull('newsize')->pluck('newsize')->unique();?>--}}
{{--                        <div class="col-12">--}}
{{--                            <select class="form-control select2" name="size" style="margin-bottom: 20px;"  onchange="location = this.options[this.selectedIndex].value;">--}}
{{--                                <option value="" >Choisissez votre taille</option>--}}

{{--                                        <option value="{{route('app.search.product.size',['size'=>encrypt('XL (44/46)')] ) }}">XL (44/46)</option>--}}
{{--                                <option value="{{route('app.search.product.size',['size'=>encrypt('1XL (46/48)')] ) }}">1XL (46/48)</option>--}}
{{--                                <option value="{{route('app.search.product.size',['size'=>encrypt('2XL (48/50)')] ) }}">2XL (48/50)</option>--}}
{{--                                <option value="{{route('app.search.product.size',['size'=>encrypt('3XL (50/52)')] ) }}">3XL (50/52)</option>--}}
{{--                                <option value="{{route('app.search.product.size',['size'=>encrypt('4XL (52/54)')] ) }}">4XL (52/54)</option>--}}
{{--                                <option value="{{route('app.search.product.size',['size'=>encrypt('5XL (54/56)')] ) }}">5XL (54/56)</option>--}}



{{--                            </select>--}}
{{--                        </div>--}}

                        <h4 style="margin-bottom: 20px;">SWEET DIVA</h4>


                    </div>

                </div>

            </form>
        </div>
    @else
            <div class="back-button"><a href="#" onclick="history.back()"><i class="lni lni-arrow-left"></i></a></div>
            <!-- Page Title-->
            <div class="page-heading">
                <h5 class="mb-0">{{$title??''}}</h5>
            </div>
    @endif
        <!-- Navbar Toggler-->
        <div class="revumo-navbar-toggler d-flex flex-wrap" id="revumoNavbarToggler"><span></span><span></span><span></span></div>
    </div>
</div>
<!-- Sidenav Black Overlay-->
<div class="sidenav-black-overlay"></div>
<!-- Side Nav Wrapper-->
<div class="revumo-sidenav-wrapper" id="sidenavWrapper">
    <!-- Sidenav Profile-->
    <div class="sidenav-profile">
        @auth
        <?php $user = Auth::user();?>
        @if($user->photo))
        <div class="user-profile"><img src="{{asset($user->photo)}}"  loading="lazy" alt=""></div>
        @else
        <div class="user-profile"><img src="{{asset('app/assets/img/bg-img/9.jpg')}}"  loading="lazy" alt=""></div>
            @endif
                @else
                    <div class="user-profile"><img src="{{asset('app/assets/img/bg-img/9.jpg')}}"  loading="lazy" alt=""></div>

            @endauth
        <div class="user-info">
            @auth
            <h6 class="user-name mb-0">{{$user->name .' '.$user->surname }}</h6>
            <p class="available-balance"> <span style="font-size: 10px;">{{$user->email ?? ''}}</span></p>
            @endauth
        </div>
    </div>
    <!-- Sidenav Nav-->
    <ul class="sidenav-nav pl-0">
        @auth
        <li><a href="{{route('app.myprofile')}}"><i class="lni lni-user"></i>Mon profil</a></li>
        <li><a href="{{route('app.myorder')}}"><i class="lni lni-cart"></i>Mes Commandes </a></li>
        <li><a href="{{route('app.points')}}"><i class="lni lni-paypal"></i>Mes points cadeaux </a></li>
        @endauth
{{--        <li><a href="{{route('app.notfication')}}"><i class="lni lni-alarm lni-tada-effect"></i>Notification<span class="ml-3 badge badge-warning">3</span></a></li>--}}
        <li><a href="{{route('app.allcards')}}"><i class="lni lni-gift"></i>Carte cadeaux </a></li>
{{--        <li><a href="{{route('app.my.post')}}"><i class="lni lni-protection"></i>Mon message</a></li>--}}
        @auth
        <li><a href="{{route('app.sponser.history')}}"><i class="lni lni-user"></i>Parrainage</a></li>
        @endauth
        <li ><a href="{{route('app.about')}}"><i class="lni lni-protection" ></i>À propos de nous</a></li>
        <li ><a href="tel:{{$gs->phone}}"><i class="lni lni-phone" ></i>Contact</a></li>
        <li><a href="{{route('setting.index')}}"><i class="lni lni-cog"></i>Réglages </a></li>
        @auth
        <li> <a href="{{route('app.logout')}}">
                <i class="lni lni-power-switch"></i>Déconnecter</a>
        </li>
        @else
            <li> <a href="{{route('app.login')}}">
                    <i class="lni lni-user"></i>Connexion</a>
            </li>
            <li> <a href="{{route('app.register')}}">
                    <i class="lni lni-user"></i>Inscrire</a>
            </li>
        @endauth
        <div class="container" style="margin-top: 10px;">
                <div class="row">
                 <div class="col-3">
                     <a href="https://www.facebook.com/sharer/sharer.php?u=contact.sweetdiva/"><img src="{{asset('app/assets/img/fb.png')}}" style="width: 34px; "></a>
                 </div>
                    <div class="col-3">
                        <a href="http://instagram.com/sweet_diva_fwi/"><img src="{{asset('app/assets/img/insta.png')}}" style="width: 34px; "></a>
                    </div>

                    <div class="col-3">
                        <a href="https://www.tiktok.com/@sweet_diva.fwi?lang=fr"><img src="{{asset('app/assets/img/tiktok_logo_png_transparent512.png')}}" style="width: 34px; "></a>
                    </div>

                </div>

            </div>

    </ul>
    <!-- Go Back Button-->
    <div class="go-home-btn" id="goHomeBtn" ><i class="lni lni-arrow-left" style="margin: 10px!important;"></i></div>
</div>

@yield('content')

<!-- Internet Connection Status-->
<div class="internet-connection-status" id="internetStatus"></div>
<!-- Footer Nav-->
<div class="footer-nav-area" id="footerNav">
    <div class="container h-100 px-0">
        <div class="revumo-footer-nav h-100">
            <ul class="h-100 d-flex align-items-center justify-content-between pl-0">
                <li class="{{  request()->is('app/index') ? 'active':'' }}"><a href="{{route('app.index')}}"><i class="lni lni-home"></i>Accueil</a></li>


                <li class="{{  request()->is('app/wishlist/product') ? 'active':'' }}"><a href="{{route('app.wishlist')}}"><i class="lni lni-heart"></i>Favori</a></li>
                @auth
                <?php $noti = \App\Notfication::where('r_id','=',Auth::user()->id)->where('status','=',0)->count();
                ?>
                @endauth
                <li class="{{  request()->is('app/notifications') ? 'active':'' }}"><a href="{{route('app.notfication')}}"  class="notification"><span class="badge">@auth{{$noti}}@else 0 @endauth</span><i class="lni lni-alarm "></i>Notification</a></li>

{{--                <li class="{{  request()->is('app/filter') ? 'active':'' }}"><a href="{{route('app.filter')}}"  ><i class="lni lni-camera"></i>Caméra</a></li>--}}
{{--                <li class="{{  request()->is('app/sweet/look') ? 'active':'' }}"><a href="{{route('sweet.look')}}"><i class="lni lni-heart"></i>Sweet look</a></li>--}}
                <li class="{{  request()->is('app/cart') ? 'active':'' }}"><a href="{{route('app.cart')}}"><i class="lni lni-shopping-basket"></i>Panier</a></li>



            </ul>
        </div>
    </div>
</div>


<!-- All JavaScript Files-->


<script src="{{asset('app/assets/js/jquery.min.js')}}"></script>
<script src="{{asset('app/assets/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('app/assets/js/waypoints.min.js')}}"></script>
<script src="{{asset('app/assets/js/jquery.easing.min.js')}}"></script>
<script src="{{asset('app/assets/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('app/assets/js/jquery.counterup.min.js')}}"></script>
<script src="{{asset('app/assets/js/jquery.countdown.min.js')}}"></script>
<script src="{{asset('app/assets/js/default/jquery.passwordstrength.js')}}"></script>
<script src="{{asset('app/assets/js/wow.min.js')}}"></script>
<script src="{{asset('app/assets/js/jarallax.min.js')}}"></script>
<script src="{{asset('app/assets/js/jarallax-video.min.js')}}"></script>
<script src="{{asset('app/assets/js/default/dark-mode-switch.js')}}"></script>
<script src="{{asset('app/assets/js/default/no-internet.js')}}"></script>
<script src="{{asset('app/assets/js/default/active.js')}}"></script>
{{--<script src="{{asset('app/assets/js/pwa.js')}}"></script>--}}
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script>
    $(".paymentformsubmit").submit(function(){
        $(this).find(':input[type=submit]').prop('disabled', true);
        $(this).find(':input[type=submit]').val("Chargement..");
    });
</script>


<script>

    function addtowishlist(elem) {
        event.preventDefault();
        let id = $(elem).attr("id");
        // alert(id);
        $.ajax({
            method:"GET",
            url: "{{url('/fetch/wishlist')}}/"+id,
            async: false,
            success : function(response) {
             if(response){
                 $(".okk"+id).hide();
                 $(".fav"+id).html('<i class="fa fa-heart" style="color: #ff0000;"></i>');
             }

            },
            error: function() {

            }
        });
    }
</script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
        @if(Session::has('messege'))
    var type="{{Session::get('alert-type','info')}}"
    switch(type){
        case 'info':
            toastr.info("{{ Session::get('messege') }}");
            break;
        case 'success':
            toastr.success("{{ Session::get('messege') }}");
            break;
        case 'warning':
            toastr.warning("{{ Session::get('messege') }}");
            break;
        case 'error':
            toastr.error("{{ Session::get('messege') }}");
            break;
    }
    @endif
</script>


@yield('script')
</body>
</html>
