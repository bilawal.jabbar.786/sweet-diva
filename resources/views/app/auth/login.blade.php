<!DOCTYPE html>
<html lang="en">
<head>
    <?php
    $gs = \App\GeneralSettings::find(1);
    ?>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, viewport-fit=cover, shrink-to-fit=no">
    <meta name="description" content="Revumo - Multipurpose Ecommerce Mobile HTML Template">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="theme-color" content="#100DD1">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <!-- The above tags *must* come first in the head, any other head content must come *after* these tags-->
    <!-- Title-->
    <title>{{$gs->sitename}}</title>
    <!-- Favicon-->
    <link rel="icon" href="{{asset($gs->logo)}}">
    <!-- Apple Touch Icon-->
    <link rel="apple-touch-icon" href="{{asset($gs->logo)}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset($gs->logo)}}">
    <link rel="apple-touch-icon" sizes="167x167" href="{{asset($gs->logo)}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset($gs->logo)}}">
    <!-- Stylesheet-->
    <link rel="stylesheet" href="{{asset('app/assets/style.css')}}">
    <!-- Web App Manifest-->
    <link rel="manifest" href="{{asset('app/assets/manifest.json')}}">


</head>
<body>
<!-- Preloader-->
<div class="preloader" id="preloader">
    <div class="spinner-grow text-secondary" role="status">
        <div class="sr-only">Loading...</div>
    </div>
</div>
<!-- Login Wrapper Area-->
<div class="login-wrapper d-flex align-items-center justify-content-center text-center">
    <!-- Background Shape-->
    <div class="background-shape">

    </div>
    <div class="container">

        <div class="row justify-content-center">
            <div class="col-12 col-sm-9 col-md-7 col-lg-6 col-xl-5"><img class="big-logo" style="width: 56%;" src="{{asset('logo png sweet diva.png')}}" alt="">
                <!-- Register Form-->
                <div class="register-form mt-5 px-4">
                    <form action="{{route('app.login.index')}}" class="loginformsubmit"  method="POST">
                        @csrf
                        @if(Session::has('message'))
                            <p style="text-align: left; color: red">{{ Session::get('message') }}</p>
                        @endif
                        <div class="form-group text-left mb-4"><span>E-mail</span>
                            <label for="email"><i class="lni lni-user"></i></label>
                            <input class="form-control" id="email" type="email" name="email" required placeholder="marta@revumo.com">
                        </div>
                        <div class="form-group text-left mb-4"><span>Mot de pass</span>
                            <label for="password"><i class="lni lni-lock"></i></label>
                            <input class="form-control" id="password" type="password" required name="password" placeholder="***************">
                        </div>
                        <div class="form-group text-left mb-4">

                            <input type="checkbox" id="vehicle1"  name="accepttos" class="accepttos" required>
                            <label for="vehicle1" style="position: unset!important;"> Conditions générales de vente</label>

                        </div>
                        <button class="btn btn-warning btn-lg w-100" type="submit">Connexion</button>
                    </form>
                </div>
                <!-- Login Meta-->
                <div class="login-meta-data"><a class="forgot-password d-block mt-3 mb-1" href="{{route('app.forget')}}">Mot de passe oublié?</a>
                    <p class="mb-0"><a class="ml-1" href="{{route('app.register')}}">Inscription</a></p>
                    <p><a class="ml-1"  href="#" onclick="history.back()">Retour à la page d’accueil</a></p>
                    <a href="{{route('app.off.terms')}}"  style="color: white;">Conditions générales de vente</a>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- All JavaScript Files-->
<script>
    function checkBox(){
        $('#select-all').
    }
</script>

<script src="{{asset('app/assets/js/jquery.min.js')}}"></script>
<script src="{{asset('app/assets/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('app/assets/js/waypoints.min.js')}}"></script>
<script src="{{asset('app/assets/js/jquery.easing.min.js')}}"></script>
<script src="{{asset('app/assets/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('app/assets/js/jquery.counterup.min.js')}}"></script>
<script src="{{asset('app/assets/js/jquery.countdown.min.js')}}"></script>
<script src="{{asset('app/assets/js/default/jquery.passwordstrength.js')}}"></script>
<script src="{{asset('app/assets/js/wow.min.js')}}"></script>
<script src="{{asset('app/assets/js/jarallax.min.js')}}"></script>
<script src="{{asset('app/assets/js/jarallax-video.min.js')}}"></script>
<script src="{{asset('app/assets/js/default/dark-mode-switch.js')}}"></script>
<script src="{{asset('app/assets/js/default/no-internet.js')}}"></script>
<script src="{{asset('app/assets/js/default/active.js')}}"></script>
<script src="{{asset('app/assets/js/pwa.js')}}"></script>
<script>
    $(".loginformsubmit").submit(function(){
        $(this).find(':input[type=submit]').prop('disabled', true);
        $(this).find(':input[type=submit]').html("Chargement..");
    });
</script>
</body>
</html>
