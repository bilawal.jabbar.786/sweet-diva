
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {{--    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">--}}
    <title>Mail</title>

</head>
<body>
<p>

    Aide sur le mot de passe <br>
    Pour vérifier votre identité, veuillez utiliser le code suivant : <br>
    <b>{{$dataa['otp']}} </b> <br>
    Sweet diva prend très au sérieux la sécurité de votre compte. Sweet diva ne vous adressera jamais d'e-mail pour vous demander d'indiquer ou pour vérifier votre mot de passe Sweet diva, votre numéro de carte de crédit ou de compte bancaire. Si vous recevez un e-mail suspect comportant un lien pour mettre à jour les informations de votre compte, ne cliquez pas sur le lien—. Au lieu de cela, signalez cet e-mail à Sweet diva afin que nous puissions l'examiner.
    Nous espérons vous revoir bientôt.
</p>


</body>
</html>
