<!DOCTYPE html>
<html lang="en">
<head>
    <?php
    $gs = \App\GeneralSettings::find(1);
    ?>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, viewport-fit=cover, shrink-to-fit=no">
    <meta name="description" content="Revumo - Multipurpose Ecommerce Mobile HTML Template">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="theme-color" content="#100DD1">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <!-- The above tags *must* come first in the head, any other head content must come *after* these tags-->
    <!-- Title-->
    <title>{{$gs->sitename}}</title>
    <!-- Favicon-->
    <link rel="icon" href="{{asset($gs->logo)}}">
    <!-- Apple Touch Icon-->
    <link rel="apple-touch-icon" href="{{asset($gs->logo)}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset($gs->logo)}}">
    <link rel="apple-touch-icon" sizes="167x167" href="{{asset($gs->logo)}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset($gs->logo)}}">
    <!-- Stylesheet-->
    <link rel="stylesheet" href="{{asset('app/assets/style.css')}}">
    <!-- Web App Manifest-->
    <link rel="manifest" href="{{asset('app/assets/manifest.json')}}">


</head>
<body>
<!-- Preloader-->
<div class="preloader" id="preloader">
    <div class="spinner-grow text-secondary" role="status">
        <div class="sr-only">Loading...</div>
    </div>
</div>
<!-- Login Wrapper Area-->
<div class="login-wrapper d-flex align-items-center justify-content-center text-center">
    <!-- Background Shape-->
    <div class="background-shape"></div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-sm-9 col-md-7 col-lg-6 col-xl-5"><img class="big-logo"style="width: 40%;" src="{{asset('logo png sweet diva.png')}}" alt="">
                <!-- Register Form-->
                <div class="register-form mt-5 px-4">
                    <form action="{{route('app.register.index')}}" class="loginformsubmit"  method="POST">
                        @csrf
                        @if(Session::has('message'))
                            <p style="text-align: left; color: red">{{ Session::get('message') }}</p>
                        @endif
                        <div class="form-group text-left mb-4"><span>Nom</span>
                            <label for="username"><i class="lni lni-user"></i></label>
                            <input class="form-control"  name="name" type="text" placeholder="Nom" required>
                        </div>
                        <div class="form-group text-left mb-4"><span>Prénom</span>
                            <label for="username"><i class="lni lni-user"></i></label>
                            <input class="form-control"   name="surname" type="text" placeholder="Prénom"required>
                        </div>
                        <div class="form-group text-left mb-4"><span>E-mail</span>
                            <label for="email"><i class="lni lni-envelope"></i></label>
                            <input class="form-control" id="email" name="email" type="email" placeholder="marta@revumo.com" required>
                        </div>
                        <div class="form-group text-left mb-4"><span>Téléphoner</span>
                            <label for="email"><i class="lni lni-phone"></i></label>
                            <input class="form-control" id="email" name="phone" type="text" placeholder="Téléphoner" required>
                        </div>
                        <div class="form-group text-left mb-4"><span>Date de naissance</span>
                            <label for="username"><i class="lni lni-user"></i></label>
                            <input class="form-control" id="dob"  name="dob" type="date">
                        </div>
                        <div class="form-group text-left mb-4"><span> Choisissez votre taille</span>

                            <select class="mb-3 form-select" id="topic" name="size" required>

                                <option value="TU">TU</option>
                                <option value="XL(44/46)">XL (44/46)</option>
                                <option value="1XL(46/48)">1XL (46/48)</option>
                                <option value="2XL(48/50)">2XL (48/50)</option>
                                <option value="3XL(50/52)">3XL (50/52)</option>
                                <option value="4XL(52/54)">4XL (52/54)</option>
                                <option value="5XL(54/56)">5XL (54/56)</option>



                            </select>

                        </div>
                        <div class="form-group text-left mb-4"><span>Adresse</span>
                            <label for="email"><i class="lni lni-map-marker"></i></label>
                            <input class="form-control" id="email" name="address" type="text" placeholder="Adresse" required>
                        </div>
                        <div class="form-group text-left mb-4"><span>Code Postal</span>
                            <label for="email"><i class="lni lni-map-marker"></i></label>
                            <input class="form-control" name="cp"  id="email"  type="number" placeholder="Code Postal" required>
                        </div>
                        <div class="form-group text-left mb-4"><span>Ville</span>
                            <label for="city"><i class="lni lni-map-marker"></i></label>
                            <input class="form-control" name="city"    type="text" placeholder="Ville" required>
                        </div>
                        <div class="form-group text-left mb-4"><span>Pays</span>

                            <select class="mb-3 form-select" id="topic" name="country" required>
                                @foreach($countries as $country)
                                    <option value="{{$country->name}}">{{$country->name}}</option>
                                @endforeach
                            </select>

                        </div>
                        <div class="form-group text-left mb-4"><span>Mot de passe</span>
                            <label for="password"><i class="lni lni-lock"></i></label>
                            <input class="input-psswd form-control" id="registerPassword" name="password" type="password" placeholder="********************" required>
                        </div>


                        <div class="form-group text-left mb-4"><span>Confirmer le mot de passe</span>
                            <label for="password"><i class="lni lni-lock"></i></label>
                            <input class="input-psswd form-control" id="registerPassword" name="password_confirmation" type="password" placeholder="********************" required>
                        </div>
                        <div class="form-group text-left mb-4"><span>Code de parrainage(facultatif)</span>
                            <label for="username"><i class="lni lni-user"></i></label>
                            <input class="form-control" id="refferal" name="refferal" type="text" placeholder="Code de parrainage(facultatif)" >
                        </div>
                        <div class="form-group text-left mb-4">

                            <input type="checkbox" id="vehicle1"  name="accepttos" class="accepttos" required>
                            <label for="vehicle1" style="position: unset!important;"> Conditions générales de vente</label>
                        </div>

                            <input type="hidden" name="role" value="2">
                        <button class="btn btn-success btn-lg w-100" type="submit">Inscrire</button>
                    </form>
                </div>
                <!-- Login Meta-->
                <div class="login-meta-data">
                    <p class="mt-3 mb-0">Vous avez déjà un compte?<a class="ml-1" href="{{route('app.login')}}">Connexion</a></p>
                    <p><a class="ml-1"  href="#" onclick="history.back()">De retour à la maison</a></p>
                    <a href="{{route('app.off.terms')}}"  style="color: white;">Conditions générales de vente</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- All JavaScript Files-->
<script src="{{asset('app/assets/js/jquery.min.js')}}"></script>
<script src="{{asset('app/assets/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('app/assets/js/waypoints.min.js')}}"></script>
<script src="{{asset('app/assets/js/jquery.easing.min.js')}}"></script>
<script src="{{asset('app/assets/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('app/assets/js/jquery.counterup.min.js')}}"></script>
<script src="{{asset('app/assets/js/jquery.countdown.min.js')}}"></script>
<script src="{{asset('app/assets/js/default/jquery.passwordstrength.js')}}"></script>
<script src="{{asset('app/assets/js/wow.min.js')}}"></script>
<script src="{{asset('app/assets/js/jarallax.min.js')}}"></script>
<script src="{{asset('app/assets/js/jarallax-video.min.js')}}"></script>
<script src="{{asset('app/assets/js/default/dark-mode-switch.js')}}"></script>
<script src="{{asset('app/assets/js/default/no-internet.js')}}"></script>
<script src="{{asset('app/assets/js/default/active.js')}}"></script>
<script src="{{asset('app/assets/js/pwa.js')}}"></script>
<script>
    $(".loginformsubmit").submit(function(){
        $(this).find(':input[type=submit]').prop('disabled', true);
        $(this).find(':input[type=submit]').html("Chargement..");
    });
</script>
</body>
</html>
