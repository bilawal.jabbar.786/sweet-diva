<!DOCTYPE html>
<html lang="en">
<head>
    <?php
    $gs = \App\GeneralSettings::find(1);
    ?>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, viewport-fit=cover, shrink-to-fit=no">
    <meta name="description" content="Revumo - Multipurpose Ecommerce Mobile HTML Template">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="theme-color" content="#100DD1">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <!-- The above tags *must* come first in the head, any other head content must come *after* these tags-->
    <!-- Title-->
    <title>{{$gs->sitename}}</title>
    <!-- Favicon-->
    <link rel="icon" href="{{asset($gs->logo)}}">
    <!-- Apple Touch Icon-->
    <link rel="apple-touch-icon" href="{{asset($gs->logo)}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset($gs->logo)}}">
    <link rel="apple-touch-icon" sizes="167x167" href="{{asset($gs->logo)}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset($gs->logo)}}">
    <!-- Stylesheet-->
    <link rel="stylesheet" href="{{asset('app/assets/style.css')}}">
    <!-- Web App Manifest-->
    <link rel="manifest" href="{{asset('app/assets/manifest.json')}}">

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">

    <style>
        .sidenav-profile .user-profile img {
            border-radius: 50%;
            height: 100%;
        }
        .featured-product-card .custom-badge::before {
            border-color: #ad628a transparent #b2698f #f4d0da;
            border-style: solid;
            border-width: 11px 8px;
            content: "";
            height: 22px;
            position: absolute;
            right: -17px;
            top: 0;
            width: 17px;
            z-index: 1;
        }
        .user-info-card .user-profile img {
            border-radius: 50%;
            height: 100%;
        }

        element.style {
        }
        .page-item.active .page-link {
            z-index: 3;
            color: #fff;
            background-color: #ffe1e7;
            border-color: #ffe1e7;
        }


        .revumo-sidenav-wrapper {
            -webkit-transition-duration: 500ms;
            transition-duration: 500ms;
            position: fixed;
            width: 100%;
            z-index: 999999;
            top: 0;
            left: -250px;
            height: 100%;
            background: #1f1f1f;
            background: -webkit-gradient(linear, right top, left top, from(#1f1f1f), to(#313131));
            background: linear-gradient(to left, #ffe1e7, #a55580);
            overflow-y: auto;
            width: 240px;
        }
        .bg-warning, .badge-warning {
            background: linear-gradient(to left, #ffe1e7, #a55580); !important;
            color: #020310;
        }
        .single-order-status .order-status {
            color: white !important;
        }
        .single-order-status .order-icon {
            color: white;
            margin-right: 0.5rem;
        }
        .user-info-card {
            position: relative;
            z-index: 1;
            background: linear-gradient(to left, #ffe1e7, #a55580); !important;
            border-color: #ffe1e7 !important;
        }
    </style>



</head>
<body>
<div class="page-content-wrapper">
    <div class="container">
        <!-- Privacy Policy-->
        <div class="privacy-policy-wrapper py-3">
            <p >

            <h1 style="text-align: center; color: #353576">Conditions générales de vente</h1>
            <br>
            <br>
            <h3 style="color: #353576">ARTICLE 1 : Mentions obligatoires</h3>
            <br><br>
            Le site Internet 2142 route de Cocoyer 97190 Besson Le Gosier est un service de : <br>
            Le palais des femmes <br>
            2142 route de Cocoyer 97190 Besson Le Gosier Guadeloupe France <br>
            NAF 4771Z SIRET 88112583500010 <br>
            Numéro individuel d&#39;identification fiscale / n° de TVA <br>
            contact.lepalaisdesfemmes@gmail.com <br>
            0690399802 <br><br>
            <h3 style="color: #353576">ARTICLE 2 : Caractéristiques essentielles des produits et services vendus</h3>
            <br><br>

            Le site Internet www.lepalaisdesfemmes.com vend : <br>
            Vente de vêtements, chaussures et accessoires <br>
            Le client déclare avoir pris connaissance et avoir accepté les conditions générales de vente
            antérieurement à la passation de sa commande. La validation de votre commande vaut donc pour
            acceptation des conditions générales de vente. <br>
            <br>
            <h3 style="color: #353576">ARTICLE 3 : Prix</h3>
            <br><br>
            Les prix de nos produits sont indiqués en euros toutes taxes comprises (TTC). <br>
            En cas de commande livrée dans un pays autre que la France métropolitaine, le client est
            l&#39;importateur des produits qu’il achète. Pour tous les produits expédiés hors des collectivités d’outre-
            mer ou de l’Union européenne, la facture est établie sur le prix hors taxes. Le client est seul
            responsable des déclarations et paiements de tout droit de douane ou autre taxe susceptibles d&#39;être
            exigibles dans son pays. <br>
            Selon le cas : <br>

            Les frais de livraison ne sont pas compris dans le prix. Le bon de commande indique le
            montant des frais de livraison avant toute passation de commande. <br>
            Les frais de livraison sont inclus dans les prix des produits proposés en ligne. <br>
            <br>
            <h3 style="color: #353576">ARTICLE 4 : Délai de disponibilité des produits</h3>
            <br><br>
            Les produits disponibles apparaissent sur notre site accompagnés de la mention « disponible ». <br> Afin
            de répondre au mieux aux attentes de notre clientèle, la disponibilité de nos produits est
            régulièrement mise à jour sur notre site internet. <br>
            Si vous avez commandé un produit indisponible postérieurement à la validation de votre commande,
            vous en serez immédiatement informé. Nous procéderons à l&#39;annulation de votre achat. Si la somme
            avait déjà été débitée, vous serez immédiatement remboursé. <br>
            <br>
            <h3 style="color: #353576">ARTICLE 5 : Commande</h3>
            <br><br>
            Vous avez la possibilité de commander nos produits directement sur notre site internet ou par
            téléphone au 0690399802 (coût en euros € TTC / min) du lundi au samedi, de 8 heure à 18
            heure. <br>
            Pour passer une commande sur notre site, choisissez vos articles et ajoutez-les au panier. Validez le
            contenu de votre panier. <br>
            Selon le cas : <br>
            Si vous possédez déjà un compte client sur notre site, veuillez vous identifier. <br>
            Si vous ne possédez pas de compte client sur notre site, veuillez en créer un. <br>
            Selon le cas : <br>
            Cochez la case « livraison » pour validation de celle-ci. <br>
            Choisissez un mode de livraison en fonction de votre commande. <br>
            Selon le cas : <br>
            Cochez la case « paiement » pour honorer votre commande en ligne. <br>
            Choisissez votre mode de paiement et cochez la case « acceptation des CGV ». <br>
            Validez votre paiement. <br>
            Vous recevrez un e-mail de confirmation de votre commande sur l&#39;adresse mail que vous avez
            indiquée lors de la création de votre compte client. <br>
            Vérifiez les détails et le montant total de votre commande. Rectifiez au préalable les éventuelles
            erreurs avant toute acceptation. <br>
            Le transfert de propriété du produit n&#39;aura lieu qu&#39;au paiement complet de votre commande. <br>
            <br>
            <h3 style="color: #353576">ARTICLE 6 : Livraison Selon le cas :</h3>
            <br><br>


            Nous effectuons nos livraisons uniquement en France métropolitaine. Sont donc exclus la
            Corse et les DOM-TOM. Nous livrons exclusivement à l&#39;adresse indiquée par le client. <br>
            Nous procédons à la livraison de nos produits en France métropolitaine y compris en Corse et
            dans les DOM-TOM. La livraison a lieu à l&#39;adresse indiquée par l&#39;acheteur. <br>
            Selon le cas : <br>
            Nous vous proposons un seul type de livraison à savoir colissimo, d&#39;un montant de 05 a 10
            euros € par commande. <br>
            Les délais de livraison sont donnés à titre indicatif. Ils peuvent évoluer en raison de différents
            facteurs comme la disponibilité de notre transporteur / du traitement de la commande, etc.. <br>
            <br>

            <h3 style="color: #353576">ARTICLE 8 : Modalités de paiement</h3>
            <br><br>
            Selon le cas :<br>
            Plusieurs moyens de paiement sont acceptés. En tant que client, vous avez la possibilité de
            payer par : carte bancaire / PayPal / 2,3,4 fois. <br>
            Un seul mode de paiement est accepté : le consommateur doit payer par carte bancaire <br>
            Selon le cas : <br>
            Nous acceptons les paiements par carte bleue / visa / master card / eurocard. Lors de votre
            paiement en ligne, indiquez le numéro de votre carte bancaire, sa date d&#39;expiration et les 3
            chiffres inscrits au dos de celle-ci. Le paiement en ligne est sécurisé. <br>
            Éventuellement : <br>
            Nous exigeons un paiement intégral de la commande. <br>
            <br>
            <h3 style="color: #353576">ARTICLE 9 : Droit de rétractation Selon le cas :</h3>
            <br><br>
            Conformément à l&#39;article L121-20 du Code de la consommation, vous disposez d&#39;un droit de
            rétractation à exercer dans un délai de sept jours ouvrables à compter de la réception de
            l&#39;article. Vous n&#39;avez ni à justifier de motifs ni à payer de pénalités, mis à part les frais de
            retour ; <br>
            Dès réception du produit ou au jour de l&#39;acceptation de l&#39;offre, vous disposez d&#39;un droit de
            rétractation de 15 jours calendaires, soit 8 jours de plus que ce que prévoit la législation
            française. <br>
            Éventuellement : <br>
            Éventuellement : <br>
            Le droit de rétractation ne s&#39;applique pas pour les produits réalisés selon vos indications
            spécifiques et pour les produits personnalisés. Sont également exclus du droit de rétractation
            les produits qui sont par nature périssables, qui ne peuvent être réexpédiés (comme les
            téléchargements) ou qui peuvent se détériorer. <br>

            Réexpédiez le produit dans le délai mentionné ci-dessus. Dans le même délai, informez-nous de
            l&#39;exercice de votre droit de rétractation par courrier postal à l&#39;adresse suivante : 2142 route de
            Cocoyer 97190 Besson Le Gosier Guadeloupe ou par mail à
            contact.lepalaisdesfemmes@gmail.com. <br>
            La marchandise est à retourner à l&#39;adresse suivante, les frais de retour étant à votre charge : 2142
            route de Cocoyer 97190 Besson Le Gosier Guadeloupe. <br>
            Afin que notre produit retourné soit à nouveau commercialisable, vous vous engagez à nous le
            renvoyer dans son emballage d&#39;origine et en parfait état. L&#39;emballage peut être ouvert et l&#39;article
            utilisé tant qu&#39;il est possible de le commercialiser. <br>
            <br>
            <h3 style="color: #353576"> ARTICLE 10 : Garantie et droit de retour du produit (vice caché ou
                défectuosité)</h3>
            <br><br>
            Vous bénéficiez de la garantie légale de conformité et donc d&#39;une application de l&#39;article L211-4 du
            Code de la consommation qui dispose que : <br>
            « Le vendeur est tenu de livrer un bien conforme au contrat et répond des défauts de
            conformité existant lors de la délivrance. » <br>
            « Il répond également des défauts de conformité résultant de l&#39;emballage, des instructions de
            montage ou de l&#39;installation lorsque celle-ci a été mise à sa charge par le contrat ou a été
            réalisée sous sa responsabilité. » <br>
            La garantie est applicable à nos produits neufs ayant un caractère défectueux (absence de
            fonctionnalité, produit impropre à l&#39;usage auquel vous pouvez vous attendre, absence des
            caractéristiques présentées en ligne, dysfonctionnement partiel ou total du produit, panne du produit)
            et ce, même en l&#39;absence de garantie contractuelle. <br>
            Un défaut de conformité peut apparaître jusqu&#39;à 6 mois après la transaction. Son existence est
            présumée au jour de la délivrance du bien et vous n&#39;avez pas à démontrer l&#39;existence du défaut de
            conformité. <br>
            Dans le cas d&#39;un défaut de conformité, nous nous engageons à : <br>
            Remplacer le produit ; <br>
            Réparer le produit dans un délai de 30 jours et sans frais. <br>
            En cas de défaillance dans l&#39;exécution de notre obligation ou en cas de défectuosité majeure, nous
            nous engageons à vous rembourser la totalité du prix versé ou une partie du prix si vous souhaitez
            conserver le produit. <br>
            La présomption du défaut de conformité ne s&#39;applique plus s&#39;il apparaît après 6 mois suivants la
            vente et avant 2 ans. Vous devez rapporter la preuve de la défectuosité de notre produit. <br>
            En cas de vice caché de votre produit, vous bénéficiez de la garantie légale des vices cachés des
            articles 1641 à 1649 du Code civil. Elle s&#39;applique à tous nos produits neufs. Cette garantie
            s&#39;applique lorsque le vice rend le produit impropre à l&#39;usage ou lorsqu&#39;il réduit à tel point son usage
            que vous ne l&#39;auriez pas acheté ou l&#39;auriez payé à un moindre prix. La garantie contre les vices
            cachés ne s&#39;applique que lorsque le vice est antérieur à la vente. Vous disposez d&#39;un délai de 2 ans à
            compter de la découverte du vice pour agir. <br>

            En présence d&#39;un vice, nous nous engageons à remplacer votre produit ou à vous rembourser dans
            les plus brefs délais. <br>
            <br><br>
            <h3 style="color: #353576"> ARTICLE 11 : Conditions et délais de remboursement</h3>
            <br><br>
            Le remboursement des produits est intégral. Il s&#39;effectue par virement bancaire / PayPal / chèque,
            etc. dans les plus brefs délais et au plus tard dans les 30 jours à compter de la date d&#39;exercice du
            droit de rétractation. <br><br>

            <h3 style="color: #353576"> ARTICLE 12 : Réclamations du consommateur</h3>
            <br><br>
            Toute réclamation du consommateur est à adresser par courrier postal à l&#39;adresse mentionnée ci-
            contre 2142 route de Cocoyer 97190 Besson Le Gosier Guadeloupe ou par voie électronique à
            contact.lepalaisdesfemmes@gmail.com. <br>
            <br>

            <h3 style="color: #353576"> ARTICLE 13 : Propriété intellectuelle</h3>
            <br><br>
            Tous les commentaires, images, illustrations de notre site nous sont exclusivement réservés. Au titre
            de la propriété intellectuelle et du droit d&#39;auteur, toute utilisation est prohibée sauf à usage privé.
            Sans autorisation préalable, toute reproduction de notre site, qu&#39;elle soit partielle ou totale, est
            strictement interdite. <br>
            <br>

            <h3 style="color: #353576"> ARTICLE 14 : Responsabilité</h3>
            <br><br>
            Conformément à l&#39;article 1147 du Code civil, nous engageons notre responsabilité contractuelle de
            plein droit à votre égard en cas d&#39;inexécution ou de mauvaise exécution du contrat conclu. <br>
            Toutefois, notre responsabilité contractuelle ne peut être engagée dans les situations mentionnées ci-
            dessous : <br>
            cas de la force majeure ; <br>
            fait étranger qui ne peut nous être imputable ; <br>
            <br>
            Éventuellement : <br>
            Les photographies / illustrations / images de notre site n&#39;ont aucune valeur contractuelle. <br>
            Elles ne sauraient donc engager notre responsabilité. <br>
            <br>
            <h3 style="color: #353576">ARTICLE 15 : Données à caractère personnel
                Selon le cas :</h3>
            <br><br>
            Certaines informations relatives au client seront transmises au vendeur (à savoir le nom,
            prénom, adresse, code postal et numéro de téléphone) et ce, afin de permettre le traitement et
            la livraison des produits commandés. <br>
            Les offres commerciales du site seront adressées au client par SMS / mail / téléphone / voie
            postale si aucune opposition n&#39;a été émise. Le client peut à tout moment s&#39;opposer sans frais
            <br>
            aux offres commerciales en se connectant sur son espace personnel ou par courrier postal au
            <br>
            Les informations relatives au client pourront être transmises à des partenaires commerciaux
            du site, sauf s&#39;il y fait opposition. <br>
            Le site assure au client une collecte et un traitement d&#39;informations personnelles dans le respect de
            la vie privée conformément à la loi n°78-17 du 6 janvier 1978 relative à l&#39;informatique, aux fichiers et
            aux libertés. En vertu des articles 39 et 40 de la loi en date du 6 janvier 1978, le client dispose d&#39;un
            droit d&#39;accès, de rectification, de suppression et d&#39;opposition de ses données personnelles. Le client
            exerce ce droit via : <br>
            son espace personnel <br>
            un formulaire de contact <br>
            par mail à contact.lepalaisdesfemmes@gmail.com <br>
            par voie postale au 2142 route de Cocoyer 97190 Besson Le Gosier Guadeloupe. <br>
            <br>

            <h3 style="color: #353576">ARTICLE 16 : Juridiction compétente et droit applicable</h3>
            <br><br>
            En cas de litige entre le client consommateur et notre société, le droit applicable est le droit français /
            indiquer le droit applicable. <br>
            Selon le cas : <br>
            Les juridictions françaises ont seules compétences pour trancher le litige. <br>
            Les tribunaux France sont les seuls compétents en cas de litige. <br>
            </p>
        </div>
    </div>
</div>


<!-- All JavaScript Files-->
<script src="{{asset('app/assets/js/jquery.min.js')}}"></script>
<script src="{{asset('app/assets/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('app/assets/js/waypoints.min.js')}}"></script>
<script src="{{asset('app/assets/js/jquery.easing.min.js')}}"></script>
<script src="{{asset('app/assets/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('app/assets/js/jquery.counterup.min.js')}}"></script>
<script src="{{asset('app/assets/js/jquery.countdown.min.js')}}"></script>
<script src="{{asset('app/assets/js/default/jquery.passwordstrength.js')}}"></script>
<script src="{{asset('app/assets/js/wow.min.js')}}"></script>
<script src="{{asset('app/assets/js/jarallax.min.js')}}"></script>
<script src="{{asset('app/assets/js/jarallax-video.min.js')}}"></script>
<script src="{{asset('app/assets/js/default/dark-mode-switch.js')}}"></script>
<script src="{{asset('app/assets/js/default/no-internet.js')}}"></script>
<script src="{{asset('app/assets/js/default/active.js')}}"></script>
<script src="{{asset('app/assets/js/pwa.js')}}"></script>
</body>
</html>
