<div>

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <!-- <h1>DataTables</h1> -->
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Domicile</a></li>
                            <li class="breadcrumb-item active">catégories de produits</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        @if(Session::has('message'))
            <div id="successMessage" class="alert alert-success">
                {{ Session::get('message') }}
            </div>
    @endif
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <!-- /.card -->

                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Catégories de produits</h3>
                                <button style="margin-left: 30px;" class="btn btn-primary btn-sm float-right" data-toggle="modal" data-target="#exampleModal">Ajouter un nouveau</button>
                                {{--                                <button class="btn btn-secondary btn-sm float-right" data-toggle="modal" data-target="#exampleModal1">Séquence de catégorie</button>--}}
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Séquence</th>
                                        <th>Nom</th>

                                        <th>Category</th>
                                        <th>Photo</th>

                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($maincategory as $row)
                                        <tr>
                                            <td>{{$row->id}}</td>
                                            @if($row->no)
                                                <td>{{$row->no}}</td>
                                            @else
                                                <td>Aucune séquence assignée</td>
                                            @endif
                                            <td>{{$row->name}}</td>
                                            <td>{{$row->categorytype->name}}</td>
                                            <td>
                                                <img height="30px" src="{{asset($row->photo)}}" alt="">
                                            </td>

                                            <td>
                                                <button type="button" class="btn btn-primary" data-toggle="modal" wire:click="edit({{$row->id}})" data-target="#updateModal" data-whatever="@mdo">Edit</button>
                                            </td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>

@include('livewire.maincategory.update')
@include('livewire.maincategory.create')


