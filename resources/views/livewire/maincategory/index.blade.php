@extends('layouts.admin')
@livewireStyles
@section('content')

        @livewire('main-category')
        @livewireScripts
        <script>
            window.livewire.on('userStore',()=>{
                $('#exampleModal').modal('hide');
                $('#updateModal').modal('hide');
            })
        </script>
    <script>
        $(document).ready(function(){
            $("#successMessage").delay(5000).slideUp(300);

        });
    </script>
@endsection
