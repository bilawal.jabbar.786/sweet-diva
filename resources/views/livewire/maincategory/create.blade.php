
<div wire:ignore.self class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ajouter une nouvelle catégorie</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <form >

                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="title"><b>Catégorie</b><span class="text-danger">*</span></label>
                            <select wire:model="category_id"  class="form-control" id="">
                                @foreach($category as $cat)
                                    <option value="{{$cat->id}}">{{$cat->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="title"><b>Nom de catégorie</b><span class="text-danger">*</span></label>
                            <input type="text" wire:model="name"  required placeholder="Nom de catégorie" class="form-control">
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="title"><b>Image1 (420 x 420)</b><span class="text-danger">*</span></label>
                            <input type="file"  wire:model="photo"  required class="form-control">
                            @error('photo')
                            <span class="text-danger error" role="alert">
                                        <strong>{{$message}}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="title"><b>Image2 (420 x 420)</b><span class="text-danger">*</span></label>
                            <input type="file" wire:model="photo2" name="photo2" required class="form-control">
                            @error('photo2')
                            <span class="text-danger error" role="alert">
                                        <strong>{{$message}}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="title"><b>Image3 (420 x 420)</b><span class="text-danger">*</span></label>
                            <input type="file" wire:model="photo3" name="photo3" required class="form-control">
                            @error('photo3')
                            <span class="text-danger error" role="alert">
                                        <strong>{{$message}}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="title"><b>Bannière (1920 x 800)</b><span class="text-danger">*</span></label>
                            <input type="file" wire:model="banner"  required class="form-control">
                            @error('banner')
                            <span class="text-danger error" role="alert">
                                        <strong>{{$message}}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>
                </form>


                    <div class="col-md-12 pull-right">
                        <div class="form-group">
                            <button type="button" class="btn btn-primary btn-block" wire:click.prevent="storeCategory()">Sauvegarder</button>
                        </div>
                    </div>

            </div>
        </div>



    </div>

</div></div>
<!-- Modal -->

<div wire:ignore.self class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ajouter Séquence Catégorie</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <form action="{{route('maincategory.sequence')}}" method="post" enctype="multipart/form-data" data-parsley-validate>
                    @csrf
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="title"><b>Catégorie</b><span class="text-danger">*</span></label>
                            <select name="category_id" class="form-control select2" id="">
                                @foreach($maincategory as $cat)
                                    <option value="{{$cat->id}}">{{$cat->name}}-{{$cat->no}} </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="title"><b>Séquence Catégorie</b><span class="text-danger">*</span></label>
                            <input type="number"  name="no" required placeholder="Séquence" class="form-control">
                        </div>
                    </div>




                    <div class="col-md-12 pull-right">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-block">Sauvegarder</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>



    </div>

</div></div>

<!-- Modal -->
