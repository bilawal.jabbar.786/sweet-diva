<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{route('home')}}" class="brand-link">
        <span class="brand-text font-weight-light pl-4"><strong>Le palais des femmes</strong></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{asset('front/assets/logo.png')}}" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">{{Auth::user()->name}}</a>
            </div>
        </div>
    @php

        if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on')
        {
          $link = "https";
        }
        else
        {
          $link = "http";

          // Here append the common URL characters.
          $link .= "://";

          // Append the host(domain name, ip) to the URL.
          $link .= $_SERVER['HTTP_HOST'];

          // Append the requested resource location to the URL
          $link .= $_SERVER['REQUEST_URI'];
        }

    @endphp
    <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                <li class="nav-item menu-open">
                    <a href="{{route('home')}}" class="nav-link {{ $link == route('home') ? 'active':'' }}">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Tableau de bord
                        </p>
                    </a>
                </li>
                @if(Auth::user()->role == '0')
                    <li style="background-color: #dc3545"
                        class="nav-item {{  request()->is('main/*') ? 'menu-open':'' }}">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-ellipsis-h"></i>
                            <p style="color: white">
                                Catégories
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{route('maincategory.index')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p style="color: white">Catégories principales</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('subcategory.index')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p style="color: white">Sous catégories</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li style="background-color: #28a745"
                        class="nav-item {{  request()->is('general/*') ? 'menu-open':'' }}">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-ellipsis-h"></i>
                            <p style="color: white">
                                Réglages Généraux
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{route('general.settings')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p style="color: white">Réglages Généraux</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('general.slider')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p style="color: white">Bannières</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('saleproduct')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p style="color: white">Produit de vente</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('admin.about')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p style="color: white">À propos de nous</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('sms.compain')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p style="color: white">Campagne SMS</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('admin.home')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p style="color: white">Texte d'accueil</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li style="background-color: #ffc107"
                        class="nav-item {{  request()->is('products/*') ? 'menu-open':'' }}">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-ellipsis-h"></i>
                            <p style="color: black">
                                Produits
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{route('products.create')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p style="color: black">Ajouter produits</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('products.index')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p style="color: black">Tous les produits</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('products.size')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p style="color: black">Tailles des produits</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('product.outofstock')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p style="color: black">Rupture de stock</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li style="background-color: purple"
                        class="nav-item {{  request()->is('orders/*') ? 'menu-open':'' }}">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-ellipsis-h"></i>
                            <p style="color: white">
                                Commande
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{route('order.index')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p style="color: white">Nouvelles commandes </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('order.pad')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p style="color: white">Pad commandes </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('order.complete')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p style="color: white">Commandes traiter </p>
                                </a>
                            </li>
                            <li class="nav-item" style="background-color: blue">
                                <a href="{{route('order.videdressing')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p style="color: white">Commandes vide dressing </p>
                                </a>
                            </li>
                            <li class="nav-item" style="background-color: blue">
                                <a href="{{route('order.complete-vide')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p style="color: white">Vide dressing commandes traiter </p>
                                </a>
                            </li>
                        </ul>
                    </li>
                @endif
                @if(Auth::user()->role == '0' || Auth::user()->role == '3')
                    <li class="nav-item {{  request()->is('sweet/*') ? 'menu-open':'' }}">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-ellipsis-h"></i>
                            <p style="color: white">
                                Sweet Look
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{route('sweet.index')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p style="color: white">Tous les messages</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('sweet.toppost')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p style="color: white">Top Like Post</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('sweet.topics')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p style="color: white">Les sujets</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                @endif
                @if(Auth::user()->role == '0' || Auth::user()->role == '4')
                <li style="background-color: pink" class="nav-item {{  request()->is('cards/*') ? 'menu-open':'' }}">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-ellipsis-h"></i>
                        <p style="color: black">
                            Cartes cadeaux
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('cards.index')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p style="color: black">Cartes cadeaux</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="{{route('cards.sale')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p style="color: black">Vente de cartes-cadeaux</p>

                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('promo.code.index')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p style="color: black">Code promo</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="#" style="background-color: grey"  class="nav-link  {{  request()->is('clients/*') ? 'menu-open':'' }}">
                        <i class="nav-icon fas fa-ellipsis-h"></i>
                        <p>
                            Clients
                            <i class="fas fa-angle-left right"></i>

                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <!--                    <li class="nav-item">
                        <a href="{{route('admin.buyers')}}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p style="color: white">Acheteuses</p>
                        </a>
                    </li>-->
                        <li class="nav-item">
                            <a href="{{route('admin.sellers')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p style="color: white">Clients</p>
                            </a>
                        </li>
                        @if(Auth::user()->role == '0')
                            <li class="nav-item">
                                <a href="{{route('admin.newsletter')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p style="color: white">Bulletin</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('admin.messages')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p style="color: white">messages</p>
                                </a>
                            </li>
                        @endif
                    </ul>
                </li>
                <li style="background-color: blue"
                    class="nav-item {{  request()->is('notification/*') ? 'menu-open':'' }}">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-ellipsis-h"></i>
                        <p style="color: white">
                            Notifications
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">

                        <li class="nav-item">
                            <a href="{{route('admin.group.index')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p style="color: white">Groupes</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('admin.users')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p style="color: white">Avis aux utilisateurs</p>
                            </a>
                        </li>

                    </ul>
                </li>
                <li class="nav-item {{  request()->is('reports/*') ? 'menu-open':'' }}">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-ellipsis-h"></i>
                        <p style="color: white">
                            Caisses
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('reports.index')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p style="color: white">Caisses</p>
                            </a>
                        </li>
                        @if(Auth::user()->role == 0)
                            <li class="nav-item">
                                <a href="{{route('reports.calender')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p style="color: white">Calendrier</p>
                                </a>
                            </li>
                        @endif
                    </ul>
                </li>
                    <li style="background-color: brown"
                        class="nav-item {{  request()->is('points/*') ? 'menu-open':'' }}">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-ellipsis-h"></i>
                            <p style="color: white">
                                Points-cadeaux
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            @if(Auth::user()->role == '0')
                            <li class="nav-item">
                                <a href="{{route('points.index')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p style="color: white">Points-cadeaux</p>
                                </a>
                            </li>
                            @endif
                            <?php $notificationsCount= \App\AdminNotfication::where('status','=',0)->count();
                            ?>
                            <li class="nav-item">
                                <a href="{{route('admin.giftrequests')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p style="color: white">Demandes de cadeaux</p>
                                    <span class="badge badge-info right">{{$notificationsCount}}</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                @endif
                @if(Auth::user()->role == '0')
                    <li class="nav-item">
                        <a href="{{route('change.password')}}" class="nav-link">
                            <i class="nav-icon fas fa-key"></i>
                            <p>
                                Changer le mot de passe
                            </p>
                        </a>
                    </li>
                @endif

                <li class="nav-item">
                    <a href="{{route('logout')}}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"
                       class="nav-link">
                        <i class="nav-icon fas fa-sign-out-alt"></i>
                        <p>
                            Log Out
                        </p>
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
