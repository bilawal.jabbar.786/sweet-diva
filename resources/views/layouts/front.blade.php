<!doctype html>
<html class="no-js" lang="en">
<?php
$gs = \App\GeneralSettings::find(1);
$categories = \App\Category::all();

?>
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>{{$gs->sitename}}</title>
    <meta name="robots" content="noindex, follow" />
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{asset($gs->logo)}}">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <!-- CSS
	============================================ -->
    <meta name="facebook-domain-verification" content="wvw2okljn1lp2c9khfcdq2rp6keq1m" />
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('front/assets/css/vendor/bootstrap.min.css')}}">
    <!-- Fontawesome -->
    <link rel="stylesheet" href="{{asset('front/assets/css/vendor/font-awesome.min.css')}}">
    <!-- Fontawesome Star -->
    <link rel="stylesheet" href="{{asset('front/assets/css/vendor/fontawesome-stars.min.css')}}">
    <!-- Ion Icon -->
    <link rel="stylesheet" href="{{asset('front/assets/css/vendor/ion-fonts.css')}}">
    <!-- Slick CSS -->
    <link rel="stylesheet" href="{{asset('front/assets/css/plugins/slick.css')}}">
    <!-- Animation -->
    <link rel="stylesheet" href="{{asset('front/assets/css/plugins/animate.css')}}">
    <!-- jQuery Ui -->
    <link rel="stylesheet" href="{{asset('front/assets/css/plugins/jquery-ui.min.css')}}">
    <!-- Nice Select -->
    <link rel="stylesheet" href="{{asset('front/assets/css/plugins/nice-select.css')}}">
    <!-- Timecircles -->
    <link rel="stylesheet" href="{{asset('front/assets/css/plugins/timecircles.css')}}">

    <!-- Vendor & Plugins CSS (Please remove the comment from below vendor.min.css & plugins.min.css for better website load performance and remove css files from the above) -->
    <!--
    <script src="assets/js/vendor/vendor.min.js"></script>
    <script src="assets/js/plugins/plugins.min.js"></script>
    -->

    <!-- Main Style CSS (Please use minify version for better website load performance) -->
    <link rel="stylesheet" href="{{asset('front/assets/css/style.css')}}">
    <!--<link rel="stylesheet" href="assets/css/style.min.css">-->
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">
   @include('include.css')
    @laravelPWA
</head>
<div id='whatsapp-chat' class='hide'>
    <div class='header-chat'>
        <div class='head-home'>
            <div class='info-avatar'><img src='{{asset('front/assets/logo.png')}}' /></div>
            <p><span class="whatsapp-name">LPDF</span><br><small>Répond généralement dans l'heure</small></p>

        </div>
        <div class='get-new hide'>
            <div id='get-label'></div>
            <div id='get-nama'></div>
        </div>
    </div>
    <div class='home-chat'>

    </div>
    <div class='start-chat'>
        <div pattern="https://elfsight.com/assets/chats/patterns/whatsapp.png" class="WhatsappChat__Component-sc-1wqac52-0 whatsapp-chat-body">
            <div class="WhatsappChat__MessageContainer-sc-1wqac52-1 dAbFpq">
                <div style="opacity: 0;" class="WhatsappDots__Component-pks5bf-0 eJJEeC">
                    <div class="WhatsappDots__ComponentInner-pks5bf-1 hFENyl">
                        <div class="WhatsappDots__Dot-pks5bf-2 WhatsappDots__DotOne-pks5bf-3 ixsrax"></div>
                        <div class="WhatsappDots__Dot-pks5bf-2 WhatsappDots__DotTwo-pks5bf-4 dRvxoz"></div>
                        <div class="WhatsappDots__Dot-pks5bf-2 WhatsappDots__DotThree-pks5bf-5 kXBtNt"></div>
                    </div>
                </div>
                <div style="opacity: 1;" class="WhatsappChat__Message-sc-1wqac52-4 kAZgZq">
                    <div class="WhatsappChat__Author-sc-1wqac52-3 bMIBDo">LPDF</div>
                    <div class="WhatsappChat__Text-sc-1wqac52-2 iSpIQi">Salut 👋<br><br>Comment puis-je t'aider?</div>
{{--                    <div class="WhatsappChat__Time-sc-1wqac52-5 cqCDVm">1:40</div>--}}
                </div>
            </div>
        </div>

        <div class='blanter-msg'>
            <textarea id='chat-input' placeholder='Écrire un message' maxlength='120' row='1'></textarea>
            <a href='javascript:void;' id='send-it'><svg viewBox="0 0 448 448"><path d="M.213 32L0 181.333 320 224 0 266.667.213 416 448 224z"/></svg></a>

        </div>
    </div>
    <div id='get-number'></div><a class='close-chat' href='javascript:void'>×</a>
</div>
<a class='blantershow-chat' href='javascript:void' title='Show Chat'><svg width="20" viewBox="0 0 24 24"><defs/><path fill="#eceff1" d="M20.5 3.4A12.1 12.1 0 0012 0 12 12 0 001.7 17.8L0 24l6.3-1.7c2.8 1.5 5 1.4 5.8 1.5a12 12 0 008.4-20.3z"/><path fill="#4caf50" d="M12 21.8c-3.1 0-5.2-1.6-5.4-1.6l-3.7 1 1-3.7-.3-.4A9.9 9.9 0 012.1 12a10 10 0 0117-7 9.9 9.9 0 01-7 16.9z"/><path fill="#fafafa" d="M17.5 14.3c-.3 0-1.8-.8-2-.9-.7-.2-.5 0-1.7 1.3-.1.2-.3.2-.6.1s-1.3-.5-2.4-1.5a9 9 0 01-1.7-2c-.3-.6.4-.6 1-1.7l-.1-.5-1-2.2c-.2-.6-.4-.5-.6-.5-.6 0-1 0-1.4.3-1.6 1.8-1.2 3.6.2 5.6 2.7 3.5 4.2 4.2 6.8 5 .7.3 1.4.3 1.9.2.6 0 1.7-.7 2-1.4.3-.7.3-1.3.2-1.4-.1-.2-.3-.3-.6-.4z"/></svg>Discuter</a>
<body class="template-color-2">

<div class="main-wrapper">

    <!-- Begin Loading Area -->
<!--    <div class="loading">
        <div class="text-center middle">
                <span class="loader">
            <span class="loader-inner"></span>
                </span>
        </div>
    </div>-->
    <!-- Loading Area End Here -->

    <!-- Begin Main Header Area Two -->
    <header class="main-header_area-2">
        <div class="header-top_area d-none d-lg-block">
            <div class="container">
                <div class="header-top_nav">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="ht-menu">

                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="header-top_right">
                                <ul>
                                    <!--<li>
                                        <a href="{{route('front.checkout')}}">Commande</a>
                                    </li>-->
                                    @guest
                                    <li>
                                        <a href="{{route('register')}}" style="font-size:12px">Inscription</a>
                                    </li>
                                    <li>
                                        <a href="{{route('login')}}" style="font-size:12px">Connexion</a>
                                    </li>
                                    @endguest
                                    @auth

                                        <li>
                                          <a href="{{route('user.wishlist')}}"><i class="ion-heart"></i></a>
                                        </li>
                                        <li>
                                            <a href="{{route('user.dashboard')}}"><i class="ion-person"></i></a>
                                        </li>
                                            <!--<li>
                                                <a href="{{route('user.dashboard')}}">
                                                <img src="{{empty(Auth::user()->photo)? asset('front/assets/images/avatar.png') : asset(Auth::user()->photo)}}" style="height: 30px; border-radius: 50%" alt="">
                                                </a>
                                            </li>-->
                                    @endauth

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-middle_area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="header-middle_nav">
                            <div class="header-logo_area">
                                <a href="{{route('front.index')}}">
                                    <img style="height: 65px" src="{{asset($gs->logo)}}" alt="Header Logo">
                                </a>
                            </div>
                            <div class="header-contact d-none d-md-flex">
                                <i class="fa fa-headphones-alt"></i>
                                <div class="contact-content">
                                    <p>
                                      <!-- Appelez-nous
                                        <br>-->
                                        Assistance gratuite: <a href="tel:{{$gs->phone}}">{{$gs->phone}}</a>
                                    </p>
                                </div>
                            </div>
                            <div class="header-search_area d-none d-lg-block">
                                <form class="search-form" action="{{route('front.search')}}" method="POST">
                                    @csrf
                                    <input type="text" placeholder="Recherche" name="name">
                                    <button class="search-button"><i class="ion-ios-search"></i></button>
                                </form>
                            </div>
                            <div class="header-right_area d-none d-lg-inline-block">
                                <ul>
                                    <li class="mobile-menu_wrap d-flex d-lg-none">
                                        <a href="#mobileMenu" class="mobile-menu_btn toolbar-btn color--white">
                                            <i class="ion-android-menu"></i>
                                        </a>
                                    </li>
                                    <li class="minicart-wrap">
                                        <a href="#miniCart" class="minicart-btn toolbar-btn">
                                            <div class="minicart-count_area">
                                                <span class="item-count quantitycart"></span>
                                                <i class="ion-bag"></i>
                                            </div>
                                            <div class="minicart-front_text">
                                                <span>Panier:</span>
                                                <span class="total-price total">0</span>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="minicart-wrap" style="display: none">
                                        <a href="#popup" class="minicart-btn1 toolbar-btn">
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="header-right_area header-right_area-2 d-inline-block d-lg-none">
                                <ul>
                                    <li class="mobile-menu_wrap d-inline-block d-lg-none">
                                        <a href="#mobileMenu" class="mobile-menu_btn toolbar-btn color--white">
                                            <i class="ion-android-menu"></i>
                                        </a>
                                    </li>
                                    <li class="minicart-wrap">
                                        <a href="#miniCart" class="minicart-btn toolbar-btn">
                                            <div class="minicart-count_area">
                                                <span class="item-count quantitycart"></span>
                                                <i class="ion-bag"></i>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#searchBar" class="search-btn toolbar-btn">
                                            <i class="pe-7s-search"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#offcanvasMenu" class="menu-btn toolbar-btn color--white d-none d-lg-block">
                                            <i class="ion-android-menu"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<!--        <div>
            <img style="width: 100%" src="{{asset('front/assets/01.jpeg')}}" alt="">
        </div>-->
        <div class="container-fluid">
            <div class="row" style="background-color: black; text-align: center; color: white">
                <div class="col-md-2">
                    <p style="color:white !important;font-size: 16px;" class="mdsdh marge">Assistance gratuite : <a href="tel:{{$gs->phone}}">{{$gs->phone}}</a></p>
                    <div class="main-menu_area d-none d-lg-block d-md-block">
                        <nav class="main-nav d-flex justify-content-center">
                            <ul>
                                    <li>
                                        <a style="color: white !important;" href="{{route('front.index')}}">ACCUEIL <i
                                                class="ion-chevron-down"></i></a>
                                        <ul class="kenne-dropdown">
                                                <li>
                                                    <a href="{{route('front.category', ['id' => '1'])}}">SWEET DIVA</a>
                                                </li>
                                            @if($gs->working == 1)
                                                <li>
                                                    <a href="{{route('front.category', ['id' => '2'])}}">WORKING GIRL</a>
                                                </li>
                                            @endif
                                            @if($gs->fashioncod == 1)
                                                <li>
                                                    <a href="{{route('front.category', ['id' => '3'])}}">FASHION COD</a>
                                                </li>
                                            @endif
                                        </ul>
                                    </li>

                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="col-md-8">
                    <img style="width: 100%" class="marge" src="{{asset('front/assets/01.jpeg')}}" alt="">
                </div>
                <div class="col-md-2">
                    <div class="main-main-menu_area d-none d-lg-block d-md-block">
                        <nav class="main-nav d-flex justify-content-center">
                            <ul>
                                <li>
                                    <a style="color: white !important;" href="{{route('front.about')}}">A PROPOS DE NOUS</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
<!--        <div class="header-bottom_area d-none d-lg-block">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="main-menu_area position-relative">
                            <nav class="main-nav d-flex justify-content-center">
                                <ul>
                                    <li class="dropdown-holder"><a href="{{route('front.index')}}">Domicile </a>
                                    </li>
                                    @foreach($categories as $cat)
                                    <li><a href="{{route('front.category', ['id' => $cat->id])}}">{{$cat->name}} <i
                                                class="ion-chevron-down"></i></a>
                                        <ul class="kenne-dropdown">
                                            @foreach($cat->subcategories as $subcat)
                                            <li><a href="{{route('front.maincategory', ['id' => $subcat->id])}}">{{$subcat->name}}</a></li>
                                            @endforeach
                                        </ul>
                                    </li>
                                    @endforeach
                                    <li><a href="">Nous contacter</a></li>
                                    <li><a href="">À propos de nous</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>-->
<!--
        <div class="header-sticky">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="sticky-header_nav position-relative">
                            <div class="row align-items-center justify-content-between">
                                <div class="col-lg-2 col-sm-6">
                                    <div class="header-logo_area">
                                        <a href="{{route('front.index')}}">
                                            <img style="height: 65px" src="{{asset($gs->logo)}}" alt="Header Logo">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-lg-8 d-none d-lg-block position-static">
                                    <div class="main-menu_area">
                                        <nav class="main-nav d-flex justify-content-center">
                                            <ul>
                                                <li class="dropdown-holder"><a href="{{route('front.index')}}">Domicile</a>

                                                </li>
                                                @foreach($categories as $cat)
                                                    <li><a href="{{route('front.category', ['id' => $cat->id])}}">{{$cat->name}} <i
                                                                class="ion-chevron-down"></i></a>
                                                        <ul class="kenne-dropdown">
                                                            @foreach($cat->subcategories as $subcat)
                                                                <li><a href="{{route('front.maincategory', ['id' => $subcat->id])}}">{{$subcat->name}}</a>
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                    </li>
                                                @endforeach

                                            </ul>
                                        </nav>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-sm-6">
                                    <div class="header-right_area header-right_area-2">
                                        <ul>
                                            <li class="mobile-menu_wrap d-inline-block d-lg-none">
                                                <a href="#mobileMenu" class="mobile-menu_btn toolbar-btn color&#45;&#45;white">
                                                    <i class="ion-android-menu"></i>
                                                </a>
                                            </li>
                                            <li class="minicart-wrap">
                                                <a href="#miniCart" class="minicart-btn toolbar-btn">
                                                    <div class="minicart-count_area">
                                                        <span class="item-count quantitycart">0</span>
                                                        <i class="ion-bag"></i>
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#searchBar" class="search-btn toolbar-btn">
                                                    <i class="ion-android-search"></i>
                                                </a>
                                            </li>
                                            <li class="d-none d-lg-inline-block">
                                                <a href="#offcanvasMenu" class="menu-btn toolbar-btn color&#45;&#45;white">
                                                    <i class="ion-android-menu"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
-->

        <div class="offcanvas-minicart_wrapper" id="miniCart">
            <div class="offcanvas-menu-inner">
                <a href="#" class="btn-close"><i class="ion-android-close"></i></a>
                <div class="minicart-content">
                    <div class="minicart-heading">
                        <h4>Panier</h4>
                    </div>

                    <div style="display: none" id="loader"></div>

                    <ul class="minicart-list">

                    </ul>
                </div>
                <div class="minicart-item_total">
                    <span>Total</span>
                    <span class="ammount total">0 €</span>
                </div>
                <div class="minicart-btn_area">
                    <a href="{{route('front.cart')}}" class="kenne-btn kenne-btn_fullwidth">Voir le panier</a>
                </div>
            </div>
        </div>

        <div class="offcanvas-minicart_wrapper" id="popup">
            <div class="offcanvas-menu-inner" style="padding-top: 0px !important;">
                <a href="#" class="btn-close"><i class="ion-android-close"></i></a>
                <div class="offcanvas-inner_logo">
                    <a href="{{route('front.index')}}">
                        <img  src="{{asset('front/assets/09.jpeg')}}" alt="Header Logo">
                    </a>
                </div>
                <div class="row">
                    <p style="text-align: center">Vous devez avoir l'âge légal pour commander sur l'application. pour les mineurs veuillez obtenir le consentement d'un parent ou tuteur</p>
                </div>
                <div class="minicart-btn_area">
                    <a href="{{route('login')}}" class="kenne-btn kenne-btn_fullwidth">Se Connecter</a>
                </div>
                <div class="minicart-btn_area">
                    <a href="{{route('register')}}" class="kenne-btn kenne-btn_fullwidth">Creer Un Compte</a>
                </div>
            </div>
        </div>

        <div class="mobile-menu_wrapper" id="mobileMenu">
            <div class="offcanvas-menu-inner">
                <div class="container">
                    <a href="#" class="btn-close white-close_btn"><i class="ion-android-close"></i></a>
                    <div class="offcanvas-inner_logo">
                        <a href="{{route('front.index')}}">
                            <img style="height: 65px" src="{{asset($gs->logo)}}" alt="Header Logo">
                        </a>
                    </div>
                    <nav class="offcanvas-navigation">
                        <ul class="mobile-menu">
                            <li class="menu-item-has-children active"><a href="{{route('front.index')}}"><span
                                        class="mm-text">Accueil</span></a>
                            </li>
                            @foreach($categories as $cat)
                            <li class="menu-item-has-children">
                                <a href="{{route('front.category', ['id' => $cat->id])}}">
                                    <span class="mm-text">{{$cat->name}}</span>
                                </a>
                                <ul class="sub-menu">
                                    @foreach($cat->subcategories as $subcat)
                                    <li class="menu-item-has-children">
                                        <a href="{{route('front.maincategory', ['id' => $subcat->id])}}">
                                            <span class="mm-text">{{$subcat->name}}</span>
                                        </a>
                                    </li>
                                    @endforeach
                                </ul>
                            </li>
                            @endforeach
                            <li class="menu-item-has-children">
                                <a href=""><span
                                        class="mm-text">Nous contacter</span>
                                </a>
                            </li>
                            <li class="menu-item-has-children">
                                <a href=""><span
                                        class="mm-text">À propos de nous</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                    <nav class="offcanvas-navigation user-setting_area">
                        <ul class="mobile-menu">
                            <li class="menu-item-has-children active">
                                <a href="#">
                                        <span class="mm-text">
                                            Paramètres utilisateur
                                    </span>
                                </a>
                                <ul class="sub-menu">
                                    @auth
                                    <li>
                                        <a href="{{route('user.dashboard')}}">
                                            <span class="mm-text">Mon compte</span>
                                        </a>
                                    </li>
                                    @endauth
                                    @guest
                                    <li>
                                        <a href="{{route('login')}}">
                                            <span class="mm-text">Connexion</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{route('register')}}">
                                            <span class="mm-text">S'inscrire</span>
                                        </a>
                                    </li>
                                    @endguest
                                    @auth
                                        <li>
                                            <a href="">
                                                <span class="mm-text">{{Auth::user()->name}}</span>
                                            </a>
                                        </li>
                                    @endauth
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        <div class="offcanvas-menu_wrapper" id="offcanvasMenu">
            <div class="offcanvas-menu-inner">
                <a href="#" class="btn-close"><i class="ion-android-close"></i></a>
                <div class="offcanvas-inner_logo">
                    <a href="{{route('front.index')}}">
                        <img style="height: 65px" src="{{asset($gs->logo)}}" alt="Munoz's Offcanvas Logo">
                    </a>
                </div>
                <div class="short-desc">
                    <p>
                        {{$gs->footer}}
                    </p>
                </div>
                <div class="offcanvas-component">
                    <span class="offcanvas-component_title">Mon compte</span>
                    <ul class="offcanvas-component_menu">
                        <li><a href="{{route('register')}}">S'inscrire</a></li>
                        <li><a href="{{route('login')}}">Connexion</a></li>
                    </ul>
                </div>
                <div class="offcanvas-inner-social_link">
                    <div class="kenne-social_link">
                        <ul>
                            <li class="facebook">
                                <a href="{{$gs->facebook}}" data-toggle="tooltip" target="_blank" title="Facebook">
                                    <i class="fab fa-facebook"></i>
                                </a>
                            </li>
                            <li class="twitter">
                                <a href="{{$gs->twitter}}" data-toggle="tooltip" target="_blank" title="Twitter">
                                    <i class="fab fa-twitter-square"></i>
                                </a>
                            </li>
                            <li class="youtube">
                                <a href="{{$gs->youtube}}" data-toggle="tooltip" target="_blank" title="Youtube">
                                    <i class="fab fa-youtube"></i>
                                </a>
                            </li>
                            <li class="instagram">
                                <a href="{{$gs->instagram}}" data-toggle="tooltip" target="_blank" title="Instagram">
                                    <i class="fab fa-instagram"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="offcanvas-search_wrapper" id="searchBar">
            <div class="offcanvas-menu-inner">
                <div class="container">
                    <a href="#" class="btn-close"><i class="ion-android-close"></i></a>
                    <!-- Begin Offcanvas Search Area -->
                    <div class="offcanvas-search">
                        <form action="#" class="hm-searchbox">
                            <input type="text" placeholder="Search for item...">
                            <button class="search_btn" type="submit"><i
                                    class="ion-ios-search-strong"></i></button>
                        </form>
                    </div>
                    <!-- Offcanvas Search Area End Here -->
                </div>
            </div>
        </div>
        <div class="global-overlay"></div>
    </header>
    <!-- Main Header Area End Here Two -->

    <div id="snackbar">Article ajouté avec succès</div>
    <div id="snackbar1">Élément supprimé avec succès</div>

@yield('content')

    <a style="display: none" class="mybutton" id="imagepopupclick" data-toggle="modal" data-target="#imagepopup"  href="#">Me contacter lorsque ma taille est disponible à nouveau</a>


<!-- Begin Kenne's Footer Area -->
    <div class="kenne-footer_area bg-smoke_color">
        <div class="footer-top_area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-5">
                        <div class="newsletter-area">
                            <div class="newsletter-logo">
                                <a href="javascript:void(0)">
                                    <img loading="lazy" style="height: 65px" src="{{asset($gs->logo)}}" alt="Logo">
                                </a>
                            </div>
                            @if(Session::has('success'))
                                <div class="alert1">
                                    <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
                                    <strong>{{ Session::get('success') }}!</strong>
                                </div>
                            @endif
                            <p class="short-desc">Abonnez-vous à notre newsletter, entrez votre adresse e-mail</p>
                            <div class="newsletter-form_wrap">
                                <form action="{{route('newsletter.submit')}}" method="post">
                                    @csrf
                                    <div id="mc_embed_signup_scroll">
                                        <div id="mc-form" class="mc-form subscribe-form">
                                            <input name="name" class="newsletter-input" type="text" required autocomplete="off" placeholder="Entrer Nom" />
                                            <input name="email" class="newsletter-input" type="email" required autocomplete="off" placeholder="Entrer l'adresse e-mail" />
                                            <button class="mybutton" style="width: 50%">Valider</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 offset-lg-1">
                        <div class="row footer-widgets_wrap">
                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <div class="footer-widgets_title">
                                    <h4>Achats</h4>
                                </div>
                                <div class="footer-widgets">
                                    <ul>
                                        <li><a href="/#allproducts">Nos produits</a></li>
                                        @auth
                                          <li><a href="{{route('front.cart')}}">Mon panier</a></li>
                                        @endauth
                                        @if($gs->diva == 1)
                                        <li><a href="{{route('sellers.products')}}">Vide Dressing</a></li>
                                        @endif
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <div class="footer-widgets_title">
                                    <h4>Compte</h4>
                                </div>
                                <div class="footer-widgets">
                                    <ul>
                                        <li><a href="{{route('login')}}">Connexion</a></li>
                                        @guest
                                          <li><a href="{{route('register')}}">Inscription</a></li>
                                        @endguest
<!--                                        <li><a href="javascript:void(0)">Aider</a></li>-->
                                        <li><a href="{{route('user.contact')}}">Support</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <div class="footer-widgets_title">
                                    <h4>Catégories</h4>
                                </div>
                                <div class="footer-widgets">
                                    <ul>
                                        @foreach($categories as $cat3)
                                        <li><a href="javascript:void(0)">{{$cat3->name}}</a></li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom_area">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-6">
                        <div class="copyright">
                            <span>Copyright &copy; {{date('Y')}} <a href="https://ikaedigital.com/">Ikae Digital.</a> Tous droits réservés.</span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="payment">
                            <span> <a href="{{route('front.legal')}}">Mentions legales</a> | <a href="{{route('front.terms')}}">Conditions générales de vente</a></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Scroll To Top Start -->
    <a class="scroll-to-top" href=""><i class="ion-chevron-up"></i></a>
    <!-- Scroll To Top End -->

</div>
<div class="modal fade" id="imagepopup" role="dialog">
    <div class="modal-dialog" style="max-width: 800px">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body" style="background-color: #fad1d5;">
                <img src="{{asset('front/img1.jpg')}}" style="width: 100%" alt="">
                <div class="row" >
                    <div class="col-4">
                        <a href="https://play.google.com/store/apps/details?id=com.sweet.diva.app"><img  src="{{asset('front/img3.png')}}"  style="width: 100%" alt=""></a>
                    </div>
                    <div class="col-4">
                        <a href="https://apps.apple.com/pk/app/sweet-diva/id1608772278"><img src="{{asset('front/img4.png')}}"  style="width: 100%" alt=""></a>
                    </div>
                    <div class="col-4">

                    </div>
                </div>
            </div>
            <div class="modal-footer"  style="background-color: #fad1d5;">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
            </div>
        </div>

    </div>
</div>
<!-- JS
============================================ -->

<!-- jQuery JS -->
<script src="{{asset('front/assets/js/vendor/jquery-1.12.4.min.js')}}"></script>
<!-- Modernizer JS -->
<script src="{{asset('front/assets/js/vendor/modernizr-2.8.3.min.js')}}"></script>
<!-- Popper JS -->
<script src="{{asset('front/assets/js/vendor/popper.min.js')}}"></script>
<!-- Bootstrap JS -->
<script src="{{asset('front/assets/js/vendor/bootstrap.min.js')}}"></script>

<!-- Slick Slider JS -->
<script src="{{asset('front/assets/js/plugins/slick.min.js')}}"></script>
<!-- Barrating JS -->
<script src="{{asset('front/assets/js/plugins/jquery.barrating.min.js')}}"></script>
<!-- Counterup JS -->
<script src="{{asset('front/assets/js/plugins/jquery.counterup.js')}}"></script>
<!-- Nice Select JS -->
<script src="{{asset('front/assets/js/plugins/jquery.nice-select.js')}}"></script>
<!-- Sticky Sidebar JS -->
<script src="{{asset('front/assets/js/plugins/jquery.sticky-sidebar.js')}}"></script>
<!-- Jquery-ui JS -->
<script src="{{asset('front/assets/js/plugins/jquery-ui.min.js')}}"></script>
<script src="{{asset('front/assets/js/plugins/jquery.ui.touch-punch.min.js')}}"></script>
<!-- Theia Sticky Sidebar JS -->
<script src="{{asset('front/assets/js/plugins/theia-sticky-sidebar.min.js')}}"></script>
<!-- Waypoints JS -->
<script src="{{asset('front/assets/js/plugins/waypoints.min.js')}}"></script>
<!-- jQuery Zoom JS -->
<script src="{{asset('front/assets/js/plugins/jquery.zoom.min.js')}}"></script>
<!-- Timecircles JS -->
<script src="{{asset('front/assets/js/plugins/timecircles.js')}}"></script>

<!-- Vendor & Plugins JS (Please remove the comment from below vendor.min.js & plugins.min.js for better website load performance and remove js files from avobe) -->
<!--
<script src="assets/js/vendor/vendor.min.js"></script>
<script src="assets/js/plugins/plugins.min.js"></script> -->
<script type="module">
    // Import the functions you need from the SDKs you need
    import { initializeApp } from "https://www.gstatic.com/firebasejs/10.1.0/firebase-app.js";
    import { getAnalytics } from "https://www.gstatic.com/firebasejs/10.1.0/firebase-analytics.js";
    // TODO: Add SDKs for Firebase products that you want to use
    // https://firebase.google.com/docs/web/setup#available-libraries

    // Your web app's Firebase configuration
    // For Firebase JS SDK v7.20.0 and later, measurementId is optional
    const firebaseConfig = {
        apiKey: "AIzaSyDSztKwG57QqCxM-FZ7W8wfDQTlLd1Kc2M",
        authDomain: "sweet-diva-v2.firebaseapp.com",
        projectId: "sweet-diva-v2",
        storageBucket: "sweet-diva-v2.appspot.com",
        messagingSenderId: "20214126991",
        appId: "1:20214126991:web:bb0069e8427061e94f53fa",
        measurementId: "G-EMVR8W04ZM"
    };

    // Initialize Firebase
    const app = initializeApp(firebaseConfig);
    const analytics = getAnalytics(app);
</script>
<script>
    @if(Request::is('/'))
    $("#imagepopupclick").click();
    @endif
</script>
<!-- Main JS -->
<script src="{{asset('front/assets/js/main.js')}}"></script>
<script>
    var mainurl = "{{url('/')}}";
    function addtowishlist(elem) {
        event.preventDefault();
        let id = $(elem).attr("id");
        let _token   = $('meta[name="csrf-token"]').attr('content');

        $.ajax({
            url: "{{route('addtowishlist')}}",
            type:"POST",
            data:{
                id:id,
                _token: _token
            },
            success:function(response){
                if(response) {
                    var x = document.getElementById("snackbar");
                    x.className = "show";
                    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
                }
            },
        });
    }
    function addtocart(elem) {
        event.preventDefault();
        let id = $(elem).attr("id");
        let _token   = $('meta[name="csrf-token"]').attr('content');

        $.ajax({
            url: "{{route('addtocart')}}",
            type:"POST",
            data:{
                id:id,
                _token: _token
            },
            success:function(response){
                if(response) {
                    cartitems();
                    // gettotal();
                    var x = document.getElementById("snackbar");
                    x.className = "show";
                    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
                }
            },
        });
    }

    function cartitems(){
        $('.minicart-list').html('<p class="ps-cart-item">Articles du panier</p>');
        $.ajax({
            url: "{{route('cartitems')}}",
            type:"GET",
            success:function(response){
                $.each(response, function(i, item) {
                    $('.minicart-list').append('  <li class="minicart-product">\n' +
                        '                            <a class="product-item_remove" id="'+item.id+'" onclick="removecart(this)"><i\n' +
                        '                                    class="ion-android-close"></i></a>\n' +
                        '                            <div class="product-item_content">\n' +
                        '                                <a class="product-item_title" href="">'+item.name+'</a>\n' +
                        '                                <span class="product-item_quantity">'+item.quantity+' x '+item.price+' €</span>\n' +
                        '                            </div>\n' +
                        '                        </li>');
                });
                gettotal();
            },
        });
    }
    cartitems();
    function removecart(elem){
        document.getElementById("loader").style.display = "block";
        let id = $(elem).attr("id");
        let _token   = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            url: "{{route('removecart')}}",
            type:"POST",
            data:{
                id:id,
                _token: _token
            },
            success:function(response){
                if(response) {
                    cartitems();
                    gettotal();
                    mycartitems();
                    var x = document.getElementById("snackbar1");
                    x.className = "show";
                    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
                    document.getElementById("loader").style.display = "none";
                }
            },
        });
    }
    function gettotal(){
        $.ajax({
            url: "{{route('gettotal')}}",
            type:"GET",
            success:function(response){
                $('.quantitycart').html(response.quantity);
                $('.total').html(response.total + "€");
                // $('#totall').html(response.quantity);
            },
        });
    }
    gettotal();
    function mycartitems(){
        $('.items').html('<p style="display: none" class="ps-cart-item">Articles du panier</p>');
        $.ajax({
            url: "{{route('cartitems')}}",
            type:"GET",
            success:function(response){
                $.each(response, function(i, item) {
                    $('.items').append(' <tr>\n' +
                        '                                    <td class="kenne-product-remove"><a id="'+item.id+'" onclick="removecart(this)"><i class="fa fa-trash"\n' +
                        '                                                                                                     title="Remove"></i></a></td>\n' +
                        '                                    <td class="kenne-product-name"><a href="javascript:void(0)">'+item.name+'</a></td>\n' +
                        '                                    <td class="kenne-product-price"><span class="amount">'+item.price+' €</span></td>\n' +
                        '                                    <td class="quantity">\n' +
                        '                                        <label>'+item.quantity+'</label>\n' +
                        '                                    </td>\n' +
                        '                                    <td class="product-subtotal"><span class="amount">'+item.quantity * item.price +' €</span></td>\n' +
                        '                                </tr>');
                });
                // gettotal();
            },
        });
    }
</script>

<script>
    var slideIndex = 1;
    showSlides(slideIndex);
    showSlides1(slideIndex);

    function plusSlides(n) {
        showSlides(slideIndex += n);
    }

    function currentSlide(n) {
        showSlides(slideIndex = n);
    }

    function showSlides(n) {
        var i;
        var slides = document.getElementsByClassName("mySlides");
        var dots = document.getElementsByClassName("dot");
        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }
        slideIndex++;
        if (slideIndex > slides.length) {slideIndex = 1}
        for (i = 0; i < dots.length; i++) {
            dots[i].className = dots[i].className.replace(" active", "");
        }
        slides[slideIndex-1].style.display = "block";
        dots[slideIndex-1].className += " active";
        setTimeout(showSlides, 3000); // Change image every 2 seconds
    }
    function showSlides1(n) {
        var i;
        var slides = document.getElementsByClassName("mySlides");
        var dots = document.getElementsByClassName("dot");
        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }
        slideIndex++;
        if (slideIndex > slides.length) {slideIndex = 1}
        for (i = 0; i < dots.length; i++) {
            dots[i].className = dots[i].className.replace(" active", "");
        }
        slides[slideIndex-1].style.display = "block";
        dots[slideIndex-1].className += " active";
        setTimeout(showSlides1, 3000); // Change image every 2 seconds
    }
</script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    @if(Session::has('messege'))
    var type="{{Session::get('alert-type','info')}}"
    switch(type){
        case 'info':
            toastr.info("{{ Session::get('messege') }}");
            break;
        case 'success':
            toastr.success("{{ Session::get('messege') }}");
            break;
        case 'warning':
            toastr.warning("{{ Session::get('messege') }}");
            break;
        case 'error':
            toastr.error("{{ Session::get('messege') }}");
            break;
    }
    @endif

</script>
<script>
    // Get the modal
    var modal = document.getElementById("myModal");

    // Get the button that opens the modal
    var btn = document.getElementById("myBtn");

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close1")[0];

    // When the user clicks the button, open the modal
    btn.onclick = function() {
        modal.style.display = "block";
    }

    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
        modal.style.display = "none";
    }

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
</script>
<!--Start of Tawk.to Script-->
<!--<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/60e06954d6e7610a49a96d7f/1f9m9mtst';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
</script>-->
<script>
    /* Whatsapp Chat Widget by www.bloggermix.com */
    $(document).on("click", "#send-it", function() {
        var a = document.getElementById("chat-input");
        if ("" != a.value) {
            var b = $("#get-number").text(),
                c = document.getElementById("chat-input").value,
                d = "https://web.whatsapp.com/send",
                e = b,
                f = "&text=" + c;
            if (
                /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
                    navigator.userAgent
                )
            )
                var d = "whatsapp://send";
            var g = d + "?phone=+590690399802" + e + f;
            window.open(g, "_blank");
        }
    }),
        $(document).on("click", ".informasi", function() {
            (document.getElementById("get-number").innerHTML = $(this)
                .children(".my-number")
                .text()),
                $(".start-chat,.get-new")
                    .addClass("show")
                    .removeClass("hide"),
                $(".home-chat,.head-home")
                    .addClass("hide")
                    .removeClass("show"),
                (document.getElementById("get-nama").innerHTML = $(this)
                    .children(".info-chat")
                    .children(".chat-nama")
                    .text()),
                (document.getElementById("get-label").innerHTML = $(this)
                    .children(".info-chat")
                    .children(".chat-label")
                    .text());
        }),
        $(document).on("click", ".close-chat", function() {
            $("#whatsapp-chat")
                .addClass("hide")
                .removeClass("show");
        }),
        $(document).on("click", ".blantershow-chat", function() {
            $("#whatsapp-chat")
                .addClass("show")
                .removeClass("hide");
        });

</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-208574646-1">
</script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-208574646-1');
</script>
<!-- Facebook Pixel Code -->
<script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window,document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '2567886660007962');
    fbq('track', 'PageView');
</script>
<noscript>
    <img height="1" width="1"
         src="https://www.facebook.com/tr?id=2567886660007962&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->
<script>
    $(".formsubmit").submit(function(){
        $(this).find(':input[type=submit]').prop('disabled', true);
        $(this).find(':input[type=submit]').val("Chargement..");
    });
</script>
@yield('script')

</body>

</html>
