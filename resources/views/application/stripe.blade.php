<!--
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>
    <style>
        @media only screen and (max-width: 600px) {
            body {
                background-color: #fad1d5;
            }
            #bg{
                background-image: none !important;
            }
            #p-0{
                padding-top: 20px !important;
                margin-top: 20px !important;
            }
            #mds{
                display: block !important;
            }
        }
        .form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
            background-color: #f70000;
            opacity: 1;
        }
    </style>
</head>

<body id="bg" style="background-image:url('{{asset('front/assets/19.jpg')}}'); background-repeat: no-repeat; background-size: 100% 100%;">
<div class="row" style="display: none" id="mds">
    <div class="col-md-4" style="text-align: center">
        <img src="{{asset('https://lepalaisdesfemmes.com/logo%20png%20sweet%20diva.png')}}" style="height: auto; width: 60%" alt="">
    </div>
</div>
<div class="container">
    <div class='row'>
        <div class='col-md-4'></div>
        <div class='col-md-4' id="p-0" style="margin-top: 100px; padding: 20px; background-color: white">
            <script src='https://js.stripe.com/v2/' type='text/javascript'></script>
            <form accept-charset="UTF-8" action="{{route('application.stripe.submit')}}" class="require-validation"
                  data-cc-on-file="false"
                  data-stripe-publishable-key="{{env('STRIPE_PUBLISHABLE_KEY')}}"
                  id="payment-form" method="post">
                {{ csrf_field() }}
                @if ((Session::has('success-message')))
                    <div class="alert alert-success col-md-12">{{
					Session::get('success-message') }}</div>
                @endif @if ((Session::has('fail-message')))
                    <div class="alert alert-danger col-md-12">{{
					Session::get('fail-message') }}</div>
                @endif
                <div class='form-row'>
                    <div class='col-12 error form-group d-none'>
                        <div class='alert-danger alert'>Veuillez corriger les erreurs et essayer de nouveau.</div>
                    </div>
                </div>
                <div class='form-row'>
                    <div class='col-12 form-group required'>
                        <label class='control-label'>Nom sur la carte</label> <input class='form-control' size='4' type='text'>
                    </div>
                </div>
                <div class='form-row'>
                    <div class='col-12 form-group card required'>
                        <label class='control-label'>Numéro de carte</label> <input
                            autocomplete='off' class='form-control card-number' size='20'
                            type='text'>
                    </div>
                </div>
                <div class='form-row'>
                    <div class='col-4 form-group cvc required'>
                        <label class='control-label'>CVC</label> <input
                            autocomplete='off' class='form-control card-cvc'
                            placeholder='ex. 311' size='4' type='text'>
                    </div>
                    <div class='col-4 form-group expiration required'>
                        <label class='control-label'>Expiration</label> <input
                            class='form-control card-expiry-month' placeholder='MM' size='2'
                            type='text'>
                    </div>
                    <div class='col-4 form-group expiration required'>
                        <label class='control-label'>Année</label> <input
                            class='form-control card-expiry-year' placeholder='YYYY'
                            size='4' type='text'>
                    </div>
                </div>
                <div class='form-row'>
                    <div class='col-12'>
                        <div class='total'>
                            <b> Total: <span class='amount'>{{$price}} €</span></b>
                            <input type="hidden" name="price" value="{{$price}}">
                            <input type="hidden" name="id" value="{{$id}}">
                            <input type="hidden" name="user_id" value="{{$user_id}}">
                        </div>
                    </div>
                </div>
                <hr>
                <div class='form-row'>
                    <div class='col-12 form-group'>
                        <button class='loader form-control btn btn-primary submit-button'
                                type='submit' style="margin-top: 10px;">Payer »</button>
                    </div>
                </div>
            </form>
        </div>
        <div class='col-md-4'></div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-1.12.3.min.js"
        integrity="sha256-aaODHAgvwQW1bFOGXMeX+pC4PZIPsvn2h1sArYOhgXQ="
        crossorigin="anonymous"></script>
<script
    src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"
    integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS"
    crossorigin="anonymous"></script>
<script>
    $(function() {
        $('form.require-validation').bind('submit', function(e) {
            var $form         = $(e.target).closest('form'),
                inputSelector = ['input[type=email]', 'input[type=password]',
                    'input[type=text]', 'input[type=file]',
                    'textarea'].join(', '),
                $inputs       = $form.find('.required').find(inputSelector),
                $errorMessage = $form.find('div.error'),
                valid         = true;

            $errorMessage.addClass('d-none');
            $('.is-invalid').removeClass('is-invalid');
            $inputs.each(function(i, el) {
                var $input = $(el);
                if ($input.val() === '') {
                    $input.addClass('is-invalid');
                    $errorMessage.removeClass('d-none');
                    e.preventDefault(); // cancel on first error
                }
            });
        });
    });

    $(function() {
        var $form = $("#payment-form");

        $form.on('submit', function(e) {
            if (!$form.data('cc-on-file')) {
                e.preventDefault();
                Stripe.setPublishableKey($form.data('stripe-publishable-key'));
                Stripe.createToken({
                    number: $('.card-number').val(),
                    cvc: $('.card-cvc').val(),
                    exp_month: $('.card-expiry-month').val(),
                    exp_year: $('.card-expiry-year').val()
                }, stripeResponseHandler);
            }
        });
        var errorMessages = {
            incorrect_number: "Le numéro de carte est incorrect.",
            invalid_number: "Le numéro de carte n'est pas un numéro de carte de crédit valide.",
            invalid_expiry_month: "Le mois d'expiration de la carte n'est pas valide.",
            invalid_expiry_year: "L'année d'expiration de la carte est invalide.",
            invalid_cvc: "Le code de sécurité de la carte est invalide.",
            expired_card: "La carte a expiré.",
            incorrect_cvc: "Le code de sécurité de la carte est incorrect.",
            incorrect_zip: "La validation du code postal de la carte a échoué.",
            card_declined: "La carte a été refusée.",
            missing: "Il n'y a pas de carte sur un client qui est facturé.",
            processing_error: "Une erreur s'est produite lors du traitement de la carte.",
            rate_limit:  "Une erreur s'est produite en raison de demandes atteignant l'API trop rapidement. S'il vous plaît laissez-nous savoir si vous rencontrez régulièrement cette erreur."
        };
        function stripeResponseHandler(status, response) {
            if (response.error) {
                $('.error')
                    .removeClass('d-none')
                    .find('.alert')
                    .text(errorMessages[ response.error.code ]);
            } else {
                $(".loader").html("Veuillez patienter paiement en cours...");
                // token contains id, last4, and card type
                var token = response['id'];
                // insert the token into the form so it gets submitted to the server
                $form.find('input[type=text]').empty();
                $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
                $form.get(0).submit();
            }
        }
    })
</script>
</body>
</html>
-->


<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8" />
    <title>Accept a payment</title>
    <meta name="description" content="A demo of a payment on Stripe" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <style>
        @media only screen and (max-width: 600px)
        {
            form {
                width: 100vw !important;
                min-width: initial;
                padding: 20px !important;
            }
        }
        /* Variables */
        * {
            box-sizing: border-box;
        }

        body {
            font-family: -apple-system, BlinkMacSystemFont, sans-serif;
            font-size: 16px;
            -webkit-font-smoothing: antialiased;
            display: flex;
            justify-content: center;
            align-content: center;
            height: 100vh;
            width: 100vw;
        }

        form {
            width: 30vw;
            min-width: 500px;
            align-self: center;
            box-shadow: 0px 0px 0px 0.5px rgba(50, 50, 93, 0.1),
            0px 2px 5px 0px rgba(50, 50, 93, 0.1), 0px 1px 1.5px 0px rgba(0, 0, 0, 0.07);
            border-radius: 7px;
            padding: 40px;
        }

        .hidden {
            display: none;
        }

        #payment-message {
            color: rgb(105, 115, 134);
            font-size: 16px;
            line-height: 20px;
            padding-top: 12px;
            text-align: center;
        }

        #payment-element {
            margin-bottom: 24px;
        }

        /* Buttons and links */
        button {
            background: #5469d4;
            font-family: Arial, sans-serif;
            color: #ffffff;
            border-radius: 4px;
            border: 0;
            padding: 12px 16px;
            font-size: 16px;
            font-weight: 600;
            cursor: pointer;
            display: block;
            transition: all 0.2s ease;
            box-shadow: 0px 4px 5.5px 0px rgba(0, 0, 0, 0.07);
            width: 100%;
        }
        button:hover {
            filter: contrast(115%);
        }
        button:disabled {
            opacity: 0.5;
            cursor: default;
        }

        /* spinner/processing state, errors */
        .spinner,
        .spinner:before,
        .spinner:after {
            border-radius: 50%;
        }
        .spinner {
            color: #ffffff;
            font-size: 22px;
            text-indent: -99999px;
            margin: 0px auto;
            position: relative;
            width: 20px;
            height: 20px;
            box-shadow: inset 0 0 0 2px;
            -webkit-transform: translateZ(0);
            -ms-transform: translateZ(0);
            transform: translateZ(0);
        }
        .spinner:before,
        .spinner:after {
            position: absolute;
            content: "";
        }
        .spinner:before {
            width: 10.4px;
            height: 20.4px;
            background: #5469d4;
            border-radius: 20.4px 0 0 20.4px;
            top: -0.2px;
            left: -0.2px;
            -webkit-transform-origin: 10.4px 10.2px;
            transform-origin: 10.4px 10.2px;
            -webkit-animation: loading 2s infinite ease 1.5s;
            animation: loading 2s infinite ease 1.5s;
        }
        .spinner:after {
            width: 10.4px;
            height: 10.2px;
            background: #5469d4;
            border-radius: 0 10.2px 10.2px 0;
            top: -0.1px;
            left: 10.2px;
            -webkit-transform-origin: 0px 10.2px;
            transform-origin: 0px 10.2px;
            -webkit-animation: loading 2s infinite ease;
            animation: loading 2s infinite ease;
        }

        @-webkit-keyframes loading {
            0% {
                -webkit-transform: rotate(0deg);
                transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }
        @keyframes loading {
            0% {
                -webkit-transform: rotate(0deg);
                transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }

        @media only screen and (max-width: 600px) {
            form {
                width: 80vw;
                min-width: initial;
            }
        }
    </style>
</head>
<body>
<!-- Display a payment form -->

<form id="payment-form">
    <div style="text-align: center; padding: 20px">
        <img height="100px" src="{{asset('logo png sweet diva.png')}}" alt="">
    </div>
    <div id="payment-element">
        <!--Stripe.js injects the Payment Element-->
    </div>
    <h2>Total : {{$price}} €</h2>
    <button id="submit">
        <div class="spinner hidden" id="spinner"></div>
        <span id="button-text">Payez maintenant</span>
    </button>
    <div id="payment-message" class="hidden"></div>


    <script src="https://js.stripe.com/v3/"></script>
    <script data-locale="fr">
        setLoading(true);
        // This is your test publishable API key.
        const stripe = Stripe("{{env('STRIPE_PUBLISHABLE_KEY')}}", {
            locale: 'fr'
        });
        let _token   = "{{ csrf_token() }}";
        // The items the customer wants to buy


        let elements;

        initialize();
        checkStatus();

        document
            .querySelector("#payment-form")
            .addEventListener("submit", handleSubmit);

        // Fetches a payment intent and captures the client secret
        async function initialize() {
            const { clientSecret } = await fetch("/getStripeIntent", {
                method: "POST",
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify(
                    {
                        id: "{{$id}}",
                        _token: _token,
                        price : "{{$price}}"
                    }
                ),
            }).then((r) => r.json());

            elements = stripe.elements({clientSecret});

            const paymentElement = elements.create("payment");
            paymentElement.mount("#payment-element");
            setLoading(false);
        }

        async function handleSubmit(e) {
            e.preventDefault();
            setLoading(true);

            const { error } = await stripe.confirmPayment({
                elements,
                confirmParams: {
                    // Make sure to change this to your payment completion page
                    return_url: "{{route('application.payment.success', ['id' => $id, 'type' => 'order', 'user_id' => $user_id])}}",
                },
            });

            // This point will only be reached if there is an immediate error when
            // confirming the payment. Otherwise, your customer will be redirected to
            // your `return_url`. For some payment methods like iDEAL, your customer will
            // be redirected to an intermediate site first to authorize the payment, then
            // redirected to the `return_url`.
            if (error.type === "card_error" || error.type === "validation_error") {
                showMessage(error.message);
            } else {
                showMessage("An unexpected error occurred.");
            }

            setLoading(false);
        }

        // Fetches the payment intent status after payment submission
        async function checkStatus() {
            const clientSecret = new URLSearchParams(window.location.search).get(
                "payment_intent_client_secret"
            );

            if (!clientSecret) {
                return;
            }

            const { paymentIntent } = await stripe.retrievePaymentIntent(clientSecret);

            switch (paymentIntent.status) {
                case "succeeded":
                    showMessage("Payment succeeded!");
                    break;
                case "processing":
                    showMessage("Your payment is processing.");
                    break;
                case "requires_payment_method":
                    showMessage("Your payment was not successful, please try again.");
                    break;
                default:
                    showMessage("Something went wrong.");
                    break;
            }
        }

        // ------- UI helpers -------

        function showMessage(messageText) {
            const messageContainer = document.querySelector("#payment-message");

            messageContainer.classList.remove("hidden");
            messageContainer.textContent = messageText;

            setTimeout(function () {
                messageContainer.classList.add("hidden");
                messageText.textContent = "";
            }, 4000);
        }

        // Show a spinner on payment submission
        function setLoading(isLoading) {
            if (isLoading) {
                // Disable the button and show a spinner
                document.querySelector("#submit").disabled = true;
                document.querySelector("#spinner").classList.remove("hidden");
                document.querySelector("#button-text").classList.add("hidden");
            } else {
                document.querySelector("#submit").disabled = false;
                document.querySelector("#spinner").classList.add("hidden");
                document.querySelector("#button-text").classList.remove("hidden");
            }
        }
    </script>
</form>
</body>
</html>
