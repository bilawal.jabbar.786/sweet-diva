@extends('errors.layouts.include')
@section('content')
    <!-- Login Wrapper Area-->
    <div class="login-wrapper d-flex align-items-center justify-content-center text-center">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-sm-9 col-md-7 col-lg-6 col-xl-5">
                    <div class="text-left px-4">
                        <h5 class="mb-1 text-white">Mise à jour requise!</h5>
                        <p class="mb-4 text-white">Vous utilisez une très ancienne version de l'application. veuillez mettre à jour la dernière version immédiatement pour profiter de la dernière fonctionnalité. : <b>contact@lepalaisdesfemmes.com</b></p>
                    </div>
                    <!-- OTP Send Form-->
{{--                    <div class="otp-form mt-5 mx-4">--}}

{{--                        <a  href="{{route('front.index')}}" class="btn btn-warning btn-lg w-100" type="submit">De retour à la maison</a>--}}

{{--                    </div>--}}

                </div>
            </div>
        </div>
    </div>
@endsection

