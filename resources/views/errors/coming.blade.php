@extends('errors.layouts.include')
@section('content')
    <!-- Login Wrapper Area-->
    <?php $gs = \App\GeneralSettings::first();
//    dd($gs);
    ?>
    <div class="login-wrapper d-flex align-items-center justify-content-center text-center">
        <div class="container">
            <div class="row justify-content-center">
                <div class="otp-form mt-5 mx-4">
                    <img src="{{asset('logo png sweet diva.png') }}" style="width: 65%!important; margin: 60px;">

                </div><br><br>
                <div class="col-12 col-sm-9 col-md-7 col-lg-6 col-xl-5">

                    <div class="text-left px-4">
                        <h4 class="mb-1 text-white">Bientôt disponible!</h4>
                        <p class="mb-4 text-white"><b>L'application est en maintenance. Nous serons de retour bientôt.</b> </p>
                     <h3 style="color: white"><i class="lni lni-phone"></i> 0644655569</h3>
                    </div>
                    <!-- OTP Send Form-->
                    <div class="otp-form mt-5 mx-4">


                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

