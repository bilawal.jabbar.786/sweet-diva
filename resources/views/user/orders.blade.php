@extends('layouts.front')
@section('content')


    <!-- Begin Kenne's Content Wrapper Area -->
    <div class="kenne-content_wrapper" style="padding: 50px 0 95px;">
        <div class="container">
            <div class="row">
                @include('include.videdressing')
                <div class="col-xl-9 col-lg-8 order-1 order-lg-2">
                    <div class="myaccount-orders">
                        <h4 class="small-title">Nouvelles commandes</h4>
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <tbody>
                                <tr>
                                    <th>COMMANDES</th>
                                    <th>DATE</th>
                                    <th>TOTAL</th>
                                    <th></th>
                                </tr>
                                @foreach($vendor_orders as $row1)
                                    <tr>
                                        <td><a class="account-order-id" href="javascript:void(0)">#{{$row1->order->order_number}}</a></td>
                                        <td>{{$row1->order->created_at->format('d-m-y')}}</td>
                                        <td>{{$row1->order->total}} €</td>
                                        <td>
                                            <a href="{{route('user.order-detail', ['id' => $row1->order->id])}}" class="kenne-btn kenne-btn_sm"><span>Voir</span></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Kenne's Content Wrapper Area End Here -->
@endsection
