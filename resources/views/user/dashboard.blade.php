@extends('layouts.front')
@section('content')
    <main class="page-content">
        <div class="account-page-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <ul class="nav myaccount-tab-trigger" id="account-page-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="account-dashboard-tab" data-toggle="tab"
                                   href="#account-dashboard" role="tab" aria-controls="account-dashboard"
                                   aria-selected="true">Tableau de bord</a>
                            </li>


                            <li class="nav-item">
                                <a class="nav-link" id="account-orders-tab" data-toggle="tab" href="#account-orders"
                                   role="tab" aria-controls="account-orders" aria-selected="false">Facture</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="account-details-tab" data-toggle="tab" href="#account-details"
                                   role="tab" aria-controls="account-details" aria-selected="false">Détails du
                                    compte</a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('logout')}}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"
                                   class="nav-link">
                                    <p>
                                        Sortie
                                    </p>
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-9">
                        <div class="tab-content myaccount-tab-content" id="account-page-tab-content">
                            <div class="tab-pane fade show active" id="account-dashboard" role="tabpanel"
                                 aria-labelledby="account-dashboard-tab">
                                <div class="myaccount-dashboard">
                                    <p>Bonjour <b>{{$user->name}} , </b>
                                        @if($user->role == 1)
                                            Bienvenue dans le tableau de bord
                                        @else
                                            Bienvenue dans le tableau de bord
                                        @endif
                                    </p>
                                    <p>Depuis le tableau de bord de votre compte, vous pouvez consulter vos commandes
                                        récentes, gérer vos points de fidélité
                                        et <a href="javascript:void(0)">modifier votre mot de passe ainsi que les
                                            détails de votre compte.</a>.</p>
                                    <h3>Vos points {{Auth::user()->points}}</h3>
                                    <br>
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th scope="col">Titre</th>
                                            <th scope="col">Points</th>
                                            <th scope="col">Obtenir</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($points as $point)
                                            <tr>
                                                <td>{{$point->name}}</td>
                                                <td>{{$point->points}}</td>
                                                <td>
                                                    <a href="{{route('getmypoints', ['id' => $point->id])}}">Offre</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="account-orders" role="tabpanel"
                                 aria-labelledby="account-orders-tab">
                                <div class="myaccount-orders">
                                    <h4 class="small-title">MES COMMANDES</h4>
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover">
                                            <tbody>
                                            <tr>
                                                <th>COMMANDES</th>
                                                <th>DATE</th>
                                                <th>STATUT</th>
                                                <th>TOTAL</th>
                                                <th></th>
                                            </tr>
                                            @foreach($orders as $row)
                                                <tr>
                                                    <td><a class="account-order-id"
                                                           href="javascript:void(0)">#{{$row->order_number}}</a></td>
                                                    <td>{{$row->created_at->format('d-m-y')}}</td>
                                                    <td>
                                                        @if($row->status == '0')
                                                            Nouvelle commande
                                                        @else
                                                            Compléter
                                                        @endif
                                                    </td>
                                                    <td>{{$row->total}}</td>
                                                    <td><a href="{{route('user.order-detail', ['id' => $row->id])}}"
                                                           class="kenne-btn kenne-btn_sm"><span>View</span></a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>


                            <div class="tab-pane fade" id="account-details" role="tabpanel"
                                 aria-labelledby="account-details-tab">
                                <div class="myaccount-details">
                                    <form action="{{route('user.profileupdate')}}" method="POST" class="kenne-form"
                                          accept-charset="UTF-8" enctype="multipart/form-data">
                                        @csrf
                                        <div class="kenne-form-inner">
                                            <div class="single-input single-input-half">
                                                <label for="account-details-firstname">Nom*</label>
                                                <input type="text" value="{{$user->name}}" name="name" required>
                                            </div>
                                            <div class="single-input single-input-half">
                                                <label for="account-details-lastname">Telephone*</label>
                                                <input type="text" value="{{$user->phone}}" name="phone" required>
                                            </div>
                                            <div class="single-input single-input-half">
                                                <label for="account-details-email">Email*</label>
                                                <input type="email" value="{{$user->email}}" name="email" readonly>
                                            </div>
                                            <div class="single-input">
                                                <label for="account-details-email">Adresse*</label>
                                                <input type="text" value="{{$user->address}}" name="address" required>
                                            </div>
                                            <div class="single-input single-input-half">
                                                <label for="account-details-email">Pays*</label>
                                                <input type="text" name="country" value="{{$user->country}}">
                                            </div>
                                            <div class="single-input single-input-half">
                                               <label> Choisissez votre taille</label>
                                                <select class="mb-3 form-select" id="topic" name="size" required>
                                                    <option value="TU" {{ $user->size == 'TU' ? 'selected' : '' }}>TU</option>
                                                    <option value="XL(44/46)" {{ $user->size == 'XL(44/46)' ? 'selected' : '' }}>XL
                                                        (44/46)
                                                    </option>
                                                    <option value="1XL(46/48)" {{ $user->size == '1XL(46/48)' ? 'selected' : '' }}>
                                                        1XL (46/48)
                                                    </option>
                                                    <option value="2XL(48/50)" {{ $user->size == '2XL(48/50)' ? 'selected' : '' }}>
                                                        2XL (48/50)
                                                    </option>
                                                    <option value="3XL(50/52)" {{ $user->size == '3XL(50/52)' ? 'selected' : '' }}>
                                                        3XL (50/52)
                                                    </option>
                                                    <option value="4XL(52/54)" {{ $user->size == '4XL(52/54)' ? 'selected' : '' }}>
                                                        4XL (52/54)
                                                    </option>
                                                    <option value="5XL(54/56)" {{ $user->size == '5XL(54/56)' ? 'selected' : '' }}>
                                                        5XL (54/56)
                                                    </option>
                                                </select>
                                            </div>
                                            <div class="single-input single-input-half">
                                               <label>Boutique associée</label>
                                                <select class="mb-3 form-select" name="shop" required>
                                                    @if(!$user->shop)
                                                        <option value="">Sélectionnez votre boutique pertinente</option>
                                                    @endif
                                                    <option
                                                        {{ $user->shop == 'SWEET DIVA MARTINIQUE' ? 'selected' : '' }} value="SWEET DIVA MARTINIQUE">
                                                        SWEET DIVA MARTINIQUE
                                                    </option>
                                                    <option
                                                        {{ $user->shop == 'SWEET DIVA BESSON' ? 'selected' : '' }} value="SWEET DIVA BESSON">
                                                        SWEET DIVA BESSON
                                                    </option>
                                                    <option
                                                        {{ $user->shop == 'SWEET DIVA BASSE TERRE' ? 'selected' : '' }} value="SWEET DIVA BASSE TERRE">
                                                        SWEET DIVA BASSE TERRE
                                                    </option>

                                                </select>
                                            </div>
                                            <div class="single-input single-input-half">
                                                <label for="account-details-email">Photo*</label>
                                                <input type="file" name="photo">
                                            </div>
                                            <div class="single-input single-input-half">
                                                <label for="account-details-oldpass">Mot de passe actuel (laisser vide
                                                    pour laisser
                                                    inchangé)</label>
                                                <input type="password" name="oldpassword">
                                            </div>
                                            <div class="single-input single-input-half">
                                                <label for="account-details-newpass">Nouveau mot de passe (laisser vide
                                                    pour laisser
                                                    inchangé)</label>
                                                <input type="password" name="newpassword">
                                            </div>
                                            <div class="single-input">
                                                <button class="kenne-btn kenne-btn_dark" type="submit"><span>SAUVEGARDER
                                                    CHANGEMENTS</span></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Kenne's Account Page Area End Here -->
    </main>

@endsection

@section('script')
    <script>

        function categorychange(elem) {
            $('.maincategory').html('<option value="">Sélectionnez une sous-catégorie</option>');
            event.preventDefault();
            let id = elem.value;
            let _token = $('meta[name="csrf-token"]').attr('content');

            $.ajax({
                url: "{{route('fetchmaincategory')}}",
                type: "POST",
                data: {
                    id: id,
                    _token: _token
                },
                success: function (response) {
                    $.each(response, function (i, item) {
                        $('.maincategory').append('<option value="' + item.id + '">' + item.name + '</option>');
                    });
                },
            });
        }

        function maincategorychange(elem) {
            $('.subcategory').html('<option value="">Catégorie enfant</option>');
            event.preventDefault();
            let id = elem.value;
            let _token = $('meta[name="csrf-token"]').attr('content');

            $.ajax({
                url: "{{route('fetchsubcategory')}}",
                type: "POST",
                data: {
                    id: id,
                    _token: _token
                },
                success: function (response) {
                    $.each(response, function (i, item) {
                        console.log(item.name);
                        $('.subcategory').append('<option value="' + item.id + '">' + item.name + '</option>');
                    });
                },
            });
        }

        function addmoresize() {
            $('#addmoresize').append('<div class="row">\n' +
                '                                                <div class="col-md-6">\n' +
                '                                                    <input id="size" type="text" name="size[]" required placeholder="Entrez le nom de la taille"  class="form-control">\n' +
                '                                                </div>\n' +
                '                                                <div class="col-md-6">\n' +
                '                                                    <input id="quantity" type="number" name="quantity[]" required placeholder="Entrer la quantité"  class="form-control">\n' +
                '                                                </div>\n' +
                '                                            </div>');
        }

        function removesize() {
            $('#addmoresize .row').last().remove();
        }

        function addmorecolor() {
            $('#addmorecolor').append(' <div class="row">\n' +
                '                                                <div class="col-md-12">\n' +
                '                                                    <input id="color" type="color" name="color[]" required  class="form-control">\n' +
                '                                                </div>\n' +
                '                                            </div>');
        }

        function removecolor() {
            $('#addmorecolor .row').last().remove();
        }
    </script>
@endsection
