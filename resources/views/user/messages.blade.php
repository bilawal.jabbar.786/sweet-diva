@extends('layouts.front')
@section('content')
    <!-- Begin Kenne's Content Wrapper Area -->
    <div class="kenne-content_wrapper" style="padding: 50px 0 95px;">
        <div class="container">
            <div class="row">
                @include('include.videdressing')
                <div class="col-xl-9 col-lg-8 order-1 order-lg-2">
                    <div class="myaccount-details">
                        @foreach($messages as $row)
                            @if($row->s_id !== Auth::user()->id)
                            <a href="{{route('user.chat' , ['id' => $row->receiver->id])}}">
                                <div class="row">
                                    <div class="col-2">
                                        <img style="height: 40px; border-radius: 50%" src="{{empty($row->receiver->photo)? asset('front/assets/images/avatar.png') : asset($row->receiver->photo)}}" alt="">
                                    </div>
                                    <div class="col-8">
                                        <h4 style="padding-top: 10px">{{$row->receiver->name}}</h4>
                                    </div>
                                </div>
                            </a>
                                <hr>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Kenne's Content Wrapper Area End Here -->
@endsection
