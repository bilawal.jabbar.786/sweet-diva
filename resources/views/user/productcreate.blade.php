@extends('layouts.front')
@section('content')
    <!-- Begin Kenne's Content Wrapper Area -->
    <div class="kenne-content_wrapper" style="padding: 50px 0 95px;">
        <div class="container">
            <div class="row">
                @include('include.videdressing')
                <div class="col-xl-9 col-lg-8 order-1 order-lg-2">
                    <div class="myaccount-details">
                        <form action="{{route('product.store')}}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
                        @csrf
                        <!-- /.card-header -->
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Titre</label>
                                            <input type="text" name="title" required class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">SKU</label>
                                            <input type="number" name="sku" readonly value="{{rand(100000, 900000)}}" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Prix</label>
                                            <input type="number" name="price" required class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Etat</label>
                                            <select name="state" class="form-control" id="" required>
                                                <option value="Neuf avec étiquette">Neuf avec étiquette</option>
                                                <option value="Neuf sans etiquette">Neuf sans etiquette</option>
                                                <option value="Tres bon etat">Tres bon etat</option>
                                                <option value="Bon etat">Bon etat</option>
                                                <option value="Satisfaisant">Satisfaisant</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Catégorie</label>
                                            <select name="vendorcategory" class="form-control" id="" required>
                                                <option value="ROBE">ROBE</option>
                                                <option value="COMBI">COMBI</option>
                                                <option value="PANTALON">PANTALON</option>
                                                <option value="JUPE">JUPE</option>
                                                <option value="SHORT">SHORT</option>
                                                <option value="TOP">TOP</option>
                                                <option value="JEAN">JEAN</option>
                                                <option value="VESTE">VESTE</option>
                                                <option value="AUTRE">AUTRE</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Taille</label>
                                            <input type="text" name="sizesimple" required class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Photo principale</label>
                                            <input type="file" name="photo1" required class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Photo secondaire</label>
                                            <input type="file" name="photo2" required class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">La description</label>
                                            <textarea class="form-control" name="description" id="summernote" cols="30" rows="10" required></textarea>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="kenne-btn kenne-btn_sm">Sauvegarder</button>
                            </div>
                        </form>
                        <!-- /.card -->

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Kenne's Content Wrapper Area End Here -->
@endsection
