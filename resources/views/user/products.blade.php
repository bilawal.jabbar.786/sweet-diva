@extends('layouts.front')
@section('content')
    <!-- Begin Kenne's Content Wrapper Area -->
    <div class="kenne-content_wrapper" style="padding: 50px 0 95px;">
        <div class="container">
            <div class="row">
                @include('include.videdressing')
                <div class="col-xl-9 col-lg-8 order-1 order-lg-2">
                    <div class="myaccount-orders">
                        <h4 class="small-title">MES produits</h4>
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <tbody>
                                <tr>
                                    <th>Nom</th>
                                    <th>Sku</th>
                                    <th>Photo</th>
                                    <th>Prix</th>
                                    <th>Action</th>
                                </tr>
                                @foreach($products as $product)
                                    <tr>
                                        <td><a class="account-order-id" href="{{route('vendor.singleproduct', ['id' => $product->id])}}">{{$product->title}}</a></td>
                                        <td>{{$product->sku}}</td>
                                        <td>
                                            <img src="{{asset($product->photo1)}}" height="30px" alt="">
                                        </td>
                                        <td>{{$product->price}} €</td>
                                        <td><a href="{{route('product.delete', ['id' => $product->id])}}" class="kenne-btn kenne-btn_sm"><span>Delete</span></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Kenne's Content Wrapper Area End Here -->
@endsection
