@extends('layouts.front')
@section('content')
    <!-- Begin Kenne's Content Wrapper Area -->
    <div class="kenne-content_wrapper" style="padding: 50px 0 95px;">
        <div class="container">
            <div class="row">
                @include('include.videdressing')
                <div class="col-xl-9 col-lg-8 order-1 order-lg-2">
                    <div class="myaccount-details">
                        <ul id="mydiv" style="height: 400px; overflow: scroll;  overflow-x:hidden;overflow-y:visible;">
                            @foreach($messages as $message)
                                @if($message->s_id == Auth::user()->id)
                                    <li style="color:white; padding: 20px; border-radius: 30px; background-color: #de2f2f; width: 80%; margin-bottom: 3px; float: right">{{$message->message}}</li>
                                    <br>
                                @else
                                    <li style="color:white; padding: 20px; border-radius: 30px; background-color: #d76666; width: 80%; margin-bottom: 3px; float: left">{{$message->message}}</li>
                                    <br>
                                @endauth
                            @endforeach
                         </ul>
                        <ul>
                            <li>
                                <form action="{{route('message.send')}}" method="POST">
                                    @csrf
                                    <div class="row">
                                        <div class="col-8" style="padding: 0px">
                                            <input type="text" name="message" style="width: 100%; padding: 15px">
                                        </div>
                                        <div class="col-4" style="padding: 0px">
                                            <input autocomplete="off" placeholder="Tapez le message" type="hidden" name="r_id" value="{{$id}}">
                                            <button class="mybutton">Envoyer</button>
                                        </div>
                                    </div>
                                </form>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Kenne's Content Wrapper Area End Here -->
@endsection
@section('script')
    <script>
        var objDiv = document.getElementById("mydiv");
        objDiv.scrollTop = objDiv.scrollHeight;
    </script>
@endsection
