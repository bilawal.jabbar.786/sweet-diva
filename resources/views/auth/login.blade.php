@extends('layouts.front')
@section('content')
    <!-- Begin Kenne's Login Register Area -->
    <div class="kenne-login-register_area">
        <div class="container">
            <div class="row">
                <div class="col-md-3">

                </div>
                <div class="col-sm-12 col-md-6 col-xs-12 col-lg-6">
                    <!-- Login Form s-->
                    <form action="{{route('login')}}" method="POST">
                        @csrf
                        <div class="login-form">
                            <h4 class="login-title">Connexion</h4>
                            <div class="row">
                                <div class="col-md-12 col-12">
                                    <label>Adresse e-mail*</label>
                                    <input class="form-control" required name="email" type="email" placeholder="Email Address">
                                </div>
                                <div class="col-12 mb--20">
                                    <label>Mot de passe</label>
                                    <input class="form-control" required name="password" type="password" placeholder="Password">
                                </div>
                                <div class="col-md-8">
                                    <div class="check-box">
                                        <input type="checkbox" id="remember_me">
                                        <label for="remember_me">Se souvenir de moi</label>
                                    </div>
                                </div>
                                <div class="col-md-12" style="padding-top: 20px">
                                    <a href="{{ route('password.request') }}">Mot de passe oublié?</a>
                                </div>
<!--                                <div class="col-md-4">
                                    <div class="forgotton-password_info">
                                        <a href="#"> Forgotten pasward?</a>
                                    </div>
                                </div>-->
                                <div class="col-md-12">
                                    <button class="kenne-login_btn">Connexion</button>
                                    <a href="{{route('register')}}" class="kenne-login_btn" style="text-align: center;color: #ffffff;margin-top: 5px;">Inscription</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
    <!-- Kenne's Login Register Area  End Here -->

@endsection
