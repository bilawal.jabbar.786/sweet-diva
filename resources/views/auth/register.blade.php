@extends('layouts.front')
@section('content')
    <?php
    $countries = \App\Countries::all();
    ?>
    <!-- Begin Kenne's Login Register Area -->
    <div class="kenne-login-register_area">
        <div class="container">
            <div class="row">
                <div class="col-md-3">

                </div>
                <div class="col-sm-12 col-md-6 col-lg-6 col-xs-12">
                    <form action="{{route('register')}}" method="POST">
                        @csrf
                        <div class="login-form">
                            <h4 class="login-title">Inscription</h4>
                            <div class="row">
                                <div class="col-md-12 col-12">
                                    <label>Nom</label>
                                    <input class="form-control" type="text" name="name" required placeholder="Nom">
                                </div>
                                <div class="col-md-12 col-12">
                                    <label>Prénom</label>
                                    <input class="form-control" type="text" name="surname" required placeholder="Prénom">
                                </div>
                                <div class="col-md-12">
                                    <label>Adresse e-mail*</label>
                                    <input class="form-control" name="email" type="email" required placeholder="Adresse e-mail">
                                </div>
                                <div class="col-md-12">
                                    <label>Telephone*</label>
                                    <input class="form-control" type="text" required name="phone" placeholder="Telephone">
                                </div>
                                <div class="col-md-12">
                                    <label>Adresse*</label>
                                    <input class="form-control" type="text" required name="address" placeholder="Adresse">
                                </div>
                                <div class="col-md-12">
                                    <label>Code Postal*</label>
                                    <input class="form-control" type="number" required name="cp" placeholder="Code Postal">
                                </div>
                                <div class="col-md-12">
                                    <label>Ville*</label>
                                    <input class="form-control" type="text" required name="city" placeholder="Ville">
                                </div>
                                <div class="col-md-12 mb--20">
                                    <label>Pays*</label>
                                    <select name="country" class="form-control" style="margin-bottom: 10px;">
                                        @foreach($countries as $country)
                                        <option value="{{$country->name}}">{{$country->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-md-6">
                                    <label>Mot de passe</label>
                                    <input class="form-control" type="password" required name="password" placeholder="Mot de passe">
                                </div>
                                <div class="col-md-6">
                                    <label>Confirmer le mot de passe</label>
                                    <input class="form-control" type="password" required name="password_confirmation" placeholder="Confirmer le mot de passe">
                                </div>
                                <div class="col-md-12">
                                  <label class="checkbox">
                                      <input type="checkbox" name="accepttos" class="accepttos" required>
                                      J'ai lu et j'accepte les <a href="{{route('front.terms')}}" target="_blank">Conditions générales de vente</a>
                                    </label>
                                </div>
                                <input type="hidden" name="role" value="2">
                                <div class="col-12">
                                    <button class="kenne-register_btn">M'inscrire</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
    <!-- Kenne's Login Register Area  End Here -->
@endsection
