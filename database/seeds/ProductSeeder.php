<?php

use App\Product;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::create([
            'user_id' => 1,
            'title' => 'A / B DRESS',
            'description' => 'Pour les abonnés gratuits, il sagit simplement dun texte factice de lindustrie de limpression et de la composition. Lorem Ipsum est le texte factice standard de lindustrie depuis les années 1500, lorsquun imprimeur inconnu a pris une galère.',
            'price' => '110',
            'oldprice' => '150',
            'category' => '1',
            'category_id' => '5',
            'subcategory_id' => '2',
            'sku' => '109451',
            'photo1' => 'front/assets/images/product/01a.jpg',
            'photo2' => 'front/assets/images/product/01b.jpg',
            'gallery' => '',
            'size' => '["10","20"]',
            'quantity' => '["10","20"]',
            'color' => '["#000000","#ff0060","#FFC0CB"]',
        ]);
        Product::create([
            'user_id' => 1,
            'title' => 'A / B JEANS DRESS',
            'description' => 'Pour les abonnés gratuits, il sagit simplement dun texte factice de lindustrie de limpression et de la composition. Lorem Ipsum est le texte factice standard de lindustrie depuis les années 1500, lorsquun imprimeur inconnu a pris une galère.',
            'price' => '79',
            'oldprice' => '100',
            'category' => '1',
            'category_id' => '5',
            'subcategory_id' => '2',
            'sku' => '109456',
            'photo1' => 'front/assets/images/product/02a.jpg',
            'photo2' => 'front/assets/images/product/02b.jpg',
            'gallery' => '',
            'size' => '["10","20"]',
            'quantity' => '["10","20"]',
            'color' => '["#000000","#ff0060","#FFC0CB"]',
        ]);
        Product::create([
            'user_id' => 1,
            'title' => 'A / B COMBI SHORTS',
            'description' => 'Pour les abonnés gratuits, il sagit simplement dun texte factice de lindustrie de limpression et de la composition. Lorem Ipsum est le texte factice standard de lindustrie depuis les années 1500, lorsquun imprimeur inconnu a pris une galère.',
            'price' => '72',
            'oldprice' => '100',
            'category' => '1',
            'category_id' => '5',
            'subcategory_id' => '2',
            'sku' => '109457',
            'photo1' => 'front/assets/images/product/03a.jpg',
            'photo2' => 'front/assets/images/product/03b.jpg',
            'gallery' => '',
            'size' => '["10","20"]',
            'quantity' => '["10","20"]',
            'color' => '["#000000","#ff0060","#FFC0CB"]',
        ]);
        Product::create([
            'user_id' => 1,
            'title' => 'A / B DRESS',
            'description' => 'Pour les abonnés gratuits, il sagit simplement dun texte factice de lindustrie de limpression et de la composition. Lorem Ipsum est le texte factice standard de lindustrie depuis les années 1500, lorsquun imprimeur inconnu a pris une galère.',
            'price' => '62',
            'oldprice' => '80',
            'category' => '1',
            'category_id' => '5',
            'subcategory_id' => '2',
            'sku' => '109459',
            'photo1' => 'front/assets/images/product/04a.jpg',
            'photo2' => 'front/assets/images/product/04b.jpg',
            'gallery' => '',
            'size' => '["10","20"]',
            'quantity' => '["10","20"]',
            'color' => '["#000000","#ff0060","#FFC0CB"]',
        ]);
        Product::create([
            'user_id' => 1,
            'title' => 'A / B COMBI TROUSERS',
            'description' => 'Pour les abonnés gratuits, il sagit simplement dun texte factice de lindustrie de limpression et de la composition. Lorem Ipsum est le texte factice standard de lindustrie depuis les années 1500, lorsquun imprimeur inconnu a pris une galère.',
            'price' => '89',
            'oldprice' => '100',
            'category' => '1',
            'category_id' => '5',
            'subcategory_id' => '2',
            'sku' => '109458',
            'photo1' => 'front/assets/images/product/05a.jpg',
            'photo2' => 'front/assets/images/product/05b.jpg',
            'gallery' => '',
            'size' => '["10","20"]',
            'quantity' => '["10","20"]',
            'color' => '["#000000","#ff0060","#FFC0CB"]',
        ]);
        Product::create([
            'user_id' => 1,
            'title' => 'A / B DRESS',
            'description' => 'Pour les abonnés gratuits, il sagit simplement dun texte factice de lindustrie de limpression et de la composition. Lorem Ipsum est le texte factice standard de lindustrie depuis les années 1500, lorsquun imprimeur inconnu a pris une galère.',
            'price' => '76',
            'oldprice' => '80',
            'category' => '1',
            'category_id' => '5',
            'subcategory_id' => '2',
            'sku' => '109450',
            'photo1' => 'front/assets/images/product/06a.jpg',
            'photo2' => 'front/assets/images/product/06b.jpg',
            'gallery' => '',
            'size' => '["10","20"]',
            'quantity' => '["10","20"]',
            'color' => '["#000000","#ff0060","#FFC0CB"]',
        ]);
    }
}
