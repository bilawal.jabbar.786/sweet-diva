<?php

use App\Banners;
use App\Category;
use App\GeneralSettings;
use App\MainCategory;
use App\Points;
use App\SaleProduct;
use App\SubCategory;
use Illuminate\Database\Seeder;

class GeneralSettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Points::create([
            'name' => 'Un cadeaux surprise',
            'points' => '300',
            'description' => 'Un cadeaux surprise'
        ]);
        Points::create([
            'name' => 'Livraison gratuite',
            'points' => '650',
            'description' => 'Livraison gratuite'
        ]);
        Points::create([
            'name' => 'Une reduction de 05 euros',
            'points' => '900',
            'description' => 'Une reduction de 05 euros'
        ]);
        Points::create([
            'name' => 'Une reduction de 10 euros',
            'points' => '1500',
            'description' => 'Une reduction de 10 euros'
        ]);
        Points::create([
            'name' => 'Une reduction de 50 euros',
            'points' => '3200',
            'description' => 'Une reduction de 50 euros'
        ]);
        Points::create([
            'name' => 'Une repas pour deux',
            'points' => '5300',
            'description' => 'Une repas pour deux'
        ]);

        SaleProduct::create([
            'product_id' => '1',
            'date' => 'June 15, 2021 12:00:00'
        ]);

        Category::create([
            'name' => 'Sweet Diva',
            'photo' => 'front/assets/images/category/haut.jpg'
        ]);
        Category::create([
            'name' => 'Working Girl',
            'photo' => 'front/assets/images/category/haut.jpg'
        ]);
        Category::create([
            'name' => 'Fashion Cod',
            'photo' => 'front/assets/images/category/haut.jpg'
        ]);

        MainCategory::create([
            'name' => 'Escarpin',
            'category_id' => '1',
            'photo' => 'front/assets/images/category/combipantalon.jpg',
            'banner' => 'front/assets/images/category/combipantalon.jpg'
        ]);
        MainCategory::create([
            'name' => 'Sandale',
            'category_id' => '1',
            'photo' => 'front/assets/images/category/haut.jpg',
            'banner' => 'front/assets/images/category/combipantalon.jpg'
        ]);
        MainCategory::create([
            'name' => 'Compensé',
            'category_id' => '1',
            'photo' => 'front/assets/images/category/ensemble.jpg',
            'banner' => 'front/assets/images/category/combipantalon.jpg'
        ]);
        MainCategory::create([
            'name' => 'Robe',
            'category_id' => '2',
            'photo' => 'front/assets/images/category/combishort.jpg',
            'banner' => 'front/assets/images/category/combipantalon.jpg'
        ]);
        MainCategory::create([
            'name' => 'Ensemble',
            'category_id' => '2',
            'photo' => 'front/assets/images/category/bas.jpg',
            'banner' => 'front/assets/images/category/combipantalon.jpg'
        ]);
        MainCategory::create([
            'name' => 'Combishort',
            'category_id' => '2',
            'photo' => 'front/assets/images/category/robe.jpg',
            'banner' => 'front/assets/images/category/combipantalon.jpg'
        ]);
        MainCategory::create([
            'name' => 'Jean',
            'category_id' => '2',
            'photo' => 'front/assets/images/category/robe.jpg',
            'banner' => 'front/assets/images/category/combipantalon.jpg'
        ]);

        SubCategory::create([
            'category_id' => '5',
            'name' => 'Jean'
        ]);
        SubCategory::create([
            'category_id' => '5',
            'name' => 'Pantalon'
        ]);
        SubCategory::create([
            'category_id' => '5',
            'name' => 'Short'
        ]);
        SubCategory::create([
            'category_id' => '5',
            'name' => 'Jupe'
        ]);
        SubCategory::create([
            'category_id' => '5',
            'name' => 'Legging'
        ]);
        SubCategory::create([
            'category_id' => '2',
            'name' => 'Top'
        ]);
        SubCategory::create([
            'category_id' => '2',
            'name' => 'Body'
        ]);
        SubCategory::create([
            'category_id' => '3',
            'name' => 'Pantalon'
        ]);
        SubCategory::create([
            'category_id' => '3',
            'name' => 'Short'
        ]);
        SubCategory::create([
            'category_id' => '3',
            'name' => 'Jupe'
        ]);


        GeneralSettings::create([
            'sitename'     => 'LPDF',
            'address'     => 'Magasin de Basse-Terre',
            'phone'     => '0590 310 035',
            'email'     => 'info@sweetdiva.com',
            'logo'     => 'front/assets/logo.png',
            'footer'    => 'à été créé en 1992. Il y a 2 responsables de Magasin (Grand-Camps et Basse-Terre). Maroquinerie (Sacs, chaussures, accessoires), 100% Cuir, fait main. Ce sont des séries limitées.',
            'facebook'    => 'https://facebook.com/',
            'instagram'    => 'https://www.instagram.com/',
            'twitter'    => 'https://www.twitter.com/',
            'about'    => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
        ]);
        Banners::create([
            'cat1' => 'front/assets/images/banner/03.jpg',
            'cat2' => 'front/assets/images/banner/03.jpg',
            'cat3' => 'front/assets/images/banner/03.jpg',
            'image1' => 'front/assets/images/slider/01.jpg',
            'image2' => 'front/assets/images/slider/02.jpg',
            'image3' => 'front/assets/images/slider/03.jpg',
            'image4' => 'front/assets/images/banner/04.jpg',
            'image5' => 'front/assets/images/banner/05.jpg',
            'image6' => 'front/assets/images/banner/06.jpg',
            'image7' => 'front/assets/images/banner/07.jpg',
            'image8' => 'front/assets/images/banner/08.jpg',
            'image9' => 'front/assets/images/banner/09.jpg',
            'banner1' => 'front/assets/images/banner/01.jpg',
            'banner2' => 'front/assets/images/banner/02.jpg',
            'banner3' => 'front/assets/images/banner/03.jpg',
            'banner4' => 'front/assets/images/banner/01.jpg',
            'link1' => '#',
            'link2' => '#',
            'link3' => '#',
            'link4' => '#',
            'link5' => '#',
            'videobanner' => 'front/assets/03.jpg',
            'videoid' => 'tgbNymZ7vqY',
            'vide1' => 'front/assets/14.jpeg',
            'vide2' => 'front/assets/14.jpeg',
            'vide3' => 'front/assets/14.jpeg',
        ]);
    }
}
