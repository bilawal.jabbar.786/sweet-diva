<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

//    0 = Admin
//2 = users
//4 = shop admins

    public function run()
    {
        User::create([
            'name' => 'Admin',
            'email' => 'admin@gmail.com',
            'role' => '0',
            'phone' => '00000000',
            'address' => 'France',
            'city' => 'Guadeloupe',
            'country' => 'France',
            'password' => Hash::make('12345678'),
        ]);
        User::create([
            'name' => 'User',
            'email' => 'user@gmail.com',
            'role' => '2',
            'phone' => '00000000',
            'address' => 'France',
            'city' => 'Guadeloupe',
            'country' => 'France',
            'password' => Hash::make('12345678'),
        ]);
        User::create([
            'name' => 'Vendor',
            'email' => 'vendor@gmail.com',
            'role' => '2',
            'phone' => '00000000',
            'address' => 'France',
            'city' => 'Guadeloupe',
            'country' => 'France',
            'password' => Hash::make('12345678'),
        ]);


        User::create([
            'name' => 'SWEET DIVA MARTINIQUE',
            'email' => 'martinique@gmail.com',
            'role' => '4',
            'phone' => '+596690408160',
            'address' => 'Espace colibri rue Emanuel courant 97232 Le Lamentin
Martinique ',
            'city' => 'Martinique',
            'country' => 'Martinique',
            'photo' => 'front/assets/images/03.jpeg',
            'password' => Hash::make('12345678'),
            'shop' => 'SWEET DIVA MARTINIQUE',
        ]);
        User::create([
            'name' => 'SWEET DIVA BESSON',
            'email' => 'besson@gmail.com',
            'role' => '4',
            'phone' => '+590690277055',
            'address' => '2142 route de cocoyer Besson 97190 Le Gosier
Guadeloupe ',
            'city' => 'Guadeloupe',
            'country' => 'Guadeloupe',
            'photo' => 'front/assets/images/02.jpeg',
            'password' => Hash::make('12345678'),
            'shop' => 'SWEET DIVA BESSON',
        ]);
        User::create([
            'name' => 'SWEET DIVA BASSE TERRE',
            'email' => 'basseterre@gmail.com',
            'role' => '4',
            'phone' => '+00000000',
            'address' => 'SWEET DIVA BASSE TERRE',
            'city' => 'Guadeloupe',
            'country' => 'Guadeloupe',
            'photo' => 'front/assets/images/02.jpeg',
            'password' => 'SWEET DIVA BASSE TERRE',
        ]);
    }
}
