<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCardPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('card_payments', function (Blueprint $table) {
            $table->id();
            $table->integer('card_id');
            $table->integer('card_number');
            $table->string('sendername');
            $table->string('senderphone');
            $table->string('r_name');
            $table->string('r_email');
            $table->longText('message');
            $table->string('price');
            $table->integer('paymentstatus')->default('0');
            $table->integer('valid')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('card_payments');
    }
}
