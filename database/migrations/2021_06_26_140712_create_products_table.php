<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();

            $table->integer('category')->nullable();

            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->unsignedBigInteger('category_id')->nullable();
            $table->foreign('category_id')->references('id')->on('main_categories')->onDelete('cascade');

            $table->unsignedBigInteger('subcategory_id')->nullable();
            $table->foreign('subcategory_id')->references('id')->on('sub_categories')->onDelete('cascade');

            $table->string('title');
            $table->bigInteger('price');
            $table->string('oldprice')->nullable();
            $table->string('sku');
            $table->string('photo1');
            $table->string('photo2')->nullable();
            $table->string('material')->nullable();

            $table->longText('gallery')->nullable();

            $table->longText('colorimage')->nullable();

            $table->string('status')->nullable();
            $table->longText('description')->nullable();
            $table->longText('size')->nullable();
            $table->longText('color')->nullable();
            $table->longText('quantity')->nullable();

            $table->longText('state')->nullable();
            $table->longText('sizesimple')->nullable();
            $table->longText('colorsimple')->nullable();
            $table->longText('vendorcategory')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
