<?php

namespace App\Exports;

use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;

class UsersExport implements FromCollection
{
    public function __construct($country) {
        $this->country = $country;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        if ($this->country == 'All'){
            return User::where('role', '=', '2')->select('id', 'name', 'email', 'phone', 'address', 'country', 'points', 'wallet')->get();
        }else{
            return User::where('role', '=', '2')->where('country', $this->country)->select('id', 'name', 'email', 'phone', 'address', 'country', 'points', 'wallet')->get();
        }
    }
}
