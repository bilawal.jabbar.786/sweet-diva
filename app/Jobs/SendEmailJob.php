<?php

namespace App\Jobs;

use App\Mail\Facture;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $order, $gs, $file;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($order, $gs, $filepath)
    {
        $this->order = $order;
        $this->gs = $gs;
        $this->file = $filepath;
    }
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->order->email)
            ->send(new Facture($this->order, $this->gs, $this->file));
    }
}
