<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VendorOrder extends Model
{
    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }
    public function vendor()
    {
        return $this->belongsTo(User::class, 'vendor_id');
    }

}
