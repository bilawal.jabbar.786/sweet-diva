<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Facture extends Mailable
{
    use Queueable, SerializesModels;

    protected $order, $gs, $file;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($order, $gs, $filepath)
    {
        $this->order = $order;
        $this->gs = $gs;
        $this->file = $filepath;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('contact@lepalaisdesfemmes.com', 'Le Palais Des Femmes')
            ->view('email.invoice')
            ->with([
            'invoice' => $this->order,
            'gs' => $this->gs,
            ])
            ->attach(public_path($this->file));
    }
}
