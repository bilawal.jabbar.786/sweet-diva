<?php

namespace App\Http\Controllers\Front;

use App\Banners;
use App\CardPayments;
use App\Cards;
use App\ChMessage;
use App\Countries;
use App\GeneralSettings;
use App\Http\Controllers\Controller;
use App\Http\PointsHelper;
use App\Jobs\SendEmailJob;
use App\Mail\BonCadeau;
use App\MainCategory;
use App\Newsletter;
use App\Order;
use App\Product;
use App\Refferal;
use App\SaleProduct;
use App\Size;
use App\Sms;
use App\SubCategory;
use App\User;
use App\VendorOrder;
use App\Wishlist;
use Carbon\Carbon;
use Cart;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use PDF;
use Session;
use Stripe\Charge;
use Stripe\PaymentIntent;
use Stripe\Stripe;
use Twilio\Rest\Client;

class FrontendController extends Controller
{
    public function indexold()
    {
        $gs = GeneralSettings::find(1);
        return view('commingsoon', compact('gs'));
    }

    public function index()
    {
        $banners = Banners::find(1);
        $categories = MainCategory::all();
        $adminproducts = Product::orderBy('id', 'DESC')->where('draft', '=', 1)->where('user_id', '=', '1')->take(8)->get();
        $sellerproducts = Product::orderBy('id', 'DESC')->where('draft', '=', 1)->where('user_id', '!=', '1')->get();
        $saleproduct = SaleProduct::find(1);
        $arr = [3, 4, 5];
        $shops = User::whereIn('role', $arr)->get();
        $gs = GeneralSettings::find(1);
        $cards = Cards::take(3)->get();
        return view('front.index', compact('gs', 'banners', 'categories', 'adminproducts', 'sellerproducts', 'saleproduct', 'shops', 'cards'));

    }

    public function products()
    {
        $products = Product::orderBy('id', 'DESC')->where('draft', '=', 1)->where('user_id', '=', '1')->paginate(12);
        return view('front.products', compact('products'));
    }

    public function smssave(Request $request)
    {
        $sms = new Sms();
        $user = Auth::user();
        $sms->phone = $user->phone;
        $sms->email = $user->email;
        $sms->size = $request->size;
        $sms->product_id = $request->product_id;
        $sms->save();
        $notification = array(
            'messege' => 'Votre numéro de téléphone est ajouté avec succès. Nous vous préviendrons lorsquil sera disponible!',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }

    public function about()
    {
        $gs = GeneralSettings::find(1);
        return view('front.about', compact('gs'));
    }

    public function adminlogin()
    {
        return view('admin.login');
    }

    public function product($id)
    {
        $product = Product::find($id);
        if ($product) {
            return view('front.product', compact('product'));
        } else {
            $notification = array(
                'messege' => 'Produit non disponible',
                'alert-type' => 'error'
            );
            return redirect()->back()->with($notification);
        }
    }

    public function addtocart(Request $request)
    {
        $product = Product::where('id', $request->id)->first();
        $size = json_decode($product->size);
        $color = json_decode($product->color);
        Cart::add($product->id, $product->title, $product->price, 1, array('type' => 1, 'size' => empty($size) ? 0 : $size[0], 'color' => empty($color) ? 0 : $color[0]));
        return response()->json($product);
    }

    public function productaddtocart(Request $request)
    {
        $cartitems = Cart::getContent();
        foreach ($cartitems as $item) {
            if ($item->id == $request->id) {
                $notification = array(
                    'messege' => 'Le produit existe déjà dans le panier !',
                    'alert-type' => 'error'
                );
                return redirect()->back()->with($notification);
            }
        }
        $quantity = (int)$request->quantity;
        $product = Product::where('id', $request->id)->first();
        Cart::add($product->id, $product->title, $product->price, $quantity, array('type' => 1, 'size' => empty($request->size) ? 0 : $request->size, 'color' => empty($request->color) ? 0 : $request->color));
        if ($request->type == '1') {
            return redirect()->route('app.cart');
        } else {
            return redirect()->route('front.cart');
        }

    }

    public function cartitems()
    {
        $cartitems = Cart::getContent();
        foreach ($cartitems as $row) {
            $product = Size::where('product_id', '=', $row->id)->where('color', '=', $row->attributes->color)->first();
            $quantity = json_decode($product->quantity);
            foreach (json_decode($product->newsize, true) as $key => $value) {
                if ($value == $row->attributes->size) {
                    if ($quantity[$key] <= 0) {
                        Cart::remove($row->id);
                    }
                }
            }
        }
        return response()->json($cartitems);
    }

    public function removecart(Request $request)
    {
        Cart::remove($request->id);
        $product = Product::where('id', '=', $request->id)->first();
        return response()->json($product);
    }

    public function removeCartGet($id)
    {
        Cart::remove($id);
        return redirect()->back();
    }

    public function cart()
    {
        return view('front.cart');
    }

    public function checkstock(Request $request)
    {
        $product = Product::find($request->id);
        $quantity = json_decode($product->quantity);

        $sizes = Size::where('product_id', $request->id)->where('color', $request->color)->first();
        if ($sizes) {
            $size = json_decode($sizes->newsize);
            return response()->json(['success' => '1', 'size' => $size]);
        } else {
            return response()->json(['success' => '1', 'size' => 0]);
        }
    }

    public function checksizestock(Request $request)
    {
        $sizes = Size::where('product_id', $request->id)->where('color', $request->color)->first();
        $quantity = json_decode($sizes->quantity);
        foreach (json_decode($sizes->newsize, true) as $key => $value) {
            if ($value == $request->size) {
                if ($quantity[$key] > 0) {
                    return response()->json(['success' => '1', 'quantity' => $quantity[$key]]);
                } else {
                    return response()->json(['success' => '0', 'quantity' => $quantity[$key]]);
                }
            }
        }
    }

    public function category($id)
    {
        $maincategory = MainCategory::where('category_id', $id)->get();
        $products = Product::latest()->where('category', $id)->where('draft', '=', 1)->paginate(12);
        return view('front.category', compact('maincategory', 'products'));
    }

    public function maincategory($id)
    {
        $photo = MainCategory::where('id', $id)->first();
        $maincategory = SubCategory::where('category_id', $id)->get();
        $products = Product::latest()->where('draft', '=', 1)->where('category_id', $id)->paginate(12);
        return view('front.maincategory', compact('maincategory', 'products', 'photo'));
    }

    public function subcategory($id)
    {
        $products = Product::where('subcategory_id', $id)->where('draft', '=', 1)->get();
        return view('front.search', compact('products'));
    }

    public function addtowishlist(Request $request)
    {
        $wishlist = Wishlist::where('user_id', Auth::user()->id)->where('product_id', $request->id)->first();
        if (!$wishlist) {
            $wish = new Wishlist();
            $wish->user_id = Auth::user()->id;
            $wish->product_id = $request->id;
            $wish->save();
        }
        return response()->json(['success' => '1']);
    }

    public function vendorproduct($id)
    {
        $product = Product::find($id);
        return view('front.vendorproduct', compact('product'));
    }

    public function checkout()
    {
        $cartitems = Cart::getContent();
        $countries = Countries::all();
        $total = Cart::getTotal();
        $gs = GeneralSettings::find(1);
        /*  if (auth()->user()->country == "Guadeloupe" || auth()->user()->country == "Martinique" || auth()->user()->country == "French Guiana") {
              $shipping = 5;
          } else {
              $shipping = 10;
          }
          if (isset($total)) {
              \Stripe\Stripe::setApiKey(env('STRIPE_SECRET_KEY'));
              $payment_intent = \Stripe\PaymentIntent::create([
                  'amount' => ($total + $shipping) * 100,
                  'currency' => 'EUR',
                  'automatic_payment_methods' => [
                      'enabled' => true,
                  ],
              ]);
          }

          $intent = $payment_intent->client_secret;*/
        return view('front.checkout', compact('cartitems', 'countries', 'total', 'gs'));
    }

    public function gettotal()
    {
        $cartTotalQuantity = Cart::getTotalQuantity();
        $total = Cart::getTotal();
        return response()->json(['quantity' => $cartTotalQuantity, 'total' => $total]);
    }

    public function checkoutSubmit(Request $request)
    {
        $cartitems = Cart::getContent();

        $order = new Order();
        $order->ip = request()->ip();
        $order->user_id = $request->user_id;
        $order->name = $request->name;
        $order->email = $request->email;
        $order->phone = $request->phone;
        $order->resource = 'Site Internet';
        $order->address = $request->house . ' ' . $request->street . ' ' . $request->address . ' ' . $request->city . ' ' . $request->country;

        $order->shipping = $request->shipping;
        $order->notes = $request->notes;
        $order->country = $request->country;
        $order->total = $request->finalamount;
        $order->products = $cartitems;

        $order->order_number = "ON-" . rand(100000000, 900000000);
        $order->save();

        if ($request->pay == "installments") {
            $order->payment_method = 'ALMA';
            $order->save();

            $apiKey = env("ALMA_KEY", "sk_test_4GmA9z5tQOKOUiI4IEIIEO69");
            $data = [
                "payment" => [
                    "purchase_amount" => $request->finalamount * 100,
                    "return_url" => route('payment.success', ['id' => $order->id, 'type' => 'order']),
                    "ipn_callback_url" => route('application.payment.success', ['id' => $order->id, 'type' => 'order', 'user_id' => $request->user_id]),
                    "shipping_address" => [
                        "first_name" => $request->name,
                        "last_name" => $request->surname,
                        "line1" => $request->address,
                        "postal_code" => $request->cp,
                        "city" => $request->city,
                    ],
                    "locale" => "fr",
                ],

            ];
            $dataString = json_encode($data);

            $headers = [
                'Authorization: Alma-Auth ' . $apiKey,
                'Content-Type: application/json',
            ];

            $ch = curl_init();
            if (env("ALMA_ENV") == "sandbox") {
                curl_setopt($ch, CURLOPT_URL, 'https://api.sandbox.getalma.eu/v1/payments');
            } else {
                curl_setopt($ch, CURLOPT_URL, 'https://api.getalma.eu/v1/payments');
            }
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);

            $response1 = curl_exec($ch);
            $response = json_decode($response1);

            return redirect($response->url);

        } else if ($request->pay == "complete") {
            $order->payment_method = 'Stripe';
            $order->save();
            return redirect()->route('front.stripe', ['price' => base64_encode($request->finalamount), 'id' => $order->id]);
//            return $this->paymentsuccess($order->id, 'order');
        } else if ($request->pay == "pad") {
            $order->payment_method = 'pad';
            $order->save();
            $id = $order->id;
            $cartitems = Cart::getContent();
            $del = Cart::getTotal();
            $total = $order->total;

            return view('front.padcheckout', compact('cartitems', 'total', 'id', 'del'));

        } else {
            dd("nothing");
        }


        /* $payment = Mollie::api()->payments->create([
             "amount" => [
                 "currency" => "GBP",
                 "value" => number_format($request->finalamount, 2) // You must send the correct number of decimals, thus we enforce the use of strings
             ],
             "description" => "Order #". $order->id,
             "redirectUrl" => route('payment.success', ['id' => $order->id, 'type' => 'order']),
             "metadata" => [
                 "order_id" => $order->id,
             ],
         ]);
         return redirect($payment->getCheckoutUrl(), 303);*/
    }

    public function successMessage($id)
    {
        $order = Order::find($id);
        Cart::clear();
        return $this->paymentsuccess($order->id, 'order');

    }

    public function paymentsuccess($id, $type)
    {
        $order = Order::find($id);
        $order->paymentstatus = '1';
        $order->invoice = '050' . $id;
        $order->update();
        $gs = GeneralSettings::find(1);

        $user = Auth::user();
        $check_is_refferal = Refferal::where('user_id', $user->id)->first();
        if ($check_is_refferal) {
            $ref_user = User::where('id', $check_is_refferal->refferal_user)->first();
            $check_order = Order::where('user_id', $user->id)->get();
            if ($check_order->count() == 1) {
                PointsHelper::pushPoints($ref_user->id, '50', 'Se référer à', 'Ajouter');
                $ref_user->points = $ref_user->points + 50;
                $ref_user->update();
            }
        }
        foreach (json_decode($order->products) as $item) {
            if ($item->attributes->type == 0) {
                $new = new CardPayments();
                $new->card_id = $item->id;
                $new->sendername = $item->attributes->sendername;
                $new->senderphone = $item->attributes->senderphone;
                $new->r_name = $item->attributes->r_name;
                $new->r_email = $item->attributes->r_email;
                $new->message = $item->attributes->message;
                $new->price = $item->price;
                $new->paymentstatus = '1';
                $new->card_number = rand(100000000, 900000000);
                $new->save();
                $cards = Cards::where('sku', $item->id)->first();
                Mail::to($new->r_email)->send(new BonCadeau($new, $cards));
            } else {
                $product = Product::find($item->id);
                if ($product) {
                    $checkorder = VendorOrder::where('vendor_id', $product->user_id)->where('order_id', $order->id)->first();
                    if (!$checkorder) {
                        if ($product->user_id == '1') {
                            $sizeproduct = Size::where('product_id', $item->id)->where('color', $item->attributes->color)->first();
                            $quantity = json_decode($sizeproduct->quantity);
                            $size = json_decode($sizeproduct->newsize);
                            foreach ($size as $key => $value) {
                                if ($value == $item->attributes->size) {
                                    $stock = $quantity[$key] - $item->quantity;
                                    $quantity[$key] = (string)$stock;
                                    $sizeproduct->quantity = $quantity;
                                    $sizeproduct->update();
                                }
                            }
                        }
                        $product->status = '1';
                        $product->update();
                    }
                }
            }
        }
        $check = VendorOrder::where('vendor_id', '1')->where('order_id', $order->id)->first();
        if (!$check) {
            $vendororder = new VendorOrder();
            $vendororder->vendor_id = 1;
            $vendororder->order_id = $order->id;
            if ($order->payment_method == 'pad') {
                $vendororder->status = 2;
            }
            $vendororder->save();
            $user->points = $user->points + $order->total;
            $user->update();
            PointsHelper::pushPoints($user->id, $order->total, 'Ordre', 'Ajouter');
            if (env('APP_ENV') == 'production') {
                $data1 = [
                    'invoice' => $order,
                    'gs' => $gs
                ];
                $destinationPath = 'records';
                $taxform_name = 'facture-' . $order->order_number . '.pdf';
                $filepath = $destinationPath . '/' . $taxform_name;
                $pdf = PDF::loadView('myPDF', $data1);
                $pdf->stream();
                file_put_contents($filepath, $pdf->output());
                dispatch(new SendEmailJob($order, $gs, $filepath))->delay(Carbon::now()->addSeconds(5));
                $account_sid = 'AC96ac1afdd55310f2214c9bf9b928f326';
                $auth_token = '2776de46ab4e9752bb8d9ec9deef2074';
                $client = new Client($account_sid, $auth_token);
                $client->messages->create(
                    "+590690574700",
                    array(
                        'from' => 'Sweet Diva',
                        'body' => 'Nouvelle commande # ' . $order->order_number . '
Détails du client:
' . $order->name . '
' . $order->email . '
' . $order->phone . '
' . $order->address . '
' . $order->country . '
' . $order->payment_method . '
' . $order->total . '
Voir la commande
' . route('order.view', ['id' => $order->id]) . '
Daté: ' . $order->created_at . '
                ',
                    )
                );
                $client->messages->create(
                // the number you'd like to send the message to
                    "+590690399802",
                    array(
                        'from' => 'Sweet Diva',
                        'body' => 'Nouvelle commande # ' . $order->order_number . '
Détails du client:
' . $order->name . '
' . $order->email . '
' . $order->phone . '
' . $order->address . '
' . $order->country . '
' . $order->payment_method . '
' . $order->total . '
Voir la commande
' . route('order.view', ['id' => $order->id]) . '
Daté: ' . $order->created_at . '
                ',
                    )
                );
            }
            Cart::clear();
        }

        return view('front.paymentsuccess', compact('order'));

    }

    public function sendMessage(Request $request)
    {
        $messageID = mt_rand(9, 999999999) + time();
        $message = new ChMessage();
        $message->id = $messageID;
        $message->type = 'user';
        $message->from_id = Auth::user()->id;
        $message->to_id = $request->vendor_id;
        $message->body = $request->message;
        $message->save();
        $notification = array(
            'messege' => 'Message envoyé avec succès !',
            'alert-type' => 'success'
        );
        return redirect()->route('chatify')->with($notification);
    }

    public function search(Request $request)
    {
        $products = Product::where('title', 'like', '%' . $request->name . '%')->where('draft', '=', 1)->where('user_id', '1')->get();
        return view('front.search', compact('products'));
    }

    public function thisMonth()
    {
        $products = Product::where('month', 1)->where('draft', '=', 1)->where('user_id', '1')->get();
        return view('front.search', compact('products'));
    }

    public function sellersProducts()
    {
        $banners = Banners::find(1);
        $sellersProducts = Product::where('user_id', '!=', '1')->where('draft', '=', 1)->where('status', null)->get();
        if (Auth::user()) {
            $orders = Order::where('user_id', Auth::user()->id)->where('paymentstatus', '1')->where('status', '0')->first();
            return view('front.sellersProducts', compact('sellersProducts', 'banners', 'orders'));
        }
        return view('front.sellersProducts', compact('sellersProducts', 'banners'));
    }

    public function vendorcategories($category)
    {
        $banners = Banners::find(1);
        $sellersProducts = Product::where('vendorcategory', '=', $category)->where('draft', '=', 1)->get();
        return view('front.sellersProducts', compact('sellersProducts', 'banners'));
    }

    public function newsletter(Request $request)
    {
        $newsletter = new Newsletter();
        $newsletter->name = $request->name;
        $newsletter->email = $request->email;
        $check = Newsletter::where('email', '=', $request->email)->first();
        if (!$check) {
            $newsletter->save();
        }
        Session::flash('success', 'Demande soumise avec succès');
        return redirect()->back();
    }

    public function pricefilter(Request $request)
    {
        $start = Str::substr($request->price, 1, 2);
        $end = Str::substr($request->price, 7, 10);
        $products = Product::where('user_id', '=', '1')->where('draft', '=', 1)->whereBetween('price', [(int)$start, (int)$end])->get();
        return view('front.search', compact('products'));
    }

    public function cards()
    {
        $cards = Cards::orderBy('id', 'DESC')->get();
        return view('front.cards', compact('cards'));
    }

    public function cardview($id)
    {
        $card = Cards::find($id);
        return view('front.cardview', compact('card'));
    }

    public function stripe($price, $id)
    {
        return view('front.stripe', compact('price', 'id'));
    }

    public function getStripeIntent(Request $request)
    {
        Stripe::setApiKey(env('STRIPE_SECRET_KEY'));
        $order = Order::find($request->id);
        try {
            $paymentIntent = PaymentIntent::create([
                'amount' => $request->price * 100,
                'currency' => 'eur',
                'automatic_payment_methods' => [
                    'enabled' => true,
                ],
                'metadata' => [
                    'order_id' => $order->order_number,
                    'Customer Name' => $order->name,
                    'Customer Email' => $order->email,
                    'Customer Phone' => $order->phone,
                    'Customer Address' => $order->address,
                ],
            ]);

            $output = [
                'clientSecret' => $paymentIntent->client_secret,
            ];

            echo json_encode($output);
        } catch (Error $e) {
            http_response_code(500);
            echo json_encode(['error' => $e->getMessage()]);
        }
    }

    public function cardpay(Request $request, $id)
    {

        $card = Cards::find($id);

        Cart::add($card->sku, $card->title, $request->price, 1, array('size' => 0,
            'color' => 0,
            'type' => 0,
            'sendername' => $request->sendername,
            'senderphone' => $request->senderphone,
            'r_name' => $request->name,
            'r_email' => $request->email,
            'message' => $request->message,
        ));

        return redirect()->route('front.cart');
    }

    public function stripesubmit(Request $request)
    {
        Stripe::setApiKey(env('STRIPE_SECRET_KEY'));
        try {
            $stripe = Charge::create(array(
                "amount" => $request->price * 100,
                "currency" => "eur",
                "source" => $request->input('stripeToken'), // obtained with Stripe.js
                "description" => "Paiement par carte " . Auth::user()->name . " " . Auth::user()->surname . " " . Auth::user()->email
            ));
            $order = Order::find($request->id);
            $order->transaction_id = $stripe->id;
            $order->save();

            return $this->paymentsuccess($request->id, 'order');

        } catch (Exception $e) {
            Session::flash('fail-message', "Erreur! Veuillez réessayer. " . $e->getMessage());
            return redirect()->back();
        }
    }

    public function legal()
    {
        $gs = GeneralSettings::find(1);
        return view('front.legal', compact('gs'));
    }

    public function terms()
    {
        $gs = GeneralSettings::find(1);
        return view('front.terms', compact('gs'));
    }

    public function paymentsuccess2($id)
    {
        $order = Order::find($id);

        $order->paymentstatus = '2';
        $order->update();
        $vendor_order = VendorOrder::where('order_id', $id)->first();
        $vendor_order->status = '3';
        $vendor_order->update();

        foreach (json_decode($order->products) as $item) {

            $product = Product::find($item->id);

            if ($product) {
                $checkorder = VendorOrder::where('vendor_id', $product->user_id)->where('order_id', $order->id)->first();

                if ($checkorder) {

                    if ($product->user_id == '1') {

                        $sizeproduct = Size::where('product_id', $item->id)->where('color', $item->attributes->color)->first();

                        $quantity = json_decode($sizeproduct->quantity);
                        $size = json_decode($sizeproduct->newsize);
                        foreach ($size as $key => $value) {
                            if ($value == $item->attributes->size) {
                                $stock = $quantity[$key] + $item->quantity;
                                $quantity[$key] = (string)$stock;
                                $sizeproduct->quantity = $quantity;
                                $sizeproduct->update();
                            }
                        }
                    }
                    $product->status = '1';

                    $product->update();


                    $user = User::where('id', $order->user_id)->first();
                    $user->points = $user->points - $product->price;
                    $user->update();

                }
            }
        }
        return redirect()->back();

    }
}
