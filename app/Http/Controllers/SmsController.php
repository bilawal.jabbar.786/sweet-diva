<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\ModeleSms;
use Illuminate\Support\Facades\Http;

class SmsController extends Controller
{
  public function send_message(){
    $users = User::all();
    return view('admin.send_sms.send', compact('users'));
  }
  public function send(Request $request){

    $send = $this->SendSMS($request->message,$request->numbers);
    $notification = array(
        'messege' => 'Message envoyé avec succès !',
        'alert-type' => 'success'
    );
    return redirect()->back()->with($notification);
  }

  public function template(){
    $modeles_dispo = ModeleSms::all();
    return view('admin.modele_sms.template', compact('modeles_dispo'));
  }
  public function edit_modele($id){
    $modele = ModeleSms::find($id);
    return view('admin.modele_sms.edit', compact('modele'));
  }
  public function update_modele(Request $request,$id){
    $modele = ModeleSms::find($id);
    $modele->name = $request->name;
    $modele->message = $request->message;
    $modele->update();
    $notification = array(
        'messege' => 'Modèle modifié avec succès !',
        'alert-type' => 'success'
    );
    return redirect()->back()->with($notification);
  }

  public function SendSMS($message, $numbers){
    // Message from database
    //$message = ModeleSms::find(1);
    //Send an SMS using Gatewayapi.com
    $url = "https://gatewayapi.com/rest/mtsms?";
    $api_token = "OnZVuhq-Sqa4_fsh8eHLQfvItbnCLvLGeR_ss_kpLwUOw9f4JmzdSTk73kUwPBvC";

    //Set SMS recipients and content
    $recipients = $numbers;
    $json = [
      'sender' => 'LPDF',
      'message' => $message,
      'recipients' => [],
    ];
    foreach ($recipients as $msisdn) {
      $json['recipients'][] = ['msisdn' => $msisdn];
    }
    //return $url.'token='.$api_token;
    return Http::post($url.'token='.$api_token, $json);
  }
}
