<?php

namespace App\Http\Controllers;

use App\CardPayments;
use App\Order;
use App\Product;
use App\User;
use App\VendorOrder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (Auth::user()->role == 4) {
            $website_users = User::where('role', '=', '2')->where('shop', Auth::user()->shop)->where('device_token','=',null)->get()->count();
            $app_users = User::where('role', '=', '2')->where('shop', Auth::user()->shop)->where('device_token','!=',null)->get()->count();
        } else {
            $website_users = User::where('role', '2')->where('device_token','=',null)->get()->count();
            $app_users = User::where('role', '2')->where('device_token','!=',null)->get()->count();
        }
        $orders = VendorOrder::where('vendor_id', '=', '1')->where('status', '2')->pluck('order_id');
        $sum = Order::whereIn('id',$orders)->pluck('total')->sum();

        $adminproducts = Product::where('user_id', '=', '1')->get()->count();
        $videdressingproducts = Product::where('user_id', '!=', '1')->get()->count();
        $adminordersnew = VendorOrder::where('vendor_id', '1')->where('status', '1')->get()->count();
        $videdressingordersnew = VendorOrder::where('vendor_id', '!=', '1')->where('status', '1')->get()->count();
        $gitcard = CardPayments::where('paymentstatus', '1')->get()->count();
        return view('home', compact(
            'website_users',
            'adminproducts',
            'videdressingproducts',
            'adminordersnew',
            'videdressingordersnew',
            'gitcard','app_users','sum'
        ));
    }
    public function testnotification(){
        $firebaseToken = User::whereNotNull('device_token')->pluck('device_token')->all();
       //new api key
        $SERVER_API_KEY = 'AAAABLTbGY8:APA91bFyvZKIrmv24oXXIDaNRh7dUoa77-VybgacttgG3015yGi7b1EMNO11g4mRWUhDISe6UX-lDQswJZ-T0cGuAG8rdY498mEPX2qAVWuEBSTNAbYnD_cYZTavXJJswVhaM5bA1wgy';

        //        old api key

//        $SERVER_API_KEY = 'AAAArlXKlA8:APA91bEkv1d8JssqvZizT9hWUe-xHK6fRGqQeZvHNKemQtzdCs8bjzVH3eo5Mv1BLPI1RUYGTwtxW8kya2Rjpf069jmHOKx0BmF-3xz-A5e0c_OZnt1GVhMSjkJSlUsV2-ST70HG-Ud9';

        $data = [
            "registration_ids" => $firebaseToken,
            "notification" => [
                "title" => "Sweet Diva",
                "body" => "Bienvenue, dans la nouvelle application sweet diva.",
            ]
        ];
        $dataString = json_encode($data);

        $headers = [
            'Authorization: key=' . $SERVER_API_KEY,
            'Content-Type: application/json',
        ];

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);

        $response = curl_exec($ch);
        dd($response);
    }

}
