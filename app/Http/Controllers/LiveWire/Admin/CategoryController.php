<?php

namespace App\Http\Controllers\LiveWire\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\MainCategory;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
       return view('livewire.maincategory.index');
    }
}
