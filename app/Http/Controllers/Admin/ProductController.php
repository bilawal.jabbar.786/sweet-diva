<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\Product;
use App\Size;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Image;

class ProductController extends Controller
{
    public function index()
    {
        $query = Product::orderBy('id', 'DESC')->where('draft', '=', 1)->where('user_id', '=', '1');
        $allProducts = $query->get();
        $products = $query->paginate(10);
        return view('admin.product.index', compact('products', 'allProducts'));
    }

    public function filterProduct($id)
    {
        return redirect()->route('product.edit', $id);
    }

    public function indexDraft()
    {
        $query = Product::orderBy('id', 'DESC')->where('draft', '=', 2)->where('user_id', '=', '1');
        $allProducts = $query->get();
        $products = $query->paginate(10);
        return view('admin.product.index', compact('products', 'allProducts'));
    }

    public function productPublish($id)
    {
        $product = Product::find($id);
        $product->draft = 1;
        $product->update();
        $notification = array(
            'messege' => 'Publiez votre produit!',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }

    public function create()
    {
        $categories = Category::all();
        return view('admin.product.create', compact('categories'));
    }

    public function store(Request $request)
    {
        $product = new Product();
        $product->category = $request->category;
        $product->title = $request->title;
        $product->user_id = Auth::user()->id;
        $product->category_id = $request->category_id;
        $product->subcategory_id = $request->subcategory_id;
        $product->price = $request->price;
        $product->oldprice = $request->oldprice;
        $product->sku = $request->sku;
        $product->description = $request->description;
        if ($request->draft) {
            $product->draft = $request->draft;
        }
        $product->state = $request->state;
        $product->sizesimple = $request->sizesimple;
        $product->material = $request->material;
        $product->vendorcategory = $request->vendorcategory;
        $product->label_name = $request->label_name;
        $product->background_color = $request->background_color;
        $product->text_color = $request->text_color;
        if ($request->size) {
            foreach ($request->size as $size) {
                $data[] = $size;
                $product->size = json_encode($data);
            }
        }
        if ($request->quantity) {
            foreach ($request->quantity as $quantity) {
                $data1[] = $quantity;
                $product->quantity = json_encode($data1);
            }
        }
        if ($request->color) {
            foreach ($request->color as $color) {
                $data2[] = $color;
                $product->color = json_encode($data2);
            }
        }
        if ($request->month == 1) {
            $product->month = 1;
        } else {
            $product->month = 0;
        }
        if ($request->sold == 1) {
            $product->sold = 1;
        } else {
            $product->sold = 0;
        }
        if ($request->top_sweet == 1) {
            $product->top_sweet = 1;
        } else {
            $product->top_sweet = 0;
        }
        if ($request->top_10 == 1) {
            $product->top_10 = 1;
        } else {
            $product->top_10 = 0;
        }
        if ($request->hasfile('photo1')) {
            $image1 = $request->file('photo1');
            $name1 = time() . 'photo1' . '.' . $image1->getClientOriginalExtension();
            $destinationPath = 'product';
            ini_set('memory_limit', '256M');
            $img = Image::make($image1);
            $img->resize(1080, 1080)->save($destinationPath . '/' . $name1);
            $product->photo1 = $destinationPath . '/' . $name1;
        }
        if ($request->hasfile('gallery')) {
            foreach ($request->file('gallery') as $image) {
                $name = time() . 'gallery' . '.' . $image->getClientOriginalName();
                $destinationPath = 'gallery';
                ini_set('memory_limit', '256M');
                $img = Image::make($image);
                $img->resize(1080, 1080)->save($destinationPath . '/' . $name);
                $data9[] = $name;
                $product->gallery = json_encode($data9);
            }
        }
        if ($request->hasfile('colorimage')) {
            foreach ($request->file('colorimage') as $image4) {
                $name4 = time() . 'colorimage' . '.' . $image4->getClientOriginalName();
                $destinationPath = 'colorimage';
                ini_set('memory_limit', '256M');
                $img = Image::make($image4);
                $img->resize(1080, 1080)->save($destinationPath . '/' . $name4);
                $data10[] = $name4;
                $product->colorimage = json_encode($data10);
            }
        }
//        if($request->hasfile('photo1') ){
//            $image6 = $request->file('photo1');
//            $name6 = time() . 'img' . '.' . $image6->getClientOriginalExtension();
//            $destinationPath = 'product-images/';
//            $image6->move($destinationPath, $name6);
//            $product->photo1 = 'product-images/' . $name6;
//            $path6 = public_path('product-images/'.$name6);
//            Image::make($path6)->resize(1080, 1080)->save($path6);
//        }
//        if ($request->hasfile('gallery')) {
//            foreach ($request->file('gallery') as $image6) {
//                $name6 = time() . 'img' . '.' . $image6->getClientOriginalExtension();
//                $destinationPath = 'product-images/';
//                $image6->move($destinationPath, $name6);
//                $data9[] = 'product-images/' . $name6;
//                $path6 = public_path('product-images/'.$name6);
//                Image::make($path6)->resize(1080, 1080)->save($path6);
//            }
//            $product->gallery = json_encode($data9);
//        }
//        if ($request->hasfile('colorimage')) {
//            foreach ($request->file('colorimage') as $image7) {
//                $name7 = time() . 'img' . '.' . $image6->getClientOriginalExtension();
//                $destinationPath = 'product-images/';
//                $image7->move($destinationPath, $name7);
//                $data10[] = 'product-images/' . $name7;
//                $path7 = public_path('product-images/'.$name7);
//                Image::make($path7)->resize(1080, 1080)->save($path7);
//            }
//        }
        $product->save();
        $notification = array(
            'messege' => 'Sauvegarde réussie!',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }

    public function update(Request $request, $id)
    {
        $product = Product::find($id);
        $product->title = $request->title;
        $product->category_id = $request->category_id;
        $product->subcategory_id = $request->subcategory_id;
        $product->price = $request->price;
        $product->oldprice = $request->oldprice;
        $product->sku = $request->sku;
        $product->description = $request->description;
        $product->material = $request->material;
        $product->label_name = $request->label_name;
        $product->background_color = $request->background_color;
        $product->text_color = $request->text_color;
        if ($request->month == 1) {
            $product->month = 1;
        } else {
            $product->month = 0;
        }
        if ($request->sold == 1) {
            $product->sold = 1;
        } else {
            $product->sold = 0;
        }
        if ($request->draft == 1) {
            $product->draft = 2;
        } else {
            $product->draft = 1;
        }
        if ($request->top_sweet == 1) {
            $product->top_sweet = 1;
        } else {
            $product->top_sweet = 0;
        }
        if ($request->top_10 == 1) {
            $product->top_10 = 1;
        } else {
            $product->top_10 = 0;
        }
        if ($request->hasfile('photo1')) {
            $image1 = $request->file('photo1');
            $name1 = time() . 'photo1' . '.' . $image1->getClientOriginalExtension();
            $destinationPath = 'product';
            ini_set('memory_limit', '256M');
            $img = Image::make($image1);
            $img->resize(1080, 1080)->save($destinationPath . '/' . $name1);
            $product->photo1 = $destinationPath . '/' . $name1;
        }
        if ($request->hasfile('gallery')) {
            foreach ($request->file('gallery') as $image) {
                $name = time() . 'gallery' . '.' . $image->getClientOriginalName();
                $destinationPath = 'gallery';
                ini_set('memory_limit', '256M');
                $img = Image::make($image);
                $img->resize(1080, 1080)->save($destinationPath . '/' . $name);
                $data9[] = $name;
                $product->gallery = json_encode($data9);
            }
        }
        $product->update();
        $notification = array(
            'messege' => 'Sauvegarde réussie!',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }

    public function updatecolor(Request $request, $id)
    {
        $product = Product::find($id);
        if ($request->color) {
            foreach ($request->color as $color) {
                $data2[] = $color;
                $product->color = json_encode($data2);
            }
        }
        if ($request->hasfile('colorimage')) {
            foreach ($request->file('colorimage') as $image4) {
                $name4 = time() . 'colorimage' . '.' . $image4->getClientOriginalName();
                $destinationPath = 'colorimage';
                ini_set('memory_limit', '256M');
                $img = Image::make($image4);
                $img->resize(1080, 1080)->save($destinationPath . '/' . $name4);
                $data10[] = $name4;
                $product->colorimage = json_encode($data10);
            }
        }
        $product->update();
        $notification = array(
            'messege' => 'Sauvegarde réussie!',
            'alert-type' => 'success'
        );
        return redirect()->route('products.index')->with($notification);
    }

    public function edit($id)
    {
        $categories = Category::all();
        $product = Product::find($id);
        return view('admin.product.edit', compact('product', 'categories'));
    }

    public function delete($id)
    {
        $product = Product::find($id);
        $product->delete();
        $notification = array(
            'messege' => 'Supprimé avec succès',
            'alert-type' => 'error'
        );
        return redirect()->back()->with($notification);
    }

    public function productSize()
    {
        $sizes = Size::orderBy('id', 'DESC')->get();
        return view('admin.product.sizes', compact('sizes'));
    }

    public function productSizecreate()
    {
        $products = Product::all();
        return view('admin.product.sizecreate', compact('products'));
    }

    public function getproduct(Request $request)
    {
        $product = Product::find($request->id);
        return json_decode($product->color);
    }

    public function getsizes(Request $request)
    {
        $sizes = Size::where('product_id', $request->product_id)->where('color', $request->color)->first();
        return response()->json(['sizes' => json_decode($sizes->newsize), 'stock' => json_decode($sizes->quantity)]);
    }

    public function sizecolorUpdate(Request $request)
    {
        $sizes = Size::where('product_id', $request->product_id)->where('color', $request->selectedcolor)->first();
        if ($request->size) {
            foreach ($request->size as $size1) {
                $data[] = $size1;
                $sizes->newsize = json_encode($data);
            }
        }
        if ($request->quantity) {
            foreach ($request->quantity as $quantity) {
                $data1[] = $quantity;
                $sizes->quantity = json_encode($data1);
            }
        }
        $sizes->update();
        $notification = array(
            'messege' => 'Sauvegarde réussie!',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }

    public function sizestore(Request $request)
    {
        $check = Size::where('product_id', $request->product_id)->where('color', $request->color)->first();
        if ($check) {
            $notification = array(
                'messege' => 'Des enregistrements contre ce produit et cette couleur existent déjà.!',
                'alert-type' => 'error'
            );
            return redirect()->back()->with($notification);
        } else {
            $size = new Size();
            $size->product_id = $request->product_id;
            $size->color = $request->color;
            $size->size = 0;

            if ($request->size) {
                foreach ($request->size as $size1) {
                    $data[] = $size1;
                    $size->newsize = json_encode($data);
                }
            }
            if ($request->quantity) {
                foreach ($request->quantity as $quantity) {
                    $data1[] = $quantity;
                    $size->quantity = json_encode($data1);
                }
            }
            $size->save();
            $notification = array(
                'messege' => 'Sauvegarde réussie!',
                'alert-type' => 'success'
            );
            return redirect()->back()->with($notification);
        }

    }

    public function sizedelete($id)
    {
        $size = Size::find($id);
        $size->delete();
        $notification = array(
            'messege' => 'Supprimé avec succès',
            'alert-type' => 'error'
        );
        return redirect()->back()->with($notification);
    }

    public function sizeedit($id)
    {
        $size = Size::find($id);
        return view('admin.product.sizeedit', compact('size'));
    }

    public function sizeupdate(Request $request, $id)
    {
        $size = Size::find($id);
        if ($request->size) {
            foreach ($request->size as $size1) {
                $data[] = $size1;
                $size->newsize = json_encode($data);
            }
        }
        if ($request->quantity) {
            foreach ($request->quantity as $quantity) {
                $data1[] = $quantity;
                $size->quantity = json_encode($data1);
            }
        }
        $size->update();
        $notification = array(
            'messege' => 'Sauvegarde réussie!',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }
    public function outofstock(){
        $siizes = Size::all();
        $a = array();
        foreach ($siizes as $siize){
            $data = json_decode($siize->quantity);
            if (empty(array_filter($data))) {
                array_push($a,$siize->product_id);
            }
        }
        $query = Product::whereIn('id', $a);
        $allProducts = $query->get();
        $products = $query->paginate(10);
        return view('admin.product.index', compact('products', 'allProducts'));
    }
}
