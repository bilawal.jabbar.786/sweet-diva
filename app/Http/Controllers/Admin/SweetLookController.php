<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\NotificationHelper;
use App\Like;
use App\Post;
use App\Topic;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SweetLookController extends Controller
{
    public function index(){
        $posts = Post::latest()->get();
        return view('admin.sweetlook.index', compact('posts'));
    }
    public function status(Request $request, $id){
        $posts = Post::find($id);
        if ($request->top_sweet){
            $posts->top_sweet = 1;
        }else{
            $posts->top_sweet = 0;
        }
        if ($request->top_10){
            $posts->top_10 = 1;
        }else{
            $posts->top_10 = 0;
        }
        $posts->update();
        $notification = array(
            'messege' => 'Status Update Successfully!',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }
    public function sweetDelete($id){
        $posts = Post::find($id);
        $posts->delete();
        $notification = array(
            'messege' => 'Post Delete Successfully!',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }
    public function sweetTopPost(){
        $posts= Post::orderBy('top_10', 'desc')->where('topic_id', null)->take(10)->get();
        return view('admin.sweetlook.index', compact('posts'));
    }
    public function topics(){
        $topics = Topic::all();
        return view('admin.sweetlook.topics', compact('topics'));
    }
    public function topicDelete($id){
        $topic = Topic::find($id);
        $topic->delete();
        $notification = array(
            'messege' => 'Topic Deleted!',
            'alert-type' => 'error'
        );
        return redirect()->back()->with($notification);
    }
    public function topicSave(Request $request){
        $topic = new Topic();
        $topic->name = $request->name;
        $topic->person = $request->person;
        $topic->save();
        $notification = array(
            'messege' => 'Topic Added Successfully!',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }
    public function fetchtopics(Request $request){
        $topics = Topic::where('person', $request->name)->get();
        return response()->json($topics);
    }
    public function postSave(Request $request){
        $post = new Post();
        $post->user_id = Auth::user()->id;
        if ($request->topic_id){
            $post->topic_id = $request->topic_id;
        }else{
            $post->topic_id = null;
        }
        $post->description = $request->description;
        if ($request->astuce){
            $post->astuce = 1;
        }else{
            $post->astuce = 0;
        }
        if ($request->name){
            $post->name = $request->name;
        }else{
            $post->name = null;
        }
        if ($request->hasfile('image')) {
            $image1 = $request->file('image');
            $name = time() . 'img' . '.' . $image1->getClientOriginalExtension();
            $destinationPath = 'app/post/';
            $image1->move($destinationPath, $name);
            $post->img = 'app/post/' . $name;
        }
        $post->save();
        $users = User::where('sweetlook', 1)->get();
        $activity = "Sweet Look";
        $msg = "Bonjour Divas, Il n'y a pas de post disponible sur sweet look.";
        try {
            NotificationHelper::pushNotification($msg, $users->pluck('device_token'), $activity);
        }catch (\Exception $e){

        }
        $notification = array(
            'messege' => 'Post Added Successfully!',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }
    public function filter($type){
        if ($type == 'astuce'){
            $posts = Post::where('astuce', 1)->get();
        }elseif ($type == 'between'){
            $posts = Post::where('topic_id','!=' , null)->where('name', null)->get();
        }
        return view('admin.sweetlook.index', compact('posts'));
    }
}
