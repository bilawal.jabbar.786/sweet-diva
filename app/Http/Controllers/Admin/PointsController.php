<?php

namespace App\Http\Controllers\Admin;

use App\AdminNotfication;
use App\CardPayments;
use App\Gifts;
use App\Http\Controllers\Controller;
use App\Points;
use App\PointsTable;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PointsController extends Controller
{
    public function index(){
        $points = Points::latest()->get();
        return view('admin.points.index', compact('points'));
    }
    public function create(){
        return view('admin.points.create');
    }
    public function store(Request $request){
        $points = new Points();
        $points->name = $request->name;
        $points->points = $request->points;
        $points->save();
        $notification = array(
            'messege' => 'Sauvegarde réussie!',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }
    public function edit($id){
        $points = Points::find($id);
        return view('admin.points.edit', compact('points'));
    }
    public function update(Request $request, $id){
        $points = Points::find($id);
        $points->name = $request->name;
        $points->points = $request->points;
        $points->update();
        $notification = array(
            'messege' => 'Sauvegarde réussie!',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }
    public function delete($id){
        $points = Points::find($id);
        $points->delete();
        $notification = array(
            'messege' => 'supreme!',
            'alert-type' => 'error'
        );
        return redirect()->back()->with($notification);
    }

    public function giftrequests(){
        $giftrequests = Gifts::orderBy('id', 'DESC')->get();
        $notifications= AdminNotfication::where('status','=',0)->update(['status' => 1]);
        return view('admin.points.giftrequests', compact('giftrequests'));
    }
    public function redeemvoucher(Request $request){
        $cardspayment = CardPayments::where('card_number', '=', $request->code)->where('valid', '=', '0')->first();
        if ($cardspayment){
            $user = User::where('email', $request->email)->first();
            if ($user){
                $user->wallet = $user->wallet + $cardspayment->price;
                $user->update();
            }else{
                $notification = array(
                    'messege'=>'Lutilisateur contre cet e-mail nexiste pas',
                    'alert-type'=>'error'
                );
                return redirect()->back()->with($notification);
            }
            $cardspayment->valid = '1';
            $cardspayment->update();

            $notification = array(
                'messege'=>'Bon dachat avec succès',
                'alert-type'=>'success'
            );
            return redirect()->back()->with($notification);
        }else{
            $notification = array(
                'messege'=>'Le code promo nest plus valide',
                'alert-type'=>'error'
            );
            return redirect()->back()->with($notification);
        }
    }
    public function expirePointsDetails(){
        $points = PointsTable::where('reason', 'Les points expirent')->latest()->get();
        return view('admin.points.expire_points', compact('points'));
    }

}
