<?php

namespace App\Http\Controllers\Admin;

use App\Banners;
use App\GeneralSettings;
use App\Http\Controllers\Controller;
use App\Points;
use App\Product;
use App\SaleProduct;
use App\Sms;
use Golchha21\ReSmushIt\Facades\Optimize;
use Illuminate\Http\Request;

class GeneralSettingsController extends Controller
{
    public function slider(){
        $banners = Banners::find(1);
        return view('admin.generalsettings.banners', compact('banners'));
    }
    public function settings(){
        $gs = GeneralSettings::find(1);
        return view('admin.generalsettings.settings', compact('gs'));
    }
    public function settingsstore(Request $request){
        $gs = GeneralSettings::find(1);
        $gs->sitename = $request->sitename;
        $gs->address = $request->address;
        $gs->tiktok = $request->tiktok;
        $gs->phone = $request->phone;
        $gs->email = $request->email;
        $gs->footer = $request->footer;
        $gs->facebook = $request->facebook;
        $gs->instagram = $request->instagram;


        if ($request->fashioncod == 1){
            $gs->fashioncod = 1;
        }else{
            $gs->fashioncod = 0;
        }

        if ($request->working == 1){
            $gs->working = 1;
        }else{
            $gs->working = 0;
        }

        if ($request->diva == 1){
            $gs->diva = 1;
        }else{
            $gs->diva = 0;
        }
        if ($request->padstatus == 1){
            $gs->padstatus = 1;
        }else{
            $gs->padstatus = 0;
        }

        if ($request->hasfile('logo')) {
            $image1 = $request->file('logo');
            $name = time() . 'logo' . '.' . $image1->getClientOriginalExtension();
            $destinationPath = 'logo/';
            $image1->move($destinationPath, $name);
            $gs->logo = 'logo/' . $name;
            Optimize::path('logo/' . $name);
        }
        $gs->update();
        $notification = array(
            'messege' => 'Ajouté avec succès!',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }
    public function sliderstore(Request $request){
        $banners = Banners::find(1);

        if ($request->hasfile('cat1')) {
            $image1 = $request->file('cat1');
            $name = time() . 'allimages' . '.' . $image1->getClientOriginalExtension();
            $destinationPath = 'allimages/';
            $image1->move($destinationPath, $name);
            $banners->cat1 = 'allimages/' . $name;
        }
        if ($request->hasfile('cat2')) {
            $image1 = $request->file('cat2');
            $name = time() . 'allimages' . '.' . $image1->getClientOriginalExtension();
            $destinationPath = 'allimages/';
            $image1->move($destinationPath, $name);
            $banners->cat2 = 'allimages/' . $name;
        }
        if ($request->hasfile('cat3')) {
            $image1 = $request->file('cat3');
            $name = time() . 'allimages' . '.' . $image1->getClientOriginalExtension();
            $destinationPath = 'allimages/';
            $image1->move($destinationPath, $name);
            $banners->cat3 = 'allimages/' . $name;
        }
        if ($request->hasfile('image1')) {
            $image1 = $request->file('image1');
            $name = time() . 'allimages' . '.' . $image1->getClientOriginalExtension();
            $destinationPath = 'allimages/';
            $image1->move($destinationPath, $name);
            $banners->image1 = 'allimages/' . $name;
        }
        if ($request->hasfile('image2')) {
            $image1 = $request->file('image2');
            $name = time() . 'allimages' . '.' . $image1->getClientOriginalExtension();
            $destinationPath = 'allimages/';
            $image1->move($destinationPath, $name);
            $banners->image2 = 'allimages/' . $name;
        }
        if ($request->hasfile('image3')) {
            $image1 = $request->file('image3');
            $name = time() . 'allimages' . '.' . $image1->getClientOriginalExtension();
            $destinationPath = 'allimages/';
            $image1->move($destinationPath, $name);
            $banners->image3 = 'allimages/' . $name;
        }
        if ($request->hasfile('image5')) {
            $image1 = $request->file('image5');
            $name = time() . 'allimages' . '.' . $image1->getClientOriginalExtension();
            $destinationPath = 'allimages/';
            $image1->move($destinationPath, $name);
            $banners->image5 = 'allimages/' . $name;
        }
        if ($request->hasfile('image6')) {
            $image1 = $request->file('image6');
            $name = time() . 'allimages' . '.' . $image1->getClientOriginalExtension();
            $destinationPath = 'allimages/';
            $image1->move($destinationPath, $name);
            $banners->image6 = 'allimages/' . $name;
        }
        if ($request->hasfile('image7')) {
            $image1 = $request->file('image7');
            $name = time() . 'allimages' . '.' . $image1->getClientOriginalExtension();
            $destinationPath = 'allimages/';
            $image1->move($destinationPath, $name);
            $banners->image7 = 'allimages/' . $name;
        }
        if ($request->hasfile('image8')) {
            $image1 = $request->file('image8');
            $name = time() . 'allimages' . '.' . $image1->getClientOriginalExtension();
            $destinationPath = 'allimages/';
            $image1->move($destinationPath, $name);
            $banners->image8 = 'allimages/' . $name;
        }
        if ($request->hasfile('image9')) {
            $image1 = $request->file('image9');
            $name = time() . 'allimages' . '.' . $image1->getClientOriginalExtension();
            $destinationPath = 'allimages/';
            $image1->move($destinationPath, $name);
            $banners->image9 = 'allimages/' . $name;
        }
        if ($request->hasfile('banner1')) {
            $image1 = $request->file('banner1');
            $name = time() . 'allimages' . '.' . $image1->getClientOriginalExtension();
            $destinationPath = 'allimages/';
            $image1->move($destinationPath, $name);
            $banners->banner1 = 'allimages/' . $name;
        }
        if ($request->hasfile('banner2')) {
            $image1 = $request->file('banner2');
            $name = time() . 'allimages' . '.' . $image1->getClientOriginalExtension();
            $destinationPath = 'allimages/';
            $image1->move($destinationPath, $name);
            $banners->banner2 = 'allimages/' . $name;
        }
        if ($request->hasfile('banner3')) {
            $image1 = $request->file('banner3');
            $name = time() . 'allimages' . '.' . $image1->getClientOriginalExtension();
            $destinationPath = 'allimages/';
            $image1->move($destinationPath, $name);
            $banners->banner3 = 'allimages/' . $name;
        }
        if ($request->hasfile('banner4')) {
            $image1 = $request->file('banner4');
            $name = time() . 'allimages' . '.' . $image1->getClientOriginalExtension();
            $destinationPath = 'allimages/';
            $image1->move($destinationPath, $name);
            $banners->banner4 = 'allimages/' . $name;
        }
        if ($request->hasfile('image4')) {
            $image1 = $request->file('image4');
            $name = time() . 'allimages' . '.' . $image1->getClientOriginalExtension();
            $destinationPath = 'allimages/';
            $image1->move($destinationPath, $name);
            $banners->image4 = 'allimages/' . $name;
        }
        if ($request->hasfile('videobanner')) {
            $image1 = $request->file('videobanner');
            $name = time() . 'allimages' . '.' . $image1->getClientOriginalExtension();
            $destinationPath = 'allimages/';
            $image1->move($destinationPath, $name);
            $banners->videobanner = 'allimages/' . $name;
        }
        if ($request->hasfile('vide1')) {
            $image1 = $request->file('vide1');
            $name = time() . 'allimages' . '.' . $image1->getClientOriginalExtension();
            $destinationPath = 'allimages/';
            $image1->move($destinationPath, $name);
            $banners->vide1 = 'allimages/' . $name;
        }
        if ($request->hasfile('vide2')) {
            $image1 = $request->file('vide2');
            $name = time() . 'allimages' . '.' . $image1->getClientOriginalExtension();
            $destinationPath = 'allimages/';
            $image1->move($destinationPath, $name);
            $banners->vide2 = 'allimages/' . $name;
        }
        if ($request->hasfile('vide3')) {
            $image1 = $request->file('vide3');
            $name = time() . 'allimages' . '.' . $image1->getClientOriginalExtension();
            $destinationPath = 'allimages/';
            $image1->move($destinationPath, $name);
            $banners->vide3 = 'allimages/' . $name;
        }
        $banners->videoid = $request->videoid;

        $banners->link1 = $request->link1;
        $banners->link2 = $request->link2;
        $banners->link3 = $request->link3;
        $banners->link4 = $request->link4;
        $banners->link5 = $request->link5;

        $banners->update();
        $notification = array(
            'messege' => 'Ajouté avec succès!',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }
    public function saleproduct(){
        $products = Product::where('draft', '=', 1)->get();
        return view('admin.saleproduct.index', compact('products'));
    }
    public function saleproductstore(Request $request){
        $saleproduct = SaleProduct::find(1);
        $saleproduct->product_id = $request->product_id;
        $saleproduct->date = $request->date;
        $saleproduct->update();
        $notification = array(
            'messege' => 'Ajouté avec succès!',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }
    public function points(){
        $points = Points::find(1);
        return view('admin.generalsettings.points', compact('points'));
    }
    public function pointsstore(Request $request){
        $points = Points::find(1);
        $points->userpoints = $request->userpoints;
        $points->adminpoints = $request->adminpoints;
        $points->save();
        $notification = array(
            'messege' => 'Ajouté avec succès!',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }
    public function about(){
        $gs = GeneralSettings::find(1);
        return view('admin.generalsettings.about', compact('gs'));
    }
    public function aboutstore(Request $request){
        $gs = GeneralSettings::find(1);
        $gs->about =  $request->about;
        $gs->about1 =  $request->about1;
        $gs->condition =  $request->condition;
        $gs->legal =  $request->legal;
        $gs->pad =  $request->pad;

        $gs->update();
        $notification = array(
            'messege' => 'Ajouté avec succès!',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }
    public function sms(){
        $sms = Sms::all();
        return view('admin.generalsettings.sms', compact('sms'));
    }
    public function home(){
        $gs = GeneralSettings::find(1);
        return view('admin.generalsettings.home', compact('gs'));
    }
    public function homeStore(Request $request){
        $gs = GeneralSettings::find(1);
        $gs->home =  $request->home;
        $gs->points =  $request->points;
        $gs->privicy_app =  $request->privicy_app;
        $gs->update();
        $notification = array(
            'messege' => 'Ajouté avec succès!',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }
}
