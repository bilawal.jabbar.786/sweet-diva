<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Report;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReportsController extends Controller
{
    public function index(){
        if (Auth::user()->role==4){
            $reports = Report::where('user_id', '=', Auth::user()->id)->get();
        }else{
            $reports = Report::all();
        }
        return view('admin.reports.index', compact('reports'));
    }
    public function store(Request $request){
        $report = new Report();
        $report->user_id = Auth::user()->id;
        $report->total = $request->total;
        $report->p1 = $request->p1;
        $report->p2 = $request->p2;
        $report->p3 = $request->p3;
        $report->p4 = $request->p4;
        $report->p5 = $request->p5;
        $report->p6 = $request->p6;
        $report->comments = $request->comments;
        $report->save();
        $notification = array(
            'messege' => 'Rapport ajouté avec succès!',
            'alert-type' => 'success'
        );
        return Redirect()->back()->with($notification);
    }
    public function update(Request $request, $id){
        $report = Report::find($id);
        $report->total = $request->total;
        $report->p1 = $request->p1;
        $report->p2 = $request->p2;
        $report->p3 = $request->p3;
        $report->p4 = $request->p4;
        $report->p5 = $request->p5;
        $report->comments = $request->comments;
        $report->update();
        $notification = array(
            'messege' => 'Rapport ajouté avec succès!',
            'alert-type' => 'success'
        );
        return Redirect()->back()->with($notification);
    }
    public function calender(){
        $reports = Report::all();
        return view('admin.reports.calender', compact('reports'));
    }
    public function detail($id){
        $report = Report::find($id);
        return view('admin.reports.detail', compact('report'));
    }
}
