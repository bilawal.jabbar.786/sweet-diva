<?php

namespace App\Http\Controllers\Admin;

use App\AdminNotfication;
use App\CardPayments;
use App\Cards;
use App\Product;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Mail\BonCadeau;
use App\PromoCode;
use Golchha21\ReSmushIt\Facades\Optimize;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class CardsController extends Controller
{
    public function index(){
        $cards = Cards::all();
        return view('admin.cards.index', compact('cards'));
    }
    public function create(){
        return view('admin.cards.create');
    }
    public function store(Request $request){
        $cards = new Cards();
        $cards->title = $request->title;
        $cards->sku = $request->sku;
        if ($request->hasfile('photo')) {
            $image1 = $request->file('photo');
            $name = time() . 'photo' . '.' . $image1->getClientOriginalExtension();
            $destinationPath = 'card/';
            $image1->move($destinationPath, $name);
            $cards->photo = 'card/' . $name;
        }
        $cards->save();
        $notification = array(
            'messege' => 'Sauvegarde réussie!',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }
    public function delete($id){
        $cards = Cards::find($id);
        $cards->delete();
        $notification = array(
            'messege' => 'Supreme !',
            'alert-type' => 'error'
        );
        return redirect()->back()->with($notification);
    }
    public function sale(){
        $allcards = Cards::all();
        $cards = CardPayments::where('paymentstatus', '1')->get();
        return view('admin.cards.sale', compact('cards', 'allcards'));
    }
    public function cardadmin(Request $request){
        $new = new CardPayments();
        $new->card_id = $request->card_id;
        $new->sendername = $request->sendername;
        $new->senderphone = $request->senderphone;
        $new->r_name = $request->r_name;
        $new->r_email = $request->r_email;
        $new->message = $request->message;
        $new->price = $request->price;
        $new->paymentstatus = '1';
        $new->card_number = rand(100000000, 900000000);
        $new->save();

        $cards = Cards::where('sku', $request->card_id)->first();
        Mail::to($new->r_email)->send(new BonCadeau($new, $cards));

        $notification = array(
            'messege' => 'Sauvegarde réussie!',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }
    public function status($id){
        $card = CardPayments::find($id);
        $card->valid = '1';
        $card->update();
        $notification = array(
            'messege' => 'Sauvegarde réussie!',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }
    public function promoCodeIndex(){
        $promo = PromoCode::orderBy('id', 'DESC')->paginate(50);
        $products = Product::orderBy('id', 'DESC')->where('draft', '=', 1)->where('user_id', '=', '1')->where('sold', '!=', '1')->get();
        return view('admin.promo.index', compact('promo', 'products'));
    }
    public function promoCodeStore(Request $request){
        $promo = new PromoCode();
        $promo->code = $request->code;
        $promo->off = $request->off;
        $promo->excluded_products = json_encode($request->excluded_products);
        $promo->save();
        $notification = array(
            'messege' => 'Sauvegarde réussie!',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }
    public function promoCodeEdit($id){
        $promo =  PromoCode::find($id);
        $products = Product::orderBy('id', 'DESC')->where('draft', '=', 1)->where('user_id', '=', '1')->where('sold', '!=', '1')->get();
        return view('admin.promo.edit', compact('promo', 'products'));
    }
    public function promoCodeUpdate(Request $request,$id){
        $promo =  PromoCode::find($id);
        $promo->code = $request->code;
        $promo->off = $request->off;
        $promo->excluded_products = json_encode($request->excluded_products);
        $promo->update();
        $notification = array(
            'messege' => 'Sauvegarde réussie!',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }
    public function promoCodeDelete($id){
        $promo =  PromoCode::find($id);
        $promo->delete();
        $notification = array(
            'messege' => 'Données supprimées!',
            'alert-type' => 'errors'
        );
        return redirect()->back()->with($notification);
    }
    public function promoCodeStatus($id,$status){
        $promo =  PromoCode::find($id);
        $promo->status = $status;
        $promo->update();
        $notification = array(
            'messege' => 'Sauvegarde réussie!',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }
    public function promoCode(Request $request){
        $promo =  PromoCode::where('code','=',$request->code)->where('status','=',1)->where('created_at', '>=', Carbon::now()->subDays(7))->first();
        return response()->json($promo);
    }
}
