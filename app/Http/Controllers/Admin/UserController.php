<?php

namespace App\Http\Controllers\Admin;

use App\Contact;
use App\Countries;
use App\Exports\UsersExport;
use App\Gifts;
use App\Group;
use App\Http\Controllers\Controller;
use App\Http\NotificationHelper;
use App\Http\PointsHelper;
use App\Newsletter;
use App\Notfication;
use App\Order;
use App\PointsTable;
use App\PromoCode;
use App\User;
use App\VendorOrder;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\DataTables;

class UserController extends Controller
{
    public function sellers(Request $request)
    {
        $countries = Countries::all();
        if ($request->ajax()) {
            $data = User::latest()->where('role', '=', '2');
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $btn = '<a href="' . route('customer.edit', ['id' => $row->id]) . '"
                                                           class="btn btn-sm btn-success" data-toggle="tooltip"
                                                           title="edit">
                                                            <i class="fa fa-eye"></i>
                                                        </a>
                                                        <a id="delete"
                                                           href="' . route('customer.delete', ['id' => $row->id]) . '"
                                                           class="btn btn-sm btn-danger" data-toggle="tooltip"
                                                           title="edit">
                                                            <i class="fa fa-times"></i>
                                                        </a>';

                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('admin.users.sellers', compact('countries'));
    }

    public function buyers()
    {
        $sellers = User::where('role', '=', '1')->get();
        return view('admin.users.buyers', compact('sellers'));
    }

    public function edit($id)
    {
        $customer = User::find($id);
        $orders = VendorOrder::latest()->whereIn('order_id', $customer->orders->pluck('id'))->get();
        return view('admin.users.edit', compact('customer', 'orders'));
    }

    public function details($id)
    {
        $points = PointsTable::where('user_id', '=', $id)->latest()->get();
        return view('admin.users.details', compact('points', 'id'));
    }

    public function create()
    {
        $countries = Countries::all();
        return view('admin.users.create', compact('countries'));
    }

    public function shopsAssign(Request $request)
    {
        $cus = $request->users;
        $myArray = explode(',', $cus);
        foreach ($myArray as $key => $value) {
            User::where('id', $value)
                ->update(['shop' => $request->shop]);
        }
        $notification = array(
            'messege' => 'Sauvegarde réussie!',
            'alert-type' => 'success'
        );
        return redirect()->route('admin.sellers')->with($notification);
    }

    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->name = $request->name;
        $user->surname = $request->surname;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->address = $request->address;
        $user->size = $request->size;
        $user->cp = $request->cp;
        $user->city = $request->city;
        $user->points = $request->points;
        $user->wallet = $request->wallet;
        if ($request->password) {
            $user->password = Hash::make($request->password);
        }
        $user->update();
        $notification = array(
            'messege' => 'Sauvegarde réussie!',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }

    public function store(Request $request)
    {
        $user = new User();
        $user->name = $request->name;
        $user->surname = $request->surname;
        $user->role = $request->role;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->address = $request->address;
        $user->size = $request->size;
        if (Auth::user()->role == 0) {
            $user->country = $request->country;
            $user->shop = $request->shop;
        } else {
            $user->country = Auth::user()->country;
            $user->shop = Auth::user()->shop;
        }
        $user->points = $request->points;
        $user->password = Hash::make($request->password);
        $user->save();
        $notification = array(
            'messege' => 'Sauvegarde réussie!',
            'alert-type' => 'success'
        );
        return redirect()->route('admin.sellers')->with($notification);
    }

    public function filter($country)
    {
        $sellers = User::where('role', '2')->where('country', $country)->paginate(10);
        $seller = User::where('role', '2')->where('country', $country)->get();
        $countries = Countries::all();
        return view('admin.users.sellers', compact('sellers', 'countries', 'seller'));
    }

    public function filterUser($id)
    {
        return redirect()->route('customer.edit', $id);
    }

    public function userPoints(Request $request)
    {
        $user = User::find($request->user_id);
        $user->points = $user->points + $request->points;
        PointsHelper::pushPoints($user->id, $request->points, 'Admin', 'Ajouter');
        $user->update();
        NotificationHelper::addtoNitification(1, $user->id, $request->points . ' points Ajouter', 0, 'Points', 1);
        NotificationHelper::pushNotification($request->points . ' points Ajouter', [$user->device_token], 'Points de fidélité');
        $notification = array(
            'messege' => 'Points ajoutés avec succès!',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }

    public function userPointsWithdraw(Request $request)
    {
        $user = User::find($request->user_id);
        $user->points = $user->points - $request->points;
        $user->update();
        PointsHelper::pushPoints($user->id, $request->points, 'Admin', 'Retirer');
        NotificationHelper::addtoNitification(1, $user->id, $request->points . ' points Retirer', 0, 'Points', 1);
        NotificationHelper::pushNotification($request->points . ' points Retirer', [$user->device_token], 'Points de fidélité');
        $notification = array(
            'messege' => 'Retrait de points utilisateur!',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }

    public function newsletter()
    {
        $newsletter = Newsletter::orderBy('id', 'DESC')->get();
        return view('admin.users.newsletter', compact('newsletter'));
    }

    public function export(Request $request)
    {
        return Excel::download(new UsersExport($request->country), $request->country . 'users.xlsx');
    }

    public function password_change()
    {
        return view('admin.generalsettings.updatepassword');
    }

    public function password_update(Request $request)
    {
        $password = Auth::user()->password;
        $oldpass = $request->oldpass;
        $newpass = $request->password;
        $confirm = $request->password_confirmation;
        if (Hash::check($oldpass, $password)) {
            if ($newpass === $confirm) {
                $user = User::find(Auth::id());
                $user->password = Hash::make($request->password);
                $user->save();
                Auth::logout();
                $notification = array(
                    'messege' => 'Le mot de passe a été changé avec succès ! Connectez-vous maintenant avec votre nouveau mot de passe',
                    'alert-type' => 'success'
                );
                return Redirect()->route('login')->with($notification);
            } else {
                $notification = array(
                    'messege' => 'Le nouveau mot de passe et la confirmation du mot de passe ne correspondent pas!',
                    'alert-type' => 'error'
                );
                return Redirect()->back()->with($notification);
            }
        } else {
            $notification = array(
                'messege' => 'Lancien mot de passe ne correspond pas!',
                'alert-type' => 'error'
            );
            return Redirect()->back()->with($notification);
        }
    }

    public function birthdayUser()
    {
        $users = User::whereMonth('dob', '=', date('m'))->whereDay('dob', '=', date('d'))->get();
        return view('admin.birthday.index', compact('users'));
    }

    public function birthdaySend()
    {
        $promo = new PromoCode();
        $promo->code = "Sweet-diva-" . rand(1000, 9000);
        $promo->off = 10;
        $promo->save();
        $users = User::whereMonth('dob', '=', date('m'))->whereDay('dob', '=', date('d'))->get();
        $activity = "JOYEUX ANNIVERSAIRE";
        $msg = "Pour les anniversaires vous pouvez programmer l'offre à -10% valable 7 jours. Votre code promotionnel " . $promo->code;
        foreach ($users as $row) {
            NotificationHelper::addtoNitification(1, $row->id, $msg, 0, $activity, 1);
        }
        NotificationHelper::pushNotification($msg, $users->pluck('device_token'), $activity);
        return $users;
    }

    public function groupIndex()
    {
        $users = User::where('role', '=', '2')->get();
        if (Auth::user()->role == 4) {
            $groups = Group::where('role', Auth::user()->shop)->get();
        } else {
            $groups = Group::all();
        }
        $countries = Countries::all();
        return view('admin.notfication.group', compact('users', 'groups', 'countries'));
    }

    public function groupStore(Request $request)
    {
        $group = new Group();
        $group->name = $request->name;
        if (Auth::user()->role == null) {
            $group->role = 'admin';
        } else {
            $group->role = Auth::user()->shop;
        }
        $group->user_id = json_encode($request->user_id);
        $group->save();
        $notification = array(
            'messege' => 'Groupes ajoutés!',
            'alert-type' => 'success'
        );
        return Redirect()->back()->with($notification);
    }

    public function groupDelete($id)
    {
        $groups = Group::where('id', '=', $id)->first();
        $groups->delete();
        $notification = array(
            'messege' => 'Groupes supprimés!',
            'alert-type' => 'error'
        );
        return Redirect()->back()->with($notification);
    }

    public function delete($id)
    {
        $customer = User::find($id);
        $customer->delete();
        $notification = array(
            'messege' => 'Client est supprimé, toutes les données relatives à ce client seront supprimées.',
            'alert-type' => 'error'
        );
        return redirect()->back()->with($notification);
    }

    public function groupShow($id)
    {
        $groups = Group::where('id', '=', $id)->first();
        return view('admin.notfication.viewgroup', compact('groups'));
    }

    public function sendNotfication(Request $request)
    {
        $activity = "Message de Sweet Diva";
        $msg = $request->message;
        if ($request->user_id == 'send_to_all') {
            if (Auth::user()->role == 3 || Auth::user()->role == 4) {
                $users = User::where('role', '=', '2')->where('country', '=', Auth::user()->country)->get();
            } else {
                $users = User::where('role', '=', '2')->get();
            }
            foreach ($users as $row) {
                NotificationHelper::addtoNitification(1, $row->id, $msg, $request->generate_id, $activity, 1);
            }
            NotificationHelper::pushNotification($msg, $users->pluck('device_token'), $activity);
        } else {
            $groups = Group::where('id', '=', $request->user_id)->first();
            if (!empty ($groups->user_id)) {
                $usersss = json_decode($groups->user_id, true);
                foreach ($usersss as $key => $user_id) {
                    $user = User::where('id', '=', $user_id)->first();
                    NotificationHelper::addtoNitification(1, $user->id, $msg, $request->generate_id, $activity, 1);
                }
                NotificationHelper::pushNotification($msg, $usersss->pluck('device_token'), $activity);
            }
        }
        $notification = array(
            'messege' => 'Envoyez votre message Utilisateurs!',
            'alert-type' => 'success'
        );
        return Redirect()->back()->with($notification);
    }

    public function sendNotficationUsers(Request $request)
    {
        if ($request->user_id[0] == "send_to_all") {
            if (Auth::user()->role == 3 || Auth::user()->role == 4) {
                $firebaseToken = User::Where('role', '=', 2)->where('country', '=', Auth::user()->country)->whereNotNull('device_token')->pluck('device_token')->all();
                $users = User::where('role', '=', 2)->where('country', '=', Auth::user()->country)->get();
            } else {
                $firebaseToken = User::Where('role', '=', 2)->whereNotNull('device_token')->pluck('device_token')->all();
                $users = User::where('role', '=', 2)->get();
            }
            foreach ($users as $user) {
                NotificationHelper::addtoNitification(Auth::user()->id, $user->id, $request->message, $request->generate_id, 'Message de Sweet Diva', 1);
            }
        } else {
            $firebaseToken = User::Where('role', '=', 2)->whereIn('id', $request->user_id)->whereNotNull('device_token')->pluck('device_token')->all();
            foreach ($request->user_id as $user) {
                NotificationHelper::addtoNitification(Auth::user()->id, $user, $request->message, $request->generate_id, 'Message de Sweet Diva', 1);
            }
        }
        NotificationHelper::pushNotification($request->message, $firebaseToken, "Message de Sweet Diva");
        $notification = array(
            'messege' => 'Envoyez votre message Utilisateurs!',
            'alert-type' => 'success'
        );
        return Redirect()->back()->with($notification);
    }

    public function sendNotficationCountry(Request $request)
    {
        $users = User::where('country', '=', $request->country)->get();
        $activity = "Message de Sweet Diva";
        $msg = $request->message;
        foreach ($users as $row) {
            NotificationHelper::addtoNitification(1, $row->id, $msg, $request->generate_id, $activity, 1);
        }
        NotificationHelper::pushNotification($msg, $users->pluck('device_token'), $activity);
        $notification = array(
            'messege' => 'Envoyez votre message Utilisateurs!',
            'alert-type' => 'success'
        );
        return Redirect()->back()->with($notification);
    }

    public function sendNotficationShop(Request $request)
    {
        $users = User::where('role', '=', 2)->where('shop', '=', $request->shop)->get();
        $activity = "Message de Sweet Diva";
        $msg = $request->message;
        foreach ($users as $row) {
            NotificationHelper::addtoNitification(1, $row->id, $msg, $request->generate_id, $activity, 1);
        }
        NotificationHelper::pushNotification($msg, $users->pluck('device_token'), $activity);
        $notification = array(
            'messege' => 'Envoyez votre message Utilisateurs!',
            'alert-type' => 'success'
        );
        return Redirect()->back()->with($notification);
    }

    public function sendNotficationSizes(Request $request)
    {
        $users = User::where('size', '=', $request->size)->get();
        $activity = "Message de Sweet Diva";
        $msg = $request->message;
        foreach ($users as $row) {
            NotificationHelper::addtoNitification(1, $row->id, $msg, $request->generate_id, $activity, 1);
        }
        NotificationHelper::pushNotification($msg, $users->pluck('device_token'), $activity);
        $notification = array(
            'messege' => 'Envoyez votre message Utilisateurs!',
            'alert-type' => 'success'
        );
        return Redirect()->back()->with($notification);
    }

    public function confirmOrder(Request $request)
    {
        $activity = 'Livrer la commande';
        $msg = $request->message;
        $order = Order::where('order_number', '=', $request->order_no)->first();
        $user = User::where('id', '=', $order->user_id)->first();
        NotificationHelper::pushNotification($msg, $user->device_token, $activity);
        NotificationHelper::addtoNitification(1, $user->id, $msg, $order->id, $activity, 1);
        $notification = array(
            'messege' => 'Votre message livré!',
            'alert-type' => 'success'
        );
        return Redirect()->back()->with($notification);
    }

    public function notiDetialsUser()
    {
        if (Auth::user()->role == 4) {
            $users = User::where('role', '=', '2')->where('shop', Auth::user()->shop)->get();
        } else {
            $users = User::where('role', '=', '2')->get();
        }
        return view('admin.notfication.user', compact('users'));
    }

    public function notiDetials($id)
    {
        $notfication = Notfication::where('r_id', '=', $id)->get();
        return view('admin.notfication.notfication', compact('notfication'));
    }

    public function userMessages()
    {
        $contact = Contact::latest()->paginate(10);
        return view('admin.users.contact', compact('contact'));
    }

    public function giftHistory($id)
    {
        $gift_history = Gifts::where('user_id', '=', $id)->latest()->get();
        return view('admin.users.gift_history', compact('gift_history'));
    }

    public function expirieUsersPoints()
    {
        $points = PointsTable::where('transaction', 'Ajouter')->where('type', null)->get();
        foreach ($points as $point) {
            $start_date = Carbon::parse($point->created_at);
            if ($start_date->diffInMonths() >= 12) {
                $user = User::find($point->user_id);
                $user->points = $user->points - $point->points;
                $user->update();
                PointsHelper::pushPoints($user->id, $point->points, 'Les points expirent', 'Retirer');
                $point->type = 'done';
                $point->update();
                NotificationHelper::addtoNitification(1, $user->id, $point->points . ' points expirent', 0, 'Points de fidélité', 1);
                NotificationHelper::pushNotification($point->points . ' points expirent', [$user->device_token], 'Points');
            }
        }
    }

    public function search(Request $request)
    {
        $sellers = User::latest()
            ->orWhere('name', '=', $request->search)
            ->orWhere('surname', '=', $request->search)
            ->orWhere('email', '=', $request->search)
            ->orWhere('phone', '=', $request->search)
            ->paginate(10);
        $countries = Countries::all();
        return view('admin.users.sellers', compact('sellers', 'countries'));
    }

    public function searchuser(Request $request)
    {
        $sellers = User::latest()
            ->orWhere('name', 'LIKE', '%' . $request->search . "%")
            ->orWhere('surname', 'LIKE', '%' . $request->search . "%")
            ->orWhere('email', 'LIKE', '%' . $request->search . "%")
            ->orWhere('phone', 'LIKE', '%' . $request->search . "%")
            ->take(5)->get();
        return response()->json($sellers);
    }
}
