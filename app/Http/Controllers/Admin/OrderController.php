<?php

namespace App\Http\Controllers\Admin;

use App\GeneralSettings;
use App\Http\Controllers\Controller;
use App\Http\NotificationHelper;
use App\Order;
use App\User;
use App\VendorOrder;
use PDF;
use Session;

class OrderController extends Controller
{
    public function index()
    {
        $orders = VendorOrder::where('vendor_id', '=', '1')->where('status', '1')->latest('created_at')->get();
        return view('admin.order.index', compact('orders'));
    }

    public function view($id)
    {
        $order = Order::find($id);
        return view('admin.order.view', compact('order'));
    }

    public function status($id)
    {
        $order = Order::find($id);
        $order->status = '1';
        $order->update();

        $vendor_order = VendorOrder::where('order_id', $id)->first();
        $vendor_order->status = '2';
        $vendor_order->update();

        $user = User::where('id', '=', $order->user_id)->first();
        $activity = "Expédié";
        $msg = "Votre colis a été expédié";

        NotificationHelper::pushNotification($msg, $user->device_token, $activity);
        NotificationHelper::addtoNitification(1, $user->id, $msg, $id, $activity, 1);


        $notification = array(
            'messege' => 'Ajouté avec succès!',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }

    public function videdressing()
    {
        $orders = VendorOrder::orderBy('created_at', 'desc')->where('vendor_id', '!=', '1')->where('status', '1')->get();
        return view('admin.order.index', compact('orders'));
    }

    public function complete()
    {
        $orders = VendorOrder::orderBy('created_at', 'desc')->where('vendor_id', '=', '1')->where('status', '2')->get();
        return view('admin.order.index', compact('orders'));
    }

    public function completevide()
    {
        $orders = VendorOrder::orderBy('created_at', 'desc')->where('vendor_id', '!=', '1')->where('status', '2')->get();
        return view('admin.order.index', compact('orders'));
    }

    public function padOrder()
    {
        $orders = Order::orderBy('created_at', 'desc')->where('payment_method', '=', 'pad')->get();

        return view('admin.order.padindex', compact('orders'));
    }

    public function orderDownload($id)
    {
        $order = Order::find($id);
        $gs = GeneralSettings::find(1);
        $data1 = [
            'invoice' => $order,
            'gs' => $gs
        ];
        $pdf = PDF::loadView('myPDF', $data1);
        $pdf->stream();
        return $pdf->download($order->order_number.'.pdf');
    }
    public function orderPrint($id)
    {
        $order = Order::find($id);
        $gs = GeneralSettings::find(1);
        return view('print.details',compact('order','gs'));
    }
}
