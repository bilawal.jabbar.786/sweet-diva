<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use Golchha21\ReSmushIt\Facades\Optimize;
use App\MainCategory;
use App\SubCategory;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function maincategory(){
        $maincategory = MainCategory::orderBy('no','ASC')->get();
        $category = Category::all();
        return view('admin.category.maincategory', compact('maincategory', 'category'));
    }
    public function maincategorySequence(Request $request){
        $maincategory = MainCategory::where('id','=',$request->category_id)->first();
        $maincategory->no = $request->no;
        $maincategory->update();
        $notification = array(
            'messege' => 'Sauvegarde réussie!',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);

    }
    public function maincategorystore(Request $request){

        $maincategory = new MainCategory();
        $maincategory->name = $request->name;

        $maincategory->category_id = $request->category_id;

        if ($request->hasfile('photo')) {
            $image1 = $request->file('photo');
            $name = time() . 'photo' . '.' . $image1->getClientOriginalExtension();
            $destinationPath = 'category/';
            $image1->move($destinationPath, $name);
            $maincategory->photo = 'category/' . $name;
            Optimize::path('category/' . $name);
        }
        if ($request->hasfile('photo2')) {
            $image1 = $request->file('photo2');
            $name = time() . 'photo2' . '.' . $image1->getClientOriginalExtension();
            $destinationPath = 'category/';
            $image1->move($destinationPath, $name);
            $maincategory->photo2 = 'category/' . $name;
            Optimize::path('category/' . $name);
        }
        if ($request->hasfile('photo3')) {
            $image1 = $request->file('photo3');
            $name = time() . 'photo3' . '.' . $image1->getClientOriginalExtension();
            $destinationPath = 'category/';
            $image1->move($destinationPath, $name);
            $maincategory->photo3 = 'category/' . $name;
            Optimize::path('category/' . $name);
        }
        if ($request->hasfile('banner')) {
            $image2 = $request->file('banner');
            $name2 = time() . 'categorybanner' . '.' . $image2->getClientOriginalExtension();
            $destinationPath = 'categorybanner/';
            $image2->move($destinationPath, $name2);
            $maincategory->banner = 'categorybanner/' . $name2;
            Optimize::path('categorybanner/' . $name2);
        }
        $maincategory->save();
        $notification = array(
            'messege' => 'Sauvegarde réussie!',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }
    public function maincategorydelete($id){
        $maincategory = MainCategory::find($id);
        $maincategory->delete();
        $notification = array(
            'messege' => 'Effacer!',
            'alert-type' => 'error'
        );
        return redirect()->back()->with($notification);
    }
    public function maincategoryedit($id){
        $maincategory = MainCategory::find($id);

        return view('admin.category.editmaincategory',compact('maincategory'));
    }
    public function maincategoryupdate( Request $request,$id){
        $maincategory = MainCategory::find($id);
        $maincategory->name = $request->name;
        $maincategory->no = $request->no;

        if ($request->hasfile('photo')) {
            $image1 = $request->file('photo');
            $name = time() . 'photo' . '.' . $image1->getClientOriginalExtension();
            $destinationPath = 'category/';
            $image1->move($destinationPath, $name);
            $maincategory->photo = 'category/' . $name;
            Optimize::path('category/' . $name);
        }
        if ($request->hasfile('photo2')) {
            $image1 = $request->file('photo2');
            $name = time() . 'photo2' . '.' . $image1->getClientOriginalExtension();
            $destinationPath = 'category/';
            $image1->move($destinationPath, $name);
            $maincategory->photo2 = 'category/' . $name;
            Optimize::path('category/' . $name);
        }
        if ($request->hasfile('photo3')) {
            $image1 = $request->file('photo3');
            $name = time() . 'photo3' . '.' . $image1->getClientOriginalExtension();
            $destinationPath = 'category/';
            $image1->move($destinationPath, $name);
            $maincategory->photo3 = 'category/' . $name;
            Optimize::path('category/' . $name);
        }
        if ($request->hasfile('banner')) {
            $image2 = $request->file('banner');
            $name2 = time() . 'categorybanner' . '.' . $image2->getClientOriginalExtension();
            $destinationPath = 'categorybanner/';
            $image2->move($destinationPath, $name2);
            $maincategory->banner = 'categorybanner/' . $name2;
            Optimize::path('categorybanner/' . $name);
        }
        $maincategory->update();
        $notification = array(
            'messege' => 'Sauvegarde réussie!',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }


    public function subcategory(){
        $subcategory = SubCategory::all();
        $maincategory = MainCategory::all();
        return view('admin.category.subcategory', compact('subcategory', 'maincategory'));
    }
    public function subcategorystore(Request $request){
        $subcategory = new SubCategory();
        $subcategory->category_id = $request->category_id;
        $subcategory->name = $request->name;
        $subcategory->save();
        $notification = array(
            'messege' => 'Sauvegarde réussie!',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }
    public function subcategorydelete($id){
        $subcategory = SubCategory::find($id);
        $subcategory->delete();
        $notification = array(
            'messege' => 'Effacer!',
            'alert-type' => 'error'
        );
        return redirect()->back()->with($notification);
    }
    public function fetchsubcategory(Request $request){
        $subcategories = SubCategory::where('category_id', '=', $request->id)->get();
        return response()->json($subcategories);
    }
    public function fetchmaincategory(Request $request){
        $subcategories = MainCategory::where('category_id', '=', $request->id)->get();
        return response()->json($subcategories);
    }
}
