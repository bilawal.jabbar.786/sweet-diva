<?php

namespace App\Http\Controllers\User;

use App\CardPayments;
use App\Category;
use App\Contact;
use App\Gifts;
use App\Http\AdminNotificationHelper;
use App\Http\Controllers\Controller;
use App\Http\PointsHelper;
use App\Order;
use App\Points;
use App\Product;
use App\Size;
use App\User;
use App\VendorOrder;
use App\Wishlist;
use Cart;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;
use Twilio\Rest\Client;

class UserController extends Controller
{
    public function dashboard()
    {
        $user = Auth::user();
        $categories = Category::all();
        $products = Product::where('user_id', '=', $user->id)->get();
        $orders = Order::where('user_id', $user->id)->where('paymentstatus', '1')->get();
        $vendor_orders = VendorOrder::where('vendor_id', $user->id)->get();
        $points = Points::all();
        return view('user.dashboard', compact('user', 'categories', 'products', 'orders', 'vendor_orders', 'points'));
    }

    public function profileupdate(Request $request)
    {

        $id = Auth::user();
        $user = User::where('id', $id->id)->first();
        $user->name = $request->name;
        $user->phone = $request->phone;
        $user->address = $request->address;
        $user->size = $request->size;
        $user->country = $request->country;
        $user->shop = $request->shop;

        if ($request->hasfile('photo')) {
            $image1 = $request->file('photo');
            $name = time() . 'profile' . '.' . $image1->getClientOriginalExtension();
            $destinationPath = 'profile/';
            $image1->move($destinationPath, $name);
            $user->photo = 'profile/' . $name;
        }

        if ($request->oldpassword) {
            $oldpass = $request->oldpassword;
            $pass = $user->password;
            if (Hash::check($oldpass, $pass)) {
                $user->password = Hash::make($request->newpassword);
                $user->save();
                Auth::logout();
                $notification = array(
                    'messege' => 'Le mot de passe a été changé avec succès ! Connectez-vous maintenant avec votre nouveau mot de passe',
                    'alert-type' => 'success'
                );
                return Redirect()->route('login')->with($notification);
            } else {
                $notification = array(
                    'messege' => 'Lancien mot de passe ne correspond pas!',
                    'alert-type' => 'error'
                );
                return Redirect()->back()->with($notification);
            }
        }


        $user->update();

        $notification = array(
            'messege' => 'Sauvegarde réussie!',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }

    public function wishlist()
    {
        $product_id = Wishlist::where('user_id', Auth::user()->id)->pluck('product_id');
        $products = Product::whereIn('id', $product_id)->get();
        return view('front.wishlist', compact('products'));
    }

    public function wishlistdelete($id)
    {
        $wishlist = Wishlist::where('product_id', $id)->where('user_id', Auth::user()->id)->first();
        $wishlist->delete();
        $notification = array(
            'messege' => 'Supprimer Succes!',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }

    public function redeempoints()
    {
        $cartitems = Cart::getContent();
        $count = $cartitems->count();
        if ($count > 1) {
            Session::flash('success', 'Vous ne pouvez acheter quun seul produit avec des points');
            return redirect()->route('front.checkout');
        }
        $user = Auth::user();
        $order = new Order();
        $order->user_id = $user->id;
        $order->name = $user->name;
        $order->email = $user->email;
        $order->phone = $user->phone;
        $order->address = "address";
        $order->notes = "";
        $order->country = "Guadeloupe";
        $order->total = "0";
        $order->products = $cartitems;

        $order->order_number = "ON-" . rand(100000000, 900000000);
        $order->save();

        return redirect()->route('payment.success', ['id' => $order->id, 'type' => 'points']);
    }

    public function redeemsweeties()
    {
        $user = Auth::user();
        $cartitems = Cart::getContent();
        $total = Cart::getTotal();
        if ($total > $user->wallet) {
            Session::flash('success', 'Votre solde sweeties est inférieur à votre montant total !');
            return redirect()->route('front.checkout');
        }
        $user->wallet = $user->wallet - $total;
        $user->update();

        $order = new Order();
        $order->user_id = $user->id;
        $order->name = $user->name;
        $order->email = $user->email;
        $order->phone = $user->phone;
        $order->address = "address";
        $order->notes = "";
        $order->country = "Guadeloupe";
        $order->total = "0";
        $order->products = $cartitems;

        $order->order_number = "ON-" . rand(100000000, 900000000);
        $order->save();

        return redirect()->route('payment.success', ['id' => $order->id, 'type' => 'sweeties']);
    }

    public function productcreate()
    {
        return view('user.productcreate');
    }

    public function products()
    {
        $products = Product::where('user_id', '=', Auth::user()->id)->get();
        return view('user.products', compact('products'));
    }

    public function orders()
    {
        $vendor_orders = VendorOrder::where('vendor_id', Auth::user()->id)->get();
        return view('user.orders', compact('vendor_orders'));
    }

    public function orderdetails($id)
    {
        $order = Order::where('id', $id)->first();
        return view('user.orderdetails', compact('order'));
    }

    public function orderstatus($id)
    {
        $order = Order::where('id', $id)->first();
        $order->status = '1';
        $order->update();

        $vendor_order = VendorOrder::where('order_id', $id)->first();
        $vendor_order->status = '2';
        $vendor_order->update();

        foreach (json_decode($order->products) as $item) {
            $product = Product::find($item->id);

            if ($product->user_id == '1') {

            } else {
                $vendor = User::where('id', $product->user_id)->first();
                $vendor->wallet = $vendor->wallet + $product->price;
                $vendor->update();
            }
        }

        $notification = array(
            'messege' => 'Changement de statut de commande',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }

    public function getmypoints($id)
    {
        $points = Points::find($id);
        $user = Auth::user();
        if ($points->points > $user->points) {
            $notification = array(
                'messege' => 'Vous navez pas assez de points pour profiter de ce cadeau',
                'alert-type' => 'error'
            );
            return redirect()->back()->with($notification);
        } else {
            $gift = new Gifts();
            $gift->user_id = $user->id;
            $gift->point_id = $id;
            $gift->save();
            AdminNotificationHelper::pushAdminNotification($user->id,$gift->id,'Demander un cadeau');
            PointsHelper::pushPoints($user->id, $points->points, 'Acheter un cadeau', 'Retirer');
            $user->points = $user->points - $points->points;
            if ($user->update()) {
                $account_sid = 'AC96ac1afdd55310f2214c9bf9b928f326';
                $auth_token = '2776de46ab4e9752bb8d9ec9deef2074';
                $client = new Client($account_sid, $auth_token);

                $client->messages->create(
                    "+590690574700",
                    array(
                        'from' => 'Sweet Diva',
                        'body' => 'Demander des points # ' . 'POINTS' . $gift->point_id . '

Détails du client:
' . $user->name . '
' . $user->email . '
' . $user->phone . '
' . $user->address . '
' . $user->country . '
' . $points->points . '

Daté: ' . $gift->created_at . '
                ',
                    )
                );
            }
            $notification = array(
                'messege' => 'Vous avez bien retiré votre cadeau',
                'alert-type' => 'success'
            );
            return redirect()->back()->with($notification);
        }
    }

    public function voucher(Request $request)
    {
        $cardspayment = CardPayments::where('card_number', '=', $request->code)->where('valid', '=', '0')->first();
        if ($cardspayment) {
            $user = User::where('id', Auth::user()->id)->first();
            $user->wallet = $user->wallet + $cardspayment->price;
            $user->update();

            $cardspayment->valid = '1';
            $cardspayment->update();

            $notification = array(
                'messege' => 'Bon dachat avec succès',
                'alert-type' => 'success'
            );
            return redirect()->back()->with($notification);
        } else {
            $notification = array(
                'messege' => 'Le code promo nest plus valide',
                'alert-type' => 'error'
            );
            return redirect()->back()->with($notification);
        }
    }

    public function userContact()
    {

        return view('front.contact');
    }

    public function userContactStore(Request $request)
    {
        $contact = new Contact();
        $contact->name = $request->name;
        $contact->email = $request->email;
        $contact->subject = $request->subject;
        $contact->message = $request->message;
        $contact->save();

        $notification = array(
            'messege' => 'Votre message Envoyer administrateur',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }

    public function checkSizeStock(Request $request)
    {
        $sizes = Size::where('product_id', $request->id)->where('color', $request->color)->first();
        $quantity = json_decode($sizes->quantity);
        foreach (json_decode($sizes->newsize, true) as $key => $value) {
            if ($value == $request->size) {
                if ($quantity[$key] > 0) {
                    return response()->json(['success' => '1', 'quantity' => $quantity[$key]]);
                } else {
                    return response()->json(['success' => '0', 'quantity' => $quantity[$key]]);
                }
            }
        }
    }
}
