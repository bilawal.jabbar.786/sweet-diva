<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Messages;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MessageController extends Controller
{
    public function sendMessage(Request $request){
        $message = new Messages();
        $message->s_id = Auth::user()->id;
        $message->r_id = $request->r_id;
        $message->message = $request->message;
        $message->save();
        $notification = array(
            'messege' => 'Message envoyé avec succès!',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }
    public function messages(){
        $messages1 = Messages::where('s_id', Auth::user()->id)->orwhere('r_id', Auth::user()->id)->get();
        Messages::where('s_id', Auth::user()->id)->orwhere('r_id', Auth::user()->id)->update(['is_seen' => 0]);
        $messages = $messages1->unique('s_id');
        return view('user.messages', compact('messages'));
    }
    public function chat($id){
        $messages = Messages::orderBy('id','ASC')->where('s_id',Auth::user()->id)
            ->where('r_id',$id)
            ->Orwhere('s_id',$id)
            ->where('r_id',Auth::user()->id)
            ->get();
        return view('user.chat', compact('messages', 'id'));
    }
}
