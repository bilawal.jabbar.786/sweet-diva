<?php

namespace App\Http\Controllers\App;

use App\Banners;
use App\Cards;
use App\Like;
use App\Mail\OtpMail;
use Mail;
use App\SaleProduct;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use PHPUnit\Util\Test;
use App\Category;
use App\Countries;
use App\GeneralSettings;
use App\Http\Controllers\Controller;
use App\MainCategory;
use App\Order;
use App\Points;
use App\Http\NotificationHelper;
use App\PointsTable;
use App\Post;
use App\Product;
use App\Refferal;
use App\Reviews;
use App\Size;
use App\SubCategory;
use App\User;
use App\Notfication;
use App\VendorOrder;
use App\Wishlist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Hash;

use Session;

class AppController extends Controller
{
    public function index()
    {
        $banners = Banners::first();
        $maincategories = MainCategory::where('category_id', '=', '1')->orderBy('no', 'asc')->get();
        $adminproducts = Product::orderBy('id', 'DESC')->where('draft', '=', 1)->where('user_id', '=', '1')->take(8)->get();
        $saleproduct = SaleProduct::find(1);
//        return view('app.index', compact('banners', 'maincategories', 'adminproducts', 'saleproduct'));
//
        return view('errors.app');
    }

    public function register()
    {
        $countries = Countries::all();
        return view('app.auth.register', compact('countries'));
    }

    public function registerIndex(Request $request)
    {
        $user = User::where('email', $request->email)->first();
        if ($user) {
            Session::flash('message', "l\'email existe déjà! essayer à nouveau!");
            return redirect()->back();
        } elseif ($request->password_confirmation != $request->password) {
            Session::flash('message', "Votre mot de passe de confirmation ne correspond pas");
            return redirect()->back();
        } else {
            if ($request->refferal) {
                $ref_user = User::whereNotNull('refferal')->where('refferal', $request->refferal)->first();
                if ($ref_user) {
                    $user = User::create([
                        'name' => $request->name,
                        'surname' => $request->surname,
                        'email' => $request->email,
                        'phone' => $request->phone,
                        'address' => $request->city,
                        'city' => $request->address,
                        'role' => $request->role,
                        'cp' => $request->cp,
                        'dob' => $request->dob,
                        'size' => $request->size,
                        'country' => $request->country,
                        'password' => Hash::make($request->password),
                        'refferal' => 'RF-' . rand(111111, 999999) . '-Sweet-diva',
                    ]);

                    $refferal = new Refferal();
                    $refferal->user_id = $user->id;
                    $refferal->refferal_user = $ref_user->id;
                    $refferal->save();

                    $activity = "Parrainage";
                    $msg = "Congratulations " . $ref_user->name  .' '. $ref_user->surname . " vous avez parrainé " . $user->name  .' '. $user->surname . " et vous recevrez 25 points après son premier achat.";
                    NotificationHelper::pushNotification($msg, $ref_user->device_token, $activity);
                    NotificationHelper::addtoNitification(1, $ref_user->id, $msg, 0, $activity, 1);

          // Send Congratulations refer user
                    $activity1 = "Parrainage";
                    $msg1 = " Vous avez parrainé par " . $ref_user->name  .' '. $ref_user->surname . " et vous recevrez 25 points après votre première commande.";
                    NotificationHelper::pushNotification($msg1, $user->device_token, $activity1);
                    NotificationHelper::addtoNitification(1, $user->id, $msg1, 0, $activity1, 1);

                    // Send Notfication
                    $activity = "Bienvenue";
                    $msg = "Bienvenue  " . $user->name . ' ' . $user->surname . " à l’application sweet diva ici vous aurez accès à toute nos collections nos offres et au suivi de vos points de fidélités.";

                    NotificationHelper::pushNotification($msg, $user->device_token, $activity);
                    NotificationHelper::addtoNitification(1, $user->id, $msg, 0, $activity, 1);
                    Auth::login($user);

                    $notification = array(
                        'messege' => 'Bienvenue'.$user->name.' '.$user->surname.'Sweet Diva App',
                        'alert-type' => 'success'
                    );

                    return redirect('/app/index')->with($notification);

                } else {
                    Session::flash('message', "Votre code de parrainage n'existe pas");
                    return redirect()->back();
                }

            } else {

                $user = User::create([
                    'name' => $request->name,
                    'surname' => $request->surname,
                    'email' => $request->email,
                    'phone' => $request->phone,
                    'address' => $request->city,
                    'city' => $request->address,
                    'role' => $request->role,
                    'cp' => $request->cp,
                    'dob' => $request->dob,
                    'size' => $request->size,
                    'country' => $request->country,
                    'password' => Hash::make($request->password),
                    'refferal' => 'RF-' . rand(111111, 999999) . '-Sweet-diva',
                ]);

                // Send Notfication
                $activity = "Bienvenue";
                $msg = "Bienvenue  " . $user->name . ' ' . $user->surname . " à l’application sweet diva ici vous aurez accès à toute nos collections nos offres et au suivi de vos points de fidélités.";

                NotificationHelper::pushNotification($msg, $user->device_token, $activity);
                NotificationHelper::addtoNitification(1, $user->id, $msg, 0, $activity, 1);
                Auth::login($user);
                $notification = array(
                    'messege' => 'Bienvenue'.$user->name.' '.$user->surname.'Sweet Diva App',
                    'alert-type' => 'success'
                );

                return redirect('/app/index')->with($notification);

            }

        }


    }

    public function login()
    {
        return view('app.auth.login');
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('app.index');

    }

    public function loginIndex(Request $request)
    {
        $user = User::where('email', $request->email)->first();
        if ($user) {
            if (Hash::check($request->password, $user->password)) {
                if (Auth::loginUsingId($user->id)) {
                    $notification = array(
                        'messege' => 'Votre connexion a réussiblogin',
                        'alert-type' => 'success'
                    );

                    return \redirect()->route('app.index')->with($notification);

                } else {
                    Session::flash('message', 'Les identifiants ne correspondent pas!');
                    return redirect()->back();
                }
            } else {
                Session::flash('message', 'Les identifiants ne correspondent pas!');
                return redirect()->back();
            }
        } else {
            Session::flash('message', 'Utilisateur avec cet e-mail introuvable!');
            return redirect()->back();
        }
    }

    public function forgetPassword()
    {
        return view('app.auth.forget');
    }

    public function sendOtpEmail(Request $request)
    {
        $otp = rand(1000, 9999);
        $user = User::where('email', '=', $request->email)->first();
        if ($user) {
            $users = User::where('email', '=', $request->email)->update(['otp' => $otp]);
            $email = $request->email;
            $dataa = array(
                'otp' => $otp,
            );

            Mail::to($email)->send(new  OtpMail($dataa));
            Session::flash('message', "  Nous avons envoyé votre lien de réinitialisation de mot de passe par e-mail !");
            return view('app.auth.otp', compact('email'));
        } else {
            Session::flash('message', " Votre E-mail n’est pas valide !");
            return redirect()->back();
        }

    }

    public function otpEmailVerify(Request $request)
    {

        $user = User::where('email', $request->email)->where('otp', $request->otp)->first();

        $email = $request->email;
        if ($user) {

            $users = User::where('email', '=', $request->email)->update(['otp' => null]);
            Session::flash('message', "  Votre match otp entrez un nouveau mot de passe !");
            return view('app.auth.newpassword', compact('email'));
        } else {

            Session::flash('message', " Votre Otp n'est pas sorti!");
            return view('app.auth.otp', compact('email'));
        }

    }

    public function appPassword(Request $request)
    {
        $password = Hash::make($request->password);
        $users = User::where('email', '=', $request->email)->first();
        $user = User::where('email', '=', $request->email)->update(['password' => $password]);
        auth()->login($users, true);
        return redirect('/app/index');

    }

    public function subCategory($id)
    {
        $maincategory = MainCategory::where('id', '=', $id)->first();
        $title = $maincategory->name;
        $subcategories = SubCategory::where('category_id', '=', $id)->get();

        return view('app.pages.category.subcategory', compact('subcategories', 'title'));
    }

    public function mainCategory($id)
    {
        $title = 'Sous-catégorie';
        $maincategories = SubCategory::where('category_id', '=', $id)->get();

        return view('app.pages.category.maincategory', compact('maincategories', 'title'));
    }

    public function productIndex($id)
    {
        $title = 'Produits';
        $product = Product::where('category_id', $id)->where('draft', '=', 1)->latest()->paginate(10);

        return view('app.pages.product.product', compact('product', 'title'));
    }

    public function singleProduct($id)
    {
        $product = Product::where('id', '=', $id)->first();
        $reviews = Reviews::where('order_id', '=', $id)->latest()->take(8)->get();
        $title = $product->title;
        return view('app.pages.product.single', compact('product', 'title', 'id', 'reviews'));
    }

    public function settingIndex()
    {
        $title = 'Réglage';
        return view('app.pages.setting.index', compact('title'));
    }

    public function editProfile()
    {
        $title = 'Editer le profil';
        $user = Auth::user();
        return view('app.pages.setting.profile', compact('title', 'user'));
    }

    public function allProduct()
    {
        $title = 'Tous les produits';
        $product = Product::where('draft', '=', 1)->latest()->paginate(10);
        $color = Size::pluck('color')->unique();
        $size = Size::whereNotNull('newsize')->pluck('newsize')->unique();

        return view('app.pages.product.allproduct', compact('title', 'product', 'color', 'size'));
    }

    public function appTerms()
    {
        $title = 'Termes et conditions';
        $gs = GeneralSettings::first();
        return view('app.pages.setting.terms', compact('gs','title'));
    }

    public function appPrivicy()
    {
        $title = 'Politique de confidentialité';
        $gs = GeneralSettings::first();

        return view('app.pages.setting.privicy', compact('title', 'gs'));
    }
    public function appPrivicy2()
    {
        $title = 'Politique de confidentialité';
        $gs = GeneralSettings::first();

        return view('app.pages.setting.privcy2', compact('title', 'gs'));
    }

    public function appAbout()
    {
        $title = 'A PROPOS DE NOUS';
        $gs = GeneralSettings::first();
        $arr = [3, 4, 5];
        $shops = User::whereIn('role', $arr)->get();

        return view('app.pages.setting.about', compact('title', 'gs', 'shops'));
    }
    public function appAbout2()
    {
        $title = 'A PROPOS DE NOUS';
        $gs = GeneralSettings::first();
        $arr = [3, 4, 5];
        $shops = User::whereIn('role', $arr)->get();

        return view('app.pages.setting.about2', compact('title', 'gs', 'shops'));
    }

    public function appNotfication()
    {
        $title = 'Notification';
        $user = Auth::user();
        $notfications = Notfication::latest()->where('r_id', '=', $user->id)->paginate('20');
        Notfication::latest()->where('r_id', '=', $user->id)->update(['status' => 1]);
        return view('app.pages.setting.notfication', compact('title', 'notfications'));
    }

    public function myProfile()
    {
        $title = 'Mon profil';
        $user = Auth::user();
        return view('app.pages.setting.myprofile', compact('title', 'user'));
    }

    public function appMyOrder()
    {
        $title = 'Mes Commandes';
        $user = Auth::user();
        $categories = Category::all();
        $products = Product::where('draft', '=', 1)->where('user_id', '=', $user->id)->get();
        $orders = Order::where('user_id', $user->id)->where('paymentstatus', '1')->get();
        $vendor_orders = VendorOrder::where('vendor_id', $user->id)->get();
        $points = Points::all();
        return view('app.users.order.myorder', compact('title', 'user', 'categories', 'products', 'orders', 'vendor_orders', 'points'));
    }

    public function appMySingleOrder($id)
    {
        $title = 'Détails de la commande';
        $order = Order::where('id', $id)->first();
        return view('app.users.order.single', compact('order', 'title'));
    }

    public function wishlist($id)
    {
        $wishlist = Wishlist::where('user_id', Auth::user()->id)->where('product_id', $id)->first();
        if (!$wishlist) {
            $wish = new Wishlist();
            $wish->user_id = Auth::user()->id;
            $wish->product_id = $id;
            $wish->save();
        }
        return response()->json(['success' => '1']);
    }

    public function wishlistProduct()
    {
        $title = 'Liste de souhaits Produit';
        $product_id = Wishlist::where('user_id', Auth::user()->id)->pluck('product_id');
        $product = Product::whereIn('id', $product_id)->where('draft', '=', 1)->latest()->paginate(10);
        return view('app.users.favorite', compact('title', 'product'));
    }

    public function appCart()
    {
        $title = 'Mon panier';
        $cartitems = \Cart::getContent();
        $cartTotalQuantity = \Cart::getTotalQuantity();
        $total = \Cart::getTotal();
//        if (Auth::user()) {
//            $users = User::where('id', '=', Auth::user()->id)->whereMonth('dob', '=', date('m'))->whereDay('dob', '=', date('d'))->first();
//            if ($users) {
//                $total2 = ($total * 10) / 100;
//                $total1 = $total - $total2;
//            } else {
//                $total1 = $total;
//                $total2 = 0;
//            }
//        } else {
//            $total1 = $total;
//            $total2 = 0;
//        }
        $total1 = $total;
           $total2 = 0;

        return view('app.pages.product.cart.cart', compact('title', 'cartitems', 'cartTotalQuantity', 'total', 'total1','total2'));

    }

    public function appSearch(Request $request)
    {

        $product = Product::where('title', 'LIKE', '%' . $request->keywords . '%')->where('draft', '=', 1)->get();
        $title = 'Tous les produits';
        return view('app.pages.product.allproduct', compact('title', 'product'));

    }

    public function productSizes(Request $request)
    {
        $product = Product::find($request->id);
        $quantity = json_decode($product->quantity);

        $sizes = Size::where('product_id', $request->id)->where('color', $request->color)->first();
        if ($sizes) {
            $size = json_decode($sizes->newsize);
            return response()->json(['success' => '1', 'size' => $size]);
        } else {
            return response()->json(['success' => '1', 'size' => 0]);
        }
    }

    public function checkout()
    {
        $title = 'Détails de facturation';
        $cartitems = \Cart::getContent();
        $countries = Countries::all();
        $total = \Cart::getTotal();

        $users = User::where('id', '=', Auth::user()->id)->whereMonth('dob', '=', date('m'))->whereDay('dob', '=', date('d'))->first();
        if ($users) {
            $total2 = ($total * 10) / 100;
            $total = $total - $total2;
        }

//        if (auth()->user()->country == "Guadeloupe" || auth()->user()->country == "Martinique" || auth()->user()->country == "French Guiana") {
//            $shipping = 5;
//        } else {
//            $shipping = 10;
//        }
//        if (isset($total)) {
//            \Stripe\Stripe::setApiKey(env('STRIPE_SECRET_KEY'));
//            $payment_intent = \Stripe\PaymentIntent::create([
//                'amount' => ($total + $shipping) * 100,
//                'currency' => 'EUR'
//            ]);
//        }

//        $intent = $payment_intent->client_secret;
        return view('app.pages.product.cart.checkout', compact('cartitems', 'countries', 'total', 'title'));
    }

    public function thisMonthProduct()
    {
        $product = Product::where('month', 1)->where('user_id', '1')->where('draft', '=', 1)->latest()->paginate(10);;
        $title = 'Tous les produits';

        return view('app.pages.product.allproduct1', compact('product', 'title'));
    }

    public function thisSoldProduct()
    {
        $product = Product::where('sold', 1)->where('draft', '=', 1)->where('user_id', '1')->latest()->paginate(10);;
        $title = 'Tous les produits';

        return view('app.pages.product.allproduct1', compact('product', 'title'));
    }

    public function productSearchSize($size)
    {
        $sizee = decrypt($size);
        $color_size = Size::where('newsize', 'like', '%' . $sizee . '%')->pluck('product_id')->unique();
        $product = Product::whereIn('id', $color_size)->where('draft', '=', 1)->latest()->paginate(10);
        $title = 'Tous les produits';
        return view('app.pages.product.allproduct1', compact('product', 'title'));
    }
//    public function productSearchSize($size){
//        $sizee =decrypt($size);
//        $data=[];
////        $color_size = Size::where('newsize', 'like', '%' . $sizee . '%')->pluck('product_id')->unique();
////
//        $color_size1 = Size::where('newsize', 'like', '%' . $sizee . '%')->pluck('newsize')->unique();
//        $quantity = Size::where('newsize', 'like', '%' . $sizee . '%')->pluck('quantity')->unique();
//
//        foreach (json_decode($color_size1) as $row){
//
//            foreach (json_decode($row) as $key => $row1){
//
//                if($row1==$sizee){
//                    if($quantity[$key]  > 0 ){
//                        $product_id = Size::where('newsize', 'like', '%' . $row1 . '%')->where('quantity', 'like', '%' . $quantity[$key] . '%')->first();
//                        $data=$product_id;
//
//                    }
//                }
//                else{
//                    $data = 0;
//                }
//
//            }
//        }
//
//        $product = Product::whereIn('id', $data)->get();
//        $title = 'Tous les produits';
//        return view('app.pages.product.allproduct1', compact('product','title'));
//    }
    public function productSearch(Request $request)
    {

        $product = Product::where('title', 'LIKE', '%' . $request->keywords . '%')->where('draft', '=', 1)->latest()->paginate(10);
        $title = 'Tous les produits';
        return view('app.pages.product.allproduct4', compact('product', 'title'));
    }

    public function allCards()
    {

        $title = 'Vos cartes';

        $cards = Cards::orderBy('id', 'DESC')->get();
        return view('app.users.cards.index', compact('cards', 'title'));
    }

    public function cardView($id)
    {
        $card = Cards::find($id);
        $title = 'Carte unique';
        return view('app.users.cards.singcard', compact('card', 'title'));
    }

    public function cardpay(Request $request, $id)
    {

        $card = Cards::find($id);

        \Cart::add($card->sku, $card->title, $request->price, 1, array('size' => 0,
            'color' => 0,
            'type' => 0,
            'sendername' => $request->sendername,
            'senderphone' => $request->senderphone,
            'r_name' => $request->name,
            'r_email' => $request->email,
            'message' => $request->message,
        ));

        return redirect()->route('app.cart');
    }

    public function appPoints()
    {
        $points = Points::all();
        $title = 'Points de fidélité';
        return view('app.users.points.index', compact('points', 'title'));
    }

    public function appFilter()
    {

        $title = 'Filtre de caméra';
        return view('app.camera.index', compact('title'));
    }

    public function productReview(Request $request, $id)
    {

        $review = new Reviews();
        $user = Auth::user();
        $review->message = $request->message;
        $review->star = $request->star;
        $review->sender_id = $user->id;
        $review->order_id = $id;


        if ($review->save()) {
            $notification = array(
                'messege' => 'Votre avis ajouté',
                'alert-type' => 'success'
            );
            return redirect()->back()->with($notification);

        }


    }

    public function ltyPoints()
    {
        $points = PointsTable::where('user_id', '=', Auth::user()->id)->get();
        $title = 'Historique des points de fidélité';
        return view('app.users.ltypoints', compact('points', 'title'));
    }

    public function sponserHistory()
    {
        $refferal = Refferal::where('refferal_user', '=', Auth::user()->id)->get();
        $title = 'Code de Parrainage';
        return view('app.users.sponser', compact('refferal', 'title'));
    }

    public function generateCode()
    {
        $ref = User::where('id', '=', Auth::user()->id)->first();
        $ref->refferal = 'RF-' . rand(111111, 999999) . '-Sweet-diva';
        $ref->update();
        $notification = array(
            'messege' => 'Votre code de parrainage généré',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }

    public function sweetlook()
    {
        $title = "Sweet Look";
        $posts = Post::latest()->get();
        return view('app.sweetlook.index', compact('title', 'posts'));
    }

    public function sweetlookPost(Request $request)
    {
        $post = new Post();

        $post->user_id = Auth::user()->id;
        $post->description = $request->description;
        $post->filter = $request->filter;
        if ($request->hasfile('img')) {

            $image1 = $request->file('img');
            $name = time() . 'img' . '.' . $image1->getClientOriginalExtension();
            $destinationPath = 'app/post/';
            $image1->move($destinationPath, $name);
            $post->img = 'app/post/' . $name;
        }

        if ($post->save()) {
            $notification = array(
                'messege' => 'Téléversé votre message',
                'alert-type' => 'success'
            );
            return redirect()->back()->with($notification);

        }

    }

    public function myPost()
    {
        $title = "Mon Message";
        $posts = Post::where('user_id', '=', Auth::user()->id)->latest()->get();
        return view('app.sweetlook.index', compact('title', 'posts'));

    }

    public function likePost($id)
    {
        $likePost = Like::where('user_id', Auth::user()->id)->where('post_id', $id)->first();
        if (!$likePost) {
            $like = new Like();
            $like->user_id = Auth::user()->id;
            $like->post_id = $id;
            $like->save();
        }
        return response()->json(['success' => '1']);
    }

    public function offTerms()
    {

        return view('app.auth.terms');

    }
    public function offTerms2()
    {

        return view('app.auth.terms2');

    }
    public function indexold()
    {

        return view('errors.coming');

    }

    public function filterProduct(Request $request)
    {
        $start = '1';
        $end = $request->price;
        $products = Product::whereBetween('price', [(int)$start, (int)$end])->where('draft', '=', 1)->pluck('id');
        $color_size = Size::whereIn('product_id', $products)->orwhere('newsize', 'like', '%' . $request->size . '%')->orwhere('color', 'like', '%' . $request->color . '%')->pluck('product_id')->unique();
        $product = Product::whereIn('id', $color_size)->where('draft', '=', 1)->get();
        $title = 'Tous les produits';
        $color = Size::pluck('color')->unique();
        $size = Size::whereNotNull('newsize')->pluck('newsize')->unique();


        return view('app.pages.product.allproduct2', compact('title', 'product', 'color', 'size'));

    }

}
