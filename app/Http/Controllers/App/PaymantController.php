<?php

namespace App\Http\Controllers\App;

use App\CardPayments;
use App\Cards;
use App\Countries;
use App\GeneralSettings;
use App\Http\Controllers\Controller;
use App\Http\NotificationHelper;
use App\Mail\BonCadeau;
use App\Mail\Facture;
use App\Order;
use App\Points;
use App\PointsTable;
use App\Product;
use App\PromoCode;
use App\Refferal;
use App\Size;
use App\User;
use App\VendorOrder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Mollie\Laravel\Facades\Mollie;
use GuzzleHttp\Client as GuzzleClient;
use PDF;
use Session;
use Illuminate\Support\Facades\Mail;
use Twilio\Rest\Client;

class PaymantController extends Controller
{
    public function checkoutSubmit(Request $request)
    {
        $cartitems = \Cart::getContent();
        $total = $request->finalamount;

        $order = new Order();
        $order->user_id = $request->user_id;
        $order->name = $request->name;
        $order->email = $request->email;
        $order->resource = 'Application Mobile';
        $order->phone = $request->phone;
        $order->address = $request->address;
        $order->shipping = $request->shipping;
        $order->notes = $request->notes;
        $order->country = $request->country;
        $order->total = $request->finalamount;
        $order->discount = $request->discount;
        if($request->discount){
            $promo = PromoCode::find($request->promo_id);
            $promo->status = 0;
            $promo->update();
        }
        $order->products = $cartitems;
        $order->order_number = "ON-" . rand(100000000, 900000000);
        $order->save();
        if ($request->pay == "installments") {
            $order->payment_method = 'ALMA';
            $order->save();
            $apiKey = env("ALMA_KEY", "sk_test_4GmA9z5tQOKOUiI4IEIIEO69");
            $data = [
                "payment" => [
                    "purchase_amount" => $request->finalamount * 100,
                    "return_url" => route('app.payment.success', ['id' => $order->id, 'type' => 'order']),
                    "shipping_address" => [
                        "first_name" => $request->name,
                        "last_name" => $request->surname,
                        "line1" => $request->address,
                        "postal_code" => $request->cp,
                        "city" => $request->city,
                    ],
                    "locale" => "fr",
                ],

            ];
            $dataString = json_encode($data);

            $headers = [
                'Authorization: Alma-Auth ' . $apiKey,
                'Content-Type: application/json',
            ];

            $ch = curl_init();
            if (env("ALMA_ENV") == "sandbox") {
                curl_setopt($ch, CURLOPT_URL, 'https://api.sandbox.getalma.eu/v1/payments');
            } else {
                curl_setopt($ch, CURLOPT_URL, 'https://api.getalma.eu/v1/payments');
            }
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);

            $response1 = curl_exec($ch);
            $response = json_decode($response1);

            return redirect($response->url);

        } else if ($request->pay == "complete") {
            $order->payment_method = 'Stripe';
            $order->save();
            return redirect()->route('app.front.stripe', ['price' => $request->finalamount, 'id' => $order->id]);
//            return $this->paymentsuccess($order->id, 'order');
        } else if ($request->pay == "pad") {

            $order->payment_method = 'pad';
            $order->save();
            $id = $order->id;
            $title = 'Détails de facturation';
            $cartitems = \Cart::getContent();
            $del = \Cart::getTotal();
            $total = $order->total;

//            $users = User::where('id', '=', Auth::user()->id)->whereMonth('dob', '=', date('m'))->whereDay('dob', '=', date('d'))->first();
//            if ($users) {
//                $total2 = ($total * 10) / 100;
//                $total = $total - $total2;
//            }

            return view('app.pages.product.cart.padcheckout', compact('cartitems', 'total', 'title','id','del'));

        }else {
            dd("nothing");
        }


        /* $payment = Mollie::api()->payments->create([
             "amount" => [
                 "currency" => "GBP",
                 "value" => number_format($request->finalamount, 2) // You must send the correct number of decimals, thus we enforce the use of strings
             ],
             "description" => "Order #". $order->id,
             "redirectUrl" => route('payment.success', ['id' => $order->id, 'type' => 'order']),
             "metadata" => [
                 "order_id" => $order->id,
             ],
         ]);
         return redirect($payment->getCheckoutUrl(), 303);*/
    }

    public function stripe($price, $id)
    {
        return view('app.stripe', compact('price', 'id'));
    }
    public function stripesubmit(Request $request)
    {
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET_KEY'));
        try {
            \Stripe\Charge::create(array(
                "amount" => $request->price * 100,
                "currency" => "eur",
                "source" => $request->input('stripeToken'), // obtained with Stripe.js
                "description" => "Paiement par carte " . Auth::user()->name . " " .  Auth::user()->surname . " " . Auth::user()->email
            ));
            return $this->paymentsuccess($request->id, 'order');
        } catch (\Exception $e) {
            Session::flash('fail-message', "Erreur! Veuillez réessayer. ". $e->getMessage());
            return redirect()->back();
        }
    }
    public function paymentsuccess($id, $type)
    {
        $order = Order::find($id);
        $order->paymentstatus = '1';
        $order->invoice = '050' . $id;
        $order->update();
        $gs = GeneralSettings::find(1);
        $user = User::where('id', Auth::user()->id)->first();
        $points = Points::find(1);

        foreach (json_decode($order->products) as $item) {
            if ($item->attributes->type == 0) {
                $new = new CardPayments();
                $new->card_id = $item->id;
                $new->sendername = $item->attributes->sendername;
                $new->senderphone = $item->attributes->senderphone;
                $new->r_name = $item->attributes->r_name;
                $new->r_email = $item->attributes->r_email;
                $new->message = $item->attributes->message;
                $new->price = $item->price;
                $new->paymentstatus = '1';
                $new->card_number = rand(100000000, 900000000);
                $new->save();

                $cards = Cards::where('sku', $item->id)->first();
                Mail::to($new->r_email)->send(new BonCadeau($new, $cards));

            } else {

                $product = Product::find($item->id);
                if ($product) {
                    $checkorder = VendorOrder::where('vendor_id', $product->user_id)->where('order_id', $order->id)->first();
                    if (!$checkorder) {
                        if ($product->user_id == '1') {
                            $sizeproduct = Size::where('product_id', $item->id)->where('color', $item->attributes->color)->first();
                            $quantity = json_decode($sizeproduct->quantity);
                            $size = json_decode($sizeproduct->newsize);
                            foreach ($size as $key => $value) {
                                if ($value == $item->attributes->size) {

                                    $stock = $quantity[$key] - $item->quantity;
                                    $quantity[$key] = (string)$stock;
                                    $sizeproduct->quantity = $quantity;
                                    $sizeproduct->update();
                                }
                            }
                        }
                        $product->status = '1';
                        $product->update();

                        if ($type == 'order') {
                            $user = User::where('id', Auth::user()->id)->first();
                            $user->points = $user->points + $product->price;
                            $user->update();
                        }
                    }
                }
            }
        }

        $check = VendorOrder::where('vendor_id', '1')->where('order_id', $order->id)->first();
        if (!$check){
            $vendororder = new VendorOrder();
            $vendororder->vendor_id = 1;
            $vendororder->order_id = $order->id;
            if($order->payment_method=='pad'){
                $vendororder->status = 2;
            }
            $vendororder->save();

            $data1 = [
                'invoice' => $order,
                'gs' => $gs
            ];
            $destinationPath = 'records';
            $taxform_name = 'facture-' . $order->order_number . '.pdf';
            $filepath = $destinationPath . '/' . $taxform_name;

            $pdf = PDF::loadView('myPDF', $data1);

            $pdf->stream();
            file_put_contents($filepath, $pdf->output());

            Mail::to($order->email)->send(new Facture($order, $gs, $filepath));

            $account_sid = 'AC96ac1afdd55310f2214c9bf9b928f326';
            $auth_token = '2776de46ab4e9752bb8d9ec9deef2074';
            $client = new Client($account_sid, $auth_token);

            $client->messages->create(
                "+590690574700",
                array(
                    'from' => 'Sweet Diva',
                    'body' => 'Nouvelle commande # ' . $order->order_number . '

Détails du client:
' . $order->name . '
' . $order->email . '
' . $order->phone . '
' . $order->address . '
' . $order->country . '
' . $order->payment_method . '
' . $order->total . '

Voir la commande
'. route('order.view', ['id' => $order->id ]). '

Daté: ' . $order->created_at . '
                ',
                )
            );
            $client->messages->create(
                "+590690399802",
                array(
                    'from' => 'Sweet Diva',
                    'body' => 'Nouvelle commande # ' . $order->order_number . '

Détails du client:
' . $order->name . '
' . $order->email . '
' . $order->phone . '
' . $order->address . '
' . $order->country . '
' . $order->payment_method . '
' . $order->total . '

Voir la commande
'. route('order.view', ['id' => $order->id ]). '

Daté: ' . $order->created_at . '
                ',
                )
            );
            \Cart::clear();
            $activity = "Confirmation de commande";

            $msg = "Toutes nos félicitations " . $user->name . ' ' . $user->surname . " votre commande a bien été prise en compte votre numéro de suivi vous sera communiqué une fois votre colis pris en charge par les services postaux";

            NotificationHelper::pushNotification($msg, $user->device_token, $activity);
            NotificationHelper::addtoNitification(1, $user->id, $msg, $order->id, $activity, 1);

        }
//        if($order->payment_method=='pad'){
//            $notification = array(
//                'messege' => 'Message envoyé avec succès !',
//                'alert-type' => 'success'
//            );
//
//            return redirect()->route('order.pad')->with($notification);
//        }
//        else{
            return view('app.pages.product.cart.paymantsuccess', compact('order'));
//        }

    }

    public function padSuccess($id)
    {
        $order = Order::find($id);
        \Cart::clear();
        return $this->paymentsuccess($order->id, 'order');
    }
}
