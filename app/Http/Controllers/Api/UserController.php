<?php
namespace App\Http\Controllers\Api;
use App\Http\NotificationHelper;
use App\Http\Resources\UserResource;
use App\Refferal;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;
class UserController extends Controller
{
    public $successStatus = 200;
    /**
     * login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(){
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
            $user = Auth::user();
            if ($user->status == 1){
                return response()->json(['error'=>'Unauthorised'], 401);
            }else{
                $success['token'] =  $user->createToken('MyApp')-> accessToken;
                $success['user'] =  new UserResource($user);
                return response()->json(['success' => $success], $this-> successStatus);
            }
        }
        else{
            return response()->json(['error'=>'Unauthorised'], 401);
        }
    }
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
        ]);
        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $input['surname'] = $input['sur_name'];
        $input['refferal'] = 'RF-' . rand(111111, 999999) . '-Sweet-diva';
        $input['role'] = "2";

        $user = User::create($input);
        $myuser = User::find($user->id);
        $success['token'] =  $user->createToken('MyApp')-> accessToken;

        if ($request->sponser) {
            $ref_user = User::whereNotNull('refferal')->where('refferal', $request->sponser)->first();
            $refferal = new Refferal();
            $refferal->user_id = $myuser->id;
            $refferal->refferal_user = $ref_user->id;
            $refferal->save();

            $activity = "Parrainage";
            $msg = "Congratulations " . $ref_user->name  .' '. $ref_user->surname . " vous avez parrainé " . $myuser->name  .' '. $myuser->surname . " et vous recevrez 25 points après son premier achat.";
            NotificationHelper::pushNotification($msg, [$ref_user->device_token], $activity);
            NotificationHelper::addtoNitification(1, $ref_user->id, $msg, 0, $activity, 1);

            // Send Congratulations refer user
            $activity1 = "Parrainage";
            $msg1 = " Vous avez parrainé par " . $ref_user->name  .' '. $ref_user->surname . " et vous recevrez 25 points après votre première commande.";
            NotificationHelper::pushNotification($msg1, [$myuser->device_token], $activity1);
            NotificationHelper::addtoNitification(1, $myuser->id, $msg1, 0, $activity1, 1);

        }
        $activity = "Bienvenue";
        $msg = "Bienvenue  " . $myuser->name . ' ' . $myuser->surname . " à l’application sweet diva ici vous aurez accès à toute nos collections nos offres et au suivi de vos points de fidélités.";

        NotificationHelper::pushNotification($msg, [$myuser->device_token], $activity);
        NotificationHelper::addtoNitification(1, $myuser->id, $msg, 0, $activity, 1);

        $success['user'] =  new UserResource($myuser);
        return response()->json(['success'=>$success], $this-> successStatus);
    }
    /**
     * details api
     *
     * @return \Illuminate\Http\Response
     */
    public function details()
    {
        $user = Auth::user();
        $data = new UserResource($user);
        return response()->json($data);
    }
    public function deleteAccount(Request $request){
        $user = User::where('email', $request->email)->first();
        $user->status = 1;
        $user->update();
        return response()->json(['success'=>"delete success"], 200);
    }
}
