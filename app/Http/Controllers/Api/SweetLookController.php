<?php

namespace App\Http\Controllers\Api;

use App\Comment;
use App\Follow;
use App\Http\Controllers\Controller;
use App\Http\NotificationHelper;
use App\Http\Resources\CommentResource;
use App\Http\Resources\PostResource;
use App\Http\Resources\ProductCollection;
use App\Http\Resources\ProfileResource;
use App\Http\Resources\UserResource;
use App\Like;
use App\Post;
use App\Product;
use App\Topic;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class SweetLookController extends Controller
{
    public function addPost(Request $request)
    {
        $post = new Post();
        $post->user_id = auth()->user()->id;
        $post->description = $request->description;
        if ($request->hasfile('image')) {
            $image1 = $request->file('image');
            $name = time() . 'img' . '.' . $image1->getClientOriginalExtension();
            $destinationPath = 'app/post/';
            $image1->move($destinationPath, $name);
            $post->img = 'app/post/' . $name;
        }else{
            return response()->json(['error' => 'File not found'], 400);
        }
        $post->save();
        return response()->json(['success' => 'Post Added Successfully'], 200);
    }
    public function myPost()
    {
        $posts = Post::where('user_id', '=', auth()->user()->id)->latest()->get();
        $data = PostResource::collection($posts);
        return response()->json($data);
    }
    public function likePost($id)
    {
        $likePost = Like::where('user_id', auth()->user()->id)->where('post_id', $id)->first();
        $post = Post::find($id);
        if (!$likePost) {
            $like = new Like();
            $like->user_id = auth()->user()->id;
            $like->post_id = $id;
            $like->save();
            $post->top10 = $post->top10 + 1;
            $post->save();
        }else{
            $likePost->delete();
            $post->top10 = $post->top10 - 1;
            $post->save();
        }
        return response()->json(['success' => 'success'], 200);
    }
    public function commentPost(Request $request)
    {
        $comment = new Comment();
        $comment->user_id = auth()->user()->id;
        $comment->post_id = $request->post_id;
        $comment->comment = $request->comment;
        $comment->save();
        return response()->json(['success' => 'comment added successfully'], 200);
    }
    public function allPosts()
    {
        $posts = Post::where('topic_id', null)->latest()->get();
        $data = PostResource::collection($posts);
        return response()->json($data);
    }
    public function topSweet()
    {
        $products = Product::where('top_sweet', 1)->where('draft', '=', 1)->where('user_id', '1')->latest()->get();
        $data = ProductCollection::collection($products);
        return response()->json($data);
    }
    public function top10()
    {
        $posts = Post::orderBy('top_10', 'asc')->where('topic_id', null)->take(10)->get();
        $data = PostResource::collection($posts);
        return response()->json($data);
    }
    public function astuce()
    {
        $posts = Post::where('user_id', '=','1')->where('astuce', 1)->latest()->get();
        $data = PostResource::collection($posts);
        return response()->json($data);
    }
    public function comments($id){
        $comments = Comment::where('post_id', $id)->get();
        $data = CommentResource::collection($comments);
        return response()->json($data);
    }
    public function followers(){
        $followers = Follow::where('user_id', auth()->user()->id)->pluck('f_id');
        $users = User::whereIn('id', $followers)->get();
        $data = UserResource::collection($users);
        return response()->json($data);
    }
    public function profile($id){
        $user = User::find($id);
        $data = new ProfileResource($user);
        return response()->json($data);
    }
    public function singlePost($id){
        $post = Post::find($id);
        $data = new PostResource($post);
        return response()->json($data);
    }
    public function follow($id)
    {
        $follow = Follow::where('user_id', auth()->user()->id)->where('f_id', $id)->first();
        if (!$follow) {
            $follow = new Follow();
            $follow->user_id = auth()->user()->id;
            $follow->f_id = $id;
            $follow->save();
        }else{
            $follow->delete();
        }
        return response()->json(['success' => 'success'], 200);
    }
    public function topics(){
        $topics = Topic::select('id', 'name')->where('person', null)->latest()->get();
        return $topics;
    }
    public function topicPosts($id){
        $posts = Post::where('topic_id', $id)->where('user_id', 1)->get();
        $data = PostResource::collection($posts);
        return response()->json($data);
    }
    public function access(){
        $user = auth()->user();
        if ($user->sweetlook == 0){
            return response()->json(['status' => 0]);
        }else{
            return response()->json(['status' => 1]);
        }
    }
    public function grantAccess(){
        $user = auth()->user();
        $user->sweetlook = 1;
        $user->update();
        return response()->json(['status' => 1]);
    }
    public function astucePersons($id, $name){
        $posts = Post::where('topic_id', $id)->where('name', $name)->get();
        $data = PostResource::collection($posts);
        return response()->json($data);
    }
    public function topicsPersons($name){
        $topics = Topic::where('person', $name)->select('id', 'name')->latest()->get();
        return $topics;
    }
}
