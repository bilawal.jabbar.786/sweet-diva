<?php

namespace App\Http\Controllers\Api;

use App\Banners;
use App\Cards;
use App\Cart;
use App\Contact;
use App\GeneralSettings;
use App\Gifts;
use App\Http\AdminNotificationHelper;
use App\Http\Controllers\Controller;
use App\Http\PointsHelper;
use App\Http\Resources\CartResource;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\GiftHistoryResource;
use App\Http\Resources\OrderResource;
use App\Http\Resources\PointsHistoryResource;
use App\Http\Resources\ProductCollection;
use App\Http\Resources\ReferralResource;
use App\Http\Resources\UserResource;
use App\Like;
use App\Mail\OtpMail;
use App\MainCategory;
use App\Notfication;
use App\Order;
use App\Points;
use App\PointsTable;
use App\Post;
use App\Product;
use App\PromoCode;
use App\Refferal;
use App\Size;
use App\User;
use App\Wishlist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Mail;
use Twilio\Rest\Client;

class ApiController extends Controller
{
    public function settings(){
        $gs = GeneralSettings::find(1);
        return response()->json($gs);
    }
    public function banners(){
        $banners = Banners::select('image1','image2','image3')->first();
        return response()->json($banners);
    }
    public function categories(){
        $maincategories = MainCategory::where('category_id', '=', '1')->orderBy('no', 'asc')->get();
        return response()->json(CategoryResource::collection($maincategories));
    }
    public function products(){
        $products = Product::where('draft', '=', 1)->get();
        $data = ProductCollection::collection($products);
        return response()->json($data);
    }
    public function categoriesProduct($id){
        $products = Product::where('category_id', $id)->where('draft', '=', 1)->latest()->get();
        $data = ProductCollection::collection($products);
        return response()->json($data);
    }
    public function singleProduct($id){
        $product = Product::find($id);
        return response()->json($product);
    }
    public function fetchSize($id, $color){
        $mycolor = "#".$color;
        $sizes = Size::where('product_id', $id)->where('color', $mycolor)->first();
        if ($sizes) {
            $size = json_decode($sizes->newsize);
            return response()->json(['success' => '1', 'size' => $size]);
        } else {
            return response()->json(['success' => '1', 'size' => 0]);
        }
    }
    public function fetchStock(Request  $request){
        $mycolor = "#".$request->color;
        $sizes = Size::where('product_id', $request->id)->where('color', $mycolor)->first();
        $quantity = json_decode($sizes->quantity);
        foreach (json_decode($sizes->newsize, true) as $key => $value) {
            if ($value == $request->size) {
                if ($quantity[$key] > 0) {
                    return response()->json(['success' => '1', 'quantity' => $quantity[$key]]);
                } else {
                    return response()->json(['success' => '0', 'quantity' => $quantity[$key]]);
                }
            }
        }
    }
    public function addtocart(Request $request){
        $checkAlreadyAvailable = Cart::where('device_id', $request->device_id)->where('product_id', $request->product_id)->first();
        if ($checkAlreadyAvailable){
            return response()->json(['error' => 'Already Added'],400);
        }else{
            $cart = new Cart();
            $cart->device_id = $request->device_id;
            $cart->product_id = $request->product_id;
            $cart->name = $request->name;
            $cart->price = $request->price;
            $cart->quantity = $request->quantity;
            $cart->type = $request->type;
            $cart->size = $request->size;
            $cart->color = $request->color;
            $cart->save();
            return response()->json(['success' => 'Added'],200);
        }
    }
    public function addtocart1(Request $request){
        $checkAlreadyAvailable = Cart::where('device_id', $request->device_id)->where('product_id', $request->product_id)->first();
        if ($checkAlreadyAvailable){
            return response()->json(['error' => 'Already Added'],400);
        }else{
            $cart = new Cart();
            $cart->device_id = $request->device_id;
            $cart->product_id = $request->product_id;
            $cart->name = $request->name;
            $cart->price = $request->price;
            $cart->quantity = $request->quantity;
            $cart->type = $request->type;
            $cart->size = $request->size;
            $cart->color = $request->color;
            $cart->sendername = $request->sendername;
            $cart->senderphone = $request->senderphone;
            $cart->r_name = $request->r_name;
            $cart->r_email = $request->r_email;
            $cart->message = $request->message;
            $cart->save();
            return response()->json(['success' => 'Added'],200);
        }
    }
    public function cartitems($device_id){
        $cartitems = Cart::where('device_id', $device_id)->latest()->get();
        $total = 0;
        $ids = [];
        foreach ($cartitems as $key1 => $row) {
            $product = Size::where('product_id', '=', $row->product_id)->where('color', '=', $row->color)->first();
            $quantity = json_decode($product->quantity);
            foreach (json_decode($product->newsize, true) as $key => $value) {
                if ($value == $row->size) {
                    if ($quantity[$key] <= 0) {
                        $row->delete();
                    }else{
                        $ids[$key1] = $row->id;
                        $total = $total + ($row->quantity * $row->price);
                    }
                }
            }
        }
        $cart = Cart::whereIn('id', $ids)->get();
        $data = CartResource::collection($cart);
        return response()->json(['products' => $data, 'total' => $total]);
    }
    public function cartitems1($device_id){
        $cartitems = Cart::where('device_id', $device_id)->latest()->get();
        $total = 0;
        $ids = [];
        foreach ($cartitems as $key1 => $row) {
            if ($row->type == 1){
                $product = Size::where('product_id', '=', $row->product_id)->where('color', '=', $row->color)->first();
                $quantity = json_decode($product->quantity);
                foreach (json_decode($product->newsize, true) as $key => $value) {
                    if ($value == $row->size) {
                        if ($quantity[$key] <= 0) {
                            $row->delete();
                        }else{
                            $ids[$key1] = $row->id;
                            $total = $total + ($row->quantity * $row->price);
                        }
                    }
                }
            }else{
                $ids[$key1] = $row->id;
                $total = $total + ($row->quantity * $row->price);
            }
        }
        $cart = Cart::whereIn('id', $ids)->get();
        $data = CartResource::collection($cart);
        return response()->json(['products' => $data, 'total' => $total]);
    }
    public function removecart($id){
        $cart = Cart::find($id);
        $cart->delete();
        return response()->json(['success' => 'Deleted']);
    }
    public function addtowishlist(Request $request)
    {
        $wishlist = Wishlist::where('user_id', auth()->user()->id)->where('product_id', $request->product_id)->first();
        if (!$wishlist) {
            $wish = new Wishlist();
            $wish->user_id = Auth::user()->id;
            $wish->product_id = $request->product_id;
            $wish->save();
        }
        return response()->json(['success' => '1']);
    }
    public function wishlist(){
        $wishlist = Wishlist::where('user_id', auth()->user()->id)->pluck('product_id');
        $products = Product::whereIn('id', $wishlist)->where('draft', '=', 1)->latest()->get();
        $data = ProductCollection::collection($products);
        return response()->json($data);
    }
    public function thismonth(){
        $products = Product::where('month', 1)->where('draft', '=', 1)->where('user_id', '1')->latest()->get();
        $data = ProductCollection::collection($products);
        return response()->json($data);
    }
    public function sold(){
        $products = Product::where('sold', 1)->where('user_id', '1')->where('draft', '=', 1)->latest()->get();
        $data = ProductCollection::collection($products);
        return response()->json($data);
    }
    public function cards(){
        $cards = Cards::orderBy('id', 'DESC')->get();
        return response()->json($cards);
    }
    public function checkout(Request $request){
        $userId = auth()->user()->id;
        $cartitems = Cart::where('device_id', $request->device_id)->get();
        foreach ($cartitems as $cart){
            \Cart::add($cart->product_id, $cart->name, $cart->price, $cart->quantity, array(
                'type' => $cart->type,
                'size' => $cart->size,
                'color' => $cart->color,
                'sendername' => $cart->sendername,
                'senderphone' => $cart->senderphone,
                'r_name' => $cart->r_name,
                'r_email' => $cart->r_email,
                'message' => $cart->message,
            ));
        }
        $cartitemsProducts = \Cart::getContent();

//        $cartitems->each->delete();

        $order = new Order();
        $order->ip = request()->ip();
        $order->discount = $request->discount;
        $order->user_id = $userId;
        $order->name = $request->name;
        $order->email = $request->email;
        $order->resource = 'Application Mobile Flutter';
        $order->phone = $request->phone;
        $order->address = $request->address;
        $order->shipping = $request->shipping;
        $order->notes = $request->notes;
        $order->country = $request->country;
        $order->total = $request->finalamount;
        $order->products = $cartitemsProducts;
        $order->order_number = "ON-" . rand(100000000, 900000000);
        $order->save();

        if ($request->pay == "Payer en plusieurs fois avec ALMA") {
            $order->payment_method = 'ALMA';
            $order->save();
            $apiKey = env("ALMA_KEY", "sk_test_4GmA9z5tQOKOUiI4IEIIEO69");
            $data = [
                "payment" => [
                    "purchase_amount" => $request->finalamount * 100,
                    "return_url" => route('application.payment.success', ['id' => $order->id, 'type' => 'order', 'user_id' => $userId]),
                    "ipn_callback_url" => route('application.payment.success', ['id' => $order->id, 'type' => 'order', 'user_id' => $userId]),
                    "shipping_address" => [
                        "first_name" => $request->name,
                        "last_name" => $request->surname,
                        "line1" => $request->address,
                        "postal_code" => $request->cp,
                        "city" => $request->city,
                    ],
                    "locale" => "fr",
                ],

            ];
            $dataString = json_encode($data);

            $headers = [
                'Authorization: Alma-Auth ' . $apiKey,
                'Content-Type: application/json',
            ];

            $ch = curl_init();
            if (env("ALMA_ENV") == "sandbox") {
                curl_setopt($ch, CURLOPT_URL, 'https://api.sandbox.getalma.eu/v1/payments');
            } else {
                curl_setopt($ch, CURLOPT_URL, 'https://api.getalma.eu/v1/payments');
            }
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);

            $response1 = curl_exec($ch);
            $response = json_decode($response1);

            return response()->json(['type' => 'alma', 'url' => $response->url, 'user_id' => $userId]);

        } else if ($request->pay == "Payer en 1 seule fois par carte bancaire") {

            $order->payment_method = 'Stripe';
            $order->save();
            return response()->json(['type' => 'stripe', 'url' => route('application.stripe', ['price' => $request->finalamount, 'id' => $order->id, 'user_id' => $userId])]);

        } else if ($request->pay == "PAD( Paiement à Distance)") {
            $order->payment_method = 'pad';
            $order->save();
            return response()->json(['type' => 'pad', 'url' => route('application.payment.success', ['id' => $order->id, 'type' => 'order', 'user_id' => $userId])]);

        }else {
            dd("nothing");
        }

    }
    public function orders(){
        $user = Auth::user();
        $orders = Order::where('user_id', $user->id)->where('paymentstatus', '1')->latest()->get();
        $data = OrderResource::collection($orders);
        return response()->json($data);
    }
    public function notifications(){
        $user = auth()->user();
        $notfications = Notfication::latest()->where('r_id', '=', $user->id)->select('message', 'activity', 'status')->get();
        Notfication::latest()->where('r_id', '=', $user->id)->update(['status' => 1]);
        return response()->json($notfications);
    }
    public function profileUpdate(Request $request){
        $id = auth()->user()->id;
        $user = User::where('id', $id)->first();
        $user->name = $request->name;
        $user->surname = $request->surname;
        $user->phone = $request->phone;
        $user->address = $request->address;
        $user->size = $request->size;
        $user->country = $request->country;
        $user->shop = $request->shop;

        if ($request->hasfile('photo')) {
            $image1 = $request->file('photo');
            $name = time() . 'profile' . '.' . $image1->getClientOriginalExtension();
            $destinationPath = 'profile/';
            $image1->move($destinationPath, $name);
            $user->photo = 'profile/' . $name;
        }

        $user->update();
       return response()->json(['success' => 'successfully updated']);
    }
    public function userContactStore(Request $request){
        $contact = new Contact();
        $contact->name = $request->name;
        $contact->email = $request->email;
        $contact->subject = $request->subject;
        $contact->message = $request->message;
        $contact->save();


        return response()->json(['success' => 'successfully added']);
    }
    public function token($token){
        $user = auth()->user();
        $user->device_token = $token;
        $user->update();
        return response()->json(['success' => 'successfully updated']);
    }
    public function sizes($size){
        $color_size = Size::where('newsize', 'like', '%' . $size . '%')->pluck('product_id')->unique();
        $products = Product::whereIn('id', $color_size)->where('draft', '=', 1)->get();
        $data = ProductCollection::collection($products);
        return response()->json($data);
    }
    public function sendOtpEmail(Request $request)
    {
        $otp = rand(1000, 9999);
        $user = User::where('email', '=', $request->email)->first();
        if ($user) {
            $user->otp = $otp;
            $user->update();
            $dataa = array(
                'otp' => $otp,
            );
            Mail::to($request->email)->send(new  OtpMail($dataa));
            return response()->json(['success' => 'otp send'], 200);
        } else {
            return response()->json(['error' => 'user not exist'], 404);
        }

    }
    public function imageUpdate(Request $request){
        $id = auth()->user()->id;
        $user = User::find($id);
        $user->photo = $request->photo;
        $user->save();
        return response()->json(['success' => 'successfully updated']);
    }
    public function otpEmailVerify(Request $request)
    {
        $user = User::where('email', $request->email)->where('otp', $request->otp)->first();
        if ($user) {
            $user->otp = null;
            $user->update();
            return response()->json(['success' => 'otp correct'], 200);
        } else {
            return response()->json(['success' => 'otp incorrect'], 404);
        }

    }
    public function appPassword(Request $request)
    {
        $password = Hash::make($request->password);
        $user = User::where('email', '=', $request->email)->first();
        $user->password = $password;
        $user->update();
        auth()->login($user, true);
        $success['token'] =  $user->createToken('MyApp')-> accessToken;
        $success['user'] =  new UserResource($user);
        return response()->json(['success' => $success], 200);
    }
    public function search(Request $request)
    {
        $products = Product::where('title', 'like', '%' . $request->name . '%')->where('draft', '=', 1)->where('user_id', '1')->latest()->get();
        $data = ProductCollection::collection($products);
        return response()->json($data);
    }
    public function referral(){
        $refferal = Refferal::where('refferal_user', '=', auth()->user()->id)->get();
        return response()->json([
            'referral' => auth()->user()->refferal??"",
            'users' => ReferralResource::collection($refferal)
        ]);
    }
    public function points() {
        $points = Points::select('id', 'name', 'points')->get();
        return response()->json([
            'points' => auth()->user()->points,
            'point_details' => $points
        ]);
    }
    public function pointsHistory(){
        $points = PointsTable::where('user_id', '=', auth()->user()->id)->latest()->get();
        return response()->json(PointsHistoryResource::collection($points));
    }
    public function giftsHistory(){
        $points = Gifts::where('user_id', '=', auth()->user()->id)->latest()->get();
        return response()->json(GiftHistoryResource::collection($points));
    }
    public function getmypoints($id){
        $points = Points::find($id);
        $user = auth()->user();
        if ($points->points > $user->points){
            return response()->json(['error' => 'Vous navez pas assez de points pour profiter de ce cadeau'], 404);
        }else{
            $gift = new Gifts();
            $gift->user_id = $user->id;
            $gift->point_id = $id;
            $gift->save();
            AdminNotificationHelper::pushAdminNotification($user->id,$gift->id,'Demander un cadeau');
            PointsHelper::pushPoints($user->id, $points->points, 'Acheter un cadeau', 'Retirer');
            $user->points = $user->points - $points->points;
            if($user->update()) {
                $account_sid = 'AC96ac1afdd55310f2214c9bf9b928f326';
                $auth_token = '2776de46ab4e9752bb8d9ec9deef2074';
                $client = new Client($account_sid, $auth_token);

                $client->messages->create(
                    "+590690574700",
                    array(
                        'from' => 'Sweet Diva',
                        'body' => 'Demander des points # ' . 'POINTS'.$gift->point_id . '
Détails du client:
' . $user->name . '
' . $user->email . '
' . $user->phone . '
' . $user->address . '
' . $user->country . '
' . $points->points . '
Daté: ' . $gift->created_at . '
                ',
                    )
                );
            }

            return response()->json(['success' => 'Successfully redeme'], 200);
        }
    }
    public function changePassword(Request $request){
        $user = auth()->user();
        $oldpass = $request->oldpassword;
        $pass = $user->password;
        if (Hash::check($oldpass , $pass)) {
            $user->password = Hash::make($request->newpassword);
            $user->save();
            return response()->json(['success' => 'Le mot de passe a été changé avec succès ! Connectez-vous maintenant avec votre nouveau mot de passe'], 200);
        }else{
            return response()->json(['success' => 'Lancien mot de passe ne correspond pas!'], 404);
        }
    }
}
