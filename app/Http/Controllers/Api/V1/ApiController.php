<?php

namespace App\Http\Controllers\Api\V1;

use App\Cart;
use App\Http\Controllers\Controller;
use App\Http\Resources\NotificationResource;
use App\Http\Resources\PostResource;
use App\Http\Resources\ProductCollection;
use App\Http\Resources\V1\CartResource;
use App\Notfication;
use App\Post;
use App\Product;
use App\Size;
use App\Wishlist;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function categoriesProduct($id){
        $products = Product::where('category_id', $id)->where('draft', '=', 1)->latest()->paginate(20);
        return ProductCollection::collection($products);
    }
    public function thismonth(){
        $products = Product::where('month', 1)->where('draft', '=', 1)->where('user_id', '1')->latest()->paginate(20);
        return ProductCollection::collection($products);
    }
    public function sold(){
        $products = Product::where('sold', 1)->where('user_id', '1')->where('draft', '=', 1)->latest()->paginate(20);
        return ProductCollection::collection($products);
    }
    public function search(Request $request)
    {
        $products = Product::where('title', 'like', '%' . $request->name . '%')->where('draft', '=', 1)->where('user_id', '1')->latest()->paginate(20);
        return ProductCollection::collection($products);
    }
    public function sizes($size){
        $color_size = Size::where('newsize', 'like', '%' . $size . '%')->pluck('product_id')->unique();
        $products = Product::whereIn('id', $color_size)->where('draft', '=', 1)->paginate(10);
        return ProductCollection::collection($products);
    }
    public function wishlist(){
        $wishlist = Wishlist::where('user_id', auth()->user()->id)->pluck('product_id');
        $products = Product::whereIn('id', $wishlist)->where('draft', '=', 1)->latest()->paginate(20);
        return ProductCollection::collection($products);
    }
    public function allPosts()
    {
        $posts = Post::where('topic_id', null)->latest()->paginate(10);
        return PostResource::collection($posts);
    }
    public function topSweet()
    {
        $products = Product::where('top_sweet', 1)->where('draft', '=', 1)->where('user_id', '1')->latest()->paginate(20);
        return ProductCollection::collection($products);
    }
    public function cartitems($device_id){
        $cartitems = Cart::where('device_id', $device_id)->latest()->get();
        $total = 0;
        $ids = [];
        foreach ($cartitems as $key1 => $row) {
            if ($row->type == 1){
                $product = Size::where('product_id', '=', $row->product_id)->where('color', '=', $row->color)->first();
                $quantity = json_decode($product->quantity);
                foreach (json_decode($product->newsize, true) as $key => $value) {
                    if ($value == $row->size) {
                        if ($quantity[$key] <= 0) {
                            $row->delete();
                        }else{
                            $ids[$key1] = $row->id;
                            $total = $total + ($row->quantity * $row->price);
                        }
                    }
                }
            }else{
                $ids[$key1] = $row->id;
                $total = $total + ($row->quantity * $row->price);
            }
        }
        $cart = Cart::whereIn('id', $ids)->get();
        $data = CartResource::collection($cart);
        return response()->json(['products' => $data, 'total' => $total]);
    }
    public function notifications(){
        $user = auth()->user();
        $notfications = Notfication::latest()->where('r_id', '=', $user->id)->paginate(20);
        Notfication::latest()->where('r_id', '=', $user->id)->update(['status' => 1]);
        return NotificationResource::collection($notfications);
    }
}
