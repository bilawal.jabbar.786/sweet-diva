<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Resources\PostResource;
use App\Http\Resources\ProductCollection;
use App\Http\Resources\V1\ProductResource;
use App\Post;
use App\Product;
use App\Size;
use App\Wishlist;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function categoriesProduct($id){
        $products = Product::where('category_id', $id)->where('draft', '=', 1)->latest()->paginate(20);
        return ProductResource::collection($products);
    }
    public function thismonth(){
        $products = Product::where('month', 1)->where('draft', '=', 1)->where('user_id', '1')->latest()->paginate(20);
        return ProductResource::collection($products);
    }
    public function sold(){
        $products = Product::where('sold', 1)->where('user_id', '1')->where('draft', '=', 1)->latest()->paginate(20);
        return ProductResource::collection($products);
    }
    public function search(Request $request)
    {
        $products = Product::where('title', 'like', '%' . $request->name . '%')->where('draft', '=', 1)->where('user_id', '1')->latest()->paginate(20);
        return ProductResource::collection($products);
    }
    public function sizes($size){
        $color_size = Size::where('newsize', 'like', '%' . $size . '%')->pluck('product_id')->unique();
        $products = Product::whereIn('id', $color_size)->where('draft', '=', 1)->paginate(10);
        return ProductResource::collection($products);
    }
    public function wishlist(){
        $wishlist = Wishlist::where('user_id', auth()->user()->id)->pluck('product_id');
        $products = Product::whereIn('id', $wishlist)->where('draft', '=', 1)->latest()->paginate(20);
        return ProductResource::collection($products);
    }
    public function topSweet()
    {
        $products = Product::where('top_sweet', 1)->where('draft', '=', 1)->where('user_id', '1')->latest()->paginate(20);
        return ProductResource::collection($products);
    }
}
