<?php

namespace App\Http\Controllers\Api;

use App\CardPayments;
use App\Cards;
use App\Cart;
use App\GeneralSettings;
use App\Http\Controllers\Controller;
use App\Http\NotificationHelper;
use App\Http\PointsHelper;
use App\Jobs\SendEmailJob;
use App\Mail\BonCadeau;
use App\Mail\Facture;
use App\Order;
use App\Points;
use App\Product;
use App\PromoCode;
use App\Refferal;
use App\Size;
use App\User;
use App\VendorOrder;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Twilio\Rest\Client;
use Session;
use PDF;
class PaymantController extends Controller
{
    public function stripe($price, $id, $user_id)
    {
        return view('application.stripe', compact('price', 'id', 'user_id'));
    }
    public function stripesubmit(Request $request)
    {
        $user = User::find($request->user_id);
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET_KEY'));
        try {
            $stripe = \Stripe\Charge::create(array(
                "amount" => $request->price * 100,
                "currency" => "eur",
                "source" => $request->input('stripeToken'), // obtained with Stripe.js
                "description" => "Paiement par carte " . $user->name . " " .  $user->surname . " " . $user->email
            ));

            $order = Order::find($request->id);
            $order->transaction_id = $stripe->id;
            $order->save();

            return $this->paymentsuccess($request->id, 'order', $user->id);
        } catch (\Exception $e) {
            Session::flash('fail-message', "Erreur! Veuillez réessayer. ". $e->getMessage());
            return redirect()->back();
        }
    }
    public function paymentsuccess($id, $type, $user_id)
    {
        $order = Order::find($id);
        $order->paymentstatus = '1';
        $order->invoice = '050' . $id;
        $order->update();
        $gs = GeneralSettings::find(1);
        $user = User::where('id', $user_id)->first();

        $check_is_refferal = Refferal::where('user_id', $user->id)->first();
        if ($check_is_refferal) {
            $ref_user = User::where('id', $check_is_refferal->refferal_user)->first();
            $check_order = Order::where('user_id', $user->id)->get();
            if ($check_order->count() == 1) {
                PointsHelper::pushPoints($ref_user->id,'50','Se référer à','Ajouter');
                $ref_user->points = $ref_user->points + 50;
                $ref_user->update();
            }
        }
        foreach (json_decode($order->products) as $item) {
            if ($item->attributes->type == 0) {
                $new = new CardPayments();
                $new->card_id = $item->id;
                $new->sendername = $item->attributes->sendername;
                $new->senderphone = $item->attributes->senderphone;
                $new->r_name = $item->attributes->r_name;
                $new->r_email = $item->attributes->r_email;
                $new->message = $item->attributes->message;
                $new->price = $item->price;
                $new->paymentstatus = '1';
                $new->card_number = rand(100000000, 900000000);
                $new->save();
                $cards = Cards::where('sku', $item->id)->first();
                Mail::to($new->r_email)->send(new BonCadeau($new, $cards));
            } else {
                $product = Product::find($item->id);
                if ($product) {
                    $checkorder = VendorOrder::where('vendor_id', $product->user_id)->where('order_id', $order->id)->first();
                    if (!$checkorder) {
                        if ($product->user_id == '1') {
                            $sizeproduct = Size::where('product_id', $item->id)->where('color', $item->attributes->color)->first();
                            $quantity = json_decode($sizeproduct->quantity);
                            $size = json_decode($sizeproduct->newsize);
                            foreach ($size as $key => $value) {
                                if ($value == $item->attributes->size) {
                                    $stock = $quantity[$key] - $item->quantity;
                                    $quantity[$key] = (string)$stock;
                                    $sizeproduct->quantity = $quantity;
                                    $sizeproduct->update();
                                }
                            }
                        }
                        $product->status = '1';
                        $product->update();
                    }
                }
            }
        }

        $check = VendorOrder::where('vendor_id', '1')->where('order_id', $order->id)->first();
        if (!$check){
            $vendororder = new VendorOrder();
            $vendororder->vendor_id = 1;
            $vendororder->order_id = $order->id;
            if($order->payment_method=='pad'){
                $vendororder->status = 2;
            }
            $vendororder->save();
            $user->points = $user->points + $order->total;
            $user->update();
            PointsHelper::pushPoints($user->id,$order->total,'Ordre','Ajouter');
            if (env('APP_ENV') == 'production') {
                $data1 = [
                    'invoice' => $order,
                    'gs' => $gs
                ];
                $destinationPath = 'records';
                $taxform_name = 'facture-' . $order->order_number . '.pdf';
                $filepath = $destinationPath . '/' . $taxform_name;
                $pdf = PDF::loadView('myPDF', $data1);
                $pdf->stream();
                file_put_contents($filepath, $pdf->output());
                dispatch(new SendEmailJob($order, $gs, $filepath))->delay(\Carbon\Carbon::now()->addSeconds(5));
                $account_sid = 'AC96ac1afdd55310f2214c9bf9b928f326';
                $auth_token = '2776de46ab4e9752bb8d9ec9deef2074';
                $client = new Client($account_sid, $auth_token);
                $client->messages->create(
                    "+590690574700",
                    array(
                        'from' => 'Sweet Diva',
                        'body' => 'Nouvelle commande # ' . $order->order_number . '
Détails du client:
' . $order->name . '
' . $order->email . '
' . $order->phone . '
' . $order->address . '
' . $order->country . '
' . $order->payment_method . '
' . $order->total . '
Voir la commande
' . route('order.view', ['id' => $order->id]) . '
Daté: ' . $order->created_at . '
                ',
                    )
                );
                $client->messages->create(
                    "+590690399802",
                    array(
                        'from' => 'Sweet Diva',
                        'body' => 'Nouvelle commande # ' . $order->order_number . '
Détails du client:
' . $order->name . '
' . $order->email . '
' . $order->phone . '
' . $order->address . '
' . $order->country . '
' . $order->payment_method . '
' . $order->total . '
Voir la commande
' . route('order.view', ['id' => $order->id]) . '
Daté: ' . $order->created_at . '
                ',
                    )
                );
                \Cart::clear();
            }
            $activity = "Confirmation de commande";

            $msg = "Toutes nos félicitations " . $user->name . ' ' . $user->surname . " votre commande a bien été prise en compte votre numéro de suivi vous sera communiqué une fois votre colis pris en charge par les services postaux";

            NotificationHelper::pushNotification($msg, [$user->device_token], $activity);
            NotificationHelper::addtoNitification(1, $user->id, $msg, $order->id, $activity, 1);

        }
        return view('application.paymantsuccess', compact('order'));
    }
    public function promoCode(Request $request){
        $cartitems = Cart::where('device_id', $request->device_id)->pluck('product_id');
        $promo =  PromoCode::where('code','=',$request->code)->where('status','=',1)->first();
        if ($promo){
            $excluded_products = json_decode($promo->excluded_products);
            $result = array_intersect($cartitems->toArray(), $excluded_products);
            return response()->json(["dicount" => $promo->off, 'ids' => array_values($result)]);
        }else{
            return response()->json(["error" => "Code Not Found"], 400);
        }
    }
}
