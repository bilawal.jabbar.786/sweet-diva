<?php

namespace App\Http\Livewire;

use App\Category;
use http\Env\Request;
use Livewire\Component;
use Livewire\WithFileUploads;


class MainCategory extends Component
{ use WithFileUploads;

    public $maincategory,$category;
 public $photo,$photo2,$photo3,$banner,$name,$category_id,$maincategory_id,$no;
    public $oldphoto,$oldphoto2,$oldphoto3,$oldbanner;
 public $updateCheck = false;
    public function render()
    {$this->maincategory = \App\MainCategory::orderBy('no','ASC')->get();
        $this->category = Category::all();
        return view('livewire.main-category');
    }
    public function resetField()
    {
        $this->name='';
        $this->category_id='';
        $this->photo='';
        $this->photo2='';
        $this->photo3='';
        $this->banner='';
    }
    public function storeCategory()
    {
        $validateData = $this->validate([
            'photo' => 'required|image|mimes:jpeg,png,jpg',
            'photo2' => 'required|image|mimes:jpeg,png,jpg',
            'photo3' => 'required|image|mimes:jpeg,png,jpg',
            'banner' => 'required|image|mimes:jpeg,png,jpg',
        ]);

        $maincategory = new \App\MainCategory();
        $maincategory->name = $this->name;
        $maincategory->category_id = $this->category_id;
        $this->photo = $this->photo->store('category','public');
        $this->photo2 = $this->photo2->store('category','public');
        $this->photo3 = $this->photo3->store('category','public');
        $this->banner = $this->banner->store('category','public');

        $maincategory->photo =  'storage/'.$this->photo;
        $maincategory->photo2 =  'storage/'.$this->photo2;
        $maincategory->photo3 =  'storage/'.$this->photo3;
        $maincategory->banner =  'storage/'.$this->banner;
        $maincategory->save();
        session()->flash('message','Sauvegarde réussie!');
        $this->resetField();
        $this->emit('userStore');

    }
    public function edit($id)
    {
        $maincategory =  \App\MainCategory::find($id);
        $this->name=$maincategory->name;
        $this->category_id=$maincategory->category_id;
        $this->oldphoto=$maincategory->photo;
        $this->oldphoto2=$maincategory->photo2;
        $this->oldphoto3=$maincategory->photo3;
        $this->oldbanner= $maincategory->banner;
        $this->no = $maincategory->no;
        $this->maincategory_id = $maincategory->id;
        $this->updateCheck=true;
    }
    public function cancel()
    {
        $this->resetField();
        $this->updateCheck=false;

    }
    public function updateCategory()
    {


        $maincategory =  \App\MainCategory::find($this->maincategory_id);

        $maincategory->name = $this->name;
        $maincategory->category_id = $this->category_id;
        $maincategory->no = $this->no;

   if($this->photo)
   { $this->photo = $this->photo->store('category', 'public');
       $maincategory->photo =  'storage/'.$this->photo;
   }
   else{ $this->photo = $this->oldphoto; }

        if($this->photo2){
            $this->photo2 = $this->photo2->store('category','public');
            $maincategory->photo2 =  'storage/'.$this->photo2;
        }else{
            $this->photo2 = $this->oldphoto2;
        }
        if($this->photo3){
            $this->photo3 = $this->photo3->store('category','public');
            $maincategory->photo3 =  'storage/'.$this->photo3;
        }else{
            $this->photo3 = $this->oldphoto3;
        }
        if($this->banner){
            $this->banner = $this->banner->store('category','public');
            $maincategory->banner =  'storage/'.$this->banner;
        }else{
            $this->banner = $this->oldbanner;
        }
        $maincategory->update();
        $this->resetField();
        $this->updateCheck=false;
        session()->flash('message','Category Updated SuccesFully');
        $this->emit('userStore');


    }
}
