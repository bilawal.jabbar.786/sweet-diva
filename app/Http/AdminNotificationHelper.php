<?php

namespace App\Http;


use App\AdminNotfication;

class AdminNotificationHelper
{
    public static function pushAdminNotification($user_id, $generate_id, $activity)
    {
        $notification = new AdminNotfication();
        $notification->s_id = $user_id;
        $notification->generate_id = $generate_id;
        $notification->activity = $activity;
        $notification->save();
    }
}


