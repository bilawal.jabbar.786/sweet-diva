<?php

namespace App\Http;


use App\PointsTable;

class PointsHelper{
    public static function pushPoints($user_id, $points,$reason,$transaction){
        $pointsTable = new PointsTable();
        $pointsTable->user_id = $user_id;
        $pointsTable->points = $points;
        $pointsTable->reason = $reason;
        $pointsTable->transaction = $transaction;
        $pointsTable->order_id = 0;
        $pointsTable->save();
    }
}


