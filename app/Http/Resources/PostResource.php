<?php

namespace App\Http\Resources;

use App\Like;
use Illuminate\Http\Resources\Json\JsonResource;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $like = Like::where('post_id', $this->id)->where('user_id', auth()->user()->id)->first();
        \Carbon\Carbon::setLocale('fr');
        $date = \Carbon\Carbon::parse($this->created_at);
        return [
            'id' => $this->id,
            'description' => $this->description ?? "",
            'image' => $this->img ?? "",
            'total_likes' => $this->likes->count() ?? 0,
            'total_comments' => $this->comments->count() ?? 0,
            'post_create' => $date->diffForHumans(),
            'is_like' => $like ? true : false,
            'user' => [
                'user_id' => $this->user->id,
                'user_image' => $this->user->photo ?? "avatar.png",
                'user_name' => $this->user->name . ' ' . $this->user->surname,
            ],
        ];
    }
}
