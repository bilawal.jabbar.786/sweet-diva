<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PointsHistoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'reason' => $this->reason??"",
            'points' => $this->points??"",
            'transaction_type' => $this->transaction??"",
            'date' => $this->created_at->format('d-m-Y')??"",
        ];
    }
}
