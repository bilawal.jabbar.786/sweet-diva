<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $pro = [];
        foreach (json_decode($this->products) as $key => $item){
            $data = [
                'name' => (string)$item->name,
                'quantity' => (string)$item->quantity,
                'price' => (int)$item->price,
                'size' => (string)$item->attributes->size,
                'color' => (string)$item->attributes->color,
            ];
            $pro[$key] =  $data;
        }
        return [
            'id' => $this->id,
            'user_id' => (int)$this->user_id,
            'order_number' => $this->order_number,
            'name' => $this->name??"",
            'email' => $this->email??"",
            'phone' => $this->phone??"",
            'address' => $this->address??"",
            'notes' => $this->notes??"",
            'shipping' => $this->shipping??0,
            'total' => $this->total??"",
            'discount' => $this->discount??"",
            'country' => $this->country??"",
            'payment_method' => $this->payment_method??"",
            'created_at' => $this->created_at->format('d-m-Y'),
            'products' => $pro,
        ];
    }
}
