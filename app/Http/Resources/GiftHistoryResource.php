<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class GiftHistoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'gift_name' => $this->points->name??"",
            'gift_points' => $this->points->points??"",
            'redeem_date' => $this->created_at->format('d-m-Y'),
        ];
    }
}
