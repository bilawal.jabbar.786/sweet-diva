<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CommentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'comment' => $this->comment,
            'comment_create' => $this->created_at->format('d-m-Y'),
            'user' => [
                'user_id' => $this->user->id,
                'user_image' => $this->user->photo??"avatar.png",
                'user_name' => $this->user->name . ' ' . $this->user->surname,
            ],
        ];
    }
}
