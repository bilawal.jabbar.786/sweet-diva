<?php

namespace App\Http\Resources\V1;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title??"",
            'category' => $this->maincategory->name??"",
            'sub_category' => $this->subcategory->name??"",
            'photo1' => $this->photo1??"",
            'material' => $this->material??"",
            'price' => (int)$this->price??0,
            'oldprice' => (int)$this->oldprice??"",
            'description' => $this->description??"",
            'sku' => $this->sku??"",
            'label_name' => $this->label_name??"",
            'background_color' => $this->background_color??"",
            'text_color' => $this->text_color??"",
            'color' => json_decode($this->color),
            'colorimage' => json_decode($this->colorimage),
            'gallery' => json_decode($this->gallery)??[],
        ];
    }
}
