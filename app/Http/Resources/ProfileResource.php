<?php

namespace App\Http\Resources;

use App\Follow;
use App\Post;
use Illuminate\Http\Resources\Json\JsonResource;

class ProfileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $follow = Follow::where('f_id', $this->id)->where('user_id', auth()->user()->id)->first();
        return [
            'user' => new UserResource($this),
            'posts' => PostResource::collection($this->posts),
            'post_count' => $this->posts->count(),
            'followers' => $this->followers->count(),
            'followings' => $this->followings->count(),
            'is_follow' => $follow ? true : false,
        ];
    }
}
