<?php

namespace App\Http\Resources;

use App\Product;
use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $products = Product::where('category_id' , $this->id)->latest()->take(3)->get();
        return [
            'id' => $this->id,
            'category_id' => $this->category_id,
            'name' => $this->name??"",
            'photo' => $products[0]->photo1??$this->photo??"",
            'banner' => $this->banner??"",
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'photo2' => $products[1]->photo1??$this->photo2??"",
            'photo3' => $products[2]->photo1??$this->photo3??"",
            'no' => $this->no??"",
        ];
    }
}
