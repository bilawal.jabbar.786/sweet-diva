<?php

namespace App\Http\Resources;

use App\Wishlist;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'photo1' => $this->photo1,
            'material' => $this->material,
            'price' => (int)$this->price,
            'oldprice' => (int)$this->oldprice,
            'description' => $this->description,
            'sku' => $this->sku,
            'color' => json_decode($this->color),
            'colorimage' => json_decode($this->colorimage),
        ];
    }
}
