<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class NotificationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        Carbon::setLocale('fr');
        $date = Carbon::parse($this->created_at);
        return [
            'activity'=> $this->activity,
            'message'=> $this->message,
            'created_at' => $date->diffForHumans(),
            'status' => (string)$this->status,
            'reference' => (string)$this->generate_id??"#",
        ];
    }
}
