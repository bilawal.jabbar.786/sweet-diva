<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MainCategory extends Model
{
    public function subcategories()
    {
        return $this->hasMany(SubCategory::class, 'category_id');
    }
    public function categorytype()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }
}
