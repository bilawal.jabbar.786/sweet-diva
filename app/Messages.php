<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Messages extends Model
{
    public function receiver()
    {
        return $this->belongsTo(User::class, 's_id');
    }
}
