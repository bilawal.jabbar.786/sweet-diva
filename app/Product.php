<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function size()
    {
      return $this->hasMany(Size::class);
    }
    public function categorytype()
    {
        return $this->belongsTo(Category::class, 'category');
    }
    public function maincategory()
    {
        return $this->belongsTo(MainCategory::class, 'category_id');
    }
    public function subcategory()
    {
        return $this->belongsTo(SubCategory::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
