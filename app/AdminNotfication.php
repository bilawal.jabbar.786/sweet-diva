<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminNotfication extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class, 's_id');
    }
}
