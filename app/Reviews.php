<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reviews extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class, 'sender_id');
    }
}
